import subprocess
import os
import argparse
import json
import re
import sys
import hashlib
import tokenize
import ast



project_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, project_path[:-8])

def run(*popenargs, input=None, check=False, **kwargs):
    """
    Add a run function to account for Python 3.4 (subprocess.run only in 3.5
    and later).
    """
    if input is not None:
        if 'stdin' in kwargs:
            raise ValueError('stdin and input arguments may not both be used.')
        kwargs['stdin'] = subprocess.PIPE

    process = subprocess.Popen(*popenargs, **kwargs)
    try:
        stdout, stderr = process.communicate(input)
    except:
        process.kill()
        process.wait()
        raise
    retcode = process.poll()
    if check and retcode:
        raise subprocess.CalledProcessError(
            retcode, process.args, output=stdout)
    return retcode, stdout, stderr


class Leaderboard:

    def __init__(self, iters, botDir, botTestDir, botOwnerConfig):
        self.iters = iters
        self.botDir = botDir
        self.botTestDir = botTestDir
        self.botOwnerConfig = botOwnerConfig
        self.bots = []
        self.config = None

    def configureLeaderboard(self):
        self.getBots()
        self.loadConfig()

    def getBots(self):
        """
        Get a list of bots for battling
        args - arguments passed in from the command line
        """
        try:
            botDict = readBotConfig(self.botOwnerConfig)
            self.bots = [Bot(botName, self.botDir, self.botTestDir) for botName in set(botDict.values())]
        except Exception as e:
            print("Trouble decoding bot owner config file.")
            print(e)
            sys.exit(1)

    def loadConfig(self, configFile='./config/leaderboard_config.cfg'):
        with open(configFile, 'r') as f:
            self.config = json.load(f)
        
        testBot = Bot("randomBot", self.botDir, self.botTestDir)
        
        # check whether pep8 is installed for scoring coding style
        if self.config['pep8']:
            try:
                testBot.getPep8Violations()
            except FileNotFoundError:
                self.config['pep8'] = False
                print("Install pep8 to get pep8 violation scores")

        if self.config['pylint']:
            try:
                testBot.getPylintScore()
            except FileNotFoundError:
                self.config['pylint'] = False
                print("Install pylint to get pylint scores")

        if self.config['test_coverage']:
            try:
                testBot.getTestCoverageScore()
            except FileNotFoundError:
                self.config['test_coverage'] = False
                print("Install pytest module to get test coverage scores.")

    def createLeaderboard(self):
        """
        Run the steps for scoring the bots.
        1. Score the code quality.
        2. Check the signatures for challenges against the calculated ones.
        3. Score the bot names
        4. Battle the bots.
        """
        self.scoreCodeQuality()
        
        if len(self.config['signatures']) > 0:
            self.checkSignatures()
            
        if self.config['names']:
            self.scoreNames()
        
        if self.config['fights']:
            self.runFights()

    def scoreCodeQuality(self):
        """
        Run some heuristics to highlight quality issues with code.
        """
        for bot in self.bots:
            if self.config['pep8']:
                bot.getPep8Violations()
            
            if self.config['documentation']:
                bot.scoreDocumentation()
                
            if self.config['function_length']:
                bot.checkMaxFunctionLength()
                
            if self.config['pylint']:
                bot.getPylintScore()
            
            if self.config['test_coverage']:
                bot.getTestCoverageScore()

    def scoreNames(self):
        """
        Check who gets a bonus point for a good bot name.
        """
        with open('./config/good_bot_names.cfg', 'r') as f:
            goodNames = [x.strip() for x in f.readlines()]
        
        for bot in self.bots:
            if bot.name in goodNames:
                print("%s gets a point for being a good name." % bot.name)
                bot.nameScore = 1

    def checkSignatures(self):
        """
        Check to see which bots are returning a valid signature for
        challenges.
        debug - ex 4.3
        database - ex 4.5
        """
        for bot in self.bots:
            for challenge in self.config['signatures']:
                bot.checkSignature(challenge)

    def runFights(self):
        """
        Get scores and win counts for our set of bots by
        fighting them pairwise 'iter' times.
        """
        for bot1 in self.bots:
            print("{} FIGHT!".format(bot1))
            for bot2 in self.bots:
                if bot1 != bot2:
                    for i in range(self.iters):
                        matchScores = bot1.fight(bot2)
                        bot1.updateScores(matchScores[0], matchScores[1])
                        bot2.updateScores(matchScores[1], matchScores[0])
        
            for challengeBotName in self.config['challenge_bots']:
                bot1.challengeFight(challengeBotName)

    def printLeaderboard(self):
        """
        Print the leaderboard nicely.
        Scores are: number of wins divided by (number of code quality issues + 1)
        """            
        self.calculateOverallScores()
        
        titleString = self.getTitleString()
        print(titleString)
        dividerString = '-' * len(titleString)
        print(dividerString)

        for bot in sorted(self.bots, key=lambda x: x.name):
            print(self.getResultLine(bot))

    def calculateOverallScores(self):
        """
        Calculate the overall scores for each bot, using all the 
        metrics that are currently switched on.
        """
        self.overallScores = {bot: bot.overallScore for bot in self.bots}
        allBotScores = dict()
        
        for bot in self.bots:
            if self.config['fights']:
                allBotScores[bot] = bot.wins
                bot.wins = 0
        
        if self.config['fights']:
            scorePairs = [x for x in allBotScores.items()]
            sortedPairs = sorted(scorePairs, key=lambda x: x[1], reverse=True)
            top5 = [bot[0] for bot in sortedPairs[:5]]
            for i, bot in enumerate(top5):
                bot.wins = 5*(5 - i)
                self.overallScores[bot] += bot.wins

        for bot in self.bots:
            if self.config['fights']:
                self.overallScores[bot] += bot.challengeWins * 5

            if self.config['names']:
                self.overallScores[bot] += bot.nameScore
        
            if len(self.config['signatures']) > 0:
                self.overallScores[bot] += bot.signaturePoints
                
            self.overallScores[bot] /= bot.codeQualityProblemCounts + 1
            
            if self.config['pylint']:
                self.overallScores[bot] *= float(bot.pylintScore) 
        
            if self.config['test_coverage']:
                self.overallScores[bot] *= float(bot.testCoverageScore)

    def getTitleString(self):
        """
        Get the title string for the leaderboard.
        """
        maxNameLen = max([len(bot.name) for bot in self.bots])
        
        columns = [['Bot', maxNameLen + 2]]
        
        if self.config['fights']:
            columns.append(['Battle Points', 13])
            columns.append(['House Bot Wins', 14])
        
        columns.append(['Code Qual. Probs.', 17])
        
        if self.config['names']:
            columns.append(['Name Bonus', 10])
        
        if self.config['pylint']:
            columns.append(['Pylint Score', 12])
            
        if self.config['test_coverage']:
            columns.append(['Test Coverage', 13])
            
        if len(self.config['signatures']) > 0:
            columns.append(['Challenge Points', 16])
            
        columns.append(['Overall', 9])
        
        return '|'.join([col[0].center(col[1]) for col in columns])

    def getResultLine(self, bot):
        """
        Get a single line of the output table for one bot.
        """
        maxNameLen = max([len(bot.name) for bot in self.bots])
        
        resultCols = [[bot.name, maxNameLen + 2]]
        
        if self.config['fights']:
            resultCols.append([bot.wins, 13])
            resultCols.append([bot.challengeWins, 14])
        
        resultCols.append([bot.codeQualityProblemCounts, 17])
        
        if self.config['names']:
            resultCols.append(["{0:1d}".format(bot.nameScore), 10])
        
        if self.config['pylint']:
            resultCols.append(["{0:.2f}".format(bot.pylintScore), 12])
            
        if self.config['test_coverage']:
            resultCols.append([bot.testCoverageScore, 13])
        
        if len(self.config['signatures']) > 0:
            resultCols.append([bot.signaturePoints, 16])
            
        resultCols.append(["{0:.2f}".format(self.overallScores[bot]), 9])
        
        return "|".join([str(col[0]).center(col[1]) for col in resultCols])


class Bot:
    """
    A class for storing details about a bot as we run it through the 
    scoring steps. 
    """
    
    def __init__(self, name, botDir, botTestDir):
        name = Bot.checkBotValidity(name, botDir)
        self.name = name
        self.botDir = botDir
        self.botTestDir = botTestDir
        self.module = __import__(name)
        self.instance = self.module.getBot()
        self.filename = os.path.join(botDir, "{}.py".format(name))
        self.botContents = open(self.filename, 'r').read()
                
        self.codeQualityProblemCounts = 0
        self.signaturePoints = 0
        self.pylintScore = 0
        self.testCoverageScore = 0
        self.nameScore = 0
        
        self.wins = 0
        self.challengeWins = 0
        self.totalMatchScore = 0
        
        self.overallScore = 100
        
    def __str__(self):
        return self.name
    
    @staticmethod
    def checkBotValidity(botName, botDir):
        """
        Check that we can find the bot and also that it is not using ctypes.
        """
        if not os.path.isfile(os.path.join(botDir, botName+".py")):
            print("Unable to find bot: {}".format(botName))
            return "randomBot"
        else:
            if Bot.checkForCtypes(botName):
                print("*" * 40)
                print("ALERT! POSSIBLE SNEAKY BEHAVIOUR: {} is using ctypes".format(botName))
                print("*" * 40)
            return botName
    
    @staticmethod
    def checkForCtypes(botName):
        """
        Check if a bot is being sneaky and importing ctypes.
        """
        botModule = __import__(botName)
        botInstance = botModule.getBot()
    
        try:
            botInstance.generateMoves(0, 0)
        except Exception:
            pass
        
        if 'ctypes' in sys.modules:
            del(sys.modules['ctypes'])
            return True
    
        return False

    def getPep8Violations(self):
        """
        Scores the number of pep8 violatons in this bot's file.
        """
        args = [sys.executable, "-m", "pep8", "--max-line-length=81", self.filename]
        # TODO: args = [sys.executable, "-m", "pycodestyle", "--max-line-length=81", self.filename]
        # pep8 renamed 2017-10-22
        code, stdout, stderr = run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.pep8Violations = len(stdout.decode().split("\n"))-1
        self.codeQualityProblemCounts += self.pep8Violations
        return self.pep8Violations
    
    def scoreDocumentation(self):
        """
        Scores the number of updated comments/docstrings in this bot's file
        """
    
        defaultCommentCount = self.countDefaultComments()
        self.codeQualityProblemCounts += defaultCommentCount
        
        commentLineCount = self.countComments()    
        docStringCounts, missingDocStringCounts = self.countDocStrings()
        commentLineCount += docStringCounts
        self.codeQualityProblemCounts += missingDocStringCounts
            
        if commentLineCount - defaultCommentCount < 5:
            print("{} has only {} (new) lines of comment.".format(self, commentLineCount - defaultCommentCount))
            self.codeQualityProblemCounts += 5 - (commentLineCount - defaultCommentCount)

    def checkSignature(self, challenge='debug'):
        """
        Check if the bot's signature has the right answer for a given challenge.
        """
        sig = self.instance.getSignature(challenge) 
        if sig == "Default BotBase sig":
            print("{} has not implemented getSignature({}).".format(self, challenge))
            return 
        
        try:
            answer = json.load(open('answers.txt'))[challenge]
        except (FileNotFoundError, KeyError):
            print("No solution for challenge {} available.".format(challenge))
            return
        raw =  "{}:{}".format(self, answer)
        curr = raw.encode("utf-8")
        for i in range(10000):
            curr = hashlib.sha256(curr).hexdigest().encode("utf-8")
    
        if curr.decode("utf-8") == sig:
            print("{} passed the signature check for {} challenge.".format(self, challenge))
            self.signaturePoints += 5
        else:
            print("{} did not pass the signature check for {} challenge.".format(self, challenge))

    def checkMaxFunctionLength(self):
        """
        Finds the maximum length of a function in this bot's file.
        If it's longer than 50 lines, we get cross.
        """
        class_idx = self.botContents.index('class')
        insideBotClassContents = self.botContents[class_idx:]

        funcs = insideBotClassContents.split('def')
        score = 0
        for func in funcs[1:]:
            lineCount = 0
            inMultiLineComment = False
            for line in func.split('\n'):
                if not (re.match('\W*$', line)
                        or re.match('\W*#', line)
                        or inMultiLineComment
                        or re.match('\W*""".*"""', line)):
                    lineCount += 1
                if '"""' in line and not re.match('\W*""".*"""', line):
                    inMultiLineComment = not inMultiLineComment
            if lineCount > 50:
                print("%s has a function of length %d" % (self, lineCount))
                score += lineCount//50
        self.codeQualityProblemCounts += score

    def countDefaultComments(self):
        """
        Check if the user has updated the comments from the randomBot file
        """
        if self.name == "randomBot":
            return 0
        else:
            score = 0
            for comment in open(os.path.join(self.botDir, "defaultComments.txt"), 'r').readlines():
                for line in self.botContents.split("\n"):
                    if comment.strip() in line:
                        print("{} still contains a comment from randomBot.py" .format(self))
                        score += 1
        return score

    def getPylintScore(self):
        """
        Returns pylint score of a file
        bot - name of bot to test
        botDir - directory containing bot code
        """
        args = [sys.executable, "-m", "pylint", "--rcfile", "pylintrc", self.filename]
        retcode, stdout, stderr = run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        for line in stdout.decode().split("\n"):
            if "Your code has been rated at" in line:
                print("{} : {}".format(self, line))
                score = float(line.split()[6].split("/")[0])
                break
        else:
            print("Pylint failed on bot: {}".format(self))
            score = 0
        self.pylintScore = score/10
        return score

    def countDocStrings(self):
        """
        Count the number of doc strings and also the number of missing
        doc strings in this bot's file.
        """
        a = ast.parse(self.botContents, self.filename)
        num_docstrings = 0
        num_missing_docstrings = 0
        for node in ast.walk(a):
            if isinstance(node, (ast.FunctionDef, ast.ClassDef, ast.Module)):
                ds = ast.get_docstring(node)
                if ds is not None:
                    num_docstrings += 1
                else:
                    num_missing_docstrings += 1
        return num_docstrings, num_missing_docstrings   
        
    def countComments(self):
        """
        Count the number of comments in this bot's file.
        """
        num_comments = 0
        t = tokenize.tokenize(open(self.filename, 'rb').readline)
    
        for token in t:
            if token.type == tokenize.COMMENT:
                num_comments += 1
        return num_comments

    def getTestCoverageScore(self):
        """
        Run pytest coverage to get a score of how much of the bot class
        is covered by tests.
        """
        
        args = ["python3", "-m", "pytest", "--cov=project.bots.{}".format(self),
                os.path.join(self.botTestDir, "{}Test.py".format(self))]
        retcode, stdout, stderr = run(args, stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)
        if "No module named pytest" in stderr.decode():
            raise FileNotFoundError    
        
        coverage = 0
        allTestsPass = 1  # Will be set to 0 if we have a failure
        hasTests = 0  # Will be set to 1 if tests exist
        for line in stdout.decode().split("\n"):
            if "no tests ran in" in line:
                print("No tests ran for {}".format(self))
                hasTests = 0
                break
            elif "FAILURE" in line:
                print("{} has a failing test.".format(self))
                hasTests = 1
                allTestsPass = 0
            elif ("{}.py".format(self) in line) and ("{}.py".format(self) in line.split()[0]):
                hasTests = 1
                coverage = float(line.split()[-1].strip("%"))/100
                
        self.testCoverageScore = allTestsPass*hasTests*coverage

    def fight(self, otherBot):
        """
        Fight another bot passes the result to the update function
        otherBot - the other bot object
        """
        args = ["./BvBServer.py", "-c", "config/gameConfig.cfg", "-R", self.name,
                "-B", otherBot.name]
        retcode, stdout, stderr = run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        matchScores = []
        
        for line in stdout.decode().split("\n"):
            if "ELIMINATED" in line:
                matchScores.append(0)
            elif "Total score" in line:
                matchScores.append(int(line.split(':')[1]))

        return matchScores
        
    def challengeFight(self, challengeBotName):
        challengeBot = ChallengeBot(challengeBotName)
        matchScores = self.fight(challengeBot)
        if matchScores[0] > matchScores[1]:
            self.challengeWins += 1

        return matchScores

    def updateScores(self, myScore, otherBotScore):
        """
        Takes a match result and updates our win and score total.
        """        
        self.totalMatchScore += myScore
        
        if myScore > otherBotScore:
            self.wins += 1
        elif myScore == otherBotScore:
            self.wins += 0.5


class ChallengeBot(Bot):
    def __init__(self, name):
        self.name = ".".join(['challenge_bots', name])
        

def readBotConfig(botConfigFile):
    """
    Read in the list of submitted bots from the config file.
    """
    with open(botConfigFile) as fp:
        botOwnerDict = json.load(fp)
    return botOwnerDict


def getBotDirectory():
    """
    Load the bot directory using the location of this file and
    defaulting to the bots folder in this location
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    botDir = dir_path + os.sep + "bots"
    return botDir


def getBotTestDirectory():
    """
    Load the bot directory using the location of this file and
    defaulting to the bots folder in this location
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    botTestDir = os.path.join(dir_path, "tests", "bots")
    return botTestDir

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Score bots')
    parser.add_argument('--botDir', action='store',
                        help='directory where bots are found',
                        default=getBotDirectory())
    parser.add_argument('--botTestDir', action='store',
                        help='directory where bot tests are found',
                        default=getBotTestDirectory())
    parser.add_argument('--iters', dest='iters', action='store',
                        default=1, type=int,
                        help='number of times to fight '
                             'each pair in each order')
    parser.add_argument('--botOwnerConfig', action='store', 
                        help='Config file linking bots and owners',
                        default='config/bot_owner_config.json')
    
    args = parser.parse_args()

    sys.path.append(args.botDir)
    board = Leaderboard(args.iters, args.botDir, args.botTestDir, args.botOwnerConfig)
    board.configureLeaderboard()
    board.createLeaderboard()
    board.printLeaderboard()
