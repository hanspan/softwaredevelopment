#!/usr/bin/env python3

import argparse
import os
import os.path
import sys
import importlib  # For loading the contestants
import json
import copy

import board
import BitmapSupport

project_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, project_path[:-8])

from project.bots import *

from random import random

class RNG:
    """
    Game generation RNG - allows game config to be randomly generated with
    a given seed.
    """
    def __init__(self, seed):
        self.rand_gen = random
        if seed:
            self.rand_gen.seed(seed)

    def get_rand(self):
        return round(self.rand_gen() * (2**32))


def main():
    # Read and validate the arguments
    args = read_arguments()

    # Display the bot directory
    if args.botDir is None:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        botsDir = os.path.join(dir_path, "bots")
    else:
        botsDir = args.botDir
    if not os.path.isdir(botsDir):
        raise Exception("Invalid bots directory.")
    print("Using Bot Directory: {}".format(botsDir))
    # We need to add the botDir to the path for loading the modules to work
    sys.path.insert(0, botsDir)
    sys.path.insert(0, "..")

    # Load the config
    # TODO: Should we have a dedicated GameConfig class?
    board_config = read_config_file(args.config)

    # Build the board
    game_board = board.Board.from_config(board_config)

    # Setup the players
    red_bot = importlib.import_module(".bots."+args.red, package="project").getBot()
    blue_bot = importlib.import_module(".bots."+args.blue, package="project").getBot()

    # Run the game
    # TODO: Fix max turns to make it configurable
    rng = RNG(args.seed)
    run_game(game_board, red_bot, blue_bot, rng)
    return


def run_game(game_board, red, blue, rng):
    """
    Run the game with the provided options.
    TODO: Should we have a game object?
    """

    current_turn = 0
    max_turns = game_board.maxTurns

    bmp = output_bitmap_header_info(game_board,
                                    blue.__class__.__name__,
                                    red.__class__.__name__)
    while (current_turn < max_turns and not either_player_eliminated(game_board)):
        current_turn += 1

        game_board.currTurn = current_turn
        game_board.random = rng.get_rand()

        # Board states are provided as copies to prevent illegal modification
        # of the state.

        red_board = copy.deepcopy(game_board)
        red_board.player = board.ControlColour.RED
        red_moves = red.makeMoves(red_board)

        blue_board = copy.deepcopy(game_board)
        blue_board.player = board.ControlColour.BLUE
        blue_moves = blue.makeMoves(blue_board)

        game_board.processMoves(red_moves, blue_moves)

        output_bitmap_turn_info(bmp, game_board)

    declare_winner(game_board)

def output_bitmap_header_info(game_board, blue_bot_name, red_bot_name):
    bmp = BitmapSupport.BitMapper(
            game_board.getBoardSize(),
            game_board.maxTurns,
            blue_bot_name,
            red_bot_name
        )

    return bmp

def output_bitmap_turn_info(bmp, game_board):
        # Update the bitmap output, but only have one bot do it.

    bmp.writeState(game_board.getTileControl(),
                   game_board.getTileStrength(),
                   game_board.currTurn,
                    {"Blue": game_board.calculateScore(board.ControlColour.BLUE),
                     "Red": game_board.calculateScore(board.ControlColour.RED)})



def either_player_eliminated(game_board):
    """
    Check if either player has no pieces remaining on the board.
    """
    if game_board.countPlayerPieces(board.Board.RED) == 0:
        return True
    if game_board.countPlayerPieces(board.Board.BLUE) == 0:
        return True
    return False


colour_strings = ["Neutral", "Red", "Blue"]


def declare_winner(game_board):
    """
    Print out the results of the game and declare the winner.
    """
    red_score = calculate_score_and_print_status(game_board, board.Board.RED)
    blue_score = calculate_score_and_print_status(game_board, board.Board.BLUE)

    if red_score == blue_score:
        print("Game result: DRAW!")
    else:
        winning_colour = board.Board.RED if red_score > blue_score else board.Board.BLUE
        print("Game result: {} Wins!".format(colour_strings[winning_colour]))


def calculate_score_and_print_status(game_board, colour):
    """
    Declare the results for a single player.

    TODO: printing and calculating score should not be coupled like this.
    """

    control = game_board.countPlayerControlledTiles(colour)
    pieces = game_board.countPlayerPieces(colour)
    strength = game_board.countPlayerPieceStrengths(colour)
    total = control + pieces + strength

    if pieces == 0:
        total = 0

    print("Player {}:".format(colour_strings[colour]))
    print("  Tiles controlled: {}".format(control))
    print("  Pieces on board: {}".format(pieces))
    print("  Piece strengths: {}".format(strength))

    if total == 0:
        print("  ELIMINATED!!!")
    else:
        print("  Total score: {}".format(total))

    return total


def read_config_file(file_name):
    """
    Parse the game's config file as a JSON object.
    """
    with open(file_name) as f:
        config = json.loads(f.read())
    return config


def read_arguments():
    """
    Parse the input arguments provided by the user.
    """
    parser = argparse.ArgumentParser(description="Run a battle between some bots")

    help_message = {
        "-c": "Specify the game configuration location.",
        "-D": "Override default directory for bots.",
        "-R": "Name of red player bot in bots directory.",
        "-B": "Name of blue player bot in bots directory.",
        "-s": "Random seed for the game (optional).",
        "-d": "Turn on debug output for bots.",
    }

    parser.add_argument("-c", "--config", type=str, help=help_message["-c"], required=True)
    parser.add_argument("-D", "--botDir", type=str, help=help_message["-D"], required=False)
    parser.add_argument("-R", "--red", type=str, help=help_message["-R"], required=True)
    parser.add_argument("-B", "--blue", type=str, help=help_message["-B"], required=True)
    parser.add_argument("-s", "--seed", type=int, help=help_message["-s"], required=False)
    parser.add_argument("-d", "--debug", help=help_message["-d"], required=False)

    return parser.parse_args()


if __name__=="__main__":
    main()
