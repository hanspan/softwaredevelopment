#! /bin/bash

# This script decrypts an exercise solution, using the
# private key for the course, which is expected to be
# in the same directory.

# Usage: decrypt.sh [file].tgz
# Output: [file]

PRIVKEY="courseKey.priv"

# Check for the filename argument.
if [ $# -lt 1 ]
then
    echo "This program takes one argument, a filename to decrypt."
    exit
fi

# Preserve the filename.
FILE=$1

# Check that the file exists.
if [ ! -f $FILE ]
then
    echo "Unable to find file '$FILE'."
    exit
fi

# And is a .tgz file.
if ! [[ $FILE =~ .tgz$ ]]
then
    echo "This program requires a .tgz file to run."
    exit
fi

# Check that the private key file exists.
if [ ! -f $PRIVKEY ]
then
    echo "Unable to find file '$PRIVKEY'."
    exit
fi


echo "Passed all checks; proceeding to decrypt."

#
# The main work routine: generate a local key, encrypt the file,
# and bundle up the results.
#

# Get the basename of the file.
BASE=`basename $FILE .tgz`
echo $BASE

# Get a temporary directory to work with.
TMPDIR=`mktemp -d`
cp $FILE $TMPDIR

# Extract the file, and decrypt the contents!
tar -C $TMPDIR -xf $FILE
openssl rsautl -decrypt -inkey $PRIVKEY -in $TMPDIR/$BASE.key.enc -out $TMPDIR/$BASE.key
openssl enc -d -aes-256-cbc -in $TMPDIR/$BASE.enc -out $BASE -pass file:$TMPDIR/$BASE.key

# Cleanup temporary files.
echo "Cleaning up."
rm -rf $TMPDIR

echo "Done."


