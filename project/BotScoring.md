# Bot Scoring Rules

This file describes how your bots will be scored as the course progresses.

#### Note
While battle-winning bots will score points, there are lots more
points available for completing exercises and penalties in place for
writing low quality code.


### Leaderboard
To have a bot entered on the course leaderboard (not compulsory), change the
entry next to your name in [config/bot_owner_config.json](config/bot_owner_config.json)


###Scoring
Every bot starts with a score of 100 points. You can run the bot_scorer.py 
script on your bot at any point to get an idea of how you can improve.

-----------------------
# Pull Request Checklist
-----------------------
* Check for and fix easy issues
`python3 bot_scorer.py <your bot name>`
* Remove print statements
* Make sure all your tests pass!
* Get the most recent copy of Development
`git checkout Development`
`git pull`
* Action back to your branch
`git checkout <your branch>`
* Merge in Development
`git merge Development`
* Fix any merge conflicts - give us a shout if this is confusing!
* Upload your changes 
`git push`
* Make a pull request: navigate to 'branches' on the bitbucket page and click 'Create' next to your branch name.
* Keep an eye on your pull request for comments - we'll try to get round to tell you in person but we can get quite busy!

----------
# Day 1
----------
### Battles!
There are up to 25 points available if your bot does well against other bots on the course.

We also have some 'House Bots' that you can get extra points for defeating. These will 
have some easier Bots and increase in difficulty to include some previous champions. You can 
have 5 points per House Bot you defeat.

### Names!
You can have a point for a good bot name (we're easy to convince).

----------
# Day 2
----------
## Morning 
### Testing!
Your score will be multiplied by your test coverage percentage.
BEWARE: If you have a failing test, your bot WILL SCORE 0!

## Afternoon

### Documentation!
Your bot will be penalised if it's lacking comments, or if you haven't updated 
the comments from randomBot.

### Function length!
Your bot will be penalised if it has functions that are too long.

----------
# Day 3
----------
## Afternoon 
### Formal code quality metrics
* pep8: your bot will be penalised for any pep8 violations.
* pylint: your bot will be penalised for pylint violations.

----------
# Day 4
----------
Signatures for exercises! Each correct signature will earn 5 points.

## Morning
### Debug Challenge, ex 4.3

Use your answers from ex 4.3 and the script [bot_signature.py](bot_signature.py)
to generate a signature to be returned when getSignature('debug')
is called. 

## Afternoon
### Database Challenge, ex 4.5, qu13

Use your answers from ex 4.5, qu13 and the script [bot_signature.py](bot_signature.py)
to generate a signature to be returned when getSignature('database')
is called. 

----------
# Day 5
----------
# NOTE: The following bonus is not active, yet.
## Morning
### Docker Challenge, ex 5.2, qu5

Take the Dockerfile script you created (with your bot's name):
* Name it `randomBot.dockerfile`
* Copy it to this directory (TODO: improve this step)
* Encrypt it with the course's public key:
`./encrypt.sh randomBot.dockerfile`
* Put the resulting `randomBot.dockerfile.tgz` into the bots/
  directory, and check it in.
* The leaderboard will use this file to create a docker instance,
  and challenge your bot to complete a simple python task.

==============================================================
Notes to whomever implements this capability, as I'm out of time for
this run of the course:

The following is a sample series of Python commands to communicate
with the docker instance, assuming the dockerfile has been decrypted
and used to generate an image with tag "leaderboard:randomBot":

`>>> p=subprocess.Popen(['/usr/bin/docker', 'run', '-i', '--name', 'randomBotTest', 'leaderboard:randomBot'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)`
`>>> p.stdin.write(b'print(3+5)')`
`>>> p.stdin.close()`
`>>> p.stdout.read()`
`"Enter a challenge for randomBot: 8\n"`

Given the name randomBotTest, the container could then be deleted, and freed up for the next run of the script.

