"""
Day 4, exercise3.cpp  or exercise3.py are programs with two lines
 of code that are the source of errors (one is very clearly an issue, and,
while one could argue that the other error is slightly more
generic, there is one spot where it has the most significant impact).

We will use these two numbers to generate a unique signature for
each bot. The Leaderboard may then care about whether or not your
bot is returning the right value from getSignature()!

Day 4, database exercise 4.5, question 13 will produce a list of 
propert names. You should write these as a comma-separated list to
get the hash value for the database challenge.
"""

import hashlib      # For access to sha256.
import sys

# First, gather the inputs for the routine.
bot = input("What is the name of your bot [e.g. randomBot]? ")
challenge = input("Which challenge would you like to compute a hash for? [debug, database] ")

if challenge == 'debug':
    version = input("Which version of the debug exercise did you complete? ([C] or [P]ython)").lower()
    if version in ['p', 'python', 'py']:
        line_offset = 62
    elif version in ['c', 'cpp', 'c++']:
        line_offset = 0
    else:
        print("Unexpected excerise version: {}, please specify [C] or [P]ython")
        sys.exit(1)

    print("""Analysing the exercise3 file in exercise 4.3 should lead you to two lines of code that cause problems. 
Line numbers should be entered from smallest to largest.""")
    line1 = input("What is the first problematic line number? ")
    line2 = input("What is the second problematic line number? ")
    try:
        line1 = int(line1) + line_offset
        line2 = int(line2) + line_offset
    except ValueError:
        print("Unrecognised line numbers: {} {}".format(line1, line2))
    # Build the string up.
    answer = "{},{}".format(line1, line2)
elif challenge == 'database':
    answer = input("""What is the answer to Ex 4.5 question 13?
Please enter your answer as a list property names exactly as they appear
in the exercise, separating with a single comma,
    e.g. The Strand,Mayfair
N.B. This is not the answer! ;)
Answer: """)             
else:
    print("Unrecognised challenge: %s" % challenge)
    sys.exit(0)


print("Computing...")
raw = bot + ":" + answer
# And hash it 10 thousand  times (please don't try to just exhaust
# the solution!).
curr = raw.encode("utf-8")
for i in range(10000):
    curr = hashlib.sha256(curr).hexdigest().encode("utf-8")

print("Your signature is: ", curr.decode("utf-8"))
