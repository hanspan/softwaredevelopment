"""
Crappy bot, does nothing
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    return DoNothingBot()


class DoNothingBot(BotBase.BotBase):
    def generateMoves(self, currTurn, maxTurns):
        return []    

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'do_nothing_bot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
