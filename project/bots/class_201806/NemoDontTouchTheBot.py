"""

########### NemoDontTouchTheBot ##########

 O     O           ,
  o o          .:/
    o      ,,///;,   ,;/
      o   o)::::::;;///
         >::::::::;;\\\
           ''\\\\\'" ';\
              ';\

##########################################

Created by jamesr2

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """
    return NemoDontTouchTheBot()


def findClosestEnemy(position, enemies):
    """
    Finds the closest enemy to a given position, and returns a
    tuple containing the distance and direction to that enemy.
    """

    closest_enemy = -1
    closest_distance = 100

    for enemy in enemies:
        if abs(enemy - position) < closest_distance:
            closest_enemy = enemy
            closest_distance = abs(enemy - position)

    return (closest_distance, 1 if closest_enemy > position else -1)


def calculateMove(position, strength, enemies):
    """
    Calculate the move to make from a given position.
    """

    closest_distance, direction = findClosestEnemy(position, enemies)
    if closest_distance < 3:
        if direction > 0:
            return GameBoard.Action.RIGHT
        return GameBoard.Action.LEFT

    elif strength > 5 and strength % 2 == 1:

        # If the strength is odd and larger than 5, we clone.
        return GameBoard.Action.SPLIT

    return GameBoard.Action.STAY


class NemoDontTouchTheBot(BotBase.BotBase):
    """
    This Bot just blindly charges towards the enemy when it comes close enough.
    If there are no enemies nearby, it stays still and attempts to clone.
    """

    def __init__(self):
        """
        Initialise the Bot
        """
        BotBase.BotBase.__init__(self)

    def generateMoves(self, currTurn, maxTurns):
        """
        Locate all of our pieces on the board,
        and then make a move for each one.
        """

        pieces = self.board.findMyPieces()
        enemies = self.board.findOpponentPieces()

        moves = []

        for position in pieces:
            strength = self.board.getPieceStrength(position)
            move = calculateMove(position, strength, enemies)
            moves.append([position, str(move), 0])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'NemoDontTouchTheBot' and the correct
        line numbers for the debug challenge.
        """
        if challenge == "debug":
            return ("c262f307e339ec7ac38522b13056523e"
                    "39417e5250b038fc57fb1edfb9700aac")
        elif challenge == "database":
            return ("fac9221640a6143c32052451aade4c72"
                    "b827728bc8b10a028497ddc6f4da855a")

        return "Default Bot Signature :("
