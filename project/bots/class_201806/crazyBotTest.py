import os
import unittest.mock  # Bring in Python's built-in unit testing functionality.
import json  # Process JSON to/from strings, etc.
import GameBoard  # Information on game board properties.
import crazyBot  # The Bot being tested.

import BotTestCase


class crazyBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        """ Common setup for the tests in this class."""

        self.bot = crazyBot.CrazyBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        """ Make sure a bot is returned from the getBot() function."""

        self.assertNotEqual(None, crazyBot.getBot())

    def testKnownMove(self):
        """ This is a "last known good state" test. By itself, it doesn't
        really indicate any problems. However, it establishes a known
        input/output matching set, and will highlight if something
        changes that result."""

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

    def testShortestPath(self):
        """This tests the Shortest path function to see whether it works well wrapping around the board"""
        self.assertEqual(self.bot.shortestPath(0, 1), (GameBoard.Action.JUMP_RIGHT, 1))
        self.assertEqual(self.bot.shortestPath(0, 99), (GameBoard.Action.JUMP_LEFT, 1))
        self.assertEqual(self.bot.shortestPath(50, 1), (GameBoard.Action.JUMP_LEFT, 49))
        self.assertEqual(self.bot.shortestPath(28, 43), (GameBoard.Action.JUMP_RIGHT, 15))

    def testNewJumpAttack(self):       
        """ Mock the function we're testing for and trigger it.
        Since the function isn't "real", there are followon errors to catch."""
        # Return full strength
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(return_value=25) 

        # Test unSensitivity
        self.assertEqual(self.bot.newJumpAttack(25, 75, 25), ('', 100))
        self.assertEqual(self.bot.newJumpAttack(25, 75, 0), ('', 100))
        self.assertEqual(self.bot.newJumpAttack(40, 50, 16), ('', 100))
        self.assertEqual(self.bot.newJumpAttack(40, 50, 14), (GameBoard.Action.JUMP_RIGHT, 10))

        # Test Jump Right and Jump Left
        self.assertEqual(self.bot.newJumpAttack(30, 50, 0), (GameBoard.Action.JUMP_RIGHT, 20))
        self.assertEqual(self.bot.newJumpAttack(50, 30, 0), (GameBoard.Action.JUMP_LEFT, 20))

    def testStandardMove(self):
        """Test either split, stay or split"""
        self.bot.board.boardSize = 100
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(return_value=10)
        self.assertEqual(self.bot.standardMoves(25, 10), GameBoard.Action.SPLIT)

        self.bot.board.getPieceStrength = unittest.mock.MagicMock(return_value=10)
        self.assertEqual(self.bot.standardMoves(25, 55), GameBoard.Action.STAY)

        self.bot.board.getPieceStrength = unittest.mock.MagicMock(return_value=22)
        self.assertEqual(self.bot.standardMoves(25, 55), GameBoard.Action.SPLIT)
