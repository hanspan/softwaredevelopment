"""
This is totally not rhoBot.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RhoBot()


class RhoBot(BotBase.BotBase):
    """This likes strong outsides"""

    def generateMoves(self, currTurn, maxTurns):
        """
        Spread for first 50 turns. Then grow strong and spread away from start.
        """
        pieces = self.board.findMyPieces()
        moves = []
        for location in pieces:
            if currTurn < 50:
                move = (GameBoard.Action.STAY if (currTurn % 2)
                        else GameBoard.Action.SPLIT)
            else:
                middle = location > 25 and location < 75
                is_red = self.board.player == GameBoard.Colour("Red")
                if middle ^ is_red:
                    move = [GameBoard.Action.LEFT, GameBoard.Action.SPLIT,
                            GameBoard.Action.STAY][currTurn % 3]
                else:
                    move = [GameBoard.Action.RIGHT, GameBoard.Action.SPLIT,
                            GameBoard.Action.STAY][currTurn % 3]
            arg = 1
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    def getSignature(self, challenge='debug'):
        """
        This is the hash of 'notRhoBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("510ea63c9e8a44a4280e0ed4e90cee93"
                    "481d7c1c7267de50db99a2c93b62bf52")
        elif challenge == "database":
            return ("3026354b8ddf6d0a9072243326363463"
                    "8e0d72d8a7722cc6448bdb156a20efe9")
        return "notRhoBot signature"
