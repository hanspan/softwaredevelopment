"""
rhoBot 
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RhoBot()


class RhoBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
           pick random moves from the moveList (nojump) with various probabilities:
           LEFT:  1/6
           RIGHT: 1/6
           STAY:  1/3
           SPLIT: 1/3
        """

        myPieces = self.board.findMyPieces()
        opPieces = self.board.findOpponentPieces()
        moveList = [GameBoard.Action.LEFT,
                    GameBoard.Action.RIGHT,
                    GameBoard.Action.STAY,
                    GameBoard.Action.STAY,
                    GameBoard.Action.SPLIT,
                    GameBoard.Action.SPLIT]
        moves = []

        for location in myPieces:
            
            #moveList = []
            move = random.choice(moveList)

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1

            
            while (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                move = random.choice(list(GameBoard.Action))

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
