"""
Creates a slowly marching army of 25s with period 2
Rate is 12.5 strength/turn
"""

import random      # Source for random numbers.
import sys	   # A vital utility for a vital function
import BotBase     # All of the basic Bot functionality.


def generateHeckle():
    """This function will generate devestating heckles
    Only to be used in non-competitive matches"""
    list1 = ["daft", "dozy", "foolish", "son of a", "lousy", "stinking"]
    list2 = ["nitwit", "fool of a Took", "loser"]
    output = "\n\nmans_not_bot: Take that, you {} {}!\n\n".format(
        random.choice(list1), random.choice(list2))
    return output


def getBot():
    """This function returns an instance of the mans_not_bot class"""
    return MansNotBot()


class MansNotBot(BotBase.BotBase):
    """Creates a slowly marching army of 25s (period 2).
    The bot is completely deterministic."""

    def __init__(self):
        """This is for PEP8 compliance"""
        BotBase.BotBase.__init__(self)
        self.i_l = 0
        self.center = []
        self.full_width = []
        self.lefts = []

    def parity0move(self):
        """Moves for even turns: the 'move' phase"""
        moves = []
        pieces = self.board.findMyPieces()
        for location in pieces:
            # Everything moves and splits
            if location in self.center:
                moves.append([location, "Split", 1])
            elif location in self.lefts:
                moves.append([location, "Left", 1])
            else:
                moves.append([location, "Right", 1])
        return moves

    def parity1move(self):
        """Moves for odd turns: the 'grow' phase"""
        moves = []
        pieces = self.board.findMyPieces()
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            # Everything stays still unless it's full when there's no point
            if location in self.full_width and strength < 25:
                moves.append([location, "Stay", 1])
            elif location in self.lefts:
                moves.append([location, "Left", 1])
            else:
                moves.append([location, "Right", 1])
        return moves

    def generateMoves(self, currTurn, maxTurns):
        """Generates the moves for mans_not_bot"""

        pieces = self.board.findMyPieces()

        # This is the setup of the bot's internal state
        # i_l is initial location
        # full_width is the region where the pieces grow
        # lefts is the region that shoots left
        # and don't forget to heckle
        if currTurn == 1:
            sys.stdout.write(generateHeckle())
            self.i_l = pieces[0]
            if self.i_l == 74:
                self.i_l -= 1
            self.center = [self.i_l, self.i_l + 1]
            self.full_width = list(range(self.i_l - 22, self.i_l + 24))
            board_size = self.board.boardSize
            self.lefts = [(self.i_l - i) %
                          board_size for i in range(1, board_size // 2)]

        # The first, custom moves
        if currTurn == 1 or currTurn == 2:
            pieces = self.board.findMyPieces()
            return [[pieces[0], "Stay", 1]]

        # even moves
        if currTurn % 2:
            return self.parity0move()

        # odd moves
        return self.parity1move()

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'mans_not_bot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("df4fe020952359a20ee41c2e39ad2955"
                    "5dfef5b230b019c9049595ed2e970431")
        elif challenge == "database":
            return ("9b0e5db7d9fc24287f103b62f18618d3"
                    "7a48ac18a86deefa61c2e548c3ae5757")
        return "Default bot signature"
