
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import phalanxBot      # The Bot being tested.

import ARBaseBotTest


class randomBotTestCase(ARBaseBotTest.ArBaseBotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = phalanxBot.getBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, phalanxBot.getBot())

    def testGenerateMoves(self):
        self.setBotBoardState("phalanxBotTestBoardState.txt")
        
        generated_moves = self.bot.generateMoves(0, 1000)
        self.assertIn([2, str(GameBoard.Action.SPLIT), 1], generated_moves)
        self.assertIn([21, str(GameBoard.Action.JUMP_RIGHT), 5], generated_moves)
        self.assertIn([25, str(GameBoard.Action.JUMP_RIGHT), 7], generated_moves)
        self.assertIn([26, str(GameBoard.Action.STAY), 1], generated_moves)
        self.assertIn([29, str(GameBoard.Action.JUMP_RIGHT), 1], generated_moves)
    
    def testSignature(self):
        # Check that the expected signature is returned.

        sig = phalanxBot.PhalanxBot.getSignature('debug')
        self.assertTrue(sig.endswith("779292e9c5c"))
        sig = phalanxBot.PhalanxBot.getSignature('database')
        self.assertTrue(sig.endswith("eeafb3a60b"))
        sig = phalanxBot.PhalanxBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
