
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import intelligentAggressiveSwarmBot      # The Bot being tested.

import BotTestCase


class randomBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = intelligentAggressiveSwarmBot.IntelligentAggressiveSwarmBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, intelligentAggressiveSwarmBot.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        
        # The first move for this robot should always be to stay, since it can't split
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownJumpMove(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForIntelligentAggressiveSwarmBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_RIGHT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(2, results[0][2])

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = intelligentAggressiveSwarmBot.IntelligentAggressiveSwarmBot.getSignature('debug')
        self.assertTrue(sig.endswith("68c799a7b3d1"))
        sig = intelligentAggressiveSwarmBot.IntelligentAggressiveSwarmBot.getSignature('database')
        self.assertTrue(sig.endswith("421d7539167e"))
        sig = intelligentAggressiveSwarmBot.IntelligentAggressiveSwarmBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
