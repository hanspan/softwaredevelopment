"""
This is a basic Bot; it waits for a piece to reach its maximum strength and
then splits the piece. At the beginning it will aggressively expand outwards
until it meets opposition, only then falling back on the core tactic

It should not take much to outperform this bot... although maybe it's pretty
good?
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return SpecialSeekerBot()


def jumpDecider(possible_jumps):
    """Decides on a jump to take from a list of possible jumps"""
    possible_jumps.sort(key=abs)
    jump = possible_jumps.pop(0)
    return jump


def jumpToMove(jump):
    """Translates integer jump to direction of jump"""
    if jump < 0:
        return GameBoard.Action.JUMP_LEFT
    return GameBoard.Action.JUMP_RIGHT


def leastJumpFinder(deltas):
    """Because jumps wrap around, calculating shortest jump to a
       special square requires a little processing"""
    # Dealing with case where position large, square
    # location is small
    deltas = [-x % 50 if x > 50 else x for x in deltas]
    # Dealing with case where position small, square
    # location is large
    deltas = [x % 50 if x < 0 and (abs(x) > x % 50) else x for x in deltas]
    return deltas


class SpecialSeekerBot(BotBase.BotBase):
    """This bot just looks for special squares and tries to keep them"""

    def testForStrategicJump(self, piece_location):
        """Function to find the strategic tiles that can be captured by a
        piece without sacrificing more than half of its current strength.
        """
        # modifying distances on the ring to be sensible relative values
        grow_displacements = [
            x - piece_location for x in self.board.growthTiles]
        # turning into shortest leap
        grow_displacements = leastJumpFinder(grow_displacements)
        # doing the same for defence squares
        defence_displacements = [
            x - piece_location for x in self.board.defenceTiles]
        defence_displacements = leastJumpFinder(defence_displacements)
        strength = self.board.getPieceStrength(piece_location)
        possible_defence_jumps = \
            [x for x in defence_displacements if abs(x) < strength // 2]
        possible_grow_jumps = \
            [x for x in grow_displacements if abs(x) < strength // 2]
        if possible_defence_jumps and possible_grow_jumps:
            return possible_grow_jumps, possible_defence_jumps
        elif possible_defence_jumps:
            return None, possible_defence_jumps
        elif possible_grow_jumps:
            return possible_grow_jumps, None
        return None, None

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """
        grow_tiles = set(self.board.growthTiles)
        defence_tiles = set(self.board.defenceTiles)
        pieces = self.board.findMyPieces()
        moves = []
        max_strength = self.board.maxTileStrength

        for location in pieces:
            if location in grow_tiles:
                # Splitting on growth - maximise growing possibilities
                move = GameBoard.Action.SPLIT
                arg = 1
            elif location in defence_tiles and \
                    (self.board.getPieceStrength(location) < (max_strength)):
                # Reinforcing a weak defensive tile
                move = GameBoard.Action.STAY
                arg = 1
            elif location in defence_tiles:
                # Splitting when sufficiently strong - no advantage in being
                # too OP
                move = GameBoard.Action.SPLIT
                arg = 1
            else:
                grow_jumps, defense_jumps = self.testForStrategicJump(location)
                if grow_jumps:
                    # Jump to a growth square if possible (preferred)
                    jump = jumpDecider(grow_jumps)
                    move = jumpToMove(jump)
                    arg = abs(jump)
                elif defense_jumps:
                    # If no growth square, then go for defense.
                    jump = jumpDecider(defense_jumps)
                    move = jumpToMove(jump)
                    arg = abs(jump)
                else:
                    # If no special square available, revert to the sarahBot
                    # tactics
                    move = GameBoard.Action.STAY if (currTurn) % \
                                                    (2) else GameBoard.Action.SPLIT
                    arg = 1
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        # b3079ba6258098a04e021a890cfd59f11ed84bd30ea7d5c213eca49aa57c5033
        elif challenge == "database":
            return ("b3079ba6258098a04e021a890cfd59f1"
                    "1ed84bd30ea7d5c213eca49aa57c5033")
        return "Default bot signature"
