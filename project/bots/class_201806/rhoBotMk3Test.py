"""
rhoBotMk3Tester
"""
import os
import json
import GameBoard
import rhoBotMk3

import BotTestCase


class TestRhoBotMk3(BotTestCase.BotTestCase):
    """ a class for testing the rhoBotMk3"""
    def setUp(self):
        """ Set up the bot testing class"""
        self.bot = rhoBotMk3.RhoBotMk3()
        self.bot.setup(self.getGameConfig())
        self.board = self.bot.board

    def testGetBot(self):
        """ test the getBot function"""
        self.assertNotEqual(None, rhoBotMk3.getBot())

    def testKnownMove(self):
        """test the outcomes of some known gamestates"""
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'rhoBotMk3_testStates.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

    def testSignature(self):
        """ test the signatures"""
        sig = rhoBotMk3.RhoBotMk3.getSignature('debug')
        self.assertTrue(sig.endswith("248560e79"))
        sig = rhoBotMk3.RhoBotMk3.getSignature('database')
        self.assertTrue(sig.endswith("236fee1d4dd36dc"))
        sig = rhoBotMk3.RhoBotMk3.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))

    def testMnbRedEven(self):
        """test stratOne function for red player, even turn"""

        test = rhoBotMk3.stratOne(0, 1, 1, 0, "red")
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.stratOne(0, 25, 1, 0, "red")
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.stratOne(0, 27, 1, 0, "red")
        self.assertEqual(test, GameBoard.Action.STAY)

    def testMnbRedOdd(self):
        """test stratOne function for red player, odd turn"""

        test = rhoBotMk3.stratOne(1, 1, 1, 0, "red")
        self.assertEqual(test, GameBoard.Action.LEFT)

        test = rhoBotMk3.stratOne(1, 25, 1, 0, "red")
        self.assertEqual(test, GameBoard.Action.SPLIT)

        test = rhoBotMk3.stratOne(1, 27, 1, 0, "red")
        self.assertEqual(test, GameBoard.Action.RIGHT)

    def testMnbRedHi(self):
        """test stratOne function for red player, high strength"""

        test = rhoBotMk3.stratOne(0, 50, 25, 0, "red")
        self.assertEqual(test, GameBoard.Action.RIGHT)
        test = rhoBotMk3.stratOne(0, 0, 25, 0, "red")
        self.assertEqual(test, GameBoard.Action.LEFT)

    def testMnbRedJump(self):
        """test stratOne function for red player, jump strength"""

        test = rhoBotMk3.stratOne(50, 48, 21, 0, "red")
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)

        test = rhoBotMk3.stratOne(50, 1, 21, 0, "red")
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)

    def testMnbBlueJump(self):
        """test stratOne function for red player, jump strength"""

        test = rhoBotMk3.stratOne(50, 51, 21, 0, "blue")
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)

        test = rhoBotMk3.stratOne(50, 98, 21, 0, "blue")
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)

    def testMnbBlueEven(self):
        """test stratOne function for blue player, even turn"""

        test = rhoBotMk3.stratOne(0, 1, 1, 0, "blue")
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.stratOne(0, 74, 1, 0, "blue")
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.stratOne(0, 72, 1, 0, "blue")
        self.assertEqual(test, GameBoard.Action.STAY)

    def testMnbBlueOdd(self):
        """test stratOne function for blue player, odd turn"""

        test = rhoBotMk3.stratOne(1, 1, 1, 0, "blue")
        self.assertEqual(test, GameBoard.Action.RIGHT)

        test = rhoBotMk3.stratOne(1, 74, 1, 0, "blue")
        self.assertEqual(test, GameBoard.Action.SPLIT)

        test = rhoBotMk3.stratOne(1, 72, 1, 0, "blue")
        self.assertEqual(test, GameBoard.Action.LEFT)

    def testMnbBlueHi(self):
        """test stratOne function for blue player, high strength"""

        test = rhoBotMk3.stratOne(0, 50, 25, 0, "blue")
        self.assertEqual(test, GameBoard.Action.LEFT)
        test = rhoBotMk3.stratOne(0, 0, 25, 0, "blue")
        self.assertEqual(test, GameBoard.Action.RIGHT)

    def testEarlyMoves(self):
        """test the getEarlyMoves function"""
        test = rhoBotMk3.getEarlyMove(0, 0, 1, 0)
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.getEarlyMove(0, 0, 25, 0)
        self.assertEqual(test, GameBoard.Action.SPLIT)

        test = rhoBotMk3.getEarlyMove(0, 0, 2, 0)
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.getEarlyMove(0, 0, 5, 0.5)
        self.assertEqual(test, GameBoard.Action.SPLIT)

    def testStratOneSetup(self):
        """test the stratOnesetup function"""

        test = rhoBotMk3.stratOneSetup(0, 0, 0, GameBoard.Colour.RED)
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk3.stratOneSetup(0, 0, 0, GameBoard.Colour.BLUE)
        self.assertEqual(test, GameBoard.Action.STAY)
