"""
NEW AND IMPROVED: MANSNOTBOT 2.0 (dnt h8 m8)
Creates a slowly marching army of 25s with rate two thirds
This is a huge improvement on one half!

"You miss 100% of the shots you don't take. -Wayne Gretzky"

                                                        -James Simpson

==IMPORTANT==
This will not work when too close to another bot:
It's ideal for use on larger boards but can get constrained.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
# import GameBoard   # Utilities for working with the game.


def generateHeckle():
    """This function will generate devestating heckles
    in non-competitive matches"""
    list1 = ["daft", "dozy", "foolish", "son of a", "lousy", "stinking"]
    list2 = ["nitwit", "fool of a Took", "loser"]
    output = "\n\nmans_not_bot: Take that, you {} {}!\n\n".format(
        random.choice(list1), random.choice(list2))
    return output


def getBot():
    """This function returns an instance of the turn_down_for_bot class"""
    return TurnDownForBot()


class TurnDownForBot(BotBase.BotBase):
    """Creates a slowly marching army of 25s with rate two thirds"""

    def __init__(self):
        """This is for PEP8 compliance"""
        BotBase.BotBase.__init__(self)
        self.s_p = 0
        self.center = []
        self.lefts = []

    def initial_move(self, currTurn):
        """For the first few turns, set moves are needed"""
        move_list = [[[self.s_p, "Stay", 1]],
                     [[self.s_p, "Stay", 1]],
                     [[self.s_p, "Split", 1]],
                     [[self.s_p - 1, "Left", 1], [self.s_p, "Right", 1],
                      [self.s_p + 1, "Right", 1]],
                     [[self.s_p - 2, "Stay", 1],
                      [self.s_p + 1, "Left", 1], [self.s_p + 2, "Stay", 1]],
                     [[self.s_p - 2, "Stay", 1], [self.s_p, "Stay", 1],
                      [self.s_p + 2, "Stay", 1]]]

        return move_list[currTurn - 1]

    def parity0move(self, pieces):
        """Set moves for every 3nth turn"""
        moves = [[self.s_p - 1, "Left", 1],
                 [self.s_p, "Stay", 1],
                 [self.s_p + 1, "Right", 1]]
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            #print(location, strength)

            if location in self.center:
                continue
            elif strength < 25:
                moves.append([location, "Stay", 1])
            elif location in self.lefts:
                moves.append([location, "Left", 1])
            else:
                moves.append([location, "Right", 1])
        return moves

    def parity1move(self, pieces):
        """Set moves for every 3n+1th turn"""
        moves = [[self.s_p - 2, "Split", 1],
                 [self.s_p, "Stay", 1],
                 [self.s_p + 2, "Split", 1]]
        for location in pieces:
            if location in self.center:
                continue
            elif location in self.lefts:
                moves.append([location, "Left", 1])
            else:
                moves.append([location, "Right", 1])
        return moves

    def parity2move(self, pieces):
        """Set moves for every 3n+2th turn"""
        moves = [[self.s_p - 2, "Left", 1],
                 [self.s_p - 1, "Stay", 1],
                 [self.s_p, "Split", 1],
                 [self.s_p + 1, "Stay", 1],
                 [self.s_p + 2, "Right", 1]]
        for location in pieces:
            if location in self.center:
                continue
            elif location in self.lefts:
                moves.append([location, "Left", 1])
            else:
                moves.append([location, "Right", 1])
        return moves

    def generateMoves(self, currTurn, maxTurns):
        """Generates the moves for turn_down_for_bot"""

        # This bit of code is purely devoted to heckling: unquote to heckle
        # if currTurn == 1:
        #    print(generateHeckle())

        # The position of all my pieces
        pieces = self.board.findMyPieces()

        # I want to remember my starting position on the board, and significant
        # regions of the board
        if currTurn == 1:
            self.s_p = pieces[0]
            board_size = self.board.boardSize
            self.lefts = [(self.s_p - i) %
                          board_size for i in range(3, board_size // 2)]
            self.center = [(self.s_p + 2 - i) % board_size for i in range(5)]

        # Turns in this range are special in the setup of the bot
        setup_turns = range(1, 7)

        # This bots has (eventually) a period 3
        parity = currTurn % 3

        if currTurn in setup_turns:
            return self.initial_move(currTurn)
        elif parity == 0:
            return self.parity0move(pieces)
        elif parity == 1:
            return self.parity1move(pieces)
        return self.parity2move(pieces)

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'turn_down_for_bot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        if challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
