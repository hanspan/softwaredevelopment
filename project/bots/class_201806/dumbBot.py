"""
    DumbBot calculates the next move as a result of predicted strengths
    and batches the surrounding areas into groups of 12 cells to choose
    which way to move, and what action to take
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return DumbBot()


def getLocationScores(position, scores):
    """
    helper function,
    get scores for chunks of 12 cells adjacent to position (close
    left/right) and the 12 cells adjacent to those (far left/right)
    """
    positions = getSurroundingPositions(position, 24, len(scores))
    farLeft = sum(scores[pos] for pos in positions[:12])
    closeLeft = sum(scores[pos] for pos in positions[12:24])
    closeRight = sum(scores[pos] for pos in positions[25:37])
    farRight = sum(scores[pos] for pos in positions[37:])
    return farLeft, closeLeft, closeRight, farRight


def getSurroundingPositions(center, size, boardSize):
    """ helper function, get the n positions surrounding center point """
    return [pos % boardSize for pos in
            range(center-size+boardSize, center+size+boardSize+1)]


class DumbBot(BotBase.BotBase):
    """
    DumbBot calculates the next move as a result of predicted strengths
    and batches the surrounding areas into groups of 12 cells to choose
    which way to move, and what action to take
    """

    def sumSquares(self, number):
        """ Calculate the sum of the first n squares """
        return sum(self.square(i+1) for i in range(number))

    def square(self, num):
        """ Calculate the square of a number (for an exercise) """
        return num ** 2

    def distance(self, posA, posB):
        """ Calculate the shortest distance between two points """
        distRight = posB - posA
        distLeft = posA - posB + self.board.boardSize
        if distRight < 0:
            newRight = 2 * self.board.boardSize - distLeft
            distLeft = -distRight
            distRight = newRight
        if distLeft < distRight:
            return distLeft, -1
        return distRight, +1

    def getNearestMine(self, position):
        """ Sort my pieces by distance from position """
        return self.getNearestColour(position, self.board.player)

    def getNearestOpponent(self, position):
        """ Sort opponents pieces by distance from position """
        return self.getNearestColour(position, self.board.opponent)

    def getNearestColour(self, position, colour):
        """ Sort pieces of given colour by distance from position """
        pieces = self.board.findPieces(colour)
        pieces.sort(key=lambda x: self.distance(x, position)[0])
        return pieces

    def requireStrength(self, location, reqdStrength, move, arg=0):
        """ take the given move, if the strength is sufficient, else STAY """
        currStrength = self.board.getPieceStrength(location)
        if currStrength < reqdStrength:
            move = GameBoard.Action.STAY
            arg = 0
        return move, arg

    def chooseMove(self, scores, loc):
        """
        each piece decides what it will do based on the expected strength
        distribution next turn (without making any guesses what the opponent
        will do) the surrounding region is split into four areas: far left,
        close left, close right and far right, with values of player strengths
        minus opponent strengths
        """
        farL, closeL, closeR, farR = getLocationScores(loc, scores)

        enemyTolerance = -1
        if farL >= enemyTolerance and farR >= enemyTolerance:
            # it's nice and quiet; spread out fast
            move, arg = self.requireStrength(loc, 2, GameBoard.Action.SPLIT)
        elif closeL >= enemyTolerance and closeR >= enemyTolerance:
            # it's quite quiet; spread out more carefully
            move, arg = self.requireStrength(loc, 3, GameBoard.Action.SPLIT)
        else:
            # enemy near, look out
            targets = self.getNearestOpponent(loc)
            target = targets[0]
            arg, _ = self.distance(loc, target)
            if closeR > closeL:
                jump = GameBoard.Action.JUMP_LEFT
            else:
                jump = GameBoard.Action.JUMP_RIGHT
            move, arg = self.requireStrength(loc, 25, jump, arg+1)

        return scores, move, arg

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        create expected state, iterate through each piece and choose the move
        using the function above
        """

        # assign +ve strength for my pieces and -ve strength for opponents'
        expected = [
            self.board.tileStrength[i]
            if self.board.tileControl[i] == self.board.player
            else -self.board.tileStrength[i]
            for i in range(self.board.boardSize)
            ]   # this looks bizarre, but it's like this for PEP8 compliance

        moves = []
        for location in self.board.findMyPieces():
            expected, move, arg = self.chooseMove(expected, location)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("6584bb1f2eb2cb0aa84c2bff53e08a61"
                    "f17f0402f35473b440c84250ff127b84")
        elif challenge == "database":
            return ("863ede5707ab8f39f98940ef95560cab"
                    "650395fd9f3c4ecf4cd6cb2bbbdbb391")
        return "Default bot signature"
