"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return randobotv2()


class randobotv2(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then choose a subset
        of these pieces and make a move for each one.

        This being an *improved* simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []
        if len(pieces)!=0:
            numberofpieces = len(pieces)
        numberofmoves = random.randint(1, numberofpieces)
        randompieces = random.sample(pieces, numberofmoves)
        
        for location in randompieces:
            move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # To add more randomness, we pick a number between 1 and the strength
            # of the piece and jump by that amount
            
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(random.randint(1, self.board.getPieceStrength(location))) # arg refers to the number of spaces moved, i.e. default 1.

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("")
        elif challenge == "database":
            return ("")
        else:
            return "Default bot signature"
