"""
This is a bot that moves each piece to the nearest empty tile.
"""

import BotBase  # All of the basic Bot functionality.
import GameBoard  # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return FizziestspateBot()


class FizziestspateBot(BotBase.BotBase):
    """
    Defines a bot that works by moving each piece to the nearest empty tile.
    """

    def myPiecesWeak(self, pieces):
        """
        This function just determines if any of
        the pieces we own have strength 1.
        If so we set the weak flag.
        """
        weak = False
        for tiles in pieces:
            strength = self.board.getPieceStrength(tiles)
            if strength == 1:
                weak = True
        return weak

    def getDistance(self, location, pieces, direc):
        """
        This function determines how far left we need to travel
        before not owning a tile, direction = -1 is left, +1 is right
        """
        board_size = self.board.boardSize
        distance = 0
        shift = location
        while shift in pieces:
            shift = (shift + board_size + direc) % board_size
            distance += 1
        return distance

    def safeMove(self, location, direc, strength):
        """
        Determines if we can move left or right
         without destroying ourselves on the opponent
         """
        board_size = self.board.boardSize
        opp_pieces = self.board.findOpponentPieces()

        safely_move = True

        loc_shift = (location + direc + board_size) % board_size
        str_shift = self.board.getPieceStrength(loc_shift)
        if (loc_shift in opp_pieces) and (strength < str_shift):
            safely_move = False
        return safely_move

    def safeSplit(self, location, strength):
        """
        Determines if we can split without
        destroying ourselves on the opponent
        """
        safely_split = True
        safe_left = self.safeMove(location, -1, strength // 2)
        safe_right = self.safeMove(location, +1, strength // 2)
        if not safe_left or not safe_right:
            safely_split = False
        return safely_split

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """Carries out the moves"""
        moves = []
        pieces = self.board.findMyPieces()

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            # Get distances to nearest gaps
            left_distance = self.getDistance(location, pieces, -1)
            right_distance = self.getDistance(location, pieces, +1)

            equal_distance = False
            if left_distance == right_distance:
                equal_distance = True

            left_nearest = left_distance < right_distance

            # Check if we can can move or split without destroying ourselves
            safely_split = self.safeSplit(location, strength)
            safely_move_left = self.safeMove(location, -1, strength)
            safely_move_right = self.safeMove(location, +1, strength)

            # Grow if we are only strength 1 on a position
            if self.myPiecesWeak(pieces):
                move = GameBoard.Action.STAY
            # If equally between gaps (tiles not owned by me) we split
            elif equal_distance:
                if safely_split:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
            # Otherwise more towards the nearest gap
            elif left_nearest:
                if safely_move_left:
                    move = GameBoard.Action.LEFT
                else:
                    move = GameBoard.Action.STAY
            elif not left_nearest:
                if safely_move_right:
                    move = GameBoard.Action.RIGHT
                else:
                    move = GameBoard.Action.STAY

            moves.append([location, str(move), 1])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("7173cf4b598acca3f4591ab191562c925be"
                    "830522cfbfe908aa519fc2fd14733")
        elif challenge == "database":
            return ("571316357e5db10fb17a54ee15579d9292"
                    "5975bac6ec9c0d8080a8c4519d84cd")

        return "Default bot signature"
