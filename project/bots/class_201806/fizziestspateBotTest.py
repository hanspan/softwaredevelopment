
import json  # Process JSON to/from strings, etc.
import GameBoard  # Information on game board properties.
import fizziestspateBot  # The Bot being tested.
import os
import BotTestCase
import sys


class fizziestspateBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = fizziestspateBot.FizziestspateBot()
        self.bot.setup(self.getGameConfig())

    def getGameConfig(self):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.

        testDataDir = self.getTestDataDir()
        configFilePath = os.path.join(testDataDir, 'testConfig.cfg')
        with open(configFilePath, 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, fizziestspateBot.getBot())

    def testFirstMove(self):
        # Make sure the first move is to Stay.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testMoveCases(self):
        # Test various moves that can come up
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testfizziestspateCases.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(6, len(results))
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[2][1])
        self.assertEqual(str(GameBoard.Action.STAY), results[3][1])
        self.assertEqual(str(GameBoard.Action.STAY), results[4][1])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[5][1])
    
    def testSignature(self):
        # Check that the expected signature is returned.

        sig = fizziestspateBot.FizziestspateBot.getSignature('debug')
        self.assertTrue(sig.endswith("19fc2fd14733"))
        sig = fizziestspateBot.FizziestspateBot.getSignature('database')
        self.assertTrue(sig.endswith("a8c4519d84cd"))
        sig = fizziestspateBot.FizziestspateBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
