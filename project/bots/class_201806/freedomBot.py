"""
This BOT fights for FREEDOM!
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard


def getBot():
    """This function returns an instance of the mans_not_bot class"""
    return FreedomBot()


class FreedomBot(BotBase.BotBase):
    """Creates a slowly marching army of 25s (period 2).
    The bot is completely deterministic."""

    def __init__(self):
        """This is for PEP8 compliance"""
        BotBase.BotBase.__init__(self)
        self.i_l = 0
        self.center = []
        self.opponent = []

    def shortestPath(self, moveFrom, moveTo):
        """This calculates the shortest path between two pieces"""
        distance_right = (moveTo - moveFrom) % self.board.boardSize
        distance_left = (moveFrom - moveTo) % self.board.boardSize
        if distance_left <= distance_right:
            move, distance = GameBoard.Action.JUMP_LEFT, distance_left
        else:
            move, distance = GameBoard.Action.JUMP_RIGHT, distance_right
        return move, distance

    def newJumpAttack(self, piece, enemy, unsensitivity):
        """This calculates the jump path between two pieces
        otherwise will return '' and 100"""
        move, arg = self.shortestPath(piece, enemy)
        if arg >= self.board.getPieceStrength(piece) - unsensitivity:
            move, arg = '', 100
        return move, arg

    def findNearEnemy(self, location, how_close=1):
        """Finds 'how_close'st enemy that is attackable and returns
        the appropriate move and arg to get them"""
        thingies = [self.newJumpAttack(
            location, item, 0) for item in self.opponent]
        #sort
        thingies.sort(key=lambda y: y[1], reverse=False)
        return thingies[how_close if len(thingies) > how_close else 0]

    def oddMove(self, location, lefts):
        """ODD MOVE TURNS"""
        move = ''
        if location in self.center:
            move = GameBoard.Action.SPLIT
        elif location in lefts:
            move = GameBoard.Action.LEFT
        else:
            move = GameBoard.Action.RIGHT
        return move

    def evenMove(self, location, lefts):
        """EVEN MOVE TURNS"""
        move = ''
        diff = abs(location - self.i_l)
        if diff < 24 and self.board.getPieceStrength(location) < 25:
            move = GameBoard.Action.STAY
        elif location in lefts:
            move = GameBoard.Action.LEFT
        else:
            move = GameBoard.Action.RIGHT
        return move

    def decideMove(self, location, turn, lefts):
        """Decides on moves for the turns"""
        move = ''
        if turn % 2:
            move = self.oddMove(location, lefts)
        else:
            move = self.evenMove(location, lefts)
        return move

    def generateMoves(self, currTurn, maxTurns):
        """Generates the moves for FREEEEDOM"""

        pieces = self.board.findMyPieces()
        self.opponent = self.board.findOpponentPieces()
        # This remembers the tile the bot was in first so it remembers to
        # generate from the same place each time
        # i_l is initial location
        if currTurn == 1:
            self.i_l = pieces[0]
            if self.i_l == 74:
                self.center = [self.i_l - 1, self.i_l]
            else:
                self.center = [self.i_l, self.i_l + 1]

        moves = []

        initial_position = self.i_l

        board_size = self.board.boardSize
        left_half = [(initial_position - i) %
                     board_size for i in range(1, board_size // 2)]

        for location in pieces:
            move = ''
            arg = 1
            strength = self.board.getPieceStrength(location)
            if currTurn < 3:
                move = GameBoard.Action.STAY
            # Everything moves and splits
            if strength >= 24:  # arbitrary number
                move, arg = self.findNearEnemy(location, 2)
            if move == '':
                move = self.decideMove(location, currTurn, left_half)
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'mans_not_bot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
