"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RememBot()


class RememBot(BotBase.BotBase):
    """This Bot works out what's changed since last turn and reacts"""
    previousBoard = None
    colourMe = None
    colourOpponent = None
    
    def getStrengthAndControl(self, board, colour):
        totalStrength = 0
        control = 0
        for thisColour, strength in board:
            if colour == thisColour:
                if strength > 0:
                    control += 1
                totalStrength += strength
        return totalStrength, control
    
    def getNeighbours(self, board, position):
        leftPosition = (position - 1) % self.board.boardSize
        rightPosition = (position + 1) % self.board.boardSize
        return board[leftPosition], board[rightPosition]
        
    def hasNeighbours(self, board, position):
        left, right = self.getNeighbours(board, position)
        return left[1] > 0 and right[1] > 0

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Compare the current board to the previous board
        """
        if self.previousBoard is None:
            self.previousBoard = [(GameBoard.Colour.NEUTRAL,0)] * self.board.boardSize
            self.colourMe = self.board.player
            self.colourOpponent = GameBoard.Colour.RED
            if self.colourMe == GameBoard.Colour.RED:
                self.colourOpponent = GameBoard.Colour.BLUE
        newBoard = [(colour, 0) for colour, _ in self.previousBoard]
        for colour in [GameBoard.Colour.RED, GameBoard.Colour.BLUE]:
            for position in self.board.findPieces(colour):
                newBoard[position] = (colour, self.board.getPieceStrength(position))
        
        myStrength,  myControl  = self.getStrengthAndControl(newBoard, self.colourMe)
        oppStrength, oppControl = self.getStrengthAndControl(newBoard, self.colourOpponent)
        
        """ remember the state of the board for next turn """
        self.previousBoard = newBoard

        pieces = self.board.findMyPieces()
        moves = []

        """ then use the board state for some extra decisions, before picking a random move """
        for location in pieces:
            if myStrength < oppStrength and newBoard[location][1] < self.board.maxTileStrength:
                move = GameBoard.Action.STAY
                myStrength += 1
            elif (myControl < oppControl or myStrength < oppStrength) and not self.hasNeighbours(newBoard, location):
                move = GameBoard.Action.SPLIT
                myControl += 1
            else:
                move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
