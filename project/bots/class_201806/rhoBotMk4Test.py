"""
rhoBotMk4Tester
"""
import os
import json
import GameBoard
import rhoBotMk4

import BotTestCase


class TestRhoBotMk4(BotTestCase.BotTestCase):
    """ a class for testing the rhoBotMk4"""
    def setUp(self):
        """ Set up the bot testing class"""
        self.bot = rhoBotMk4.RhoBotMk4()
        self.bot.setup(self.getGameConfig())
        self.board = self.bot.board

    def testGetBot(self):
        """ test the getBot function"""
        self.assertNotEqual(None, rhoBotMk4.getBot())

    def testKnownMove(self):
        """test the outcomes of some known gamestates"""
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'rhoBotMk3_testStates.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

    def testSignature(self):
        """ test the signatures"""
        sig = rhoBotMk4.RhoBotMk4.getSignature('debug')
        self.assertTrue(sig.endswith("6d60fbeb"))
        sig = rhoBotMk4.RhoBotMk4.getSignature('database')
        self.assertTrue(sig.endswith("1378efd5b3"))
        sig = rhoBotMk4.RhoBotMk4.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))

    def testStratOneRedEven(self):
        """test stratOne function for red player, even turn"""

        test = rhoBotMk4.stratOne([0, 1, 1, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.stratOne([0, 25, 1, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.stratOne([0, 27, 1, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.STAY)

    def testStratOneRedOdd(self):
        """test stratOne function for red player, odd turn"""

        test = rhoBotMk4.stratOne([1, 1, 1, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.LEFT)

        test = rhoBotMk4.stratOne([1, 25, 1, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.SPLIT)

        test = rhoBotMk4.stratOne([1, 27, 1, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.RIGHT)

    def testStratOneRedHi(self):
        """test stratOne function for red player, high strength"""

        test = rhoBotMk4.stratOne([0, 50, 25, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.RIGHT)
        test = rhoBotMk4.stratOne([0, 0, 25, 25, 25], 0, "red", [])
        self.assertEqual(test, GameBoard.Action.LEFT)

    def testStratOneRedJump(self):
        """test stratOne function for red player, jump strength"""

        test = rhoBotMk4.stratOne([50, 48, 21, 25, 25], 0, "red", [51])
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)
        test = rhoBotMk4.stratOne([53, 48, 21, 25, 25], 0, "red", [51])
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)
        test = rhoBotMk4.stratOne([54, 48, 25, 25, 25], 0, "red", [51])
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)

        test = rhoBotMk4.stratOne([50, 1, 21, 25, 25], 0, "red", [98])
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)
        test = rhoBotMk4.stratOne([53, 1, 21, 25, 25], 0, "red", [98])
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)
        test = rhoBotMk4.stratOne([54, 1, 25, 25, 25], 0, "red", [98])
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)

    def testStratOneBlueJump(self):
        """test stratOne function for red player, jump strength"""

        test = rhoBotMk4.stratOne([52, 51, 17, 74, 25], 0, "blue", [48])
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)
        test = rhoBotMk4.stratOne([53, 51, 21, 74, 25], 0, "blue", [48])
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)
        test = rhoBotMk4.stratOne([54, 51, 25, 74, 25], 0, "blue", [48])
        self.assertEqual(test, GameBoard.Action.JUMP_LEFT)

        test = rhoBotMk4.stratOne([52, 98, 17, 74, 25], 0, "blue", [1])
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)
        test = rhoBotMk4.stratOne([53, 98, 21, 74, 25], 0, "blue", [1])
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)
        test = rhoBotMk4.stratOne([54, 98, 25, 74, 25], 0, "blue", [1])
        self.assertEqual(test, GameBoard.Action.JUMP_RIGHT)

    def testStratOneBlueEven(self):
        """test stratOne function for blue player, even turn"""

        test = rhoBotMk4.stratOne([0, 1, 1, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.stratOne([0, 74, 1, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.stratOne([0, 72, 1, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.STAY)

    def testStratOneBlueOdd(self):
        """test stratOne function for blue player, odd turn"""

        test = rhoBotMk4.stratOne([1, 1, 1, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.RIGHT)

        test = rhoBotMk4.stratOne([1, 74, 1, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.SPLIT)

        test = rhoBotMk4.stratOne([1, 72, 1, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.LEFT)

    def testStratOneBlueHi(self):
        """test stratOne function for blue player, high strength"""

        test = rhoBotMk4.stratOne([0, 50, 25, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.LEFT)
        test = rhoBotMk4.stratOne([0, 0, 25, 74, 25], 0, "blue", [])
        self.assertEqual(test, GameBoard.Action.RIGHT)

    def testEarlyMoves(self):
        """test the getEarlyMoves function"""
        test = rhoBotMk4.getEarlyMove([0, 0, 1, 74, 25], 0)
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.getEarlyMove([0, 0, 25, 74, 25], 0)
        self.assertEqual(test, GameBoard.Action.SPLIT)

        test = rhoBotMk4.getEarlyMove([0, 0, 2, 74, 25], 0)
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.getEarlyMove([0, 0, 5, 74, 25], 0.5)
        self.assertEqual(test, GameBoard.Action.SPLIT)

    def testStratOneSetup(self):
        """test the stratOnesetup function"""

        test = rhoBotMk4.stratOneSetup([0, 0, 0, 25,25], GameBoard.Colour.RED, [])
        self.assertEqual(test, GameBoard.Action.STAY)

        test = rhoBotMk4.stratOneSetup([0, 0, 0, 74, 25], GameBoard.Colour.BLUE, [])
        self.assertEqual(test, GameBoard.Action.STAY)
