"""Test suite for mammothBot.py"""

import GameBoard      # Information on game board properties.
import mammothBot     # The Bot being tested.
import unittest.mock

import BotTestCase


class mammothBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = mammothBot.MammothBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, mammothBot.getBot())
        
    def testInit(self):
        self.assertTrue(hasattr(self.bot, 'closest_growth_tile'))
        self.assertTrue(hasattr(self.bot, 'n_growth_tiles'))
        self.assertTrue(hasattr(self.bot, 'jumpRoutineProgress'))
        
    def testUseUnusedPieces(self):
        moves = [[1, str(GameBoard.Action.STAY), 1],
                 [2, str(GameBoard.Action.JUMP_RIGHT), 10]]
        pieces = [2, 3]
        remaining_moves = [3, str(GameBoard.Action.STAY), 1]
        all_moves = moves[:]
        all_moves.append(remaining_moves)
        self.assertEqual(mammothBot.useUnusedPieces(moves, pieces), all_moves)
        
    def testDoJumpRoutineAtProgress0(self):
        self.bot.centre = 25
        self.bot.jumpRoutineProgress = 0

        pieces = [25]
        tile = 40

        moves = self.bot.doJumpRoutine(pieces, tile)
        exp_moves = [[25, str(GameBoard.Action.SPLIT), 1]]
        
        self.assertEqual(moves, exp_moves)
        self.assertEqual(self.bot.jumpRoutineProgress, 1)
        
    def testDoJumpRoutineAtProgress1Right(self):
        self.bot.centre = 25
        self.bot.jumpRoutineProgress = 1
        
        pieces = [24, 25, 26]
        tile = 40
        
        moves = self.bot.doJumpRoutine(pieces, tile)
        exp_moves = [[25, str(GameBoard.Action.STAY), 1],
                     [24, str(GameBoard.Action.JUMP_RIGHT), 2],
                     [26, str(GameBoard.Action.STAY), 1]]
        
        self.assertEqual(moves, exp_moves)
        self.assertEqual(self.bot.jumpRoutineProgress, 2)
        
    def testDoJumpRoutineAtProgress2Right(self):
        self.bot.centre = 25
        self.bot.jumpRoutineProgress = 2
        
        pieces = [25, 26]
        tile = 40
        
        moves = self.bot.doJumpRoutine(pieces, tile)
        exp_moves = [[25, str(GameBoard.Action.STAY), 1],
                     [26, str(GameBoard.Action.JUMP_RIGHT), 14]]
        
        self.assertEqual(moves, exp_moves)
        self.assertEqual(self.bot.jumpRoutineProgress, None)
        
    def testDoJumpRoutineAtProgress1Left(self):
        self.bot.centre = 74
        self.bot.jumpRoutineProgress = 1
        
        pieces = [73, 74, 75]
        tile = 59
        
        moves = self.bot.doJumpRoutine(pieces, tile)
        exp_moves = [[74, str(GameBoard.Action.STAY), 1],
                     [73, str(GameBoard.Action.STAY), 1],
                     [75, str(GameBoard.Action.JUMP_LEFT), 2]]
        
        self.assertEqual(moves, exp_moves)
        self.assertEqual(self.bot.jumpRoutineProgress, 2)
        
    def testDoJumpRoutineAtProgress2Left(self):
        self.bot.centre = 74
        self.bot.jumpRoutineProgress = 2
        
        pieces = [73, 74]
        tile = 59
        
        moves = self.bot.doJumpRoutine(pieces, tile)
        exp_moves = [[74, str(GameBoard.Action.STAY), 1],
                     [73, str(GameBoard.Action.JUMP_LEFT), 14]]
        
        self.assertEqual(moves, exp_moves)
        self.assertEqual(self.bot.jumpRoutineProgress, None)
        
    def testGetSignature(self):
        with self.subTest('Checking debug signature'):
            debug = "c68cc1563d127369c3875649a11f912388cb0b5785d78383b9768b6c0e1fead3"
            sig = self.bot.getSignature('debug')
            self.assertEqual(debug, sig)
        with self.subTest('Checking database signature'):
            db = "03451213f18c74076dbc32b1fd789a8ec957ad2d0767dfec6177c53def69f925"
            sig = self.bot.getSignature('database')
            self.assertEqual(db, sig)
        with self.subTest('Check default signature'):
            default = "MAMMOTHBOT"
            sig = self.bot.getSignature('default')
            self.assertEqual(default, sig)
        
    
class controlTileTestCase(BotTestCase.BotTestCase):
    
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = mammothBot.MammothBot()
        self.bot.setup(self.getGameConfig())
    
    def testPreJumpRoutineWithOnePiece(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 10
        
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = [40]
        
        self.bot.centre = 40
        moves = self.bot.controlTile(50)
        exp_moves = [[40, str(GameBoard.Action.STAY), 1]]
        
        self.assertEqual(moves, exp_moves)
        
    def testPreJumpRoutineWithManyPieces(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 10
        
        pieces = [38, 39, 40, 41, 42]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.doJumpRoutine = unittest.mock.MagicMock(name='doJumpRoutine')
        self.bot.doJumpRoutine.return_value = 'Jump Moves'
        
        self.bot.centre = 40
        moves = self.bot.controlTile(50)
        exp_moves = 'Jump Moves'
        
        self.assertEqual(moves, exp_moves)
        self.assertEqual(self.bot.jumpRoutineProgress, 0)
        self.bot.doJumpRoutine.assert_called_once_with([39, 40, 41], 50)
        
    def testCentreOnGrowthTile(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 1
        
        pieces = [38, 39, 40, 41, 42]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.centre = 40
        moves = self.bot.controlTile(50)
        exp_moves = [[40, str(GameBoard.Action.STAY), 1]]
        self.assertEqual(moves, exp_moves)
        
    def testNoNbhrsToCentreOneStrength(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 1
        
        pieces = [28, 30, 32]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.centre = 30
        moves = self.bot.controlTile(40)
        exp_moves = [[30, str(GameBoard.Action.STAY), 1]]
        self.assertEqual(moves, exp_moves)
        
    def testNoNbhrsToCentreThreeStrength(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 3
        
        pieces = [28, 30, 32]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.centre = 30
        moves = self.bot.controlTile(40)
        exp_moves = [[30, str(GameBoard.Action.SPLIT), 1]]
        self.assertEqual(moves, exp_moves)
        
    def testNbhrsWithCombinedStrengthToJump(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 3
        
        pieces = [29, 30, 31]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.centre = 30
        moves = self.bot.controlTile(40)
        exp_moves = [[29, str(GameBoard.Action.RIGHT), 1],
                     [30, str(GameBoard.Action.STAY), 1],
                     [31, str(GameBoard.Action.LEFT), 1]]
        self.assertEqual(moves, exp_moves)
        
    def testDefaultReturnIsNone(self):
        # Set up the functions to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 1
        
        pieces = [29, 30, 31]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.centre = 30
        moves = self.bot.controlTile(40)
        self.assertEqual(moves, None)
        
class endgameStrategyTestCase(BotTestCase.BotTestCase):
    
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = mammothBot.MammothBot()
        self.bot.setup(self.getGameConfig())
        
    def testSingleStrengthPiecesStay(self):
        # Set up the function to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 1
        
        self.bot.centre = 50
        currTurn = 1
        
        with self.subTest('Test growth tiles stay on single strength'):
            moves = self.bot.endgameStrategy([40], currTurn)
            exp_moves = [[40, str(GameBoard.Action.STAY), 1]]
            self.assertEqual(moves, exp_moves)
        with self.subTest('Test outer pieces stay on odd turned moves'):
            moves = self.bot.endgameStrategy([60], currTurn)
            exp_moves = [[60, str(GameBoard.Action.STAY), 1]]
            self.assertEqual(moves, exp_moves)
        with self.subTest('Test intermediate pieces stay on one strength'):
            moves = self.bot.endgameStrategy([58], currTurn)
            exp_moves = [[58, str(GameBoard.Action.STAY), 1]]
            self.assertEqual(moves, exp_moves)
        
    def testStrongPiecesMove(self):
        # Set up the function to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 3
        
        self.bot.centre = 50
        currTurn = 2
        
        with self.subTest('Test growth tiles split on odd > 1 strength'):
            moves = self.bot.endgameStrategy([40], currTurn)
            exp_moves = [[40, str(GameBoard.Action.SPLIT), 1]]
            self.assertEqual(moves, exp_moves)
        with self.subTest('Test outer pieces move out on even turned moves'):
            moves = self.bot.endgameStrategy([60], currTurn)
            exp_moves = [[60, str(GameBoard.Action.RIGHT), 1]]
            self.assertEqual(moves, exp_moves)
        with self.subTest('Test intermediate pieces jump when they can'):
            moves = self.bot.endgameStrategy([58], currTurn)
            exp_moves = [[58, str(GameBoard.Action.JUMP_RIGHT), 2]]
            self.assertEqual(moves, exp_moves)
            
    def testMiscMoves(self):
        # Set up the function to mock
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 2
        
        self.bot.centre = 50
        currTurn = 1
        
        with self.subTest('Test growth tiles stay on even strength'):
            moves = self.bot.endgameStrategy([40], currTurn)
            exp_moves = [[40, str(GameBoard.Action.STAY), 1]]
            self.assertEqual(moves, exp_moves)
        with self.subTest('Test left intermediate pieces jump'):
            moves = self.bot.endgameStrategy([41], currTurn)
            exp_moves = [[41, str(GameBoard.Action.JUMP_LEFT), 2]]
            self.assertEqual(moves, exp_moves)
            
            
class testGenerateMoves(BotTestCase.BotTestCase):
    
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = mammothBot.MammothBot()
        self.bot.setup(self.getGameConfig())
        
    def testOpeningMove(self):
        pieces = [25]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.get_closest_growth_tile = unittest.mock.MagicMock(name='get_closest_growthTile')
        self.bot.get_closest_growth_tile.return_value = 35
        
        self.bot.controlTile = unittest.mock.MagicMock(name='controlTile')
        self.bot.controlTile.return_value = 'return 1'
        
        mammothBot.useUnusedPieces = unittest.mock.MagicMock(name='useUnusedPieces')
        mammothBot.useUnusedPieces.return_value = 'return 2'
        
        currTurn = 1
        maxTurns = 1000
        moves = self.bot.generateMoves(currTurn, maxTurns)
        
        self.assertEqual(self.bot.centre, 25)
        self.assertEqual(self.bot.closest_growth_tile, 35)
        self.bot.controlTile.assert_called_once_with(35)
        mammothBot.useUnusedPieces.assert_called_once_with('return 1', [25])
        self.assertEqual(moves, 'return 2')
        
    def testWhenOwningAGrowthSquare(self):
        pieces = [40]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.closest_growth_tile = 40
        self.bot.controlTile = unittest.mock.MagicMock(name='controlTile')
        self.bot.controlTile.return_value = 'return 1'
        mammothBot.useUnusedPieces = unittest.mock.MagicMock(name='useUnusedPieces')
        mammothBot.useUnusedPieces.return_value = 'return 2'
        currTurn = 10
        maxTurns = 1000
        moves = self.bot.generateMoves(currTurn, maxTurns)
        
        self.assertEqual(self.bot.closest_growth_tile, 59)
        self.assertEqual(self.bot.centre, 40)
        self.bot.controlTile.assert_called_once_with(59)
        mammothBot.useUnusedPieces.assert_called_once_with('return 1', [40])
        self.assertEqual(moves, 'return 2')
        
    def testWhenOwningBothGrowthSquares(self):
        pieces = [40, 59]
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = pieces
        
        self.bot.closest_growth_tile = 59
        self.bot.n_growth_tiles = 1
        self.bot.endgameStrategy = unittest.mock.MagicMock(name='controlTile')
        self.bot.endgameStrategy.return_value = 'return 1'
        mammothBot.useUnusedPieces = unittest.mock.MagicMock(name='useUnusedPieces')
        mammothBot.useUnusedPieces.return_value = 'return 2'
        
        currTurn = 20
        maxTurns = 1000
        moves = self.bot.generateMoves(currTurn, maxTurns)
        
        self.assertEqual(self.bot.closest_growth_tile, 40)
        self.assertEqual(self.bot.centre, 49.5)
        self.bot.endgameStrategy.assert_called_once_with([40, 59], currTurn)
        mammothBot.useUnusedPieces.assert_called_once_with('return 1', [40, 59])
        self.assertEqual(moves, 'return 2')
        
        
