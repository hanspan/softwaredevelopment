
import os
import unittest.mock  # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import dumbBot        # The Bot being tested.

import BotTestCase


class dumbBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = dumbBot.DumbBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, dumbBot.getBot())
    
    def testSumSquares(self):
        # check real square function first
        self.assertEqual(self.bot.sumSquares(5), 55)

        # create mock 'square' function for use by bot
        squares_example = {1:1, 2:4, 3:9, 4:16, 5:25}
        def get_square(i): return squares_example[i]
        self.bot.square = unittest.mock.MagicMock(name='square')
        self.bot.square.side_effect = get_square

        # check that sumSquares returns the correct value for first 5
        self.assertEqual(self.bot.sumSquares(5), 55)
        # and correctly calls the function five times
        self.assertEqual(self.bot.square.call_count, 5)
    
    def testDistances(self):
        # this function should give the 'shortest' distance from a to b
        # not the test board is 100 squares
        self.assertEqual(self.bot.distance(0, 0), (0, 1))
        self.assertEqual(self.bot.distance(0, 99), (1, -1))
        self.assertEqual(self.bot.distance(99, 0), (1, 1))
        self.assertEqual(self.bot.distance(50, 60), (10, 1))
    
    def testFindingFunctions(self):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForDumbBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        self.bot.makeMoves(state.encode("utf-8"))

        self.assertEqual(self.bot.getNearestOpponent(4)[0], 99)
        self.assertEqual(self.bot.getNearestMine(4)[0], 4)

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(0, results[0][2])

    def testKnownSplitMove(self):
        # Testing that the expected output comes from a move that
        # should include a split.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForRandomBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(0, results[0][2])
    
    def testChosenMove(self):
        # check some given cases
        expected, move, arg = self.bot.chooseMove([0]*100, 70)
        self.assertEqual(GameBoard.Action.STAY, move)

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = dumbBot.DumbBot.getSignature('debug')
        self.assertTrue(sig.endswith("f127b84"))
        sig = dumbBot.DumbBot.getSignature('database')
        self.assertTrue(sig.endswith("bdbb391"))
        sig = dumbBot.DumbBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
