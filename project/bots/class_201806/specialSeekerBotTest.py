import os
import json

import BotTestCase
import specialSeekerBot
from unittest import mock


class specialSeekerBotTest(BotTestCase.BotTestCase):
    """
    Some very simple tests for a bot that only has one goal
    """

    def setUp(self):
        """Common setup for the tests in this class."""
        self.bot = specialSeekerBot.SpecialSeekerBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        """Make sure a bot is returned from the getBot() function."""
        self.assertNotEqual(None, specialSeekerBot.getBot())

    def testIsolatedMoveOdd(self):
        """Test that odd isolated moves are stay"""
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(
            testDataDir, 'specialSeekerBotTest1.txt')
        with open(boardStateFile, 'r') as stateFile:
            stateturn = stateFile.read()
        resultsRawturn = self.bot.makeMoves(stateturn.encode("utf-8"))
        results = json.loads(resultsRawturn)
        self.assertEqual(results, [[25, 'Stay', 1]])

    def testIsolatedMoveEven(self):
        """Test that even isolated moves are split"""
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(
            testDataDir, 'specialSeekerBotTest2.txt')
        with open(boardStateFile, 'r') as stateFile:
            stateturn = stateFile.read()
        resultsRawturn = self.bot.makeMoves(stateturn.encode("utf-8"))
        results = json.loads(resultsRawturn)
        self.assertEqual(results, [[25, 'Split', 1]])

    def testGrowTileMove(self):
        """ Test that if the piece is on a growth tile, it splits"""
        self.bot.board.findMyPieces = mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.return_value = [40]
        self.bot.board.growthTiles = [40]
        moves = self.bot.generateMoves(1, 10)
        self.assertEqual(moves, [[40, 'Split', 1]])

    def testDefendTileMove(self):
        """ Test that if the piece is on a defence tile, it grows
         if it doesn't have max strength, splits if it does"""
        self.bot.board.findMyPieces = mock.MagicMock()
        self.bot.board.getPieceStrength = mock.MagicMock()
        self.bot.board.findMyPieces.return_value = [40]
        self.bot.board.getPieceStrength.return_value = 30
        self.bot.board.defenceTiles = [40]
        self.bot.board.growthTiles = []
        with self.subTest("Test when at max strength"):
            self.bot.board.maxTileStrength = 30
            moves = self.bot.generateMoves(1, 10)
            self.assertEqual(moves, [[40, 'Split', 1]])
        with self.subTest("Test when not at max strength"):
            self.bot.board.maxTileStrength = 50
            moves = self.bot.generateMoves(1, 10)
            self.assertEqual(moves, [[40, 'Stay', 1]])

    def testStrategicJumpTester(self):
        self.bot.board.growthTiles = [10]
        self.bot.board.defenceTiles = [15]
        self.bot.board.maxTileStrength = 20
        self.bot.board.getPieceStrength = mock.MagicMock()
        self.bot.board.getPieceStrength.return_value = 20
        jumps = self.bot.testForStrategicJump(12)
        self.assertEqual(jumps, ([-2], [3]))

    def testGrowOverDefend(self):
        """ Tests that if can both move to a defence tile and a 
         growth tile, then it chooses the growth tile"""
        self.bot.board.findMyPieces = mock.MagicMock()
        self.bot.board.getPieceStrength = mock.MagicMock()
        self.bot.board.findMyPieces.return_value = [45]
        self.bot.board.maxTileStrength = 20
        self.bot.board.getPieceStrength.return_value = 20
        self.bot.board.defenceTiles = [40]
        self.bot.board.growthTiles = [50]
        moves = self.bot.generateMoves(1, 10)
        self.assertEqual(moves, [[45, 'Jump_Right', 5]])

    def testDefendNoGrowth(self):
        """ Tests that if can jump to defend and not growth,
         then it will jump to a defend tile"""
        self.bot.board.findMyPieces = mock.MagicMock()
        self.bot.board.getPieceStrength = mock.MagicMock()
        self.bot.board.findMyPieces.return_value = [45]
        self.bot.board.maxTileStrength = 20
        self.bot.board.getPieceStrength.return_value = 20
        self.bot.board.defenceTiles = [40]
        self.bot.board.growthTiles = [90]
        moves = self.bot.generateMoves(1, 10)
        self.assertEqual(moves, [[45, 'Jump_Left', 5]])

    def testGrowthNoDefend(self):
        """ Tests that if can jump to growth and not defend,
         then it will jump to a growth tile"""
        self.bot.board.findMyPieces = mock.MagicMock()
        self.bot.board.getPieceStrength = mock.MagicMock()
        self.bot.board.findMyPieces.return_value = [45]
        self.bot.board.maxTileStrength = 20
        self.bot.board.getPieceStrength.return_value = 20
        self.bot.board.defenceTiles = [90]
        self.bot.board.growthTiles = [40]
        moves = self.bot.generateMoves(1, 10)
        self.assertEqual(moves, [[45, 'Jump_Left', 5]])

    def testDebugSignature(self):
        """ Check that the expected signature is returned."""
        sig = self.bot.getSignature('debug')
        self.assertTrue(sig.startswith("e81e676a7b0b8"))

    def testDatabaseSignature(self):
        """ Check that the expected signature is returned."""
        sig = self.bot.getSignature('database')
        self.assertTrue(sig.startswith("b3079ba6258098"))

    def testWrongSignature(self):
        """ Check that the expected signature is returned."""
        sig = self.bot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
