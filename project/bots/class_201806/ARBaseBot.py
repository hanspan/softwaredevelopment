"""
Some basic functionality for other bots to use
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
from numpy import diff

class ARBaseBot(BotBase.BotBase):
    """Adds some extra utility functions"""

    def newTurn(self):
        self.strengthsAfterMove = [self.board.getPieceStrength(i) for i in range(self.board.boardSize)]
    
    def forecastStrengthAfterMove(self, location, move, arg=1):
        """
        Estimate the strength distribution of your bot after the move. Returns true if the move will push any tile over its maximum strength.
        """
        move_string = str(move)
        if move_string == str(GameBoard.Action.LEFT):
            move_string = str(GameBoard.Action.JUMP_LEFT)
            arg = 1
        if move_string == str(GameBoard.Action.RIGHT):
            move_string = str(GameBoard.Action.JUMP_RIGHT)
            arg = 1
        if move_string == str(GameBoard.Action.JUMP_LEFT):
            move_string = str(GameBoard.Action.JUMP_RIGHT)
            arg *= -1
        if move_string == str(GameBoard.Action.JUMP_RIGHT):
            self.strengthsAfterMove[(location + arg) % self.board.boardSize] += self.board.getPieceStrength(location) - (abs(arg) - 1)
            self.strengthsAfterMove[location] -= self.board.getPieceStrength(location)
            return max(self.strengthsAfterMove[(location + arg) % self.board.boardSize] - self.board.maxTileStrength, 0)
        if move_string == str(GameBoard.Action.STAY):
            self.strengthsAfterMove[location] += 1
            return max(self.strengthsAfterMove[location] - self.board.maxTileStrength, 0)
        if move_string == str(GameBoard.Action.SPLIT):
            self.strengthsAfterMove[location] -= self.board.getPieceStrength(location)
            if self.board.getPieceStrength(location) % 2:
                self.strengthsAfterMove[location] += 1
            self.strengthsAfterMove[(location + 1) % self.board.boardSize] += self.board.getPieceStrength(location)//2
            self.strengthsAfterMove[(location - 1) % self.board.boardSize] += self.board.getPieceStrength(location)//2
            return max(self.strengthsAfterMove[(location - 1) % self.board.boardSize] - self.board.maxTileStrength, 0) + max(self.strengthsAfterMove[(location + 1) % self.board.boardSize] - self.board.maxTileStrength, 0)


    def jumpToSquare(self, current_location, target_location):
        """
        Compiles a jump move and argument to jump to the specified square
        """
        difference = target_location - current_location
        if difference > self.board.boardSize // 2:
            difference -= self.board.boardSize
        if difference < - self.board.boardSize // 2:
            difference += self.board.boardSize
        
        if difference < 0:
            return GameBoard.Action.JUMP_LEFT, - difference
        else:
            return GameBoard.Action.JUMP_RIGHT, difference

    def moveTowardsEnemy(self, location, length):
        """
        Returns the square that is 'length' squares towards the frontline
        """
        if self.distanceToEnemy(location) > self.distanceToEnemy((location + 1) % self.board.boardSize):
            return (location + length) % self.board.boardSize
        else:
            return (location - length) % self.board.boardSize

    def distanceToEnemy(self, location):
        """
        The distance to the nearest enemy piece from the specified location
        """
        pieces = self.board.findOpponentPieces()
        return min([self.boardDistance(piece, location) for piece in pieces])
            
    def nextToFreeSquare(self, location):
        """
        Test whether a neighbouring square is free
        """
        return self.board.getPieceStrength((location - 1) % self.board.boardSize) == 0 \
               or self.board.getPieceStrength((location + 1) % self.board.boardSize) == 0

    def surroundedByOpponents(self, location):
        """
        Test whether this piece has opposing pieces on both neighbouring squares
        """
        pieces = self.board.findOpponentPieces()
        return (location - 1) % self.board.boardSize in pieces and (location + 1) % self.board.boardSize in pieces

    def surroundedByAllies(self, location):
        """
        Test whether this piece has allied pieces on both neighbouring squares
        """
        pieces = self.board.findMyPieces()
        return (location - 1) % self.board.boardSize in pieces and (location + 1) % self.board.boardSize in pieces

    def hasOpponentNeighbours(self, location):
        """
        Test whether this piece has an opposing piece as a neighbour
        """
        pieces = self.board.findOpponentPieces()
        return (location - 1) % self.board.boardSize in pieces or (location + 1) % self.board.boardSize in pieces

    def hasAlliedNeighbours(self, location):
        """
        Test whether this piece has an allied piece as a neighbour
        """
        pieces = self.board.findMyPieces()
        return (location - 1) % self.board.boardSize in pieces or (location + 1) % self.board.boardSize in pieces

    def boardDistance(self, location_1, location_2):
        """
        Calculate the shortest distance between two points
        """
        absolute_distance = abs(location_1 - location_2)
        return min(absolute_distance, self.board.boardSize - absolute_distance)

    def calculateCollateralDamage(self, damage, location, pieces, include_battle_tile):
        """
        Calculate the collateral damage dealt to pices in 'pieces' if there were to be a battle at 'location' with damage 'damage'
        """
        damage_list = [damage // 5, damage // 2, damage if include_battle_tile else 0, damage // 2, damage // 5]
        
        total_damage = 0
        for i in range(5):
            if ((location - 2 + i) % self.board.boardSize) in pieces:
                total_damage += min(damage_list[i], self.board.getPieceStrength((location - 2 + i) % self.board.boardSize))
        
        return total_damage
        
    def intelligentAttack(self, location, attack_range, require_survival):
        """
        Choose a location to jump to that:
        - Contains an enemy piece
        Maximising:
        - The damage dealt to your opponent after that jump (including collateral), minus the damage dealt to you (including collateral)
        
        Parameter location is the location from which the attack will be launched
        Parameter attack_range is the range to scan for the optimum attack
        Paremeter require_survival specifies whether the bpot must survive the jump
        """
        location_strength = self.board.getPieceStrength(location)
        opponent_pieces = self.board.findOpponentPieces()
        my_pieces = self.board.findMyPieces()        
        
        possible_move_dict = {} # this dictionary will have keys as possible jump offsets, and values as the strength that would remain after doing that move
        
        for i in range(location - attack_range, location + attack_range):
            if i % self.board.boardSize in opponent_pieces:
                opponent_strength = self.board.getPieceStrength(i % self.board.boardSize)
                jump_penalty = max(abs(i - location) - 1, 0)
                my_strength_after_jump = location_strength - jump_penalty
                damage = min(my_strength_after_jump, opponent_strength)
                
                collateral_damage_to_me = self.calculateCollateralDamage(damage, i % self.board.boardSize, my_pieces, not require_survival)
                collateral_damage_to_opponent = self.calculateCollateralDamage(damage, i % self.board.boardSize, opponent_pieces, not require_survival)
                                
                if my_strength_after_jump - damage > 0 or not require_survival: # check that I survive
                    possible_move_dict[i - location] = collateral_damage_to_opponent - collateral_damage_to_me # don't need to include damage from the actual battle as this is equal for both players
                        
        if possible_move_dict:
            jump_index = max(possible_move_dict, key=lambda i: possible_move_dict[i])
        else:
            return
        
        if jump_index < 0:
            return GameBoard.Action.JUMP_LEFT, - jump_index
        else:
            return GameBoard.Action.JUMP_RIGHT, jump_index
    
    def intelligentReinforce(self, location, threshold, max_jump_length):
        """
        Choose a location to reinforce that has strength less than 'threshold', 
        is within 'max_jump_length' of 'location' and prioritises squares
        closest to the enemy.
        """
        squares_to_test = [self.moveTowardsEnemy(location, i) for i in reversed(range(-min(max_jump_length, self.distanceToEnemy(location)), min(max_jump_length, self.distanceToEnemy(location))))]
        
        for square in squares_to_test:
            if self.strengthsAfterMove[square] < threshold:
                if self.board.getPieceStrength(location) >= self.boardDistance(location, square):
                    return self.jumpToSquare(location, square)
        
        
        
        
        