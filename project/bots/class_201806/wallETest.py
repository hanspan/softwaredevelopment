import os
import json

import BotTestCase
import wallE
import GameBoard 
import unittest.mock

class wallE_jmBotTest(BotTestCase.BotTestCase):
    """
    Some very simple tests for a very simple bot.
    """

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = wallE.JamesBot()
        self.bot.setup(self.getGameConfig())

    def getGameConfigAlt(self,name):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.

        configFilePath = os.path.join(self.getTestDataDir(), name)
        with open(configFilePath, 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, wallE.getBot())

    def testFirstMove(self):
        # Test that the first move is a Stay
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        strategic_locations=self.bot.board.defenceTiles+self.bot.board.growthTiles
        if 25 in strategic_locations:
            self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        else:
            self.assertIn(results[0][1], [str(GameBoard.Action.LEFT), str(GameBoard.Action.RIGHT)])
            
        self.assertEqual(1, results[0][2])
        
    def testsortLocations(self):
        # Test that sortLocations works
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState2_wallE.txt')
        #board has locations [1, 9, 10, 25, 89] with 25 strength at 89
        
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        #pieces=self.bot.board.findMyPieces()
        #print(pieces)
        strategic_locations=self.bot.board.defenceTiles+self.bot.board.growthTiles
        pieces=self.bot.board.findMyPieces()
        
        self.assertEqual(len(self.bot.sortLocations(strategic_locations,pieces)), len(pieces))
        
    def testcheckStrategiclocations(self):
        # Test that checkStrategiclocations works
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState2_wallE.txt')
        #board has locations [1, 9, 10, 25, 89] with 25 strength at 89
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        strategic_locations=self.bot.board.defenceTiles+self.bot.board.growthTiles
        len_strat=len(strategic_locations)
        #print(strategic_locations)
        pieces=self.bot.board.findMyPieces()
        #print(pieces)
        locations_sorted=self.bot.sortLocations(strategic_locations,pieces)
        checked_locations=[0]*len(pieces)
        self.bot.checkStrategicLocations(checked_locations,strategic_locations,locations_sorted,[])
        
        self.assertLess(len(strategic_locations), len_strat)
        
        
    def testmoreMoves(self):
        # Test that we move towards strategic locations
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState3_wallE.txt')
        #board has locations [8,12,25] with strength 1
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(3, len(results))
        self.assertEqual(3, len(results[0]))
        strategic_locations=self.bot.board.defenceTiles+self.bot.board.growthTiles
        for result in results:          
            if result[0] in strategic_locations:
                self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
            else:
                self.assertIn(results[0][1], [str(GameBoard.Action.LEFT), str(GameBoard.Action.RIGHT)])
    
    def testNoStrategicLocations(self):
        
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState2_wallE.txt')
        #board has locations [1, 9, 10, 25, 89] with 25 strength at 89
        strategic_locations=[] 
        self.bot.setup(self.getGameConfigAlt('testConfigAlt_wallE_1.cfg'))
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        for result in results:          
            if result[0]==89:
                self.assertEqual(str(GameBoard.Action.SPLIT), result[1])
        
            
    def testwrapAround(self):
       # Test that we move towards strategic locations and wrap around properly
        testDataDir = self.getTestDataDir()
       
        boardStateFile = os.path.join(testDataDir, 'testBoardState2_wallE.txt')
        #board has locations [1, 9, 10, 25, 89] with 25 strength at 89 
        #strategic_locations=96
        self.bot.setup(self.getGameConfigAlt('testConfigAlt_wallE_2.cfg'))
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        for result in results:          
            if result[0]==1:
                self.assertEqual(str(GameBoard.Action.LEFT), result[1])

        #strategic_locations=5
        self.bot.setup(self.getGameConfigAlt('testConfigAlt_wallE_3.cfg'))
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        for result in results:          
            if result[0]==89:
                self.assertEqual(str(GameBoard.Action.RIGHT), result[1])
        
        
        
        
    #def testSumSquares(self):
    #    self.bot.square= unittest.mock.MagicMock(name='square') 
    #    
    #    squares_dict={1:1,2:4,3:9,4:16}
    #    def side_eff(arg):
    #        return squares_dict[arg]
    #    
    #    self.bot.square.side_effect=side_eff
    #    
    #    self.assertEqual(self.bot.sum_squares(4),30)
    #    self.assertEqual

    def testDebugSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature('debug')
        self.assertTrue(sig.startswith("b07a25676f4660"))

    def testDatabaseSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature('database')
        self.assertTrue(sig.startswith("3721f59fc125c84d13a"))

    def testWrongSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
