"""
rhoBotMk3
"""

import random      # rng
import BotBase     # basic bot stuff
import GameBoard   # game utilities


def getBot():
    """Returns an instance of RhoBotMk3"""

    return RhoBotMk3()


class RhoBotMk3(BotBase.BotBase):
    """RhoBotMk3 base class"""

    def __init__(self):
        """init function for RhoBot"""

        return

    def generateMoves(self, currTurn, maxTurns):
        """
            Get the moves for each active tile
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            strength = self.board.getPieceStrength(location)

            if currTurn < 10:
                rng = random.random()
                move = getEarlyMove(currTurn, location, strength, rng)
                arg = 1
                moves.append([location, str(move), arg])

            else:
                move = stratOneSetup(currTurn, location, strength,
                                     self.board.player)
                arg = 3
                moves.append([location, str(move), arg])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        Hash of 'rhoBotMk3' and the answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("7937b28b69e236aae75fab76835cb6"
                    "f1ccfacbe4a00541a8ce5162f248560e79")
        elif challenge == "database":
            return ("d8f19ca93651d3acaff9c76eb7044664f"
                    "0f95fd316400df96236fee1d4dd36dc")

        return "Default bot signature"


def stratOneSetup(currTurn, location, strength, currPlayer):
    """Prepare the current player and rng for the stratOne function"""
    rng = random.random()
    if currPlayer == GameBoard.Colour("Red"):
        col = "red"
    else:
        col = "blue"
    move = stratOne(currTurn, location, strength, rng, col)
    return move


def getEarlyMove(currTurn, location, strength, rng):
    """Generate early moves"""
    if strength == 1:
        move = GameBoard.Action.STAY
    elif strength == 25:
        move = GameBoard.Action.SPLIT
    else:
        if rng < 0.1:
            move = GameBoard.Action.STAY
        else:
            move = GameBoard.Action.SPLIT
    return move


def stratOne(currTurn, location, strength, rng, col):
    """calls the main bot strategy"""

    if col == "red":
        start1, start2 = 25, 26
        jump1, jump2 = 48, 1
        jump = [GameBoard.Action.JUMP_RIGHT, GameBoard.Action.JUMP_LEFT]
        left = GameBoard.Action.LEFT
        right = GameBoard.Action.RIGHT
    else:
        start1, start2 = 74, 75
        jump1, jump2 = 51, 98
        jump = [GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT]
        left = GameBoard.Action.RIGHT
        right = GameBoard.Action.LEFT

    if location == jump1 and strength > 20:
        return jump[0]
    if location == jump2 and strength > 20:
        return jump[1]
    if currTurn % 2:
        if location in [start1, start2]:
            move = GameBoard.Action.SPLIT
        elif location <= 74 and location >= 25:
            move = right
        else:
            move = left
    else:
        move = GameBoard.Action.STAY
        if strength == 25 and rng < 0.9:
            if location in range(25, 75):
                move = right
            else:
                move = left
    return move
