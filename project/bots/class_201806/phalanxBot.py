"""
A bot inspired by ancient greek battle tactics.
Will easily defeat 10,000 Persians.
"""

import ARBaseBot  # All of the basic Bot functionality.
import GameBoard  # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PhalanxBot()


class PhalanxBot(ARBaseBot.ARBaseBot):
    """
    This Bot creates a defensive frontline and attacks by jumping
    units over that frontline . It also reinforces any weak squares
    in its internal ranks.
    https://www.youtube.com/watch?v=XR1l01cZQV0
    """

    def handleAttack(self, location, phalanx_depth, attack_range, require_survival):
        """
        If behind the frontline and at max strength, then jump in to attack
        """
        distance_to_enemy = self.distanceToEnemy(location)
        strength = self.board.getPieceStrength(location)
        
        if distance_to_enemy > phalanx_depth and \
                    strength == self.board.maxTileStrength:

                if strength == self.board.maxTileStrength:
                    # Attack
                    move_candidate = self.intelligentAttack(
                        location,
                        attack_range,
                        require_survival
                    )

                    if move_candidate:
                        move = move_candidate[0]
                        arg = move_candidate[1]
                        self.forecastStrengthAfterMove(location, move, arg)
                        return move, arg

        return GameBoard.Action.STAY, 1
    
    def handleSplit(self, location, move, arg, phalanx_depth):
        """
        If you have vacant squares next to you, then split
        """
        distance_to_enemy = self.distanceToEnemy(location)
        strength = self.board.getPieceStrength(location)
        
        if distance_to_enemy > phalanx_depth and \
                move == GameBoard.Action.STAY:
    
            if self.nextToFreeSquare(location):
                if strength > 1 and strength % 2:
                    move = GameBoard.Action.SPLIT
                    self.forecastStrengthAfterMove(location, move)
                    return move, 1
                
        return move, arg

    def handleReinforce(self, location, move, arg, phalanx_depth, strength_before_reinforce, reinforcement_threshold, reinforcement_range):
        """
        Reinforce nearby squares that are weak, prioritising those that are nearer the enemy.
        """
        distance_to_enemy = self.distanceToEnemy(location)
        strength = self.board.getPieceStrength(location)
        
        if distance_to_enemy > phalanx_depth and \
                strength > strength_before_reinforce and \
                move == GameBoard.Action.STAY:
            move_candidate = self.intelligentReinforce(
                location,
                reinforcement_threshold,
                reinforcement_range
            )

            if move_candidate:
                move = move_candidate[0]
                arg = move_candidate[1]
                self.forecastStrengthAfterMove(location, move, arg)
                return move, arg

        return move, arg

    def handleCharge(self, location, move, arg, phalanx_depth):
        """
        Reinforce nearby squares that are weak, prioritising those that are nearer the enemy.
        """
        distance_to_enemy = self.distanceToEnemy(location)
        strength = self.board.getPieceStrength(location)
        
        if strength >= self.board.maxTileStrength / 2 and \
            distance_to_enemy <= phalanx_depth and \
            move == GameBoard.Action.STAY:

            target_move_square = self.moveTowardsEnemy(location, 1)
            if self.board.getPieceStrength(target_move_square) == 0:
                move, arg = self.jumpToSquare(location, target_move_square)
                self.forecastStrengthAfterMove(location, move, arg)
                return move, arg

        return move, arg

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This method will attack if a piece is not in the frontline
        (by jumping over), split into any adjacent vacant squares
        Reinforce any nearby weak squares by jumping into them
        (both towards and away from the enemy), and charge towards the
        enemy if there is a gap between the two forces.
        """

        self.newTurn()

        # provide reinforcements from up to this many squares
        reinforcement_range = 7
        # reinforce squares that are at most this strong
        reinforcement_threshold = 7
        # only reinforce other squares if you are at least this strong
        strength_before_reinforce = 10
        # attack the enemy from at most this many squares away
        attack_range = 8
        # keep the frontline this thick
        phalanx_depth = 5
        # should you only attack the enemy when you'd survive?
        require_survival = False

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            move = GameBoard.Action.STAY
            arg = 1

            move, arg = self.handleAttack(
                location,
                phalanx_depth,
                attack_range,
                require_survival
            )

            move, arg = self.handleSplit(
                location,
                move,
                arg,
                phalanx_depth
            )

            # If you're behind the frontline and above a set strength
            # threshold, then jump to reinforce weak squares
            move, arg = self.handleReinforce(
                location, 
                move,
                arg,
                phalanx_depth, 
                strength_before_reinforce, 
                reinforcement_threshold, 
                reinforcement_range
            )

            # Otherwise, you're within phalanx distance, but you may need
            # to close the gap to the enemy.
            # Only do this if you are strong enough.
            move, arg = self.handleCharge(
                location, 
                move,
                arg,
                phalanx_depth
            )
            

            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'phalanxBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("283bafa7d34563ea0ee97480f1d81a49"
                    "66f8e042f376ef765ecfb779292e9c5c")
        elif challenge == "database":
            return ("914d9d838e5dc9ce53f7b603e5522287"
                    "3f3dd1f7725bd495236daceeafb3a60b")
        else:
            return "Default bot signature"
