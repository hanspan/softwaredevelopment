"""MammothBot is a good bot. It likes to grow big and strong."""
import babyBot
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return MammothBot()


def useUnusedPieces(moves, pieces):
    """For the pieces with an unassigned move, we tell them to stay."""
    moved_pieces = [m[0] for m in moves]
    non_moved_pieces = [loc for loc in pieces if loc not in moved_pieces]
    for loc in non_moved_pieces:
        moves.append([loc, str(GameBoard.Action.STAY), 1])
    return moves


class MammothBot(babyBot.BabyBot):
    """This bot grabs both growth tiles quickly, and then
    grows away from the centre of them."""
    def __init__(self):
        """Initialise the bot."""
        babyBot.BabyBot.__init__(self)
        self.closest_growth_tile = None
        self.n_growth_tiles = 0
        self.jumpRoutineProgress = None

    def doJumpRoutine(self, pieces, tile):
        """This method is to be called towards the end of the controlTile
        moves, in order to leave a piece behind after jumping."""
        if self.jumpRoutineProgress == 0:
            moves = [[self.centre, str(GameBoard.Action.SPLIT), 1]]
            self.jumpRoutineProgress += 1
        elif self.jumpRoutineProgress == 1:
            self.jumpRoutineProgress += 1
            moves = []
            moves.append([self.centre, str(GameBoard.Action.STAY), 1])
            if (self.quickest_direction(self.centre, tile) ==
                    GameBoard.Action.RIGHT):
                moves.append([pieces[0], str(GameBoard.Action.JUMP_RIGHT), 2])
                moves.append([pieces[2], str(GameBoard.Action.STAY), 1])
            else:
                moves.append([pieces[0], str(GameBoard.Action.STAY), 1])
                moves.append([pieces[2], str(GameBoard.Action.JUMP_LEFT), 2])
        else:
            moves = []
            moves.append([self.centre, str(GameBoard.Action.STAY), 1])
            distance = self.tile_distance(self.centre, tile)
            if (self.quickest_direction(self.centre, tile) ==
                    GameBoard.Action.RIGHT):
                moves.append([pieces[1],
                              str(GameBoard.Action.JUMP_RIGHT), distance - 1])
            else:
                moves.append(
                    [pieces[0], str(GameBoard.Action.JUMP_LEFT), distance - 1])
            self.jumpRoutineProgress = None
        return moves

    def controlTile(self, tile):
        """This is a private method to call within generateMoves
        in order to control a new tile quickly, using tiles around self.centre.

        It uses the following algorithm if you don't already
        control a growth square:
        STAY
        STAY
        SPLIT
        [STAY, STAY, STAY] n times for the necessary n
        [RIGHT, STAY, LEFT]
        Subroutine to jump to new tile and leave something behind.

        Otherwise, it stays on the growth square until it has enough
        strength to jump to the new tile.
        """
        pieces = [self.centre - 1, self.centre, self.centre + 1]
        pieces = [piece % self.board.boardSize for piece in pieces]
        pieces = [piece for piece in pieces
                  if piece in self.board.findMyPieces()]
        distance = self.tile_distance(self.centre, tile)
        can_jump = self.board.getPieceStrength(self.centre) >= distance
        if can_jump or self.jumpRoutineProgress is not None:
            if (((self.board.getPieceStrength(self.centre) & 1) == 0)
                    & (len(pieces) == 1)):
                return [[loc, str(GameBoard.Action.STAY), 1] for loc in pieces]
            if self.jumpRoutineProgress is None:
                self.jumpRoutineProgress = 0
            return self.doJumpRoutine(pieces, tile)
        if self.centre in self.board.growthTiles:
            # In this case, we assume that we only have one piece,
            # on a growth tile.
            move = GameBoard.Action.STAY
            return [[self.centre, str(move), 1]]
        if len(pieces) == 1:
            #One piece, not a growth tile.
            if self.board.getPieceStrength(self.centre) == 3:
                move = GameBoard.Action.SPLIT
                moves = [[self.centre, str(move), 1]]
            else:
                move = GameBoard.Action.STAY
                moves = [[self.centre, str(move), 1]]
            return moves
        if (sum([self.board.getPieceStrength(loc) for loc in pieces])
                >= distance - 1):
            moves = []
            moves.append([pieces[0], str(GameBoard.Action.RIGHT), 1])
            moves.append([self.centre, str(GameBoard.Action.STAY), 1])
            moves.append([pieces[2], str(GameBoard.Action.LEFT), 1])
            return moves
        return None

    def _firstMoveSetup(self, piece):
        """Initial setup for strategy."""
        self.centre = piece
        self.closest_growth_tile = self.get_closest_growth_tile(self.centre)

    def _findOtherGrowthTile(self, currentGrowthTile):
        """This method is for the situation where you control some
        growth squares and want to find the next closest one."""
        self.closest_growth_tile = [tile for tile in self.board.growthTiles
                                    if tile != currentGrowthTile][0]
        self.centre = currentGrowthTile

    def endgameStrategy(self, pieces, currTurn):
        """Action out from the centre."""
        moves = []
        for loc in pieces:
            # Three cases:
            # 1. Growth square
            if loc in self.board.growthTiles:
                if self.board.getPieceStrength(loc) == 1:
                    move = GameBoard.Action.STAY
                elif self.board.getPieceStrength(loc) & 1:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
                arg = 1
            # 2. Further from the centre than a growth square
            elif (self.quickest_direction(
                    loc, self.get_closest_growth_tile(loc)) ==
                  self.quickest_direction(loc, self.centre)):
                move = (GameBoard.Action.STAY if (currTurn & 1) else
                        self.move_away_from_centre(loc))
                arg = 1
            # 3. Between the centre and a growth square
            # Two sub cases: strength == 1?
            elif self.board.getPieceStrength(loc) == 1:
                move = GameBoard.Action.STAY
                arg = 1
            else:
                direction = self.move_away_from_centre(loc)
                if direction == GameBoard.Action.LEFT:
                    move = GameBoard.Action.JUMP_LEFT
                else:
                    move = GameBoard.Action.JUMP_RIGHT
                arg = 2
            moves.append([loc, str(move), arg])
        return moves

    def generateMoves(self, currTurn, maxTurns):
        """Method to generate the moves."""
        pieces = self.board.findMyPieces()

        if currTurn == 1:
            self._firstMoveSetup(pieces[0])

        if ((self.closest_growth_tile in pieces) &
                (self.n_growth_tiles < len(self.board.growthTiles))):
            self.n_growth_tiles += 1
            self._findOtherGrowthTile(self.closest_growth_tile)

        if self.n_growth_tiles < len(self.board.growthTiles):
            # Here, we want to gain control of the next growth tile.
            moves = self.controlTile(self.closest_growth_tile) or []
        elif self.n_growth_tiles == len(self.board.growthTiles):
            # Here, we have control of the growth squares,
            # so now we need to expand out from the centre.
            self.centre = sum(self.board.growthTiles) / self.n_growth_tiles
            moves = self.endgameStrategy(pieces, currTurn)

        moves = useUnusedPieces(moves, pieces)
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'mammothBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("c68cc1563d127369c3875649a11f9123"
                    "88cb0b5785d78383b9768b6c0e1fead3")
        elif challenge == "database":
            return ("03451213f18c74076dbc32b1fd789a8e"
                    "c957ad2d0767dfec6177c53def69f925")
        return "MAMMOTHBOT"
