"""This bot improves RunnerBot by having the centre at a growth tile"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return BabyBot()


class BabyBot(BotBase.BotBase):
    """This bot improves RunnerBot by having the centre at a growth tile.
    It walks straight to the closest growth tile and then spreads from there.
    """
    def __init__(self):
        """Initialise bot."""
        BotBase.BotBase.__init__(self)
        self.centre = None

    def tile_distance(self, loc1, loc2):
        """Find the distance between two tiles."""
        dist = abs(loc1 - loc2)
        return (dist if dist < self.board.boardSize/2
                else self.board.boardSize-dist)

    def get_closest_growth_tile(self, location):
        """Find the closest growth tile."""
        return min(self.board.growthTiles,
                   key=lambda tile: self.tile_distance(location, tile))

    def quickest_direction(self, location, target):
        """Determine which direction to travel in order to get to
        target from location quickest.
        This method assumes location != target.
        """
        if target > location:
            if target - location < self.board.boardSize/2:
                return GameBoard.Action.RIGHT
            return GameBoard.Action.LEFT
        elif location-target > self.board.boardSize/2:
            return GameBoard.Action.RIGHT
        return GameBoard.Action.LEFT

    def move_away_from_centre(self, location):
        """This method makes pieces move away from the
        centre point for this bot.
        """
        if location < self.centre:
            if self.centre - location < self.board.boardSize/2:
                return GameBoard.Action.LEFT
            return GameBoard.Action.RIGHT
        elif location - self.centre > self.board.boardSize/2:
            return GameBoard.Action.LEFT
        return GameBoard.Action.RIGHT

    def generateMoves(self, currTurn, maxTurns):
        """Generate the moves this bot will make."""
        if currTurn == 1:
            self.centre = None

        pieces = self.board.findMyPieces()
        moves = []
        # If the bot has no pieces on a growth tile,
        # everything move towards the closest growth tile.
        if set(pieces).intersection(set(self.board.growthTiles)):
            if self.centre is None:
                self.centre = set(pieces).intersection(
                    set(self.board.growthTiles)).pop()
            for loc in pieces:
                if loc != self.centre:
                    move = self.move_away_from_centre(loc)
                elif ((self.board.getPieceStrength(loc) & 1) &
                      (self.board.getPieceStrength(loc) > 1)):
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
                moves.append([loc, str(move), 1])
        else:
            for loc in pieces:
                move = self.quickest_direction(
                    loc, self.get_closest_growth_tile(loc))
                moves.append([loc, str(move), 1])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'babyBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("3ce67520c086d01048d01d95c1e29a4"
                    "828af65579f25d6d3882d62cb0d0b74fa")
        elif challenge == "database":
            return ("6784bd549057a9ebc388070e9856a44b0"
                    "ac8fc1813f1d8f45948ec6bd99aa0e4")
        return "xxbabyBotxx"
