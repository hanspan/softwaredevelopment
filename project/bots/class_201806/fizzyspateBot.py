"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return newBot()


class newBot(BotBase.BotBase):


    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'split' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """
        
        pieces = self.board.findMyPieces()
        moves = []
        
        #Locate special locations, opponent pieces and max tile strength
        maxTileStrength = self.board.maxTileStrength
        opp_pieces = self.board.findOpponentPieces()
        board_size = self.board.boardSize
        my_pieces = self.board.findMyPieces()
 
        for location in pieces:
            #Get strength of current location and neighbouring locations
            strength = self.board.getPieceStrength(location)
            location_left = (location - 1 + board_size) % board_size
            location_right = (location+ 1) % board_size
            strength_left = self.board.getPieceStrength(location_left)
            strength_right = self.board.getPieceStrength(location_right)
   
            #Determine if we can split without having one or both sides immediately destroyed
            safely_split = True
            half_strength = (strength // 2)
            if (location_left in opp_pieces) and (half_strength < strength_left):
                safely_split = False
            if (location_right in opp_pieces) and (half_strength < strength_right):
                safely_split = False
 


            #If at edge we try to gain more territory if we can otherwise we secure the boarder
            if (location_left not in my_pieces) or (location_right not in my_pieces):
                if (strength >= 2) and safely_split:
                    move = GameBoard.Action.SPLIT
                else:
                    move =GameBoard.Action.STAY
            #Central tiles generate up to a point and then send them right. Divide by 3 of strength seemed to work well.
            else:
                if strength < (maxTileStrength //3):
                    move= GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.RIGHT

                
            #print(move)   
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand. We set arg to a default value of 1 as we are not jumping
            moves.append([location, str(move), 1])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
