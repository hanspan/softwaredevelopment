"""
This is a wallE bot. It heads towards the strategic locations.
"""

# import random      # Source for random numbers.
import numpy as np
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return JamesBot()


class JamesBot(BotBase.BotBase):
    """This Bot heads towards the good tiles!"""

    def sortLocations(self, strategic_locations, pieces):
        """This function makes locations appear in 'good' order"""
        good_locations = []
        less_good_locations = []
        for location in pieces:
            if location in strategic_locations:
                good_locations.append(location)
            else:
                less_good_locations.append(location)

        return good_locations+less_good_locations

    def checkStrategicLocations(self, checked_locations, strat_loc,
                                locations_sorted, moves):
        """This function decides what to do if already at
        strategic locations"""

        # now look for locations that are already in strategic places
        # if we are below max strength then stay and grow
        # otherwise split

        # we remove any strategic locations because we want our other
        # bots to head to new ones
        # update checked_locations for later on
        for i, location in enumerate(locations_sorted):
            strength = self.board.getPieceStrength(location)
            if location in strat_loc and strength < 25:
                moves.append([location, str(GameBoard.Action.STAY), 1])
                checked_locations[i] = 1
                strat_loc.remove(location)
            elif location in strat_loc:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
                checked_locations[i] = 1
                strat_loc.remove(location)

    def moveTowardsStrategic(self, moves, checked_locations,
                             strategic_locations, ilocation):
        """This function moves towards the closest strategic locations"""
        i = ilocation[0]
        location = ilocation[1]
        if checked_locations[i] == 0:
            first_list = np.abs(np.array(strategic_locations)-location)
            second_list = self.board.boardSize-first_list
            # second list is needed because of wrap around
            # index is closest strategic location
            if np.min(first_list) < np.min(second_list):
                index = np.argmin(first_list)
                # don't need to wrap around
                if location < strategic_locations[index]:
                    moves.append([location, str(GameBoard.Action.RIGHT), 1])
                else:
                    moves.append([location, str(GameBoard.Action.LEFT), 1])
            else:
                # wrap around
                index = np.argmin(second_list)
                # this is because we wrap around the list
                # just need to work out direction
                if location < int(self.board.boardSize/2):
                    moves.append([location, str(GameBoard.Action.LEFT), 1])
                else:
                    moves.append([location, str(GameBoard.Action.RIGHT), 1])

    def generateMoves(self, curr_turns, max_turns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        """
        curr_turns+max_turns
        pieces = self.board.findMyPieces()
        moves = []
        strat_loc = self.board.defenceTiles+self.board.growthTiles
        check_locations = [0]*len(pieces)

        locations_sorted = self.sortLocations(strat_loc, pieces)

        self.checkStrategicLocations(check_locations, strat_loc,
                                     locations_sorted, moves)

        # now look at bots not at strategic locations
        # if we are already at all of them (elif statement),
        # then stay and grow or split
        # otherwise, head towards nearest strategic location
        if strat_loc:  # if non empty
            for i, location in enumerate(locations_sorted):
                self.moveTowardsStrategic(moves, check_locations,
                                          strat_loc, [i, location])
        else:
            for i, location in enumerate(locations_sorted):
                if check_locations[i] == 0:
                    if self.board.getPieceStrength(location) >= 25:
                        moves.append([location, str(GameBoard.Action.SPLIT), 1])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the start of the hash of 'wallE' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            # sha256 hash of "wallEdebug"
            return ("b07a25676f4660dccfdcf29008e63a"
                    "36791e7ddef31cfa5391543b045e1df9bd")
        elif challenge == "database":
            # sha256 hash of "wallEdatabase"
            return ("3721f59fc125c84d13a0d1ebe37d00"
                    "d2cd67ad591fe3a3633cdcc1b8c05630f1")
        return "Default bot signature"
