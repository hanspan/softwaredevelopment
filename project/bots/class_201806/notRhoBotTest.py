"""
Tests for notRhoBot
"""

import os
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import notRhoBot      # The Bot being tested.

import BotTestCase


class notRhoBotTestCase(BotTestCase.BotTestCase):
    """
    Sets up a noRhoBot to test with test methods
    """

    def setUp(self):
        """
        Common setup for the tests in this class.
        """

        self.bot = notRhoBot.RhoBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        """
        Make sure a bot is returned from the getBot() function.
        """

        self.assertNotEqual(None, notRhoBot.getBot())

    def test_first(self):
        """
        Tests deterministic behaviour on first round
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'notRhoBotTestBoardState1.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        for res in results:
            self.assertEqual(str(GameBoard.Action.STAY), res[1])
            self.assertEqual(1, res[2])

    def test_second(self):
        """
        Tests deterministic behaviour on second round
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'notRhoBotTestBoardState2.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        for res in results:
            self.assertEqual(str(GameBoard.Action.SPLIT), res[1])
            self.assertEqual(1, res[2])

    def test_move(self):
        """
        Tests behaviour on 60th turn is one of two correct options.
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'notRhoBotTestBoardState60.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(3, len(results[0]))
        for res in results:
            self.assertIn(res[1], [str(GameBoard.Action.LEFT),
                                   str(GameBoard.Action.RIGHT)])

    def test_split(self):
        """
        Test behaviour on round 61
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'notRhoBotTestBoardState61.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(3, len(results[0]))
        for res in results:
            self.assertEqual(str(GameBoard.Action.SPLIT), res[1])

    def test_stay(self):
        """
        Test behaviour on round 62
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'notRhoBotTestBoardState62.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(3, len(results[0]))
        for res in results:
            self.assertEqual(str(GameBoard.Action.STAY), res[1])

    def test_sig(self):
        """
        So many tests to add
        """
        bug = self.bot.getSignature("debug")
        base = self.bot.getSignature("database")
        default = self.bot.getSignature("oops")
        self.assertIs(type(bug), str)
        self.assertIs(type(base), str)
        self.assertIs(type(default), str)


