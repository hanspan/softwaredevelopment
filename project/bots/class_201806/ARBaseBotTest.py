
import os
import unittest.mock  # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Provides some game-specific enumerations.
import ARBaseBot        # The class to test.
import BotTestCase

class ArBaseBotTestCase(BotTestCase.BotTestCase):
    
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = ARBaseBot.ARBaseBot()
        self.bot.setup(self.getGameConfig())
        
    def setBotBoardState(self, board_state_filename):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, board_state_filename)
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        boardStateJson = json.loads(state)
        self.bot.board.updateState(boardStateJson)
        
    def testValidSetup(self):
        self.assertEqual(self.bot.board.player, GameBoard.Colour.RED)
        self.assertEqual(self.bot.board.boardSize, 100)

    def testInvalidSetup(self):
        # Make sure the expected exception is raised on bad inputs to setup.
        # Most other exceptions are suppressed, but one in setup pretty
        # effectively makes the game unplayable.

        bot = ARBaseBot.ARBaseBot()
        config = b'This is not valid JSON'
        # Course has switched to Python 3.4; JSONDecodeError is 3.5.
        # with self.assertRaises(json.JSONDecodeError):
        with self.assertRaises(ValueError):
            bot.setup(config)

    def testNewTurn(self):
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        
        self.assertEqual(self.bot.strengthsAfterMove, [1 if i in [25, 74] else 0 for i in range(self.bot.board.boardSize)])
       
    def testForecastStrengthAfterMove(self):
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        self.bot.forecastStrengthAfterMove(25, GameBoard.Action.RIGHT, 1)
        self.assertEqual(self.bot.strengthsAfterMove, [1 if i in [26, 74] else 0 for i in range(self.bot.board.boardSize)])
        
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        self.bot.forecastStrengthAfterMove(25, GameBoard.Action.LEFT, 1)
        self.assertEqual(self.bot.strengthsAfterMove, [1 if i in [24, 74] else 0 for i in range(self.bot.board.boardSize)])
        
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        self.bot.forecastStrengthAfterMove(25, GameBoard.Action.JUMP_RIGHT, 1)
        self.assertEqual(self.bot.strengthsAfterMove, [1 if i in [26, 74] else 0 for i in range(self.bot.board.boardSize)])
        
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        self.bot.forecastStrengthAfterMove(25, GameBoard.Action.JUMP_LEFT, 1)
        self.assertEqual(self.bot.strengthsAfterMove, [1 if i in [24, 74] else 0 for i in range(self.bot.board.boardSize)])
        
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        self.bot.forecastStrengthAfterMove(25, GameBoard.Action.STAY, 1)
        self.assertEqual(self.bot.strengthsAfterMove, [[25, 25, 74].count(i) for i in range(self.bot.board.boardSize)])
        
        self.setBotBoardState("testBoardState.txt")
        self.bot.newTurn()
        self.bot.forecastStrengthAfterMove(25, GameBoard.Action.SPLIT, 1)
        self.assertEqual(self.bot.strengthsAfterMove, [1 if i in [25, 74] else 0 for i in range(self.bot.board.boardSize)])
    
    def testJumpToSquare(self):
        self.assertEqual(self.bot.jumpToSquare(50, 55), (GameBoard.Action.JUMP_RIGHT, 5))
        self.assertEqual(self.bot.jumpToSquare(55, 50), (GameBoard.Action.JUMP_LEFT, 5))
        
        self.assertEqual(self.bot.jumpToSquare(0, 99), (GameBoard.Action.JUMP_LEFT, 1))
        self.assertEqual(self.bot.jumpToSquare(99, 0), (GameBoard.Action.JUMP_RIGHT, 1))
    
    def testMoveTowardsEnemy(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")
        
        self.assertEqual(self.bot.moveTowardsEnemy(28, 10), 38)
        self.assertEqual(self.bot.moveTowardsEnemy(25, 10), 15)
    
    def testDistanceToEnemy(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")
        
        self.assertEqual(self.bot.distanceToEnemy(25), 47)
        
    def testNextToFreeSquare(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")
        
        self.assertEqual(self.bot.nextToFreeSquare(25), True)
        self.assertEqual(self.bot.nextToFreeSquare(26), False)

    def testSurroundedByOpponents(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")

        self.assertEqual(self.bot.surroundedByOpponents(75), True)
        self.assertEqual(self.bot.surroundedByOpponents(79), False)
        self.assertEqual(self.bot.surroundedByOpponents(25), False)
    
    def testSurroundedByAllies(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")

        self.assertEqual(self.bot.surroundedByAllies(75), False)
        self.assertEqual(self.bot.surroundedByAllies(26), True)
        self.assertEqual(self.bot.surroundedByAllies(25), False)
        
    def testHasOpponentNeighbours(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")

        self.assertEqual(self.bot.hasOpponentNeighbours(75), True)
        self.assertEqual(self.bot.hasOpponentNeighbours(79), True)
        self.assertEqual(self.bot.hasOpponentNeighbours(25), False)  
      
    def testHasAlliedNeighbours(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")

        self.assertEqual(self.bot.hasAlliedNeighbours(75), False)
        self.assertEqual(self.bot.hasAlliedNeighbours(26), True)
        self.assertEqual(self.bot.hasAlliedNeighbours(25), True)
      
    def testBoardDistance(self):
        self.setBotBoardState("testBoardState.txt")
        
        self.assertEqual(self.bot.boardDistance(1, 2), 1)
        self.assertEqual(self.bot.boardDistance(0, 99), 1)
        self.assertEqual(self.bot.boardDistance(25, 75), 50)
    
    def testCalculateCollateralDamage(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")
        
        self.assertEqual(self.bot.calculateCollateralDamage(25, 77, self.bot.board.findOpponentPieces(), True), 25)
      
    def testIntelligentAttack(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")
        
        self.assertEqual(self.bot.intelligentAttack(75, 3, False)[1], 2)
        self.assertEqual(self.bot.intelligentAttack(79, 3, True)[1], 3)
        self.assertEqual(self.bot.intelligentAttack(79, 0, True), None)
        
    def testIntelligentReinforce(self):
        self.setBotBoardState("ARBaseBotTestBoardState.txt")
        self.bot.newTurn()

        self.assertEqual(self.bot.intelligentReinforce(25, 0, 10), None)
        self.assertEqual(self.bot.intelligentReinforce(25, 2, 10)[1], 1)
        self.assertEqual(self.bot.intelligentReinforce(29, 5, 10)[1], 9)



        
        