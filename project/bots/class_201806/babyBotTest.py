"""Test suite for babyBot.py"""

import GameBoard      # Information on game board properties.
import babyBot        # The Bot being tested.
import unittest.mock

import BotTestCase


class babyBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = babyBot.BabyBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, babyBot.getBot())
        
    def testTileDistance(self):
        # Ensure that the calculation of distance between tiles is correct for a few cases.
        with self.subTest('Test distance from a tile to itself is 0'):
            self.assertEqual(self.bot.tile_distance(10, 10), 0)
            self.assertEqual(self.bot.tile_distance(55, 55), 0)
        with self.subTest('Test distance without looping around board'):
            self.assertEqual(self.bot.tile_distance(30, 65), 35)
        with self.subTest('Test distance with looping around board'):
            self.assertEqual(self.bot.tile_distance(90, 10), 20)
            
    def testClosestGrowthTile(self):
        # Ensure that the bot finds the closest growth tiles correctly.
        # Only checks for one standard setup, in which the growth tiles are 40 and 59.
        with self.subTest('Test the closest growth tile to a growth tile is itself'):
            self.assertEqual(self.bot.get_closest_growth_tile(40), 40)
        with self.subTest('Test edge cases find nearest growth tile'):
            self.assertEqual(self.bot.get_closest_growth_tile(50), 59)
            self.assertEqual(self.bot.get_closest_growth_tile(0), 40)
        with self.subTest('Test nearby growth tiles are found'):
            self.assertEqual(self.bot.get_closest_growth_tile(65), 59)
            
    def testQuickestDirection(self):
        # Ensure that the bot goes in the quickest direction to get to a square
        self.assertEqual(self.bot.quickest_direction(10, 20), GameBoard.Action.RIGHT)
        self.assertEqual(self.bot.quickest_direction(5, 95), GameBoard.Action.LEFT)
        self.assertEqual(self.bot.quickest_direction(90, 5), GameBoard.Action.RIGHT)
        self.assertEqual(self.bot.quickest_direction(70, 50), GameBoard.Action.LEFT)
        
    def testMoveAwayFromCentre(self):
        with self.subTest('Test with a high centre'):
            self.bot.centre = 90
            self.assertEqual(self.bot.move_away_from_centre(10), GameBoard.Action.RIGHT)
            self.assertEqual(self.bot.move_away_from_centre(80), GameBoard.Action.LEFT)
        with self.subTest('Test with a low centre'):
            self.bot.centre = 10
            self.assertEqual(self.bot.move_away_from_centre(20), GameBoard.Action.RIGHT)
            self.assertEqual(self.bot.move_away_from_centre(90), GameBoard.Action.LEFT)
            
    def testGenerateMovesBeforeReachingGrowthTile(self):
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.side_effect = [[25]]
        self.bot.board.growthTiles = [23, 26]
        
        moves = self.bot.generateMoves(1, 1000)
        self.assertEqual(moves, [[25, 'Right', 1]])
        self.assertEqual(self.bot.centre, None)
        
    def testGenerateMovesUponReachingGrowthTile(self):
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.side_effect = [[40]]
        
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.return_value = 2

        self.bot.board.growthTiles = [40, 69]
        self.bot.centre = None
        
        moves = self.bot.generateMoves(5, 1000)
        expected_moves = [[40, 'Stay', 1]]
        self.assertEqual(moves, expected_moves)
        #self.assertEqual(self.bot.centre, 40)
        
    def testGenerateMovesAfterReachingGrowthTile(self):
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name='findMyPieces')
        self.bot.board.findMyPieces.side_effect = [[39, 40, 41]]
        
        piece_strengths = {39: 2, 40: 7, 41: 3}
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name='getPieceStrength')
        self.bot.board.getPieceStrength.side_effect = lambda loc: piece_strengths[loc]

        self.bot.board.growthTiles = [40, 69]
        self.bot.centre = 40
        
        moves = self.bot.generateMoves(10, 1000)
        expected_moves = []
        expected_moves.append([39, 'Left', 1])
        expected_moves.append([40, 'Split', 1])
        expected_moves.append([41, 'Right', 1])
        
        self.assertEqual(moves, expected_moves)
        
    def testGetSignature(self):
        with self.subTest('Checking debug signature'):
            debug = "3ce67520c086d01048d01d95c1e29a4828af65579f25d6d3882d62cb0d0b74fa"
            sig = self.bot.getSignature('debug')
            self.assertEqual(debug, sig)
        with self.subTest('Checking database signature'):
            db = "6784bd549057a9ebc388070e9856a44b0ac8fc1813f1d8f45948ec6bd99aa0e4"
            sig = self.bot.getSignature('database')
            self.assertEqual(db, sig)
        with self.subTest('Check default signature'):
            default = "xxbabyBotxx"
            sig = self.bot.getSignature('default')
            self.assertEqual(default, sig)
