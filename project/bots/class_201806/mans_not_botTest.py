"""Unit tests for mans not bot"""
import os
import json           # Process JSON to/from strings, etc.
import mans_not_bot      # The Bot being tested.
import BotTestCase


class MansNotBotTestCase(BotTestCase.BotTestCase):
    """Contains functions for testing mans_not_bot"""

    def setUp(self):
        """Common setup for the tests in this class"""
        self.bot = mans_not_bot.MansNotBot()
        self.bot.setup(self.getGameConfig())
        self.board = self.bot.board

    def testGetBot(self):
        """Make sure a bot is returned from the getBot() function."""
        self.assertNotEqual(None, mans_not_bot.getBot())

    def testGenerateHeckle(self):
        """Make sure the heckle is devastating"""
        self.assertTrue(mans_not_bot.generateHeckle())

    def testGenerateMoves(self):
        """Make sure the attributes are set up correctly"""
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(
            testDataDir, 'testBoardStateForMansNotBot1.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        self.bot.makeMoves(state.encode("utf-8"))
        # Tests that the bot stores its attributes properly
        self.assertEqual(self.bot.generateMoves(
            1, 100), [[self.bot.i_l, "Stay", 1]])
        self.assertEqual(self.bot.i_l, 2)
        self.assertEqual(self.bot.center, [2, 3])

        # resetting the bot's internal state
        self.bot.i_l = 4
        self.bot.center = [4, 5]
        self.bot.lefts = [0, 1, 2, 3]

        # Tests the two parity cases
        self.assertEqual(len(self.bot.generateMoves(3, 100)), 6)
        self.assertEqual(len(self.bot.generateMoves(4, 100)), 6)

        # Testing the center shifting functionality:
        boardStateFile = os.path.join(
            testDataDir, 'testBoardStateForMansNotBot2.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        update = json.loads(state)
        self.bot.board.updateState(update)

        self.bot.generateMoves(1, 100)
        self.assertEqual(self.bot.i_l, 73)
        self.assertEqual(self.bot.center, [73, 74])

    def testSignature(self):
        """Check that the expected signature is returned."""

        sig = mans_not_bot.MansNotBot.getSignature('debug')
        self.assertTrue(sig.endswith("95ed2e970431"))
        sig = mans_not_bot.MansNotBot.getSignature('database')
        self.assertTrue(sig.endswith("e548c3ae5757"))
        sig = mans_not_bot.MansNotBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
