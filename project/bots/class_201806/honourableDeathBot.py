"""
This Bot tries to lose as quickly as possible!
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return HonourableDeathBot()


class HonourableDeathBot(BotBase.BotBase):
    """This Bot tries to lose as quickly as possible!"""
    middle = None
    startTurn = 0

    def getNearestOpponent(self, position):
        """ Sort opponents pieces by distance from position """
        return self.getNearestColour(position, self.board.opponent)

    def getNearestColour(self, position, colour):
        """ Sort pieces of given colour by distance from position """
        pieces = self.board.findPieces(colour)
        pieces.sort(key=lambda x: self.distance(x, position)[0])
        return pieces

    def distance(self, posA, posB):
        """ Calculate the shortest distance between two points """
        distRight = posB - posA
        distLeft = posA - posB + self.board.boardSize
        if distRight < 0:
            newRight = 2 * self.board.boardSize - distLeft
            distLeft = -distRight
            distRight = newRight
        if distLeft < distRight:
            return distLeft, -1
        return distRight, +1

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        moves = list()
        myPos = self.board.findMyPieces()
        if self.middle is None:
            self.middle = myPos[0]
        enemy = self.getNearestOpponent(self.middle)[0]
        enemyDistance, enemyDirection = self.distance(self.middle, enemy)
        myStrength = sum(self.board.getPieceStrength(pos) for pos in myPos)
        combine = [GameBoard.Action.JUMP_RIGHT, GameBoard.Action.RIGHT, GameBoard.Action.STAY, GameBoard.Action.LEFT, GameBoard.Action.JUMP_LEFT]
        if currTurn-self.startTurn in [3,5]:
            for pos in myPos:
                moves.append((pos, str(GameBoard.Action.SPLIT), 1))
        elif myStrength >= 24 or myStrength >= enemyDistance:
            if len(myPos) > 1:
                for pos, move, num in zip(myPos, combine, [2,0,0,0,2]):
                    moves.append((pos, str(move), num))
            else:
                target = (self.middle - myStrength) % self.board.boardSize
                jump = GameBoard.Action.JUMP_LEFT
                if enemyDirection > 0:
                    jump = GameBoard.Action.JUMP_RIGHT
                    target = (self.middle + myStrength) % self.board.boardSize
                moves.append((self.middle, str(jump), min(myStrength, enemyDistance)))
                self.middle = target
                self.startTurn = currTurn
            

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
