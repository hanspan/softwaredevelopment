"""
Tests for turn_down_for_bot
"""
import os
import json
import turn_down_for_bot      # The Bot being tested.
import BotTestCase


class TurnDownForBotTestCase(BotTestCase.BotTestCase):
    """
    Implements unit tests for the mans_not_bot class
    """

    def setUp(self):
        """Common setup for the tests in this class."""
        self.bot = turn_down_for_bot.TurnDownForBot()
        self.bot.setup(self.getGameConfig())
        self.board = self.bot.board

    def testGetBot(self):
        """Make sure a bot is returned from the getBot() function."""
        self.assertNotEqual(None, turn_down_for_bot.getBot())

    def test_initial_move(self):
        """This tests the known bespoke initial moves of the bot"""
        self.bot.s_p = 10
        self.assertEqual(len(self.bot.initial_move(1)), 1)
        self.assertEqual(len(self.bot.initial_move(2)), 1)
        self.assertEqual(len(self.bot.initial_move(3)), 1)
        self.assertEqual(len(self.bot.initial_move(4)), 3)
        self.assertEqual(len(self.bot.initial_move(5)), 3)
        self.assertEqual(len(self.bot.initial_move(6)), 3)

    def test_parity0move(self):
        """Tests the known central moves for the first move"""
        # Setting up the bot for the following tests
        self.bot.s_p = 10
        self.bot.center = [8, 9, 10, 11, 12]
        board_size = self.bot.board.boardSize
        self.bot.lefts = [(self.bot.s_p - i) %
                          board_size for i in range(3, board_size // 2)]

        pieces = [5, 6, 8, 9, 10, 11, 12, 14, 15]
        self.assertEqual(self.bot.parity0move(pieces)[0], [9, 'Left', 1])
        self.assertEqual(self.bot.parity0move(pieces)[1], [10, 'Stay', 1])
        self.assertEqual(self.bot.parity0move(pieces)[2], [11, 'Right', 1])
        self.assertEqual(self.bot.parity0move(pieces)[3], [5, 'Stay', 1])
        self.assertEqual(self.bot.parity0move(pieces)[4], [6, 'Stay', 1])
        self.assertEqual(self.bot.parity0move(pieces)[5], [14, 'Stay', 1])
        self.assertEqual(self.bot.parity0move(pieces)[6], [15, 'Stay', 1])

    def test_parity1move(self):
        """Tests the known central moves for the second move"""
        # Setting up the bot for the following tests
        self.bot.s_p = 10
        self.bot.center = [8, 9, 10, 11, 12]
        board_size = self.bot.board.boardSize
        self.bot.lefts = [(self.bot.s_p - i) %
                          board_size for i in range(3, board_size // 2)]

        pieces = [5, 6, 8, 9, 10, 11, 12, 14, 15]
        self.assertEqual(self.bot.parity1move(pieces)[0], [8, "Split", 1])
        self.assertEqual(self.bot.parity1move(pieces)[1], [10, "Stay", 1])
        self.assertEqual(self.bot.parity1move(pieces)[2], [12, "Split", 1])
        self.assertEqual(self.bot.parity1move(pieces)[3], [5, "Left", 1])
        self.assertEqual(self.bot.parity1move(pieces)[4], [6, "Left", 1])
        self.assertEqual(self.bot.parity1move(pieces)[5], [14, "Right", 1])
        self.assertEqual(self.bot.parity1move(pieces)[6], [15, "Right", 1])

    def test_parity2move(self):
        """Tests the known central moves for the second move"""
        # Setting up the bot for the following tests
        self.bot.s_p = 10
        self.bot.center = [8, 9, 10, 11, 12]
        board_size = self.bot.board.boardSize
        self.bot.lefts = [(self.bot.s_p - i) %
                          board_size for i in range(3, board_size // 2)]

        pieces = [5, 6, 8, 9, 10, 11, 12, 14, 15]
        self.assertEqual(self.bot.parity2move(pieces)[0], [8, "Left", 1])
        self.assertEqual(self.bot.parity2move(pieces)[1], [9, "Stay", 1])
        self.assertEqual(self.bot.parity2move(pieces)[2], [10, "Split", 1])
        self.assertEqual(self.bot.parity2move(pieces)[3], [11, "Stay", 1])
        self.assertEqual(self.bot.parity2move(pieces)[4], [12, "Right", 1])
        self.assertEqual(self.bot.parity2move(pieces)[5], [5, "Left", 1])
        self.assertEqual(self.bot.parity2move(pieces)[6], [6, "Left", 1])
        self.assertEqual(self.bot.parity2move(pieces)[7], [14, "Right", 1])
        self.assertEqual(self.bot.parity2move(pieces)[8], [15, "Right", 1])

    def test_generate_moves(self):
        """For a given board state, the main move generating function
        is tested here"""
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(
            testDataDir, 'testBoardStateForTurnDownForBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        update = json.loads(state)
        self.bot.board.updateState(update)

        # Making sure the internal state is initialised
        self.assertEqual(self.bot.s_p, 0)

        self.bot.s_p = 10
        self.bot.center = [8, 9, 10, 11, 12]
        board_size = self.bot.board.boardSize
        self.bot.lefts = [(self.bot.s_p - i) %
                          board_size for i in range(3, board_size // 2)]

        # Making sure each move decision is properly implemented
        self.assertEqual(len(self.bot.generateMoves(20, 100)), 9)
        self.assertEqual(len(self.bot.generateMoves(21, 100)), 7)
        self.assertEqual(len(self.bot.generateMoves(22, 100)), 7)
        self.assertEqual(len(self.bot.generateMoves(1, 100)), 1)

    def test_generate_heckle(self):
        """Tests the fun heckle for opponents"""
        self.assertIsInstance(turn_down_for_bot.generateHeckle(), str)

    def testSignature(self):
        """Check that the expected signature is returned."""

        sig = turn_down_for_bot.TurnDownForBot.getSignature('debug')
        self.assertTrue(sig.endswith("68c799a7b3d1"))
        sig = turn_down_for_bot.TurnDownForBot.getSignature('database')
        self.assertTrue(sig.endswith("421d7539167e"))
        sig = turn_down_for_bot.TurnDownForBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
