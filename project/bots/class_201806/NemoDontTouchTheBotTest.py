
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import NemoDontTouchTheBot      # The Bot being tested.

import BotTestCase


class NemoDontTouchTheBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = NemoDontTouchTheBot.NemoDontTouchTheBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        ''' Make sure a bot is returned from the getBot() function.'''

        self.assertNotEqual(None, NemoDontTouchTheBot.getBot())

    def testClosestEnemy(self):
        ''' Check that a valid result is returned if there are no enemies '''
        self.assertEqual(NemoDontTouchTheBot.findClosestEnemy(10, []), (100, -1))
    
    def testKnownMove(self):
        
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(results, [[25, 'Stay', 0]])
        
    def testCalculateMoves(self):
        # Check that the piece splits if it has odd strength and the enemy if far away
        self.assertEqual(NemoDontTouchTheBot.calculateMove(10,9,[50]), GameBoard.Action.SPLIT)
        
        # Check that the piece does nothing if it has even strength and the enemy if far away
        self.assertEqual(NemoDontTouchTheBot.calculateMove(10,10,[50]), GameBoard.Action.STAY)
        
        # Check that the piece goes left if the enemy is close to the left
        self.assertEqual(NemoDontTouchTheBot.calculateMove(10,9,[8]), GameBoard.Action.LEFT)
        
        # Check that the piece goes right if the enemy is close to the right
        self.assertEqual(NemoDontTouchTheBot.calculateMove(10,9,[12]), GameBoard.Action.RIGHT)
        
    
    def testGetSignature(self):
        self.assertEqual(self.bot.getSignature(challenge="debug"), "c262f307e339ec7ac38522b13056523e39417e5250b038fc57fb1edfb9700aac")
        self.assertEqual(self.bot.getSignature(challenge="database"), "fac9221640a6143c32052451aade4c72b827728bc8b10a028497ddc6f4da855a")
        self.assertEqual(self.bot.getSignature(challenge="random") , "Default Bot Signature :(")
        
        