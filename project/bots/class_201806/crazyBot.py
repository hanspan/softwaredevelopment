"""
This is an advanced Bot; it identifies the pieces it has on the game
board and will attack you with questionable performance

It should not take much to outperform this bot...
"""

import BotBase  # All of the basic Bot functionality.
import GameBoard  # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return CrazyBot()


class CrazyBot(BotBase.BotBase):
    """This Bot just makes crazy moves all the time!"""

    def shortestPath(self, moveFrom, moveTo):
        """This calculates the shortest path between two pieces"""
        distance_right = (moveTo - moveFrom) % self.board.boardSize
        distance_left = (moveFrom - moveTo) % self.board.boardSize
        if distance_left <= distance_right:
            move, distance = GameBoard.Action.JUMP_LEFT, distance_left
        else:
            move, distance = GameBoard.Action.JUMP_RIGHT, distance_right
        return move, distance

    def newJumpAttack(self, piece, enemy, unsensitivity):
        """This calculates the jump path between two pieces
        otherwise will return '' and 100"""
        move, arg = self.shortestPath(piece, enemy)
        if arg >= self.board.getPieceStrength(piece) - unsensitivity:
            move, arg = '', 100
        return move, arg

    def standardMoves(self, location, length):
        """ If in the early game, keep splitting, and then stay
        to build strength of more than 20 before splitting
        """
        # split for first 50 to dominate
        if self.shouldLocationSplit(length, location):
            move = GameBoard.Action.SPLIT
        elif self.board.getPieceStrength(location) <= 20:  # otherwise stay
            move = GameBoard.Action.STAY
        else:
            move = GameBoard.Action.SPLIT
        return move

    def shouldLocationSplit(self, length, location):
        """Check whether we have occupied half the board and
        have more than 2 strength """
        isLengthSmall = length < (self.board.boardSize/2)
        isPieceStrong = self.board.getPieceStrength(location) > 2
        return isLengthSmall and isPieceStrong

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        # This paramater sets how un-sensitive the jump strategy is
        # Higher is less sensitive
        unsensitivity = 6

        pieces = self.board.findMyPieces()
        opponent = self.board.findOpponentPieces()
        moves = []

        for location in pieces:
            move = ''
            arg = 100

    # Looping through the opponents pieces and see which ones to jump attack
    #Try and attack the second closest
            thingies = [self.newJumpAttack(
                location, item, unsensitivity) for item in opponent]
            #sort
            thingies.sort(key=lambda y: y[1], reverse=False)
            move, arg = thingies[1 if len(thingies) > 1 else 0]

            if move == '':
                move = self.standardMoves(location, len(pieces))
            # Most move commands don't need an argument, but the jump ones do.

            # Store the move. JSON needs a string so str method
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("df3d5a7ff86e44d0272e5b2e7287fc27"
                    "90e53d27df8dbfae4a9d4d4e2447a366")
        elif challenge == "database":
            return ("53bc2f09943d08ca6986c670cccf9faf"
                    "8366e1b781f53b23b18fdd9b1d580ec3")
        return "Default bot signature"
