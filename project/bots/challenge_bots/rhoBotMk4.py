"""
rhoBotMk4
"""

import random      # rng

import project.bots.BotBase as BotBase     # All of the basic Bot functionality.
import project.board as board   # Utilities for working with the game.


def getBot():
    """Returns an instance of RhoBotMk4"""

    return RhoBotMk4()


class RhoBotMk4(BotBase.BotBase):
    """RhoBotMk4 base class"""

    def __init__(self):
        """Call the base __init__ funciton"""
        BotBase.BotBase.__init__(self)
        self.startSquare = 1
        self.maxStrength = 1

    def generateMoves(self, currTurn, maxTurns):
        """
            Get the moves for each active tile.
            Initially do early game strat (see getEarlyMove docs)
            Then do strat 1 (see stratOne docs)
        """

        pieces = self.board.findMyPieces()
        xpieces = self.board.findOpponentPieces()
        moves = []

        if currTurn == 1:
            self.startSquare = pieces[0]
            self.maxStrength = self.board.maxTileStrength

        for location in pieces:
            strength = self.board.getPieceStrength(location)

            turnData = [currTurn, location, strength,
                        self.startSquare, self.maxStrength]

            if currTurn < 10:
                rng = random.random()
                move = getEarlyMove(turnData, rng)
                arg = 1
                moves.append([location, str(move), arg])

            else:
                move = stratOneSetup(turnData, self.board.player,
                                     xpieces)
                arg = 3
                moves.append([location, str(move), arg])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        Hash of 'rhoBotMk4' and the answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("48141ecd8ef5305a7fd0ba25372cbb8d7"
                    "4cc6a811541147f2dc845616d60fbeb")
        elif challenge == "database":
            return ("c5f36e1074dc636d7d85046b6dae4c90c5"
                    "409fe28fddbb18cfa3051378efd5b3")

        return "Default bot signature"


def stratOneSetup(turnData, currPlayer, xpieces):
    """Prepare the current player and rng for the stratOne function"""
    rng = random.random()
    if currPlayer == board.Colour("Red"):
        col = "red"
    else:
        col = "blue"
    move = stratOne(turnData, rng, col, xpieces)
    return move


def getEarlyMove(turnData, rng):
    """
    Generate early moves:
       1) if strength 1 then stay
       2) if strength 25 then split
       3) otherwise stay or split with probability rng
    """
    strength = turnData[2]
    maxStrength = turnData[4]
    if strength < 2:
        move = board.Action.STAY
    elif strength == maxStrength:
        move = board.Action.SPLIT
    else:
        if rng < 0.1:
            move = board.Action.STAY
        else:
            move = board.Action.SPLIT
    return move


def stratOne(turnData, rng, col, xpieces):
    """calls the main bot strategy:
        1) jump onto any enemy 3 spots away
        2) if no enemy 3 spots away then (depending on turn parity) do:
          2a) moving right/left (unless on start tiles)
          2b) staying still  (unless on max strength)
    """
    currTurn = turnData[0]
    location = turnData[1]
    strength = turnData[2]
    start = turnData[3]
    maxStrength = turnData[4]
    if col == "red":
        jump1 = (location+3) % 100 in xpieces
        jump2 = (location-3) % 100 in xpieces
        jump = [board.Action.JUMP_RIGHT, board.Action.JUMP_LEFT]
        direction = (board.Action.LEFT, board.Action.RIGHT)
    else:
        jump1 = (location-3) % 100 in xpieces
        jump2 = (location+3) % 100 in xpieces
        jump = [board.Action.JUMP_LEFT, board.Action.JUMP_RIGHT]
        direction = (board.Action.RIGHT, board.Action.LEFT)

    jumpMove = getJumpMove(currTurn, strength, jump1, jump2)
    if jumpMove >= 0:
        return jump[jumpMove]

    if currTurn % 2:
        if location in [start, start + 1]:
            move = board.Action.SPLIT
        elif location <= 74 and location >= 25:
            move = direction[1]
        else:
            move = direction[0]
    else:
        move = board.Action.STAY
        if strength == maxStrength:
            if location in range(25, 75):
                move = direction[1]
            else:
                move = direction[0]
    return move


def getJumpMove(currTurn, strength, jump1, jump2):
    """function to decide which jump move to go for (if any)"""
    index = -1
    if jump1:
        if strength > 16 and currTurn % 3 == 1:
            index = 0
        elif strength > 20 and currTurn % 3 == 2:
            index = 0
        elif strength > 24 and currTurn % 3 == 0:
            index = 0
    elif jump2:
        if strength > 16 and currTurn % 3 == 1:
            index = 1
        elif strength > 20 and currTurn % 3 == 2:
            index = 1
        elif strength > 24 and currTurn % 3 == 0:
            index = 1
    return index
