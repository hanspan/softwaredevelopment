"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.
Unlike RandomBot, we can choose the distribution for the next move
"""

import numpy       # Needed for random sampling
import project.bots.BotBase as BotBase     # All of the basic Bot functionality.
import project.board as board   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return JonnyRandomBot()


class JonnyRandomBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def __init__(self):
        """ Basic init: The only setup required is to save an internal RNG """

        BotBase.BotBase.__init__(self)
        self.rng = numpy.random

    def seedBot(self, seed):
        """ Seed the random in this bot """
        
        self.rng.seed(seed)

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        possibleMoves = [board.Action.STAY,
                         board.Action.LEFT,
                         board.Action.RIGHT,
                         board.Action.JUMP_LEFT,
                         board.Action.JUMP_RIGHT,
                         board.Action.SPLIT]

        probabilityMoves = {board.Action.STAY: 0.35,
                            board.Action.LEFT: 0.05,
                            board.Action.RIGHT: 0.1,
                            board.Action.JUMP_LEFT: 0.05,
                            board.Action.JUMP_RIGHT: 0.1,
                            board.Action.SPLIT: 0.35}

        probsList = [probabilityMoves[move] for move in possibleMoves]

        for location in pieces:
            # Random choice using the probability distribution defined above
            # Note that random choice always returns a list so we need to
            # index in to retrieve the 1 item
            move = self.rng.choice(possibleMoves, 1, p=probsList)[0]

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == board.Action.JUMP_LEFT
                    or move == board.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'jonnyRandomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("94592b0658d83f85b90a79323bae58f9"
                    "0c23b2f02b446a184422d6ce574dca66")
        elif challenge == "database":
            return ("6536b10937e2c5d5f5126ecf0f3aeae6"
                    "07bfb007f7604e6f6738b201f9f1bb50")
        return "Default bot signature"
