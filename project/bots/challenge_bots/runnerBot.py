"""
This is a basic Bot; it spawns new low-weight pieces, and then
makes them run to the right or left, as appropriate.

It should not take too much to outperform this bot...
"""

import project.bots.BotBase as BotBase     # All of the basic Bot functionality.
import project.board as board   # Utilities for working with the game.


def getBot():
    """
    his function needs to return an instance of this Bot's class.
    :return: RunnerBot instance
    """

    return RunnerBot()


class RunnerBot(BotBase.BotBase):
    """This Bot just spawns low-weight pieces and runs the across the board."""

    def generateMoves(self, currTurn, maxTurns):
        """
        Locate all of our pieces on the board, and then make a move for each one.

        On the first turn, it identifies where the starting pieces are. This
        will then be used to determine how all future pieces move.
        :param currTurn: int indicating the current turn
        :param maxTurns: int indicating the maximum number of turns in the game
        :return: list of lists, each of which specifies a move for a given piece
        """

        pieces = self.board.findMyPieces()
        moves = []

        # First turn? Locate starting pieces.
        if currTurn == 1:
            self.myStart = pieces[0]
            self.oppStart = self.board.findOpponentPieces()[0]
            moves.append([self.myStart, str(board.Action.STAY), 1])
            return moves

        # No longer the first turn, so more decisions to consider.
        for location in pieces:
            if location == self.myStart:
                # Decide what to do based on the strength of the piece.
                # This is weak, though - if something causes the "source"
                # piece to split and not leave a piece of itself behind,
                # this Bot will be in trouble!
                if self.board.getPieceStrength(location) >= 3:
                    moves.append([location, str(board.Action.SPLIT), 1])
                else:
                    moves.append([location, str(board.Action.STAY), 1])

            else:
                # This is a Runner piece.
                if self.isMoveRight(location):
                    moves.append([location, str(board.Action.RIGHT), 1])
                else:
                    moves.append([location, str(board.Action.LEFT), 1])

        return moves

    def isMoveRight(self, pos):
        """
        Determine whether or not moving 'Right' is appropriate for a piece
        at the given location. This mainly comes down to determining whether
        this Bot started on the right or left half of the board, and using
        that information to decide what the proper interval is.
        :param pos: int indicating a position on the board
        :return: bool indicating if the piece at pos should move right
        """

        if self.myStart < self.oppStart:
            # Started on the left.
            if self.myStart < pos < self.oppStart:
                return True
            return False

        # Started on the right.
        if pos < self.oppStart or self.myStart < pos:
            return True
        return False

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'runnerBot' and the correct line numbers for
        the debug challenge.
        """
        if challenge == "debug":
            return ("f07c69b36fb8af7f8cdb699f38826f96"
                    "541c616e026a7ccaf410a8819aab0a8d")
        elif challenge == "database":
            return ("f06f4d7e7f1c1cbe06297e8b0ad60dae"
                    "3ea98674af6e8834dc89313cce657da8")

        return "Default Bot Signature :("
