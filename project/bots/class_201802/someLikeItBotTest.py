import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import someLikeItBot    # The bot being tested

from unittest.mock import Mock

import BotTestCase

class someLikeItBotTestCase(BotTestCase.BotTestCase):
    
    def setUp(self):
        # Common setup for the tests in this class.
        
        self.bot = someLikeItBot.SomeLikeItBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Check that bot is returned from the getBot() function.
        
        self.assertNotEqual(None, someLikeItBot.getBot())
        
    def testKnownMove(self):
         # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))  
        results = json.loads(resultsRaw)
        
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])      

    def testKnownJumpMove(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSomeLikeItBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), results[0][1])
        
        # The jump distance should equal half the piece strength.
        self.assertEqual(5.5, results[0][2])
        
    def testKnownBadInput(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSomeLikeItBotBadInput.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        with self.assertRaises(KeyError):
            self.bot.makeMoves(state.encode("utf-8"))
            
    def testMockSumSquareFunction(self):
        # checks that sumSquares works even though squares doesn't exist right now
        # a nice excuse to use the mock functionality
        
        val = [0,1,4,9,16]
        def square_side_effects(i):
            return val[i]
        
        self.bot.square = Mock(side_effect = square_side_effects)
        
        self.assertEqual(1, self.bot.sumSquares(1))
        self.assertEqual(5, self.bot.sumSquares(2))
        self.assertEqual(14, self.bot.sumSquares(3))
        self.assertEqual(30, self.bot.sumSquares(4))
        with self.assertRaises(IndexError):
            self.bot.sumSquares(5)
            
    def testLeftThenRight(self):
        # checks that the LeftThen right function returns the correct values
        with (self.subTest("Check basic left then right")):
            self.assertTrue(self.bot.leftThenRight(40, 50))
        with (self.subTest("Check basic right then left")):
            self.assertFalse(self.bot.leftThenRight(60, 50))
        with (self.subTest("Check wrapping left then right")):
            self.assertTrue(self.bot.leftThenRight(90, 5))
        with (self.subTest("Check wrapping right then left")):         
            self.assertFalse(self.bot.leftThenRight(5, 95))

    def testNearby(self):
        # checks that the nearby function works
        with (self.subTest("Check nearby to right")):
            self.assertTrue(self.bot.nearby(10,15,6))
        with (self.subTest("Check nearby to left")):
            self.assertTrue(self.bot.nearby(20,15,6))
        with (self.subTest("Check nearby to right wraparound")):
            self.assertTrue(self.bot.nearby(1,99,6))
        with (self.subTest("Check nearby to left wraparound")):
            self.assertTrue(self.bot.nearby(99,1,6))   
        with (self.subTest("Check not nearby to right")):
            self.assertFalse(self.bot.nearby(10,30,6))
        with (self.subTest("Check not nearby to left")):
            self.assertFalse(self.bot.nearby(50,15,6))
        with (self.subTest("Check not nearby to right wraparound")):
            self.assertFalse(self.bot.nearby(1,90,6))
        with (self.subTest("Check not nearby to left wraparound")):
            self.assertFalse(self.bot.nearby(99,10,6)) 
            
    def testRespondsToStrength(self):
        # Checks bot retreats near strong opponent
        
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSomeLikeItBotStrongEnemy.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))

        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])

        self.assertEqual(3, len(results[1]))
        self.assertEqual(33, results[1][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])
        
        
        
        