
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import botOutOfHell   # The Bot being tested.

import BotTestCase


class randomBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = botOutOfHell.EleanorBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, botOutOfHell.getBot())

    def testStartMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir + '/eleanorBotTests/',
                                      'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownOutwardMove(self):
        # Testing that the expected output comes from a move that
        # should include pieces moving left and right.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir + '/eleanorBotTests/',
                                      'testBoardStateForEleanorBotMove.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[0]))
        # The first piece (on tile 1) should have moved right.
        self.assertEqual(1, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
        # The second piece (on tile 3) should have moved left.
        self.assertEqual(3, results[1][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])

    def testSplitMove(self):
        # Testing that the expected output comes from a move that
        # should involve a piece splitting.

        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir + '/eleanorBotTests/',
                                      'testBoardStateForEleanorBotSplit.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        # The only piece (on tile 1) should have split.
        self.assertEqual(1, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])

    def testCentreOfPiecesWrapAroundEdge(self):
        # Testing that we get the expected result from the centreOfPieces
        # method when wrapping aroung the edge.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir + '/eleanorBotTests/',
                                      'testBoardStateForEleanorBotCentre.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        # The only piece (on tile 3) should have moved left 
        self.assertEqual(3, results[0][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[0][1])

    def testDebugSignature(self):
        # Check that the expected signature is returned for the debug challenge.

        sig = botOutOfHell.EleanorBot.getSignature('debug')
        self.assertTrue(sig.endswith("502f572c"))

    def testDatabaseSignature(self):
        # Check that the expected signature is returned for the debug challenge.

        sig = botOutOfHell.EleanorBot.getSignature('database')
        self.assertTrue(sig.endswith("718f2cdf"))
