"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomBot()


class RandomBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""
   
    
    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []
        
        for location in pieces:
            arg =1
            strength = self.board.getPieceStrength(location)
            if strength != 1 and strength%2 == 1 :
                move = GameBoard.Action.SPLIT
            else:
                move =  GameBoard.Action.STAY  #random.choice(list(GameBoard.Action))
            
            # # Most move commands don't need an argument, but the jump ones do.
            # # Just have them use up half their strength while jumping.
            
            # # if (move == GameBoard.Action.JUMP_LEFT
            #         or move == GameBoard.Action.JUMP_RIGHT):
            #     arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            
            moves.append([location, str(move), arg])

        
            #print(moves)
        return moves

    @staticmethod
    def getSignature():
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
