
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import iBotthatyoulookgoodonthedancefloor      # The Bot being tested.

import BotTestCase


class iBotthatyoulookgoodonthedancefloorTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = iBotthatyoulookgoodonthedancefloor.iBotthatyoulookgoodonthedancefloor()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, iBotthatyoulookgoodonthedancefloor.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), results[0][1])
        self.assertEqual(0, results[0][2])

    def testKnownJumpMove(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForRandomBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(2, results[0][2])

    def testSignature(self):
        # Check that the expected signature for debug is returned.

        sig = iBotthatyoulookgoodonthedancefloor.iBotthatyoulookgoodonthedancefloor.getSignature('debug')
        self.assertTrue(sig.endswith("a862419dc5b741b729"))

    def testSignature2(self):
	# Check that the expected signature for database is returned
        sig2 = iBotthatyoulookgoodonthedancefloor.iBotthatyoulookgoodonthedancefloor.getSignature('database')
        self.assertTrue(sig2.endswith("0671e89cfbf3a982cf"))

    def testSignature3(self):
	# Check that when signature function is called incorrectly it returns nothing
        sig3 = iBotthatyoulookgoodonthedancefloor.iBotthatyoulookgoodonthedancefloor.getSignature('testnonsense')
        self.assertEqual(sig3,"")


