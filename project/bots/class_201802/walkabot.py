"""
This is walkabot
It's not very good
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return Walkabot()


class Walkabot(BotBase.BotBase):
    """This Bot alternates between staying and jumping left once. If it has strength 10 it will split """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        
        """

        pieces = self.board.findMyPieces()
        moves = []
        
        arg = 1
        for location in pieces:
            if self.board.getPieceStrength(location) == 10:
                move = GameBoard.Action.SPLIT
            elif currTurn%2 == 1:
                move = GameBoard.Action.JUMP_LEFT
            else:
                move = GameBoard.Action.STAY

            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
