"""
This is a basic Bot, which has a basic strategy,
so I can concentrate on getting my head round doing everything else.
"""


import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return BigBot()


class BigBot(BotBase.BotBase):
    """This bot stays if strength is less than 5.
    If >5 and even then it randomly jumps left or right with half strength.
    If >5 and odd it splits.
    """

    def generateMoves(self, currTurn, maxTurns):
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            arg = 1
            if strength < 5:
                move = GameBoard.Action.STAY
            elif strength % 2 == 0:
                move = random.choice([GameBoard.Action.JUMP_LEFT,
                                      GameBoard.Action.JUMP_RIGHT])
                arg = int(self.board.getPieceStrength(location) / 2)
            else:
                move = GameBoard.Action.SPLIT
            moves.append([location, str(move), arg])

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'BigBot' and the correct answers for
        the debug and database challenges:
        debug: '165,185'
        database: 'Bond Street,Regent Street'
        """
        if challenge == 'debug':
            return("e03724030f4dc1ad3ca34791a11a024b"
                   "25ca642a244131f9b464d59c02cb8b32")
        elif challenge == 'database':
            return("ef2ec4f4941101e09f95b9ed4253316b"
                   "479f6fce89abc53a308de337920f21a4")
        else:
            return ""
