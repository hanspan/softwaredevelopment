
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import sarahf1Bot      # The Bot being tested.

import BotTestCase

class sarahf1BotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = sarahf1Bot.Sarahf1Bot()
        self.bot.setup(self.getGameConfig())
    
    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, sarahf1Bot.getBot())
        
    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownJumpMove(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForRandomBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_RIGHT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(2, results[0][2])

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = sarahf1Bot.Sarahf1Bot.getSignature()
        self.assertTrue(sig.endswith("bbcaa4e39610"))
       
    def testgenerateMoves_forrandom(self):
        # Check bot generates moves we would expect when piece is not on a special tile (which would be the same as testing randomBot's moves
        
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForRandomBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_RIGHT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(2, results[0][2])
        
    def testgenerateMoves_forspecial(self):
        # Check bot generates moves we would expect when piece is on a special tile 
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSarahf1Bot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(10, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
    