"""
This is an update to the basic Bot randomBot.py; it identifies the pieces
it has on the game board, and then picks a move for each piece.

Weak pieces are pieces with strength under 15, at the start of the game
weak pieces are more likely to move left or right to avoid detection,
later in the game pieces are more likely to stay to turn into strong pieces

weak pieces will try and move away from middle of the board to avoid detection
at the start of the game

Strong pieces are pieces with strength over or equal to 15, at the start of
the game strong pieces will split or jump or stay, as we progress further
into the game strong pieces will be more likely to split

strong pieces will try to move into the middle of the board

All Pieces stay to gain strength for first 0.01% of turns

test

"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():

    """This function needs to return an instance of this Bot's class."""

    return elliBot()


def getMoveOptionsStrongPiece(turn_percentage):

    """This function creates a list of the valid moves given the game state
    for a strong piece"""

    if turn_percentage < 0.3:
        possible_moves = [GameBoard.Action.JUMP_RIGHT]
        possible_moves.append(GameBoard.Action.JUMP_LEFT)
        possible_moves.append(GameBoard.Action.STAY)
        possible_moves.append(GameBoard.Action.SPLIT)

    elif turn_percentage >= 0.3 and turn_percentage < 0.6:
        possible_moves = [GameBoard.Action.JUMP_RIGHT]
        possible_moves.append(GameBoard.Action.JUMP_LEFT)
        possible_moves.append(GameBoard.Action.STAY)
        # once we hit a third of the way into the game
        # have a higher chance of splitting
        i = 0
        while i < 5:
            possible_moves.append(GameBoard.Action.SPLIT)
            i += 1

    else:
        possible_moves = [GameBoard.Action.JUMP_RIGHT]
        possible_moves.append(GameBoard.Action.JUMP_LEFT)
        possible_moves.append(GameBoard.Action.STAY)
        # once we hit two thirds of the way into the game
        # have an even higher chance of splitting
        i = 0
        while i < 10:
            possible_moves.append(GameBoard.Action.SPLIT)
            i += 1

    return possible_moves


def getMoveOptionsWeakPiece(turn_percentage, location):

    """This function creates a list of the valid moves given the game state
    for a weak piece"""

    if turn_percentage < 0.3:
        # at the start move away from the center of the board
        if location < 50:
            possible_moves = [GameBoard.Action.LEFT]
        else:
            possible_moves = [GameBoard.Action.RIGHT]

        i = 0
        while i < 2:
            possible_moves.append(GameBoard.Action.STAY)
            i += 1
    elif turn_percentage >= 0.3 and turn_percentage < 0.6:
        # once we hit a third of the way into the game
        # have a higher chance of splitting
        possible_moves = [GameBoard.Action.LEFT]
        possible_moves.append(GameBoard.Action.RIGHT)
        i = 0
        while i < 4:
            possible_moves.append(GameBoard.Action.STAY)
            i += 1
    else:
        # once we hit two thirds of the way into the game
        # have an even higher chance of staying
        possible_moves = [GameBoard.Action.LEFT]
        possible_moves.append(GameBoard.Action.RIGHT)
        i = 0
        while i < 6:
            possible_moves.append(GameBoard.Action.STAY)
            i += 1

    return possible_moves


class elliBot(BotBase.BotBase):

    """ This is a class docstring for this bot """

    def generateMoves(self, currTurn, maxTurns):

        """Locate all of our pieces on the board, and then make a move for
        each one."""

        pieces = self.board.findMyPieces()
        moves = []

        for loc in pieces:

            jump = 1
            turn_percent = float(currTurn) / maxTurns

            # if within the first 1% of turns stay to grow

            if turn_percent < 0.01:
                move = GameBoard.Action.STAY

            # strong pieces split or jump

            if self.board.getPieceStrength(loc) >= 15:

                jmp_or_splt_or_sty = getMoveOptionsStrongPiece(turn_percent)
                move = random.choice(jmp_or_splt_or_sty)

            # weak pieces dont split or jump

            else:

                dont_split_or_jump = getMoveOptionsWeakPiece(turn_percent, loc)
                move = random.choice(dont_split_or_jump)

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.

            if move in [GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT]:
                jump = int(self.board.getPieceStrength(loc) / 2)

        # Store the move. Note the str() entry: we need a form that
        # can be passed to the server, and this turns the "thing of type
        # Action" into "a string", which is easier for the JSON library
        # to understand.

        moves.append([loc, str(move), jump])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the signature of elliBot for the database exercise and
        debug exercise;
        challenge variable passed into which exercise
        """
        if challenge == 'database':
            return \
             "19304b0ef088faeb40dd1ff1d63ce6bea3e229c82e4ae8513732dfff33638f40"
        elif challenge == 'debug':
            return \
             "331091e880dd62430ec479d0a2c40ddc6467cf6460ff51b814f4f98af61e6355"
