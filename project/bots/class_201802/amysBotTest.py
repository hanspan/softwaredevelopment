
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import amysBot      # The Bot being tested.

import BotTestCase


class amysBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = amysBot.AmysBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, amysBot.getBot())

    def testKnownWeakStayMove(self):
        # Testing that the expected output comes from a move that
        # should include a stay move for a weak piece.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,'amysBotTests','testBoardStateForWeakSTAYAmyBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownWeakRightMove(self):
        # Testing that the expected output comes from a move that
        # should include a right move for a weak piece.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'amysBotTests','testBoardStateForWeakRIGHTAmyBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
        self.assertEqual(1, results[0][2])
        
    def testKnownStrongSplitMove(self):
        # Testing that the expected output comes from a move that
        # should include a split move for a strong piece.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'amysBotTests','testBoardStateForStrongSPLITAmyBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownStrongRightMove(self):
        # Testing that the expected output comes from a move that
        # should include a right move for a strong piece.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'amysBotTests','testBoardStateForStrongRIGHTAmyBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
        self.assertEqual(1, results[0][2])
        
    def testSignatureDebug(self):
        # Check that the expected signature for "debug" is returned.

        sig = amysBot.AmysBot.getSignature("debug")
        self.assertTrue(sig.endswith("0d5a4e8b2cf3"))
        
    def testSignaureDatabase(self):
        # Check that the expected signature for "database" is returned.
        
        sig = amysBot.AmysBot.getSignature("database")
        self.assertTrue(sig.endswith("26273a359480"))

    def testSignaureOther(self):
        # Check that the expected signature for any other string is returned.
        
        sig = amysBot.AmysBot.getSignature("anystring")
        self.assertTrue(sig.endswith("000000000000"))

        
        

   
       
   
        
        
       
