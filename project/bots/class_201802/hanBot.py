"""
This is a hannah Bot; she is trying to grow real big in both width and strength.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomBot()


class RandomBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            arg = 1
            strength = self.board.getPieceStrength(location)

            if currTurn < (maxTurns/4):
                #in first quarter gaining strength

                if (strength > 1) and (strength % 2 == 1): #odd
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY

            else:
                #in the rest of the time, follow a slightly different strategy

                if strength > 7:
                    #if strong get stronger
                    move = GameBoard.Action.STAY
                elif (strength > 4) and (strength % 2 == 1):
                    #if medium and odd split.
                    move = GameBoard.Action.SPLIT
                else:
                    # if weak conserve strength
                    move = GameBoard.Action.STAY

            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature():
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
