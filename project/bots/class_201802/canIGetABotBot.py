"""
This is canIGetABotBot; it identifies the pieces it has on the game board,
and just makes a random move unless it is on a special tile where it will
stay or split with equal probability.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return CanIGetABotBot()


class CanIGetABotBot(BotBase.BotBase):
    """This is canIGetABotBot; it identifies the pieces it has on the game board,
       and just makes a random move unless it is on a special tile where it
       will stay or split with equal probability."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # Stay or split on special tiles
            if location in (10, 89, 30, 69, 40, 59):
                random_probability = random.random()
                if random_probability <= 0.5:
                    move = GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.SPLIT
            # Otherwise make a random move
            else:
                move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'canIGetABotBot' with either the correct
        line numbers for the debug challenge or the correct street
        names for the database challenge.
        """

        if challenge == 'debug':
            return \
                "5f577e4b81f5c30d7a5013cdb2f4c3684284f1ae9"\
                "6e3d7dfeee5dceb3b4bdbf2"

        elif challenge == 'database':
            return \
                "3d2c61fcdca7574b46603d06a385df8d434e0cd90f"\
                "f16494d5cd4edc69dd2ada"

        else:
            return ''
