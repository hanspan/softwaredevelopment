"""
flyingBotsman - the next generation in stupidly-named mediocre attack bots

flyingBotsman starts by building up strength, then goes on an all-out assault
by splitting into two separate streams.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

LEFT = -1
RIGHT = 1


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return FlyingBotsman()


def sideof(point, start, size):
    """A helper funciton that determines whether a point is to the left of,
       or right of, a different point. This takes into account the
       wrap-around, since [0,100)/(0~100) is homeomorphic to S^1. """
    if point <= start:
        ldist = start - point
        if ldist <= size / 2:
            return LEFT
        else:
            return RIGHT
    else:
        ldist = point - start
        if ldist <= size / 2:
            return RIGHT
        else:
            return LEFT


class FlyingBotsman(BotBase.BotBase):
    """Our bot class"""
    startIndex = 0

    def generateMoves(self, currTurn, maxTurns):
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            # We need to find the start point for future reference
            # (this is probably stored in a variable somewhere already)
            if currTurn == 1:
                self.startIndex = location

            if currTurn < 60:
                move = self.findEarlyMoves(currTurn, maxTurns, location)
            else:
                # After a while, we split streams and ATTACK
                move = self.findLateMoves(currTurn, maxTurns, location)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            # We never jump, so we just pass arg as the constant "1"
            moves.append([location, str(move), 1])

        return moves

    def findEarlyMoves(self, currTurn, maxTurns, location):
        """ Near the beginning, the strategy is to build up strength """

        strength = self.board.getPieceStrength(location)

        # The indentation is suuuuper dumb here, but it's the only thing
        # that appears to satisfy PEP8 and pylint
        if strength > 1:
            if (self.board.getPieceStrength((location + 1) % 100) == 0 or
                    self.board.getPieceStrength((location + 99) % 100) == 0):
                move = GameBoard.Action.SPLIT

            elif (self.board.getPieceStrength((location + 1) % 100) != 0 and
                  self.board.getPieceStrength((location + 99) % 100) != 0):
                move = GameBoard.Action.STAY
        else:
            move = GameBoard.Action.STAY

        return move

    def findLateMoves(self, currTurn, maxTurns, location):
        """Finds moves late on. The plan is to split streams and attack"""

        if sideof(location, self.startIndex, self.board.boardSize) == LEFT:
            move = GameBoard.Action.LEFT
        else:
            move = GameBoard.Action.RIGHT

        return move

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'flyingBotsman' and the correct answer for either
        the debug or the database challenge.
        """

        if challenge == "database":
            return \
                ("4275b7d5fc6868b486321fb92567f74e"
                 "5c2090000862c09445e43e72b4a1255d")
        elif challenge == "debug":
            return \
                ("80501ced5b4fa04a72b5072f122b133e"
                 "f360959452da8912cca8c23625bb470c")


if __name__ == '__main__':
    pass
