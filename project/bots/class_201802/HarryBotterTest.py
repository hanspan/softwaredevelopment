import os
import unittest     
import json         
import GameBoard     
import HarryBotter	     
import BotTestCase

from unittest.mock import Mock

class HarryBotterTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.
        self.bot = HarryBotter.HarryBotter()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Check that bot is returned from the getBot() function.
        self.assertNotEqual(None, HarryBotter.getBot())

    # def testKnownMove(self):
        # To Be Completed
