"""
Pol_bot - yeah it's not very good
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PolBot()


class PolBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Our bot class """
        
        """Locate pieces and make a move for each one"""
        pieces = self.board.findMyPieces()
        moves = []

        """The next move is based on the strength of the tile in question"""
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            
            """If a bot is not surrounded by the same type of bot, we split.
               Otherwise, we stay"""
            if (self.board.getPieceStrength((location + 1)%100) == 0 or \
                self.board.getPieceStrength((location + 99)%100) == 0) \
                and strength > 1:
                move = GameBoard.Action.SPLIT
            
            elif (self.board.getPieceStrength((location + 1)%100) != 0 and \
                   self.board.getPieceStrength((location + 99)%100) != 0):
                move = GameBoard.Action.STAY
            else:
                move = random.choice(list(GameBoard.MoveReduced))




            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
