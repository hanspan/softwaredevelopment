import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard 
import edwardhBot     

import BotTestCase # Base class will be a BotTestCase

class edwardhBotTestCase(BotTestCase.BotTestCase):
    
    def setUp(self):
        
        # initiliases an edwardhBot for testing
        
        self.bot = edwardhBot.edwardhBot()
        self.bot.setup(self.getGameConfig())
        
    def testGetBot(self):
        # checks that edwardhBot does return a Bot when called.
        self.assertNotEqual(edwardhBot.getBot(), None)

                
    def testKnownMove(self):
        
        
        # This finds a standard state for the game board and asks the Bot to generate a
        # move. We then check the Bot has applied the correct logic for making it's move
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateedwardhBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        self.assertNotEqual(resultsRaw, None);
        results = json.loads(resultsRaw)
        print(results)
        
        boardStateJson = json.loads(state)
        # checks the bot returns at least one move
        self.assertGreater(len(results), 0)
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(str(GameBoard.Action.STAY), results[1][1])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[2][1])
        self.assertEqual(str(GameBoard.Action.STAY), results[3][1])
        self.assertEqual(results[0][2], 1)
            
    def testsumSquares(self):
        
        from unittest.mock import create_autospec
        
        values= {0: 0, 1: 1, 2: 4, 3: 9, 4: 16}
        
        def square_side_effect(num):
            return values[num]
        
        self.bot.square = unittest.mock.MagicMock(name = 'square')
        self.bot.square.side_effect = square_side_effect
        
        self.assertEqual(1, self.bot.sumSquares(1))
        self.assertEqual(5, self.bot.sumSquares(2))
        self.assertEqual(14, self.bot.sumSquares(3))
        self.assertEqual(30, self.bot.sumSquares(4))
        with self.assertRaises(KeyError):
            self.bot.sumSquares(5)
            
            
        