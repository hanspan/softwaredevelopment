"""
This is a super basic Bot, it picks a random number between 0 and it's current 
strength and jumps randomly left and right.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomBot()


class RandomBot(BotBase.BotBase):
    """This Bot just jumps a random amount everytime in a random direction!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """
        pieces = self.board.findMyPieces()
        moves = []
        for location in pieces:
            random_bot = random.randint(0,1)
            if random_bot == 0:
                move = self.bouncyBot(currTurn, location)[0]
                arg = self.bouncyBot(currTurn, location)[1]
            elif random_bot == 1:
                move = self.lazyBot()
                arg = 1
            
            
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    def bouncyBot(self, currTurn, location):
        """ This function makes a bot jump a random number of spaces in a 
        random direction for a turn and then rest for a turn.
        """
        if currTurn % 2 == 1:
            move = "Stay"
            arg = 1
        else:
            strength = self.board.getPieceStrength(location)
            jump = random.randint(0, strength-1)
            direction = random.randint(0,1)
            if direction % 2 == 0:
                move = "Jump_Left"
                #print("Jump")
                arg = jump
            elif direction % 2 == 1:
                move = "Jump_Right"
                #print("Jump")
                arg = jump
        return (move, arg)
            
    def lazyBot(self):
        """This bot only moves 1 in 5 times, randomly left or right
        """
        random_int = random.randint(0,9)
        if random_int == 0:
            move = "Left"
        elif random_int == 5:
            move = "Right"
        else:
            move = "Stay"
        return(move)
    
    def greedyBot(self, location):
        """This bot moves to the growth tiles and sits there, growing and splitting
        """
        growth = list(self.board.growthTiles)
        strength = self.board.getPieceStrength(location)
        #If the bot is already on a growth tile it will either split if it
        #is strong or it will stay there and grow
        if location in growth:
            if strength > 10:
                move = "Split"
                arg = 1
            else:
                move = "Stay"
                arg = 1
        #finds the closest growth tile
        else:
            closest = 50
            for tile in growth:
                distance = min(abs(location - tile), abs(100-(location-tile)))
                if distance < closest:
                    target = tile
            if closest < self.board.getPieceStrength(location)/2:
                if target < location:
                    move = "Jump_Left"
                    arg = closest
                else:
                    move = "Jump_Right"
                    arg = closest
            elif target < location:
                move = "Left"
                arg = 1
            else:
                move = "Right"
                arg = 1
        return (move, arg)

    @staticmethod
    def getSignature():
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
