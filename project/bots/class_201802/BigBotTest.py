
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import BigBot      # The Bot being tested.

import BotTestCase


class BigBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = BigBot.BigBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, BigBot.getBot())

    def testKnownStayMove(self):
        # Testing that the expected output comes from a move that
        # should be a stay.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForBigBot1.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownSplitMove(self):
        # Testing that the expected output comes from a move that
        # should be a split.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForBigBot2.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])

    def testKnownJumpMove(self):
        # Testing that the expected output comes from a move that
        # should be a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForBigBot3.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT or GameBoard.Action.JUMP_RIGHT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(3, results[0][2])
        
    def testSignatureDebug(self):
        # Check that the expected signature is returned.
        sig = BigBot.BigBot.getSignature('debug')
        self.assertTrue(sig.endswith("59c02cb8b32"))
        
    def testSignatureDatabase(self):
        # Check that the expected signature is returned.
        sig = BigBot.BigBot.getSignature('database')
        self.assertTrue(sig.endswith("337920f21a4"))
     
    def testSignatureNone(self):
        # Check that the expected signature is returned.   
        sig = BigBot.BigBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith(""))
