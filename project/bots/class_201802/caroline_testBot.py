"""
Caroline's first bot. decides thinkgs slightly less randomly than randomBot
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return caroline_testBot()


class caroline_testBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for i, location in enumerate(pieces):
            if (self.board.getPieceStrength(location) > 20):
                move = GameBoard.Action.SPLIT
            else:
                r = random.randint(0, 4)
                if r == 0:
                   move =  GameBoard.Action.LEFT
                elif r==1:
                   move =  GameBoard.Action.RIGHT
                else:
                   move =  GameBoard.Action.STAY
            
            arg = 1
            # Most move commands don't need an argument, but the jump ones do.

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            #print [location, str(move), arg]
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature():
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
