import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return edwardhBot()


class edwardhBot(BotBase.BotBase):

    """This bot alternates between splitting and staying, dependant on the strengths of the pieces."""


    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        Decides between two options, SPLIT when the piece can leave something behind. Otherwise it stays
        """
        arg = 1
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            arg = 1
            strength = int(self.board.getPieceStrength(location))
            if strength % 2 == 1 and strength > 2:
                move = GameBoard.Action.SPLIT
            else:
                move = GameBoard.Action.STAY
            moves.append([location, str(move), arg])
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.

        return moves
    
    
        
    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
