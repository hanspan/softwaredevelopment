'''The module defining SirDavidAttenbot_aka_BottyMcBotFace
for battle in BvB.'''

import random
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return SirDavidAttenbotAKABottyMcBotFace()


class SirDavidAttenbotAKABottyMcBotFace(BotBase.BotBase):
    '''If a piece is low strength then it tries to fortify its
    position by splitting/staying. If a piece is high strength
    then it looks for the closest piece it can successfully
    attack that is not being attacked by another piece and
    attacks it. If there is nothing nearby it can attack then
    it fortifies its position as if it were weak.'''
    def __init__(self):
        '''Initialises the bot.'''
        super().__init__()
        self.rNG = random.Random()

        self._already_attacked = {}

    def _getPlayerPieceStrengths(self, pieces):
        '''Looks at the strengths of the pieces in positions "pieces"'''
        strengths = {}
        for piece in pieces:
            strengths[piece] = self.board.getPieceStrength(piece)

        return strengths

    def _canWinBattle(self, my_piece, my_strength, op_pieces, op_strengths):
        '''A function that searches for the nearest opponent piece that
        my_piece can successfully beat, it randomly looks either left
        first or right first.'''

        for counter in range(50):

            random_number = self.rNG.randint(0, 1)
            opponent_info = [op_pieces, op_strengths]
            if random_number == 0:
                if self._canWinBattle_searchLeft(my_piece,
                                                 my_strength,
                                                 counter,
                                                 opponent_info):
                    return True, counter, "L"
                if self._canWinBattle_searchRight(my_piece,
                                                  my_strength,
                                                  counter,
                                                  opponent_info):
                    return True, counter, "R"
            else:
                if self._canWinBattle_searchRight(my_piece,
                                                  my_strength,
                                                  counter,
                                                  opponent_info):
                    return True, counter, "R"
                if self._canWinBattle_searchLeft(my_piece,
                                                 my_strength,
                                                 counter,
                                                 opponent_info):
                    return True, counter, "L"

        return False, None, None

    def _canWinBattle_searchLeft(self,
                                 my_piece,
                                 my_strength,
                                 counter,
                                 opponent_info):
        '''Searches for enemies I can conquer to the left'''
        op_pieces, op_strengths = opponent_info
        op_piece = (my_piece - counter) % 100
        if op_piece in self._already_attacked:
            return False
        if op_piece in op_pieces:
            if my_strength > op_strengths[op_piece] + counter + 2:
                self._already_attacked[op_piece] = None
                return True
        return False

    def _canWinBattle_searchRight(self,
                                  my_piece,
                                  my_strength,
                                  counter,
                                  opponent_info):
        '''Searches for enemies I can conquer to the right'''
        op_pieces, op_strengths = opponent_info
        op_piece = (my_piece + counter) % 100
        if op_piece in self._already_attacked:
            return False
        if op_piece in op_pieces:
            if my_strength > op_strengths[op_piece] + counter + 2:
                self._already_attacked[op_piece] = None
                return True
        return False

    @staticmethod
    def _strategyFortifyBase(currTurn):
        '''This strategy is designed to generate quick growth,
        staying for two moves and then splitting.'''
        if currTurn % 3 == 0:
            move = GameBoard.Action.SPLIT
        else:
            move = GameBoard.Action.STAY
        return move

    def _strategyATTACK(self,
                        moi,
                        them,
                        arg,
                        currTurn):
        '''This strategy checks if I can launch a successful attack
        and if strong enough performs it.If there is no attack then
        it fortifies unless it is super strong, in which case it reseeds'''
        my_piece, my_strength = moi
        op_pieces, op_strengths = them

        aTTACK = self._canWinBattle(my_piece,
                                    my_strength,
                                    op_pieces,
                                    op_strengths)
        if my_strength <= 5:
            move = self._strategyFortifyBase(currTurn)
        elif aTTACK[0]:
            if aTTACK[2] == "R":
                move = GameBoard.Action.JUMP_RIGHT
            elif aTTACK[2] == "L":
                move = GameBoard.Action.JUMP_LEFT
            arg = aTTACK[1]
        elif my_strength <= 22:
            move = self._strategyFortifyBase(currTurn)
        else:
            move, arg = self._strategyReseed()

        return move, arg

    def _strategyReseed(self):
        '''This currently acts radomly, splitting and
        moving to spread some of the equity.'''
        # TO DO - Make this better so I look for nearby regions of low
        # equity and move to one of them to better spread equity
        random_number = self.rNG.randint(0, 2)
        if random_number == 0:
            move = GameBoard.Action.SPLIT
        elif random_number == 1:
            move = GameBoard.Action.LEFT
        else:
            move = GameBoard.Action.RIGHT

        return move, 1
#         arg = 1
#         random_number = self.rNG.randint(0,1)
#         for i in range(10):
#             if random_number == 0:
#                 if _strategyReseed_searchLeft():
#                     pass
#                 if _strategyReseed_searchRight():
#                     pass
#             else:
#                 if _strategyReseed_searchRight():
#                     pass
#                 if _strategyReseed_searchLeft():
#                     pass
#         return move, arg

    def generateMoves(self, currTurn, maxTurns):
        '''The main move generation function of the bot.
        Inititally I fortify in order to build equity and
        then start to attack where possible'''
        moves = []

        my_pieces = self.board.findMyPieces()
        my_strengths = self._getPlayerPieceStrengths(my_pieces)

        op_pieces = self.board.findOpponentPieces()
        op_strengths = self._getPlayerPieceStrengths(op_pieces)

        self._already_attacked = {}

        for my_piece in my_pieces:
            arg = 1
            move = GameBoard.Action.STAY
            my_strength = my_strengths[my_piece]

#             if currTurn < 50:
#                 move = self._strategyFortifyBase(currTurn)
#             else:
#                 move, arg = self._strategyATTACK(my_piece,
#                                                  my_strength,
#                                                  op_pieces,
#                                                  op_strengths,
#                                                  arg,
#                                                  currTurn)
            moi = [my_piece, my_strength]
            them = [op_pieces, op_strengths]
            move, arg = self._strategyATTACK(moi,
                                             them,
                                             arg,
                                             currTurn)

            moves.append([my_piece, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This returns the signature for the relevant challenge.
        """
        if challenge == "debug":
            return ("2ef293e5ed0c772ecb96db54847"
                    "15127219f01ff91d7a8f4cbbd50fa0164985b")
        elif challenge == "database":
            return ("f5d31d5fa9bc70ebdf96ffee572"
                    "2c2329b94fc9086b9d0ea1ee26b48a0b77748")
        else:
            return "Please select either debug or database."
