"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return michaelb1Bot()


class michaelb1Bot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""
    def __init__(self):
        self.fighters = []
        self.farmers = []
        self.near = 5
        self.farmer_to_fighter_prob = 0.1
        self.fighter_to_farmer_prob = 0.1
        self.ENOUGHSTRENGTH = 3

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """
        # each farmer can generate new farmer(s), which we set to empty
        # list at the start of each move
        self.new_farmers = []
        # locate all our pieces
        pieces = self.board.findMyPieces()

        # how we assign pieces to farmers or fighters depends on how many 
        # pieces we have:
        if len(pieces) < 5 :
            # if we have only a few pieces, make them all farmers
            self.farmers = self.board.findMyPieces()
            self.fighters = []
            # set our startpos if we only have one piece left
            if len(pieces) == 1:
                self.startpos = pieces[0]
        else:
            # if we have sufficient pieces, each keeps their state. if a farmer and a fighter 
            # merge, it becomes a fighter. Killed pieces are removed
            self.farmers = list(set(self.farmers) & set(self.board.findMyPieces())) 
            self.fighters = list(set(self.board.findMyPieces()) - set(self.farmers))
        
        # our moves are outsourced to the methods self.manage_farmers() and self.manage_fighters
        moves = self.manage_farmers(currTurn, maxTurns) + self.manage_fighters(currTurn, maxTurns)
        return moves
    
    def manage_farmers(self, currTurn, maxTurns):
        """
        method to return moves for each of the farmer units in self.farmers
        """
        num_farmers = len(self.farmers)
        farmer_moves = []
        self.new_farmers = []
        for farmer in self.farmers:
            move = ""
            if self.board.getPieceStrength(farmer) == 1:
                # if strength of 1, stay
                move = GameBoard.Action.STAY
                farmer_moves.append([farmer, str(move), 1])
            else:
                # farmer switches to a fighter with some random probability
                if random.random() < self.farmer_to_fighter_prob and num_farmers > 10:
                    # farmer is now a fighter
                    self.fighters.append(
                        self.farmers.pop(self.farmers.index(farmer)))
                else:
                    # farmer splits into two farmers if strength is odd 
                    # (but not 1, as that case is dealt with above), 
                    # and stays still if even 
                    strength_parity = self.board.getPieceStrength(farmer) % 2
                    move = [GameBoard.Action.STAY,
                            GameBoard.Action.SPLIT][strength_parity]
                    farmer_moves.append([farmer, str(move), 1])
            if move == GameBoard.Action.SPLIT:
                self.new_farmers += [farmer - 1, farmer + 1]
        # update the farmers list with any new farmers created
        self.farmers += self.new_farmers
        return(farmer_moves)
    
    def manage_fighters(self, currTurn, maxTurns):
        """
        returns the moves for the fighter units in self.fighters 
        (including any farmers that turn into fighters)
        """
        fighter_moves = []
        for fighter in self.fighters:
            # make fighter become a fighter with some random probability,
            # or if no opponent or own pieces nearby
            nearby_others = (set([(fighter - i) % 100 for i in range(self.near)]) | set([(fighter + i) % 100 for i in range(self.near)])) & (set(self.board.findMyPieces()) | set(self.board.findOpponentPieces()))
            if (random.random() < self.fighter_to_farmer_prob or nearby_others == set()):
                # fighter is now a farmer, and should stay for this turn
                move = GameBoard.Action.STAY
                arg = 0
                fighter_moves.append([fighter, str(move), arg])
                self.new_farmers.append(fighter)
            else:
                # get a move for the fighter
                fighter_move = self.getFighterMove(currTurn, maxTurns, fighter)
                fighter_moves.append(fighter_move)
        return(fighter_moves)

    def whichSideOfStart(self, unit):
        """
        determines which side of the start position a unit is
        """
        direction = ""
        if unit in [(self.startpos - i) % 100 for i in range(50)]:
            direction = "Left"
        else:
            direction = "Right"
        return(direction)

    def getFighterMove(self, currTurn, maxTurns, fighter):
        """
        Generate a move for a single fighter
        """
        # see which side of the start position we are on
        direction = self.whichSideOfStart(fighter)
        # choose a move randomly in that direction
        if direction == "Left":
            move = random.choice([GameBoard.Action.LEFT,
                                  GameBoard.Action.JUMP_LEFT])
        else:
            move = random.choice([GameBoard.Action.RIGHT,
                                  GameBoard.Action.JUMP_RIGHT])
        # Most move commands don't need an argument, but the jump ones do.
        # Just have them use up half their strength while jumping.
        arg = 0
        if (move in [GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT]):
            arg = int(self.board.getPieceStrength(fighter) / 2)
        return([fighter, str(move), arg])

    @staticmethod
    def getSignature(challenge="debug"):
        """
        No idea what this does.
        """
        return ""
