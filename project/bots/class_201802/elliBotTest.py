
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import elliBot        # The Bot being tested.

import BotTestCase
import random


class elliBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = elliBot.elliBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, elliBot.getBot())

    def testWeakMove(self):
        # This is a test to make sure that when pieces have weaker strength than 5 they dont split or jump 

        # read in initial board state with pieces of strength 4 
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState1ForElliBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        with (self.subTest("Testing against invalid moves.")):
            self.assertNotEqual(str(GameBoard.Action.SPLIT), results[0][1])
            self.assertNotEqual(str(GameBoard.Action.JUMP_LEFT), results[0][1])
            self.assertNotEqual(str(GameBoard.Action.JUMP_RIGHT), results[0][1])
  

    def testStrongMove(self):
        # Testing that when pieces have strength greater than 15 they only split or jump

        # Read in  board state with , and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState2ForElliBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        with (self.subTest("Testing against invalid moves.")):
            self.assertNotEqual(str(GameBoard.Action.STAY), results[0][1])
            self.assertNotEqual(str(GameBoard.Action.RIGHT), results[0][1])
            self.assertNotEqual(str(GameBoard.Action.LEFT), results[0][1])

    
    def testStrongStart(self):
        #test for correct possible moves for strong piece at start
        
        game_percentage=0.2
        possible_moves=elliBot.getMoveOptionsStrongPiece(game_percentage)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.JUMP_RIGHT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.JUMP_LEFT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.SPLIT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.STAY))

    def testStrongMid(self):
        #test for correct possible moves for strong piece in middle of game
        game_percentage=0.4
        possible_moves=elliBot.getMoveOptionsStrongPiece(game_percentage)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.JUMP_RIGHT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.JUMP_LEFT))
        self.assertEqual(5, possible_moves.count(GameBoard.Action.SPLIT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.STAY))

    def testStrongEnd(self):
        #test for correct possible moves for strong piece at end of game
        game_percentage=0.7
        possible_moves=elliBot.getMoveOptionsStrongPiece(game_percentage)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.JUMP_RIGHT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.JUMP_LEFT))
        self.assertEqual(10, possible_moves.count(GameBoard.Action.SPLIT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.STAY))

    def testWeakStart(self):
        #test for correct possible moves for weak piece at start
        game_percentage=0.2
        loc=random.randint(0,100)
        possible_moves=elliBot.getMoveOptionsWeakPiece(game_percentage,loc)

        self.assertEqual(2, possible_moves.count(GameBoard.Action.STAY))
        self.assertEqual(3,len(possible_moves))

    def testWeakMid(self):
        #test for correct possible moves for weak piece in middle of game
        
        game_percentage=0.4
        loc=random.randint(0,100)
        possible_moves=elliBot.getMoveOptionsWeakPiece(game_percentage,loc)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.RIGHT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.LEFT))
        self.assertEqual(4, possible_moves.count(GameBoard.Action.STAY))
        self.assertEqual(6,len(possible_moves))

    def testWeakEnd(self):
        #test for correct possible moves for weak piece at end of game
        
        game_percentage=0.7
        loc=random.randint(0,100)
        possible_moves=elliBot.getMoveOptionsWeakPiece(game_percentage,loc)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.RIGHT))
        self.assertEqual(1, possible_moves.count(GameBoard.Action.LEFT))
        self.assertEqual(6, possible_moves.count(GameBoard.Action.STAY))
        self.assertEqual(8,len(possible_moves))

    def testWeakLeft(self):
        game_percentage = 0.2
        loc=random.randint(0,49)
        possible_moves=elliBot.getMoveOptionsWeakPiece(game_percentage,loc)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.LEFT))

    def testWeakRight(self):
        game_percentage = 0.2
        loc=random.randint(50,100)
        possible_moves=elliBot.getMoveOptionsWeakPiece(game_percentage,loc)
        self.assertEqual(1, possible_moves.count(GameBoard.Action.RIGHT))

    def testSignatureData(self):
        # Check that the expected signature is returned.

        sig = elliBot.elliBot.getSignature('database')
        self.assertTrue(sig.endswith("fff33638f40"))
        
    def testSignatureDebug(self):
        # Check that the expected signature is returned
        
        sig = elliBot.elliBot.getSignature('debug')
        self.assertTrue(sig.endswith("98af61e6355"))




      
 

