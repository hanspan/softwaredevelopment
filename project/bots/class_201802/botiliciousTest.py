
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import botilicious      # The Bot being tested.

from unittest.mock import MagicMock

import BotTestCase


class patsyBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = botilicious.PatsyBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, botilicious.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownMoves(self):
        # Testing that the expected output comes in each possible scenario

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForPatsyBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        
        #Tests I have the correct number of pieces on the board
        self.assertEqual(7, len(results))
        
        #Tests for piece 1 at location 3 with strength 20. should jump left 3. 
        self.assertEqual(3, len(results[0]), "Not correct length of array")
        self.assertEqual(3, results[0][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), results[0][1], "Piece not making correct move")
        self.assertEqual(3, results[0][2], "Piece does not have correct argument")
        
        #Tests for piece 2 at location 11 with strength 20. Should split.
        self.assertEqual(3, len(results[1]), "Not correct length of array")
        self.assertEqual(10, results[1][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1], "Piece not making correct move")
        self.assertEqual(1, results[1][2], "Piece does not have correct argument")
        
        #Tests for piece 3 at location 25 with strength 5. Should stay (given random seed)
        self.assertEqual(3, len(results[2]), "Not correct length of array")
        self.assertEqual(25, results[2][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.STAY), results[2][1], "Piece not making correct move")
        self.assertEqual(1, results[2][2], "Piece does not have correct argument")

        #Tests for pieve 4 at location 89 (defence) with strength 10. Should Stay.
        self.assertEqual(3, len(results[3]), "Not correct length of array")
        self.assertEqual(89, results[3][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.STAY), results[3][1], "Piece not making correct move")
        self.assertEqual(1, results[3][2], "Piece does not have correct argument")

        #Tests for piece 6 at location 95 with strength 20. Should jump right 5.
        self.assertEqual(3, len(results[5]), "Not correct length of array")
        self.assertEqual(95, results[5][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.JUMP_RIGHT), results[5][1], "Piece not making correct move")
        self.assertEqual(5, results[5][2], "Piece does not have correct argument")

        #Tests for piece 7 at location 99 with strength 8 in growth. Should stay.
        self.assertEqual(3, len(results[6]), "Not correct length of array")
        self.assertEqual(99, results[6][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.STAY), results[6][1], "Piece not making correct move")
        self.assertEqual(1, results[6][2], "Piece does not have correct argument")
        
        #Tests for piece 5 at location 94 with strength 7.
        self.assertEqual(3, len(results[4]), "Not correct length of array")
        self.assertEqual(94, results[4][0], "Piece not in correct position")
        self.assertEqual(str(GameBoard.Action.STAY), results[4][1], "Piece not making correct move")
        self.assertEqual(1, results[4][2], "Piece does not have correct argument")
        

    def testDatabaseSignature(self):
        # Check that the expected signature is returned for the database challenge.
        sig = botilicious.PatsyBot.getSignature()
        self.assertTrue(sig.endswith("f4a6effb483e8"))

    def testDebugSignature(self):
        # Check that the expected signature is returned for the debug challenge.
        sig = botilicious.PatsyBot.getSignature(challenge = "debug")
        self.assertTrue(sig.endswith("539cf6a825681"))


        
































