"""
This is a basic Bot; it will sing a nursery rhyme.

It should not take much to outperform this bot...

Made by John
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RhymeBot()


class RhymeBot(BotBase.BotBase):
    """This Bot sits still gathering karma"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'sit still' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []
        
        rhyme = ['Hey diddle diddle,',
                 'The cat and the fiddle,',
                 'The cow jumped over the moon.',
                 'The little dog laughed,',
                 'To see such a sight,',
                 'And the dish ran away with the spoon.']

        for location in pieces:
            if currTurn < len(rhyme)+1:
                print(rhyme[currTurn-1])
            
            move = GameBoard.Action.STAY

            arg = 1

            moves.append([location, str(move), arg])

        return moves
