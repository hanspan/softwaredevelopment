"""
This is sarahf1Bot; it identifies the pieces it has on the game
board, and then just stays or splits on a special tile and otherwise stays with prob 0.5 and makes a random move with prob 0.5

"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return Sarahf1Bot()


class Sarahf1Bot(BotBase.BotBase):
    """This Bot just stays or splits on a special tile and otherwise stays with prob 0.5 and makes a random move with prob 0.5"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # try staying or splitting on special tiles
            if location in (10,89,30,69,40,59):
                r = random.random()
                print(r)
                if r<=0.5:
                    move = GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.SPLIT
            else:
                move = random.choice(list(GameBoard.Action))


            # Code to only split or jump if strength > 1
            '''
            if int(self.board.getPieceStrength(location))>1:
                move = random.choice(list(GameBoard.Action))
            else:
                move = random.choice([GameBoard.Action.STAY, GameBoard.Action.LEFT, GameBoard.Action.RIGHT])
            '''
            

            # Code to attempt to move onto a "special tile"
            '''
            if location in (9, 88, 29, 68):
                move = GameBoard.Action.RIGHT
            elif location in (11, 90, 31, 70):
                move = GameBoard.Action.LEFT
            else:
                move = random.choice(list(GameBoard.Action))
            '''


            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])


        return moves

          

    @staticmethod
    def getSignature():
        """
        This is the hash of 'sarahf1Bot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
