"""
This is ICantBelieveItsNotBotter;
it identifies the pieces it has on the game board,
and then does something hopefully better than random.

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return ICantBelieveItsNotBotter()


class ICantBelieveItsNotBotter(BotBase.BotBase):
    """This Bot grows more when on a special square """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        When on a special tile, it will grow stay and grow.
        When on a normal tile, it will jump left.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            # try staying or spliting on special tiles
            if location in (10, 89, 30, 69, 40, 59):
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.JUMP_LEFT

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if move == GameBoard.Action.JUMP_LEFT:
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        These are the hashes of 'ICantBelieveItsNotBotter'
        and the correct answers for
        the debug and database challenge.
        """
        if challenge == "debug":
            return \
                "75b33dcd4bf113f7576f4e49563dcbffcc5d9c"\
                "a5b61efa1086ca96ca7751d30e"

        elif challenge == "database":
            return \
                "f03137191d30c7bf1174dc7a5e8c3ebf1dda61"\
                "4eda377d807a569f28bab37631"

        else:
            return ""
