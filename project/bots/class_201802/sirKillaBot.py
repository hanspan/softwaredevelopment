"""
SirKillaBot applies a cyclically chosen moves to each of its game pieces.
"""

import BotBase  # All of the basic Bot functionality.
import GameBoard  # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """

    return SirKillaBot()


class CyclicStrategy:
    """
    Strategy class which cyclically iterates through a fixed list of game
    'strategyMoves', iterating a defined 'nextMoveStep' through the list in
    order to obtain the next move.
    """

    def __init__(self, startingMoveIdx=0, nextMoveStep=1):
        """
        Initialise the CyclicStrategy namespace.

        Args:
            startingMoveIdx: The position within the list of availableMoves
            from where to start generating moves.
            nextMoveStep: The number of cyclic positions to increment when
            generating the next move.
        """

        self.availableMoves = [GameBoard.Action.SPLIT, GameBoard.Action.STAY,
                               GameBoard.Action.RIGHT, GameBoard.Action.LEFT]
        self.currentMoveIdx = startingMoveIdx % len(self.availableMoves)
        self.nextMoveStep = nextMoveStep

    def move(self):
        """
        Increment the cyclic strategy currentMoveIdx, and return the move to be
        applied to all SirKillaBot pieces.
        """

        self.currentMoveIdx = (self.currentMoveIdx +
                               self.nextMoveStep) % len(self.availableMoves)
        return self.availableMoves[self.currentMoveIdx]

    def generateMoves(self, board):
        """
        Return a list of cyclically generated moves, for each of SirKillaBot
        game piece.
        """

        sirKillaBotPieces = board.findMyPieces()
        return [[pieceLocation, str(self.move()), 1] for pieceLocation in
                sirKillaBotPieces]


class SirKillaBot(BotBase.BotBase):
    """
    SirKillaBot computes game moves according to a movement strategy specified
    during the class initialisation.
    """

    def __init__(self):
        """
        Initialise a SirKillaBot instance, and specify SirKillaBot's movement
        strategy.
        """

        super().__init__()
        self.strategy = CyclicStrategy()

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        Return the list of moves generated by SirKillaBot's move strategy for
        each of SirKillaBot's game pieces.
        """

        return self.strategy.generateMoves(self.board)

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'sirKillaBot' and either the correct line numbers
        for the debug challenge, or the "green" properties owned by Top Hat.
        """

        signature = {
            'debug': "ff4040019e77bd6b9fe56facf2789f40f3ade620f5bb4b04eac0e51"
                     "974f89a71",
            'database': "6d7e4497ba60cf74afd62dc06117e3e816c74d9d66d0dbf6a8b2"
                        "e0aaafdab0e1"
        }

        return signature[challenge]
