"""
This bot uses the first few moves to jump out and 'gain' territory
before the other bot gets there. After that, it aims to remain
stationary at the core, building up strength, while the outer
bots fend off attackers in 'suicide' missions.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

def getBot():
    """This function needs to return an instance of this Bot's
class."""
    return HarryBotter()

class HarryBotter(BotBase.BotBase):
    """This bot uses the first few moves to jump out and 'gain'
territory before the other bot gets there. After that, it aims
to remain stationary at the core, building up strength, while
the outer bots fend off attackers."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Moves 1-10: split and jump out, to gain territory.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            if self.board.getPieceStrength(location) < 2:
                # Gain strength if weak
                move = GameBoard.Action.STAY
            elif currTurn == 0:
		# Initial split
                move = GameBoard.Action.SPLIT
            elif currTurn < 50:
		# First 50 moves to gain both strength and teritory
                if (int(75 + 1) < location) \
                | (int(25 + 1) < location <= 50):
                    # Branch out, depending on the side
                    move = [GameBoard.Action.JUMP_RIGHT, GameBoard.Action.SPLIT, \
                            GameBoard.Action.STAY, GameBoard.Action.JUMP_RIGHT][currTurn % 4]
                elif (int(75 - 1) > location) \
                | (int(25 - 1) > location >= 0):
                    # Likewise, if to the right
                    move = [GameBoard.Action.JUMP_LEFT, GameBoard.Action.SPLIT, \
                            GameBoard.Action.STAY, GameBoard.Action.JUMP_LEFT][currTurn % 4]
                else:
                    # If inside the boundary, split or stay
                    move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY]\
                    [currTurn % 2]
            else:
		# Else move similarly outside the boundary, and like sarahBot inside
                if (int(75 + 10) < location) \
                | (int(25 + 10) < location <= 50):
                    # Branch out, depending on the side
                    move = [GameBoard.Action.JUMP_RIGHT, GameBoard.Action.SPLIT, \
                            GameBoard.Action.STAY][currTurn % 3]
                elif (int(75 - 10) > location) \
                | (int(25 - 10) > location >= 0):
                    # Likewise, if to the right
                    move = [GameBoard.Action.JUMP_LEFT, GameBoard.Action.SPLIT, \
                            GameBoard.Action.STAY][currTurn % 3]
                else:
                    # If inside the boundary, split or stay
                    move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY]\
                    [currTurn % 2]

            arg = int(self.board.getPieceStrength(location)) / 4

            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'HarryBotter' and the correct answers for
        the database challenge.
        """
        return \
            "01c15a59d9264f39a5d8458660824e4a010e329adda6786d0151c654754e085d"
