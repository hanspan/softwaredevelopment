"""
This is my Bot. It's Botylicious.
It was inspired by Beyonce and is as fierce as she is.
"""

import BotBase     # All of the basic Bot functionality.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PatsyBot()


class PatsyBot(BotBase.BotBase):
    """Cause my bot, he too bot-ilicious for yo, babe - Beyonce"""

    def strongBot(self, location, strength, move, arg):
        """If a Bot is strong enough then we decide what to do based
        on the location of enemies.
        """
        boardsize = 100
        opponent = self.board.findOpponentPieces()
        for target in opponent:
            to_the_left = abs(location - target)
            to_the_right = abs(boardsize-(location-target))
            distance = min(to_the_left, to_the_right)
            if distance < strength/2:
                edge = (boardsize - distance)
                if location - target <= distance or target-location >= edge:
                    move = "Jump_Left"
                    arg = distance
                    break
                elif target - location <= distance or location-target >= edge:
                    move = "Jump_Right"
                    arg = distance
                    break
        if move is None:
            move = "Split"
        return move, arg

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            move = None
            arg = 1
            strength = self.board.getPieceStrength(location)
            if strength > 10:
                move = self.strongBot(location, strength, move, arg)[0]
                arg = self.strongBot(location, strength, move, arg)[1]
            elif strength <= 10:
                move = "Stay"

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="database"):
        """
        This is the hash of 'botilicious' and the correct answer for
        the database challenge.
        """
        if challenge == "database":
            sig = ("49cd9d9e757031448659e4c40a"
                   "d72cb7d9e1cb31134aa52aff7f4a6effb483e8")
            return sig
        elif challenge == "debug":
            sig = ("b3eb4e0d4723225c2d12ff2cb59ac"
                   "ecd7a5e5e77b2c68a52a0d539cf6a825681")
            return sig
