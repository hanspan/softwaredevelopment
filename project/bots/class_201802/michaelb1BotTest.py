
import unittest.mock       # Bring in Python's built-in unit testing functionality.
import unittest       # Bring in Python's built-in unit testing functionality.
import GameBoard      # Information on game board properties.
import michaelb1Bot   # The Bot being tested.

import BotTestCase


class michaelb1BotTestCase(BotTestCase.BotTestCase):
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = michaelb1Bot.michaelb1Bot()
        self.bot.setup(self.getGameConfig())
        
    def testManageFarmers(self):
        """
        test the method manageFarmers
        """
        def returns_1(arg):
            return 1
        self.bot.farmers = [25]
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_1
        self.assertEqual(self.bot.manage_farmers(1, 1000), [[25, str(GameBoard.Action.STAY), 1]])
        
        def returns_2(arg):
            return 2
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_2
        self.assertEqual(self.bot.manage_farmers(1, 1000), [[25, str(GameBoard.Action.STAY), 1]])
        
    def testManageFighters(self):
        """
        test the method manageFighters
        """
        def returns_1(arg):
            return 1
        self.bot.fighters = [25]
        self.bot.new_farmers = []
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_1
        self.assertIsNotNone(self.bot.manage_fighters(1,1000))
        
    def testWhichSideOfStart(self):
        """
        tests every possibility on the board for both possible start points
        """
        # startpos = 25 or 75
        for self.bot.startpos in [25,75]:
            # determine the list of actual answers for each point
            if self.bot.startpos == 25:
                trueSide = ["Left"]*25 + ["Right"]*50 + ["Left"]*25
            else:
                trueSide = ["Right"]*25 + ["Left"]*50 + ["Right"]*25
            for pos in range(self.bot.board.boardSize):
                # we don't care about the answer if pos = startpos, as it is arbitrary (as long as they give the same answer)
                if pos not in [25,75]:
                    with self.subTest(name="test instance of function whichSideOfStart",msg = "startpos = {0}, testpos = {1}".format(self.bot.startpos,pos)):
                        side = self.bot.whichSideOfStart(pos)
                        self.assertEqual(side,trueSide[pos])
        
        # test what happens at startpos = pos 25 and startpos = pos = 75
        self.bot.startpos = 25
        side_at_25 = self.bot.whichSideOfStart(25)
        self.bot.startpos = 75
        side_at_75 = self.bot.whichSideOfStart(75)
        
        # check we get the symmetry we want
        self.assertEqual(side_at_25,side_at_75)
        
        # check both are valid answers
        self.assertIn(side_at_25,["Right","Left"])
        self.assertIn(side_at_25,["Right","Left"])
               
    def testGetFighterMove(self):
        """
        test that each fighter moves to the correct side (jumping or moving)
        """
        
        currTurn = 0
        maxTurns = 1000
        
        def which_side_side_effect_left(arg):
            return("Left")
        
        self.bot.whichSideOfStart = unittest.mock.MagicMock(name = "whichSideOfStart")
        self.bot.whichSideOfStart.side_effect = which_side_side_effect_left
        

        for fighter in range(self.bot.board.boardSize):
            with self.subTest(name="test instance of function getFightermove when moving left"):
                [fighter_return, move_str, arg] = self.bot.getFighterMove(currTurn, maxTurns, fighter)
                #check the resulting move (the 3-entry list returned directly above) has the same fighter entry as the input 
                self.assertEqual(fighter,fighter_return)
                #check the move was a left move or a left jump
                self.assertIn(move_str, [str(GameBoard.Action.LEFT), str(GameBoard.Action.JUMP_LEFT)])
                #check the arg is valid
                self.assertGreaterEqual(arg, 0)
                self.assertIsInstance(arg, int)
        
        
        def which_side_side_effect_right(arg):
            return("Right")
        
        self.bot.whichSideOfStart = unittest.mock.MagicMock(name = "whichSideOfStart")
        self.bot.whichSideOfStart.side_effect = which_side_side_effect_right 
        
        for fighter in range(self.bot.board.boardSize):
            with self.subTest(name="test instance of function getFightermove when moving right"):
                [fighter_return, move_str, arg] = self.bot.getFighterMove(currTurn, maxTurns, fighter)
                #check the resulting move (the 3-entry list returned directly above) has the same fighter entry as the input 
                self.assertEqual(fighter,fighter_return)
                #check the move was a right move or a right jump
                self.assertIn(move_str, [str(GameBoard.Action.RIGHT), str(GameBoard.Action.JUMP_RIGHT)])
                #check the arg is valid
                self.assertGreaterEqual(arg, 0)
                self.assertIsInstance(arg, int)
                
    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, michaelb1Bot.getBot())

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = michaelb1Bot.michaelb1Bot.getSignature()
        self.assertTrue(sig.endswith(""))
