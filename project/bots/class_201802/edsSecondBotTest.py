import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard 
import edsSecondBot     

import BotTestCase # Base class will be a BotTestCase

class edwardhBotTestCase(BotTestCase.BotTestCase):
    
    
    
    def setUp(self):
        
        # initiliases an edwardhBot for testing
        
        self.bot = edsSecondBot.EdsSecondBot()
        self.bot.setup(self.getGameConfig())
        
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateedwardhBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            self.state = stateFile.read()
        
    def testGetBot(self):
        # checks that edwardhBot does return a Bot when called.
        self.assertNotEqual(edsSecondBot.getBot(), None)


    def testGet_active_pieces(self):
        pass
    
    def testGet_growth_pieces(self):
        pass
    
    def testMake_growth_move(self):
        pass
    
    def testjump_to_target_tile(self):
        pass
        
    def testAnyMoves(self):
        pass
                
    def testKnownMove(self):
        pass
        
        # This finds a standard state for the game board and asks the Bot to generate a
        # move. We then check the Bot has applied the correct logic for making it's move
        
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        self.assertNotEqual(resultsRaw, None);
        results = json.loads(resultsRaw)
        print(results)
        
        boardStateJson = json.loads(state)
        # checks the bot returns at least one move
        self.assertGreater(len(results), 0)
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(str(GameBoard.Action.STAY), results[1][1])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[2][1])
        self.assertEqual(str(GameBoard.Action.STAY), results[3][1])
        self.assertEqual(results[0][2], 1)