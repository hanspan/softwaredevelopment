"""This is a Bot keeps weak pieces either stationary or
only stepping left or right to increase strength.
Stronger pieces either split or step to the right."""

import random      # Random numbers for jump direction and distance.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return AmysBot()


class AmysBot(BotBase.BotBase):
    """This Bot make moves dependent on piece strength."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board and then make a
        move for each one. The weakest pieces either stay where they
        are or step right, with a probability of a 1/2 of
        each outcome. The strongest pieces either split or step right,
        with a probability of a 1/2 of each outcome."""

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # move depends on strength with a threshold of 40
            prob = random.randint(0, 1)
            if self.board.getPieceStrength(location) < 40:
                if prob == 0:
                    move = GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.RIGHT
            else:
                if prob == 0:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.RIGHT
            arg = 1
            moves.append([location, str(move), arg])
            return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'amysBot' and the correct line numbers for
        the debug or database challenges.
        """
        if challenge == 'debug':
            return \
               "e756f0986b226a8dcedecd144b93512b1c0892e23f2cfa7b26b80d5a4e8b2cf3"
        elif challenge == 'database':
            return \
               "723351fdb037ea3f97992cbf05c07d5934e5c5d5aae8fb485b9a26273a359480"
        else:
            return "000000000000"
