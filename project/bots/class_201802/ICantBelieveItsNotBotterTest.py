
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import ICantBelieveItsNotBotter      # The Bot being tested.

import BotTestCase


class ICantBelieveItsNotBotterTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = ICantBelieveItsNotBotter.ICantBelieveItsNotBotter()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, ICantBelieveItsNotBotter.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), results[0][1])
        self.assertEqual(0, results[0][2])

    def testKnownMoveSpecialTile(self):
        # Testing that the expected output comes from a move that
        # should include a Stay.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForICantBelieveItsNotBotter.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(10, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(1, results[0][2])

    def testSignature_debug(self):
        # Check that the expected signature for debug is returned.

        sig = ICantBelieveItsNotBotter.ICantBelieveItsNotBotter.getSignature('debug')
        self.assertTrue(sig.endswith("86ca96ca7751d30e"))

    def testSignature_database(self):
	# Check that the expected signature for database is returned
        sig2 = ICantBelieveItsNotBotter.ICantBelieveItsNotBotter.getSignature('database')
        self.assertTrue(sig2.endswith("7a569f28bab37631"))

    def testSignature_nothing(self):
	# Check that when signature function is called incorrectly it returns nothing
        sig3 = ICantBelieveItsNotBotter.ICantBelieveItsNotBotter.getSignature('testnonsense')
        self.assertEqual(sig3,"")


