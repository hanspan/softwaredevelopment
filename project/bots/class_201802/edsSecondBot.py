"""

This bot looks for valuable tiles to jump to first. Once it has made all the jumps it can,
the remaining pieces split if they can leave a piece on their current tile. Otherwise the 
pieces stay.

"""


import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return EdsSecondBot()



        

class EdsSecondBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""
    
     
    
    def get_active_pieces(self):
        """
        only includes pieces that aren't currently on either growth or defensive tiles 
        in the list of active pieces for the next move.
        """
        pieces = self.board.findMyPieces()
        active_pieces = pieces
        for location in pieces:
            if location in self.board.defenceTiles:
                active_pieces.remove(location)
            elif location in self.board.growthTiles:
                active_pieces.remove(location)
                
        return active_pieces
    
    def get_growth_pieces(self):
        """
        want to select the pieces on growth tiles separately 
        """
        growth_pieces = []
        all_pieces = set(self.board.findMyPieces())
        growth_tiles = set(self.board.growthTiles)
        for location in all_pieces.intersection(growth_tiles):
            growth_pieces.append(location)
            
        return growth_pieces
            
    def make_growth_move(self, location, moves):
        """
        This is the standard move for a piece. It alternates between splitting and
        staying, dependant on it's strength.
        """
        arg = 1
        strength = int(self.board.getPieceStrength(location))
        if strength % 2 == 1 and strength > 2:
            move = GameBoard.Action.SPLIT
        else:
            move = GameBoard.Action.STAY
        moves.append([location, str(move), arg])
    
    def jump_to_target_tile(self, tile_pos, moves, active_pieces):
        """
        args: tile_pos is the location of a high-value tile, either a growth or 
        defensive tile. moves is the list of moves for the current turn.
        The strength used in a jump is equal to the distance jumped. The aim of
        this function is to select the single closest piece to the target tile,
        with enough strength to make the jump, and append the jump to moves. 
        The piece that jumped is removed from the list of active pieces to prevent
        a single piece from making multiple moves on a turn.
        """
        
        nearest_pieces = []
            
        for location in active_pieces:
            if abs(tile_pos - location) >= self.board.getPieceStrength(location):
                pass
                
            else:
                nearest_pieces.append([abs(tile_pos - location), location])
        
        if nearest_pieces == []:
            return None
        
            
        nearest_pieces.sort()
        arg, location = nearest_pieces[0]
        if tile_pos - location > 0:
            move = GameBoard.Action.JUMP_RIGHT
                
        elif tile_pos - location < 0:
            move = GameBoard.Action.JUMP_LEFT
                
        else:
            move = GameBoard.Action.STAY
                
        moves.append([location, str(move), arg])
        active_pieces.remove(location)

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This is an evolution of edwardhBot in that the standard move is to split if the piece
        has odd strength, unless the piece is able to jump to either a growth or defensive tile
        """
        

        active_pieces = self.get_active_pieces()
        
        growth_pieces = self.get_growth_pieces()
        
        
        moves = []
        
        for piece in growth_pieces:
            self.make_growth_move(piece, moves)
            
        
        defensive_tiles = self.board.defenceTiles
        
        growth_tiles = self.board.growthTiles
        
        
        for tile_pos in growth_tiles:
            """
            by adding jump moves for the growth tiles first, the bot is prioritising
            a move to a growth tile over a move to a defensive tile.
            """
            self.jump_to_target_tile(tile_pos, moves, active_pieces)
                
        
        
        for tile_pos in defensive_tiles:
            self.jump_to_target_tile(tile_pos, moves, active_pieces)
            
            

        for location in active_pieces:
            """
            jump_to_target_tile method automatically removes pieces that jump from
            the list of active pieces.
            """
            self.make_growth_move(location, moves)
            

        return moves

    @staticmethod
    def getSignature():
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
