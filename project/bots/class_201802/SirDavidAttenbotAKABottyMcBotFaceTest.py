
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import SirDavidAttenbotAKABottyMcBotFace
import BotTestCase
from unittest.mock import MagicMock

class sam2BotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = SirDavidAttenbotAKABottyMcBotFace.SirDavidAttenbotAKABottyMcBotFace()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.
 
        self.assertNotEqual(None, SirDavidAttenbotAKABottyMcBotFace.getBot())

    def test_getPlayerPieceStrength(self):
        test_strength = 10
        
        self.bot.board.getPieceStrength = MagicMock(return_value = test_strength)
                 
        self.assertEqual(self.bot._getPlayerPieceStrengths([1]), {1:10})
    
    def test_canWinBattle(self):
        with (self.subTest("check left first, F,F")):
            self.bot.rNG.randint = MagicMock(return_value = 0)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = False)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = False)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (False, None, None))
            
        with (self.subTest("check left first, F,T")):
            self.bot.rNG.randint = MagicMock(return_value = 0)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = False)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = True)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (True, 0, "R"))
                        
        with (self.subTest("check left first, T,F")):
            self.bot.rNG.randint = MagicMock(return_value = 0)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = True)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = False)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (True, 0, "L"))
        
        with (self.subTest("check left first, T,T")):
            self.bot.rNG.randint = MagicMock(return_value = 0)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = True)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = True)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (True, 0, "L"))
        
        with (self.subTest("check right first, F,F")):
            self.bot.rNG.randint = MagicMock(return_value = 1)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = False)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = False)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (False, None, None))
            
        with (self.subTest("check right first, F,T")):
            self.bot.rNG.randint = MagicMock(return_value = 1)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = False)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = True)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (True, 0, "R"))
                        
        with (self.subTest("check right first, T,F")):
            self.bot.rNG.randint = MagicMock(return_value = 1)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = True)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = False)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (True, 0, "L"))
        
        with (self.subTest("check right second, T,T")):
            self.bot.rNG.randint = MagicMock(return_value = 1)
            self.bot._canWinBattle_searchLeft = unittest.mock.MagicMock(return_value = True)
            self.bot._canWinBattle_searchRight = unittest.mock.MagicMock(return_value = True)
            self.assertEqual(self.bot._canWinBattle("NA", "NA", "NA", "NA"), (True, 0, "R"))
    
    def test_canWinBattle_searchLeft(self):
        with (self.subTest("already_attacked == True")):
            self.bot._already_attacked = MagicMock(return_value = [0])
            self.assertEqual(self.bot._canWinBattle_searchLeft(1,1,1,[[0],[1]]),False)
        with (self.subTest("already_attacked == False, can attack")):
            self.bot._already_attacked = MagicMock(return_value = [])
            self.assertEqual(self.bot._canWinBattle_searchLeft(1,10,1,[[0],[5]]),True)
            self.assertEqual(self.bot._canWinBattle_searchLeft(3,10,3,[[0],[4]]),True)
        with (self.subTest("already_attacked == False, can attack")):
            self.bot._already_attacked = MagicMock(return_value = [])
            self.assertEqual(self.bot._canWinBattle_searchLeft(0,10,1,[[1],[1]]),False)
            self.assertEqual(self.bot._canWinBattle_searchLeft(1,10,11,[[0],[10]]),False)
            self.assertEqual(self.bot._canWinBattle_searchLeft(3,10,3,[[0],[7]]),False)
               
    def test_canWinBattle_searchRight(self):
        with (self.subTest("already_attacked == True")):
            self.bot._already_attacked = MagicMock(return_value = [0])
            self.assertEqual(self.bot._canWinBattle_searchRight(0,1,1,[[0],[1]]),False)
        with (self.subTest("already_attacked == False, can attack")):
            self.bot._already_attacked = MagicMock(return_value = [])
            self.assertEqual(self.bot._canWinBattle_searchRight(0,10,1,[[1],[0,5]]),True)
            self.assertEqual(self.bot._canWinBattle_searchRight(0,10,3,[[3],[0,0,0,4]]),True)
        with (self.subTest("already_attacked == False, can attack")):
            self.bot._already_attacked = MagicMock(return_value = [])
            self.assertEqual(self.bot._canWinBattle_searchRight(1,10,1,[[0],[5]]),False)
            self.assertEqual(self.bot._canWinBattle_searchRight(0,10,1,[[1],[0,10]]),False)
            self.assertEqual(self.bot._canWinBattle_searchRight(0,10,3,[[3],[0,0,0,7]]),False)
    

   
    def test_strategyFortifyBase(self):

        check = [str(GameBoard.Action.SPLIT), str(GameBoard.Action.STAY), str(GameBoard.Action.STAY)]
         
        for i in range(3):
            with (self.subTest("Testing all cases")):
                self.assertEqual(str(self.bot._strategyFortifyBase(i)), check[i])
                 
                
    def test_strategyATTACK(self):
        check = [(GameBoard.Action.JUMP_RIGHT, 2),
                 (GameBoard.Action.JUMP_LEFT, 2),
                 (str(GameBoard.Action.SPLIT), 1),
                 (GameBoard.Action.SPLIT, 1)]
        
        with (self.subTest("my_strength <=5")):
            aTTACK = MagicMock(return_value = [])
            self.bot._strategyFortifyBase = MagicMock(return_value = str(GameBoard.Action.SPLIT))
            self.assertEqual(self.bot._strategyATTACK([0, 5],[[],[]],1,1),check[2])
        with (self.subTest("can attack right")):
            aTTACK = MagicMock(return_value = [True, 2, "R"])
            self.assertEqual(self.bot._strategyATTACK([0,10],[[2],[0,0,2]],2,50),check[0])
        with (self.subTest("can attack left")):
            aTTACK = MagicMock(return_value = [True, 2, "L"])
            self.assertEqual(self.bot._strategyATTACK([2,10],[[0],[2]],2,50),check[1])
        with (self.subTest("cannot attack and strength <=22")):
            aTTACK = MagicMock(return_value = [False, None, None])
            self.bot._strategyFortifyBase = MagicMock(return_value = str(GameBoard.Action.SPLIT))
            self.assertEqual(self.bot._strategyATTACK([0,10],[[0],[1]],1,50), check[2])
        with (self.subTest("reseed")):
            aTTACK = MagicMock(return_value = [False, None, None])
            self.bot._strategyReseed = MagicMock(return_value = (GameBoard.Action.SPLIT, 1))
            self.assertEqual(self.bot._strategyATTACK([0,24],[[0],[1]],1,50), check[3])
                
                
    def test_strategyReseed(self):
        check = [str(GameBoard.Action.SPLIT), str(GameBoard.Action.LEFT), str(GameBoard.Action.RIGHT)]
        
        for i in range(3):
            with (self.subTest("random = i for i={0,1,2}")):
                self.bot.rNG.randint = MagicMock(return_value = i)
                self.assertEqual(str(self.bot._strategyReseed()[0]),check[i])
                self.assertEqual(self.bot._strategyReseed()[1], 1)
    
    def test_generateMoves(self):
        self.bot._strategyATTACK = MagicMock(return_value = (GameBoard.Action.SPLIT, 1))
        self.bot.board.findMyPieces = MagicMock(return_value = [0])
        self.bot._getPlayerPieceStrengths = MagicMock(return_value = [10])
        self.bot.board.findOpponentPieces = MagicMock(return_value = [])
        self.assertEqual(self.bot.generateMoves(50,200), [[0, 'Split', 1]] )
        
    def test_getSignature(self):
        with (self.subTest('debug')):
            self.assertEqual(self.bot.getSignature('debug'), '2ef293e5ed0c772ecb96db5484715127219f01ff91d7a8f4cbbd50fa0164985b')
        with (self.subTest('database')):
            self.assertEqual(self.bot.getSignature('database'), 'f5d31d5fa9bc70ebdf96ffee5722c2329b94fc9086b9d0ea1ee26b48a0b77748')
        with (self.subTest('random string')):
            self.assertEqual(self.bot.getSignature('randomstring'), 'Please select either debug or database.')
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        
                 