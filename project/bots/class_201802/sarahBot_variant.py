"""
This is a variant on sarahBot that changes some parameters to gain an advantage.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomBot()


class RandomBot(BotBase.BotBase):
    """This Bot uses the same split-stay pattern as sarahBot, but moves differently in the first ten moves to gain an advantage."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Moves 1-10: Cycles between 'jump left', 'stay', and 'split', with a 3/5 preference for stay.
        Remaining moves: same as sarahBot
        """

        pieces = self.board.findMyPieces()
        moves = []
        counter = 0

        for location in pieces:
            if counter < 10:
                move = [GameBoard.Action.JUMP_LEFT, GameBoard.Action.STAY, GameBoard.Action.STAY, GameBoard.Action.STAY, GameBoard.Action.SPLIT][currTurn % 5]
                arg = int(self.board.getPieceStrength(location)) / 3
                counter = counter + 1
            else:
                move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]
                arg = 1
                
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
