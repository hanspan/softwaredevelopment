"""
This bot is an improvement on the randomBot.

Weak pieces will stay in place to gain strength,
stronger pieces will randomly either split or
move away from the centre of the bot's pieces. 
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
from xml.etree.ElementPath import prepare_self


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return EleanorBot()


class EleanorBot(BotBase.BotBase):
    """
    This Bot is an improvement on the randomBot. Each piece will either:
    - stay in place if low energy
    - randomly decide to split or move outwards otherwise.
    """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Locate all of our pieces on the board, and then make a move for
        each one.
        
        If a piece has low energy, it will stay in place.
        Otherwise, it will either split, or move towards the edge.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            arg = 1
            
            if strength < 3:
                move = GameBoard.Action.STAY
                print("Stay", location)
            elif random.randint(0, 1) == 0:
                move = GameBoard.Action.SPLIT
                print("Split", location)
            elif self.rightOfStart(self.centreOfPieces(pieces), location):
                move = GameBoard.Action.RIGHT
                print("Right", location)
            else:
                move = GameBoard.Action.LEFT
                print("Left", location)
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves
    
    def centreOfPieces(self, pieces):
        """
        Returns (somewhere close to) the centre of my pieces.
        
        Maths explanation: If the board did not loop, the centre would simply be the
        average of the min and max values of myieces. However, it does loop, so
        when we have pieces on the edges of the board, this function uses the opponent's
        pieces to estimate the centre.         
        """
        opponentPieces = self.board.findOpponentPieces()
        oppEdges = [min(opponentPieces), max(opponentPieces)]
        myEdges = [min(pieces), max(pieces)]
        if (myEdges[1] - myEdges[0]) < (oppEdges[1] - oppEdges[0]):
            return sum(myEdges)//2
        return (sum(oppEdges)//2 - 50) % 100
        
    

    def rightOfStart(self, location1, location2):
        """
        This function will return True if location1 is to the right of location2,
        otherwise will return false.
        """
        if (location1 - location2) % 100 > 50:
            return True
        return False

    @staticmethod
    def getSignature():
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
