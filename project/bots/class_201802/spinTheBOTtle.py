"""
This is a cycle bot. It waits, moves left and splits in a cycle
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return CycleBot()


class CycleBot(BotBase.BotBase):
    """This bot makes each piece stay, move left and split in sequence"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Given the turn  number, work out what move we're doing this turn.
        Locate all of our pieces on the board, and then make the move for
        each one.

        """

        pieces = self.board.findMyPieces()
        moves = []
        moveSet = [GameBoard.Action.STAY,
                   GameBoard.Action.LEFT,
                   GameBoard.Action.SPLIT]
        move = moveSet[(currTurn-1) % 3]  # currTurn starts at 1, we want 0!
        for location in pieces:
            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])
        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'spinTheBOTtle' and the correct line numbers for
        the debug and database challenges.
        """
        if challenge == "database":
            return ("7052db90b9d39f5d7fa7c6026abfe"
                    "21f61befff25015b2998030584479cb0660")

        elif challenge == "debug":
            return ("776265a1c3ad947122f345c2b7d517"
                    "674da4dd57623503ab9e1a252137f122f2")
        return "invalid challenge!"
