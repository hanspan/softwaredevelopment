"""
a suite of tests for the bot: BOTtomOfThePile 
"""


import unittest.mock       # Bring in Python's built-in unit testing functionality.
import unittest       # Bring in Python's built-in unit testing functionality.
import GameBoard      # Information on game board properties.
import BOTtomOfThePile   # The Bot being tested.

import BotTestCase


class BOTtomOfThePileTestCase(BotTestCase.BotTestCase):
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = BOTtomOfThePile.BOTtomOfThePile()
        self.bot.setup(self.getGameConfig())

    def testGenerateMoves1_ge5pieces(self):
        """
        test the method generateMoves with >=5 pieces
        """
        def returnslist():
            return [3,5,8,10,11,12]
        def returnslist2():
            return []
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name = "self.bot.board.findMyPieces")
        self.bot.board.findMyPieces.side_effect = returnslist
        self.bot.generateMoves(0,100)

    def testGenerateMoves2_lt5piece(self):
        """
        test the method generateMoves with <5 pieces
        """
        def returnslist():
            return [3,5,8,10]
        def returnslist2():
            return []
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name = "self.bot.board.findMyPieces")
        self.bot.board.findMyPieces.side_effect = returnslist
        self.bot.generateMoves(0,100)

    def testGenerateMoves_1piece(self):
        """
        test the method generateMoves with 1 piece
        """
        def returnslist():
            return [3]
        def returnslist2():
            return []
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name = "self.bot.board.findMyPieces")
        self.bot.board.findMyPieces.side_effect = returnslist
        self.bot.generateMoves(0,100)

    def testGenerateMoves_reallocatepieces(self):
        """
        test the method generateMoves
        """
        def returnslist():
            return [3]
        def returnslist2():
            return []
        self.bot.board.findMyPieces = unittest.mock.MagicMock(name = "self.bot.board.findMyPieces")
        self.bot.board.findMyPieces.side_effect = returnslist
        self.bot.generateMoves(101,100)
        
    def testManageFarmers(self):
        """
        test the method manageFarmers
        """
        def returns_1(arg):
            return 1
        self.bot.farmers = [25]
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_1
        self.assertEqual(self.bot.manage_farmers(1, 1000), [[25, str(GameBoard.Action.STAY), 1]])
        
        def returns_2(arg):
            return 2
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_2
        self.assertEqual(self.bot.manage_farmers(1, 1000), [[25, str(GameBoard.Action.STAY), 1]])



    def testManageFarmers_randomswitch(self):
        """
        test the method manageFarmers in a specific case when farmer switches to fighter randomly
        """
        def returns_3(arg):
            return 3
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_3      
        def returns_0():
            return 0
        def returns_1():
            return 1
        self.bot.farmers = [25,26,27,28,29,30,31,32,33,34,45,47,75,93,99]
        self.bot.random = unittest.mock.MagicMock(name = "random.random")
        self.bot.random.side_effect = returns_1
        self.bot.manage_farmers(1, 1000)
        
    def testManageFarmers_testsplitfarmers(self):
        """
        test the method manageFarmers in a specific case when farmer switches to fighter randomly
        """
        def returns_3(arg):
            return 3
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_3      
        def returns_0():
            return 0
        def returns_1():
            return 1
        self.bot.farmers = [25,26,27,28,29,30,31,32,33,34,45,47,75,93,99]
        self.bot.random = unittest.mock.MagicMock(name = "random.random")
        self.bot.random.side_effect = returns_1
        self.bot.manage_farmers(2000, 1000)
    def testManageFarmers_testnonsplitfarmers(self):
        """
        test the method manageFarmers in a specific case when farmer switches to fighter randomly
        """
        def returns_3(arg):
            return 3
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_3      
        def returns_0():
            return 0
        def returns_1():
            return 1
        self.bot.farmers = [25,26,27,28,29,30,31,32,33,34,45,47,75,93,99]
        self.bot.random = unittest.mock.MagicMock(name = "random.random")
        self.bot.random.side_effect = returns_1
        self.bot.manage_farmers(11, 1000)
        
    def testManageFighters_fighter_to_farmer(self):
        """
        test the method manageFighters
        """
        def returns_1(arg):
            return 1
        self.bot.fighters = [25]
        self.bot.new_farmers = []
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_1
        self.assertIsNotNone(self.bot.manage_fighters(1,1000))

    def testManageFighters_fighter_stays_fighter(self):
        """
        test the method manageFighters
        """
        def returns_1(arg):
            return 1
        self.bot.fighters = [25]
        self.bot.new_farmers = []
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_1
        self.random = unittest.mock.MagicMock(name = "random.random")
        self.random.side_effect = returns_1
        self.assertIsNotNone(self.bot.manage_fighters(1,1000))

    def testManageFighters_fighter_stays_fighter(self):
        """
        test the method manageFighters
        """
        def returns_1(arg):
            return 1
        self.bot.fighters = [25]
        self.bot.new_farmers = []
        self.bot.board.getPieceStrength = unittest.mock.MagicMock(name = "self.bot.board.getPieceStrength")
        self.bot.board.getPieceStrength.side_effect = returns_1
        self.random = unittest.mock.MagicMock(name = "random.random")
        self.random.side_effect = returns_1
        self.assertIsNotNone(self.bot.manage_fighters(1001,1000))
        
    def testWhichSideOfStart(self):
        """
        tests every possibility on the board for both possible start points
        """
        # startpos = 25 or 75
        for self.bot.startpos in [25,75]:
            # determine the list of actual answers for each point
            if self.bot.startpos == 25:
                trueSide = ["Left"]*25 + ["Right"]*50 + ["Left"]*25
            else:
                trueSide = ["Right"]*25 + ["Left"]*50 + ["Right"]*25
            for pos in range(self.bot.board.boardSize):
                # we don't care about the answer if pos = startpos, as it is arbitrary (as long as they give the same answer)
                if pos not in [25,75]:
                    with self.subTest(name="test instance of function whichSideOfStart",msg = "startpos = {0}, testpos = {1}".format(self.bot.startpos,pos)):
                        side = self.bot.whichSideOfStart(pos)
                        self.assertEqual(side,trueSide[pos])
        
        # test what happens at startpos = pos 25 and startpos = pos = 75
        self.bot.startpos = 25
        side_at_25 = self.bot.whichSideOfStart(25)
        self.bot.startpos = 75
        side_at_75 = self.bot.whichSideOfStart(75)
        
        # check we get the symmetry we want
        self.assertEqual(side_at_25,side_at_75)
        
        # check both are valid answers
        self.assertIn(side_at_25,["Right","Left"])
        self.assertIn(side_at_25,["Right","Left"])
               
    def testGetFighterMove(self):
        """
        test that each fighter moves to the correct side (jumping or moving)
        """
        
        currTurn = 0
        maxTurns = 1000
        
        def which_side_side_effect_left(arg):
            return("Left")
        
        self.bot.whichSideOfStart = unittest.mock.MagicMock(name = "whichSideOfStart")
        self.bot.whichSideOfStart.side_effect = which_side_side_effect_left
        

        for fighter in range(self.bot.board.boardSize):
            with self.subTest(name="test instance of function getFightermove when moving left"):
                [fighter_return, move_str, arg] = self.bot.getFighterMove(currTurn, maxTurns, fighter)
                #check the resulting move (the 3-entry list returned directly above) has the same fighter entry as the input 
                self.assertEqual(fighter,fighter_return)
                #check the move was a left move or a left jump
                self.assertIn(move_str, [str(GameBoard.Action.LEFT), str(GameBoard.Action.JUMP_LEFT)])
                #check the arg is valid
                self.assertGreaterEqual(arg, 0)
                self.assertIsInstance(arg, int)
        
        
        def which_side_side_effect_right(arg):
            return("Right")
        
        self.bot.whichSideOfStart = unittest.mock.MagicMock(name = "whichSideOfStart")
        self.bot.whichSideOfStart.side_effect = which_side_side_effect_right 
        
        for fighter in range(self.bot.board.boardSize):
            with self.subTest(name="test instance of function getFightermove when moving right"):
                [fighter_return, move_str, arg] = self.bot.getFighterMove(currTurn, maxTurns, fighter)
                #check the resulting move (the 3-entry list returned directly above) has the same fighter entry as the input 
                self.assertEqual(fighter,fighter_return)
                #check the move was a right move or a right jump
                self.assertIn(move_str, [str(GameBoard.Action.RIGHT), str(GameBoard.Action.JUMP_RIGHT)])
                #check the arg is valid
                self.assertGreaterEqual(arg, 0)
                self.assertIsInstance(arg, int)
                
    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, BOTtomOfThePile.getBot())

    def testSignature_debug(self):
        # Check that the expected signature is returned.

        sig = BOTtomOfThePile.BOTtomOfThePile.getSignature(challenge = "debug")
        self.assertTrue(sig.endswith("7963"))
    def testSignature_database(self):
        # Check that the expected signature is returned.

        sig = BOTtomOfThePile.BOTtomOfThePile.getSignature(challenge = "database")
        self.assertTrue(sig.endswith("2bbb"))


if __name__ == '__main__':
    unittest.main()
