"""
This is my Bot. It is going to be super powerful and amazing.

"""

import BotBase     # All of the basic Bot functionality.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PatsyBot()


class PatsyBot(BotBase.BotBase):
    """This Bot decides to make moves depending on how close the nearest
    enemy is.
    If there are no enemies nearby for the bots to attack then they stay
    and split alternately.
    If the bot is not of a certain strength then it will also stay and
    grow until it reaches the minimum strength
    """
    @staticmethod
    def attack(location, enemy):
        """ This function takes the location of the enemy and Jumps
        to that square in order to attack it.
        """
        distance = location - enemy
        # Do we need to attack left or right?
        if location > enemy:
            if distance > 50:
                distance = 100 - distance
                direction = 'R'
            else:
                direction = 'L'
        else:
            if distance < -50:
                distance = 100 + distance
                direction = 'L'
            else:
                direction = 'R'
                distance = abs(distance)
        if direction == 'L':
            move = 'Jump_Left'
        if direction == 'R':
            move = 'Jump_Right'
        return move, distance

    @staticmethod
    def defend(currTurn):
        """For strong bots with no nearby enemies
        """
        if currTurn % 2 == 0:
            move = "Split"
        else:
            move = 'Stay'
        return move

    @staticmethod
    def heal():
        """For weak bots whilst healing
        """
        move = 'Stay'
        return move

    @staticmethod
    def findNearestEnemy(location, opponents):
        """ Finds a given pieces nearest enemy and returns the enemy
        position and distance
        """
        closest_enemy_position = None
        closest_enemy_distance = 100
        for enemy in opponents:
            enemy_distance = abs(location - enemy)
            if enemy_distance > 50:
                enemy_distance = 100 - enemy_distance
            if enemy_distance < closest_enemy_distance:
                closest_enemy_distance = enemy_distance
                closest_enemy_position = enemy
        return closest_enemy_position, closest_enemy_distance

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """This function gathers all the information needed for the current
        pieces and then sends off each piece to the correct function based
        on the presence of enemies and the strength of the bot.
        """
        pieces = self.board.findMyPieces()
        moves = []
        opponents = self.board.findOpponentPieces()
        weak = 5
        for location in pieces:
            move = None
            arg = 1
            strength = self.board.getPieceStrength(location)
            closest_enemy = self.findNearestEnemy(location, opponents)[0]
            enemy_distance = self.findNearestEnemy(location, opponents)[1]
            if enemy_distance < strength/2 and strength > weak:
                move = self.attack(location, closest_enemy)[0]
                arg = self.attack(location, closest_enemy)[1]
            else:
                if strength > weak:
                    move = self.defend(currTurn)
                else:
                    move = self.heal()
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="database"):
        """
        This is the hash of 'botilicious' and the correct answer for
        the database challenge.
        """
        if challenge == "database":
            sig = ("9ebeb2707d457ee1dd35d4e367db"
                   "e3bca4e76609a61a01ae426749e354a9e9cb")
            return sig
        elif challenge == "debug":
            sig = ("f10b531f6eadd390e3d520ac083f0"
                   "fb4e08e65656c7337e9959fa270d77002ba")
            return sig
