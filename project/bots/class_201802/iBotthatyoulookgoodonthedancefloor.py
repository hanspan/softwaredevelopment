"""
This is iBotthatyoulookgoodonthedancefloor;
it identifies the pieces it has on the game board,
and then does something hopefully better than random.

"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return iBotthatyoulookgoodonthedancefloor()


class iBotthatyoulookgoodonthedancefloor(BotBase.BotBase):
    """This Bot grows more when on a special square """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        When on a special tile, it will grow 40% of the time, or split
        When on a normal tile, it will grow 20% of the time, or split
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            # try staying or spliting on special tiles
            if location in (10, 89, 30, 69, 40, 59):
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.JUMP_LEFT

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        These are the hashes of 'iBotthatyoulookgoodonthedancefloor'
        and the correct answers for
        the debug and database challenge.
        """
        if challenge == "debug":
            return \
                "269edf16edb4d8b27e1087d40bb649b2c5d7bd"\
                "7decf379a862419dc5b741b729"

        elif challenge == "database":
            return \
                "a76693b5e5859e723beafb5dfd123a27274034"\
                "1f8bb3830671e89cfbf3a982cf"

        else:
            return ""
