
import os
import json           # JSON handling
import GameBoard      # Information on game board properties.
import flyingBotsman      # The Bot being tested.

import BotTestCase

class flyingBotsmanTestCase(BotTestCase.BotTestCase):


    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = flyingBotsman.FlyingBotsman()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Test whether the bot is successfully returned..

        self.assertNotEqual(None, flyingBotsman.getBot())

    def testSideFunc(self):
        self.assertEqual(flyingBotsman.sideof(24, 25, 100), -1)
        self.assertEqual(flyingBotsman.sideof(26, 25, 100), 1)
        self.assertEqual(flyingBotsman.sideof(0, 75, 100), 1)
        self.assertEqual(flyingBotsman.sideof(75, 0, 100), -1)

    def testEarlyStay(self):
        # First move should be stay
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'flyingBotsman/'\
                                      +'earlyStay.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(1, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testEarlySplit(self):
        # Should split with >1 strength
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'flyingBotsman/'\
                                      +'earlySplit.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1,len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(1, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1,results[0][2])
        
    def testEarlyStrongStay(self):
        # Should split with >1 strength
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'flyingBotsman/'\
                                      +'earlyStrongStay.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(3,len(results))
        self.assertEqual(3,len(results[1]))
        self.assertEqual(1,results[1][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[1][1])
        self.assertEqual(1,results[0][2])
        
    def testLateRight(self):
        # Right of centre, so move right
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'flyingBotsman/'\
                                      +'lateRight.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(1,len(results))
        self.assertEqual(3,len(results[0]))
        self.assertEqual(1, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
        self.assertEqual(1,results[0][2])

    def testLateLeft(self):
        # Left of centre, so move left
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,'flyingBotsman/lateLeft.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1,len(results))
        self.assertEqual(3,len(results[0]))
        self.assertEqual(99, results[0][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[0][1])
        self.assertEqual(1,results[0][2])

    def testSigDebug(self):
        self.assertEqual(self.bot.getSignature("debug"),
                        ("80501ced5b4fa04a72b5072f122b133e"\
                         +"f360959452da8912cca8c23625bb470c"))

    def testSigDatabase(self):
        self.assertEqual(self.bot.getSignature("database"),
                        ("4275b7d5fc6868b486321fb92567f74e"\
                        +"5c2090000862c09445e43e72b4a1255d"))

        