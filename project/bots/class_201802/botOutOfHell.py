"""
This bot is an improvement on the randomBot.

Weak pieces will stay in place to gain strength,
stronger pieces will randomly either split or
move away from the centre of the bot's pieces.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
# from xml.etree.ElementPath import prepare_self


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return EleanorBot()
    # It is still eleanorBot at heart.


class EleanorBot(BotBase.BotBase):
    """
    This Bot is an improvement on the randomBot. Each piece will either:
    - stay in place if low energy
    - randomly decide to split or move outwards otherwise.
    """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Locate all of our pieces on the board, and then make a move for
        each one.

        If a piece has low energy, it will stay in place.
        Otherwise, it will either split, or move towards the edge.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            arg = 1

            if strength < 3:
                move = GameBoard.Action.STAY
                # print("Stay", location)
            elif random.randint(0, 1) == 0:
                move = GameBoard.Action.SPLIT
                # print("Split", location)
            elif self.rightOfStart(self.centreOfPieces(pieces), location):
                move = GameBoard.Action.RIGHT
                # print("Right", location)
            else:
                move = GameBoard.Action.LEFT
                # print("Left", location)
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    def centreOfPieces(self, pieces):
        """
        Returns (somewhere close to) the centre of my pieces.

        Maths explanation: If the board did not loop, the centre would simply
        be the average of the min and max values of myieces. However, it does
        loop, so when we have pieces on the edges of the board, this function
        uses the opponent's pieces to estimate the centre.
        """
        opponentPieces = self.board.findOpponentPieces()
        oppEdges = [min(opponentPieces), max(opponentPieces)]
        myEdges = [min(pieces), max(pieces)]
        if (myEdges[1] - myEdges[0]) < (oppEdges[1] - oppEdges[0]):
            return sum(myEdges)//2
        return (sum(oppEdges)//2 - 50) % 100

    @staticmethod
    def rightOfStart(location1, location2):
        """
        This function will return True if location1 is to the right of
         location2, otherwise will return false.
        """
        if (location1 - location2) % 100 > 50:
            return True
        return False

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'botOutOfHell' and the correct answers for
        the debug and database challenges.
        """
        if challenge == 'debug':
            return\
             '78f6ea3e6371c3829ae8b7a82de4b1626e54456fb93faa37532bd077502f572c'
        elif challenge == 'database':
            return\
             '9f6788f9acbd50d6b2ae36773c2d4354184b90c0f538fb7df6cb787b718f2cdf'
