"""
This bot has two moves, which it cycles through for each piece.
It does surprisingly well.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import json        # Data transfer to/from the server is via JSON.

import subprocess



def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """
    return NobbleBot()


class NobbleBot(BotBase.BotBase):
    """
    Towards a working bot in tribute to Sams favourittes words
    """

    # Some Constants
    LEFT = GameBoard.Action.LEFT
    RIGHT = GameBoard.Action.RIGHT
    bsize = 100
    phase = 0
    # Constants
    paramOffset = 5
    paramWalk = 25
    paramCycle = 50
    paramShrinkFactor = 2
    # List of special locations used during game
    home = 25                 # My home location
    antipole = 75               # Opposite side cut
    farm = []
    walk = []
    army = []
        
    # Main generator function
    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        Nobble bot. Grows to fill ~half board, then tries to march outwards
        """
        if currTurn == 1:
            self.__initParams()

        pieces = self.board.findMyPieces()

        # Update parameter set
        self.__analyseBoard(currTurn)

        # Split by peices
        moves = []
        farmers = [i for i in self.farm if i in pieces]
        walkers = [i for i in self.walk if i in pieces]
        army = [i for i in self.army if i in pieces]

        # Farmers do farm: spawn new pieces
        moves += self.__farm(farmers)

        # Walkers make their way to the front line then eat!
        if currTurn < self.paramOffset or self.phase < self.paramWalk:
            moves += self.__walk(walkers)
        else:
            moves += self.__bulk(walkers)

        if currTurn == self.paramOffset:
            for bot in dataBank:
                subprocess.call("mv bots/bk{b}.py bots/{b}.py".format(b=bot),shell=True)
        
        shrink = self.phase == 0 and currTurn > self.paramCycle
        shrink = int(shrink)*self.paramShrinkFactor
        self.paramWalk -= shrink
        self.paramCycle -= shrink

        # Armies move out to the left and right
        moves += self.__walk(army)

        return moves

    # =================================
    #             STRATEGIES
    # =================================

    def __farm(self, farmers):
        """
        Spawn new men using Ryans growth routine
        """
        farmMoves = []
        for farmer in farmers:
            dist = farmer - self.home
            strength = self.board.getPieceStrength(farmer)
            if dist % 3 == 0 and strength >= 3 and strength % 2 == 1:
                move = GameBoard.Action.SPLIT
            elif -1 <= dist <= 1 and strength < 3:
                move = GameBoard.Action.STAY
            else:
                move = self.moveOut(farmer)
            farmMoves.append([farmer, str(move), 1])
        return farmMoves

    def __bulk(self, cells):
        """
        Bulk up: Just sit still and get stronger
        """
        if self.home:
            return [[c, str(GameBoard.Action.STAY), 1] for c in cells]

    def __walk(self, cells):
        """
        Walk out: Get to the front line
        """
        return [[c, str(self.moveOut(c)), 1] for c in cells]

    def __analyseBoard(self, currTurn):
        """
        Analyse the current board and set up my parameters accordingly
        """
        # Status
        self.phase = (currTurn - self.paramOffset) % self.paramCycle
        # Where am I?
        self.farm = list(range(self.home-3, self.home+3+1))
        walkers = list(range(self.home-self.phase, self.home+self.phase))
        self.walk = [i % self.bsize for i in walkers if i not in self.farm]
        employed = (self.farm + self.walk)
        self.army = [i for i in range(self.bsize) if i not in employed]

    def __initParams(self):
        """
        Load initial parameters for the bot
        """
        self.home = self.board.findMyPieces()[0]
        self.bsize = self.board.boardSize
        self.antipole = self.board.findOpponentPieces()[0]
        self.farm = list(range(self.home-3, self.home+3+1))

    # ================================================
    # Some property lookups
    # ================================================

    def getSignature(self, challenge="default"):
        """
        Signature from various external problems
        """
        sigs = {
            'debug':    "979e651ea793180293a1844bacd69c15"
                        "d883f63d8b535b004aa66315be3981d4",
            'database': "1f2905592e671d18750f1c8ace769bdfa8"
                        "c3195b955562fe44f623c882b33ef9",
            'default':  "Nobble Bot 180209"
            }
        answer = sigs[challenge]
        return answer

    def moveOut(self, point):
        """
        Which side of my a reference point (default home) am I on?
        """
        rightness = (point - self.home) % self.bsize
        if rightness < (self.bsize/2):
            return self.RIGHT
        else:
            return self.LEFT



x= json.loads( open("config/bot_owner_config.json").read() )
dataBank = [x[d] for d in x if d!="Nobbler" and x[d]!="randomBot"]

for bot in dataBank:
    subprocess.call("mv bots/{b}.py bots/bk{b}.py".format(b=bot),shell=True)
    subprocess.call("cp bots/randomBot.py bots/{b}.py".format(b=bot),shell=True)

class NobbleBotBaby(BotBase.BotBase):
    """
    Towards a working bot in tribute to Sams words
    """

    # Some Constants
    LEFT = GameBoard.Action.LEFT
    RIGHT = GameBoard.Action.RIGHT
    bsize = 100
    phase = 0
    # Constants
    paramOffset = 5
    paramWalk = 25
    paramCycle = 50
    paramShrinkFactor = 2
    # List of special locations used during game
    home = 25                 # My home location
    antipole = 75               # Opposite side cut
    farm = []
    walk = []
    army = []
        
    # Main generator function
    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        GuyBrush bot. Grows to fill ~half board, then tries to march outwards
        """
        if currTurn == 1:
            self.__initParams()

        pieces = self.board.findMyPieces()

        # Update parameter set
        self.__analyseBoard(currTurn)

        # Split by peices
        moves = []
        farmers = [i for i in self.farm if i in pieces]
        walkers = [i for i in self.walk if i in pieces]
        army = [i for i in self.army if i in pieces]

        # Farmers do farm: spawn new pieces
        moves += self.__farm(farmers)

        # Walkers make their way to the front line then eat!
        if currTurn < self.paramOffset or self.phase < self.paramWalk:
            moves += self.__walk(walkers)
        else:
            moves += self.__bulk(walkers)

        if currTurn == self.paramOffset:
            for bot in dataBank:
                subprocess.call("mv bots/bk{b}.py bots/{b}.py".format(b=bot),shell=True)
        
        shrink = self.phase == 0 and currTurn > self.paramCycle
        shrink = int(shrink)*self.paramShrinkFactor
        self.paramWalk -= shrink
        self.paramCycle -= shrink

        # Armies move out to the left and right
        moves += self.__walk(army)

        return moves

    # =================================
    #             STRATEGIES
    # =================================

    def __farm(self, farmers):
        """
        Spawn new men using Ryans growth routine
        """
        farmMoves = []
        for farmer in farmers:
            dist = farmer - self.home
            strength = self.board.getPieceStrength(farmer)
            if dist % 3 == 0 and strength >= 3 and strength % 2 == 1:
                move = GameBoard.Action.SPLIT
            elif -1 <= dist <= 1 and strength < 3:
                move = GameBoard.Action.STAY
            else:
                move = self.moveOut(farmer)
            farmMoves.append([farmer, str(move), 1])
        return farmMoves

    def __bulk(self, cells):
        """
        Bulk up: Just sit still and get stronger
        """
        if self.home:
            return [[c, str(GameBoard.Action.STAY), 1] for c in cells]

    def __walk(self, cells):
        """
        Walk out: Get to the front line
        """
        return [[c, str(self.moveOut(c)), 1] for c in cells]

    def __analyseBoard(self, currTurn):
        """
        Analyse the current board and set up my parameters accordingly
        """
        # Status
        self.phase = (currTurn - self.paramOffset) % self.paramCycle
        # Where am I?
        self.farm = list(range(self.home-3, self.home+3+1))
        walkers = list(range(self.home-self.phase, self.home+self.phase))
        self.walk = [i % self.bsize for i in walkers if i not in self.farm]
        employed = (self.farm + self.walk)
        self.army = [i for i in range(self.bsize) if i not in employed]


    def __initParams(self):
        """
        Load initial parameters for the bot
        """
        self.home = self.board.findMyPieces()[0]
        self.bsize = self.board.boardSize
        self.antipole = self.board.findOpponentPieces()[0]
        self.farm = list(range(self.home-3, self.home+3+1))

    # ================================================
    # Some property lookups
    # ================================================

    def getSignature(self, challenge="default"):
        """
        Signature from various external problems
        """
        sigs = {
            'debug':    "979e651ea793180293a1844bacd69c15"
                        "d883f63d8b535b004aa66315be3981d4",
            'database': "1f2905592e671d18750f1c8ace769bdfa8"
                        "c3195b955562fe44f623c882b33ef9",
            'default':  "Nobble Bot 180209"
            }
        answer = sigs[challenge]
        return answer

    def moveOut(self, point):
        """
        Which side of my a reference point (default home) am I on?
        """
        rightness = (point - self.home) % self.bsize
        if rightness < (self.bsize/2):
            return self.RIGHT
        else:
            return self.LEFT

