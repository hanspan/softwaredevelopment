"""
A variation on randomBot, it no longer does useless moves like splitting or jumping with 1 strength.

Made by John
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomBot()


class RandomBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            move = random.choice(list(GameBoard.Action))

            Useless_Moves = [GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT, GameBoard.Action.SPLIT]

            if self.board.getPieceStrength(location) == 1:
                while move in Useless_Moves:
                    move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
