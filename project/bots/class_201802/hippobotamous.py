"""
HippoBOTamous - spreads out a lot, just like a real hippo
              - is ferocious, just like a real hippo
              - is good at breeding, just like a real hippo
              - is nature's mistake, just like a real hippo

hippobotamous spreads out as quickly as possible, builds up, and then
divides and conquers
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

LEFT = -1
RIGHT = 1


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return Hippobotamous()


class Hippobotamous(BotBase.BotBase):
    """Our bot class"""
    startIndex = 0

    def sideOf(self, point):
        """A helper function that determines whether a point is to the left of,
           or right of, a different point. This takes into account the
           wrap-around, since [0,100)/(0~100) is homeomorphic to S^1. """
        size = self.board.boardSize
        start = self.startIndex
        if point <= start:
            ldist = start - point
            if ldist <= size / 2:
                return LEFT
            else:
                return RIGHT
        else:
            ldist = point - start
            if ldist <= size / 2:
                return RIGHT
            else:
                return LEFT

    def distanceFromStart(self, point):
        """A helper function that determines how far the piece in question is from
           the start point. We need this since the behaviour changes based on
           whether we are of distance 3 from the origin or not
        """
        size = self.board.boardSize
        start = self.startIndex
        return min((point - start) % size, (start - point) % size)

    def generateMoves(self, currTurn, maxTurns):
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            # We need to find the start point for future reference
            # (this is probably stored in a variable somewhere already)
            if currTurn == 1:
                self.startIndex = location

            if currTurn < 26:
                # Spread out as fast as possible
                move = self.findEarlyMoves(location)

            elif currTurn < 51:
                # We spread out a bunch of super weak pieces, so we let them
                # grow up into nice healthy adults for 24 turns
                move = GameBoard.Action.STAY

            else:
                # Now we're all grown up, we split streams and ATTACK
                move = self.findLateMoves(location)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            # We never jump, so we just pass arg as the constant "1"
            moves.append([location, str(move), 1])

        return moves

    def findEarlyMoves(self, location):
        """ Near the beginning, the strategy is stread out as fast as
            possible. We have 3 spawn points that split when they have
            strength 3, which give us the coverage we want
        """

        strength = self.board.getPieceStrength(location)
        dFromStart = self.distanceFromStart(location)
        if (dFromStart == 0 or dFromStart == 1) and strength < 3:
            move = GameBoard.Action.STAY
        elif (dFromStart == 0 or dFromStart == 3) and strength == 3:
            move = GameBoard.Action.SPLIT
        elif self.sideOf(location) == LEFT:
            move = GameBoard.Action.LEFT
        else:
            move = GameBoard.Action.RIGHT

        return move

    def findLateMoves(self, location):
        """ Get moves in late turns.
            If starting tile has odd strength >= 3 it splits
            and everything moves away from the center.
            Otherwise, everything stays.
        """

        centreStrength = self.board.getPieceStrength(self.startIndex)

        # Every so often, the hippo waddles over to a new spot
        # This happens whenever it is sensible to split at the centre
        if centreStrength >= 3 and centreStrength % 2 == 1:

            # The centre divides, every other piece moves in the direction
            # of the centre it is in.
            if self.distanceFromStart(location) == 0:
                move = GameBoard.Action.SPLIT
            elif self.sideOf(location) == RIGHT:
                move = GameBoard.Action.RIGHT
            else:
                move = GameBoard.Action.LEFT

        # Otherwise, the hippo gets tired and has to rest.
        else:
            move = GameBoard.Action.STAY
        return move

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'hippobotamous' and the correct answer for either
        the debug or the database challenge.
        """

        if challenge == "database":
            return \
                ("be651e80402ff7b7f8c053ae04783517"
                 "64e2e81a788c664a4828e5071fba1f6d")
        elif challenge == "debug":
            return \
                ("aca99b76c47fccf58af0cdf62d0ef747"
                 "3e7411d505eb1b2d675e68c1c0562ef0")
