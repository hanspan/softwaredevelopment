
import BotTestCase     # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import spinTheBOTtle  # The Bot being tested.
import os


class cycleBotTestCase(BotTestCase.BotTestCase):
    """
    Tests the cycle bot defined in spinTheBOTtle.py
    """

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = spinTheBOTtle.CycleBot()
        self.bot.setup(self.getGameConfig())

    def getGameConfig(self):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.
        testDataDir = self.getTestDataDir()
        confFile = os.path.join(testDataDir, 'testConfig.cfg')
        with open(confFile, 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, spinTheBOTtle.getBot())

    def testFirstMoves(self):
        testDataDir = self.getTestDataDir()
        boardStateFile1 = os.path.join(testDataDir, 'testBoardState.txt')
        # Make sure the first move is to Stay.
        with(self.subTest('testing first move')):
            # Read in a standard board state, and see what moves result.
            with open(boardStateFile1, 'r') as stateFile:
                state = stateFile.read()
            resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
            results = json.loads(resultsRaw)
            self.assertEqual(1, len(results))
            self.assertEqual(3, len(results[0]))
            self.assertEqual(25, results[0][0])
            self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
            self.assertEqual(1, results[0][2])

    def testSecondMove(self):
        # Make sure the second move is to Action left.
        testDataDir = self.getTestDataDir()
        boardStateFile2 = os.path.join(testDataDir,
                                       "spinTheBOTtletestdata",
                                       "testBoardafter1Move.txt")
        with(self.subTest('testing second move')):
            # Read in a standard board state, and see what moves result.
            with open(boardStateFile2, 'r') as stateFile:
                state = stateFile.read()
            resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
            results = json.loads(resultsRaw)

            self.assertEqual(1, len(results))
            self.assertEqual(3, len(results[0]))
            self.assertEqual(25, results[0][0])
            self.assertEqual(str(GameBoard.Action.LEFT), results[0][1])
            self.assertEqual(1, results[0][2])

    def testThirdMove(self):
        # Make sure the second move is to split.
        testDataDir = self.getTestDataDir()
        boardStateFile3 = os.path.join(testDataDir,
                                       "spinTheBOTtletestdata",
                                       "testBoardafter2Moves.txt")
        with(self.subTest('testing third move')):
            # Read in a standard board state, and see what moves result.
            with open(boardStateFile3, 'r') as stateFile:
                state = stateFile.read()
            resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
            results = json.loads(resultsRaw)
            self.assertEqual(1, len(results))
            self.assertEqual(3, len(results[0]))
            self.assertEqual(24, results[0][0])
            self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
            self.assertEqual(1, results[0][2])

    def testDebugSignature(self):
        # Check that the debug expected signature is returned.

        sig = spinTheBOTtle.CycleBot.getSignature("debug")
        self.assertTrue(sig.endswith("a252137f122f2"))

    def testDatabaseSignature(self):
        # Check that the debug expected signature is returned.
        sig1 = spinTheBOTtle.CycleBot.getSignature("database")
        self.assertTrue(sig1.endswith("0584479cb0660"))

    def testOtherSignature(self):
        # Check that if we give it a challeng that isn't debug or
        # database we get the invalif challenge
        sig2 = spinTheBOTtle.CycleBot.getSignature("anything else")
        self.assertTrue(sig2.endswith("invalid challenge!"))
