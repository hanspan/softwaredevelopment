
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import sirKillaBot    # The Bot being tested.
import BotTestCase


class sirKillaBotCyclicStrategyTest(unittest.TestCase):

    def setUp(self):
        """
        Common setup for the unit tests within this class.
        """

        self.strategy = sirKillaBot.CyclicStrategy()

    def test__init__(self):
        """
        Verify that a CyclicStrategy instance is initialised with the expected parameters.
        """

        self.assertEqual(self.strategy.currentMoveIdx, 0)
        self.assertEqual(self.strategy.nextMoveStep, 1)
        self.assertEqual(len(self.strategy.availableMoves), 4)

    def test_move(self):
        """
        Verify that CyclicStrategy produces the expected first one hundred moves.
        """

        for i in range(1, 100):
            move = self.strategy.move()
            self.assertEqual(move, self.strategy.availableMoves[i % len(self.strategy.availableMoves)])

class sirKillaBotTest(BotTestCase.BotTestCase):

    def setUp(self):
        """
        Common setup for the unit tests within this class.
        """

        self.bot = sirKillaBot.SirKillaBot()
        self.bot.setup(self.getGameConfig())

    def test_getBot(self):
        """
        Verify that a SirKillaBot instance is returned from the getBot() function.
        """

        self.assertNotEqual(None, sirKillaBot.getBot())
        self.assertTrue(isinstance(sirKillaBot.getBot(), sirKillaBot.SirKillaBot))

    def test__init__(self):
        """
        Verify that a SirKillaBot instance is initialised with the expected parameters.
        """

        self.assertTrue(isinstance(self.bot.strategy, sirKillaBot.CyclicStrategy))
        self.assertEqual(self.bot.strategy.currentMoveIdx, 0)
        self.assertEqual(self.bot.strategy.nextMoveStep, 1)
        self.assertEqual(len(self.bot.strategy.availableMoves), 4)

    def test_knownMove(self):
        """
        This is a "last known good state" test. By itself, it doesn't really indicate any problems. However, it
        establishes a known input/output matching set, and will highlight if something changes that result.
        """

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def test_getSignature(self):
        """
        Verify that SirKillaBot's getSignature method returns the expected hash strings.
        """

        expected_signatures = {
            'debug': "ff4040019e77bd6b9fe56facf2789f40f3ade620f5bb4b04eac0e51"
                     "974f89a71",
            'database': "6d7e4497ba60cf74afd62dc06117e3e816c74d9d66d0dbf6a8b2"
                        "e0aaafdab0e1"
        }

        self.assertEqual(self.bot.getSignature(), expected_signatures['debug'])

        self.assertEqual(
            self.bot.getSignature(challenge='debug'), expected_signatures['debug']
        )

        self.assertEqual(
            self.bot.getSignature(challenge='database'), expected_signatures['database']
        )
