"""
This is a hannah Bot;
she is trying to grow real big in width and strength.
her favourite pasttimes are running, jumping and crushing enemies
her favourite person is you
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SomeLikeItBot()


class SomeLikeItBot(BotBase.BotBase):
    """
    This Bot is a wuss,
    it will only attack when its enemy is equal or weaker
    """

    startPoint = 0

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        When no enemy to be seen, and weak act like sarahBot
        When no enemy to be seen, and strong, jumps away from start
        If enemy nearby:
        advance if Opposition weaker,
        retreat and defend if opposition stronger.
        """

        global startPoint
        pieces = self.board.findMyPieces()
        if currTurn == 1:
            startPoint = pieces[0]
        opponentPieces = self.board.findOpponentPieces()
        moves = []

        for location in pieces:
            arg = 1
            strength = self.board.getPieceStrength(location)

            if currTurn == 1:
                startPoint = location

            respond = False   # We will respond to enemy if within 2 spaces.
            for enemy in opponentPieces:
                if self.nearby(enemy, location, 2):
                    enemyNearby = enemy
                    respond = True
                    break

            if respond:
                # Enemy nearby, respond
                if self.board.getPieceStrength(enemyNearby) > strength:
                    # If enemy is stronger retreat
                    if self.leftThenRight(enemyNearby, location):
                        move = GameBoard.Action.RIGHT
                    else:
                        move = GameBoard.Action.LEFT
                else:
                    # If enemy weaker or equal advance
                    if self.leftThenRight(location, enemyNearby):
                        move = GameBoard.Action.RIGHT
                    else:
                        move = GameBoard.Action.LEFT

            else:
                # no enemy nearby and strong, jump away froms start,
                # else strengthen and split
                if strength > 10:
                    if self.leftThenRight(startPoint, location):
                        move = GameBoard.Action.JUMP_RIGHT
                    else:
                        move = GameBoard.Action.JUMP_LEFT

                    arg = strength/2

                else:
                    move = [GameBoard.Action.SPLIT,
                            GameBoard.Action.STAY][currTurn % 2]

            moves.append([location, str(move), arg])

        return moves

    def leftThenRight(self, location1, location2):
        """If location1 is left of location2 return True, else return False"""

        boardSize = self.board.boardSize
        if ((location1 - location2) % boardSize) > boardSize/2:
            return True
        else:
            return False

    def nearby(self, location1, location2, tolerance):
        """If location1 is within tolerance of location2 return True,
        else return False"""

        boardSize = self.board.boardSize
        distance = (location1 - location2) % boardSize
        if (distance < tolerance) or (distance > boardSize - tolerance):
            return True
        else:
            return False

    def sumSquares(self, i):
        """This sumsSquares up to and including n,
        it is irrelevant to this class
        and without an "square" function won't even work!"""

        retv = 0
        for n in range(i+1):
            retv += self.square(n)
        return retv

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'randomBot' and the correct line numbers for
        the debug challenge.
        """

        return \
            "2eac51633cdd1b705dc9184f76b4bcd1e0a92f32a138bbf02690bbcaa4e39610"
