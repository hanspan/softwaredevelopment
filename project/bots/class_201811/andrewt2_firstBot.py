"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return AndrewT2FirstBot()


class AndrewT2FirstBot(BotBase.BotBase):
    """This Bot reinforces its starting position and splits when possible
    Otherwise pieces will move away from the origin """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This function stores the initial location and will either reinforce or split 
        according to its strength. Other pieces will move left (if they are left of
        the origin) or right
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # store initial location or location of single piece on board
            if len(pieces) == 1:
                self.init_location = location

            # if piece is on starting point, stay put...
            if location == self.init_location:
                move = GameBoard.Action.STAY
                # ...unless we can split and leave a piece behind
                if self.board.getPieceStrength(location) in range(3,25,2):
                    # if self.board.getPieceStrength(location) % 2 == 1:
                    move = GameBoard.Action.SPLIT
            else:
                # if location is to "left", move "left"
                if location < self.init_location:
                    move = GameBoard.Action.JUMP_LEFT
                else:
                    # if location is to the "right" move "right"
                    move = GameBoard.Action.JUMP_RIGHT
            # move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
