"""
This is a basic Bot; it spawns new low-weight pieces, and then
makes them run to the right or left, as appropriate.

It should not take too much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return peterb3_Bot1()


class peterb3_Bot1(BotBase.BotBase):
    """This Bot just spawns low-weight pieces and runs the across the board."""

    def generateMoves(self, currTurn, maxTurns):   # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for each one.

        On the first turn, it identifies where the starting pieces are. This
        will then be used to determine how all future pieces move.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            # Choose moves randomly, until we've chosen a 'sensible' one
            done = 0
            while done == 0:
                done = 1
                move = random.choice(list(GameBoard.Action))
                strength = int(self.board.getPieceStrength(location))
                # Discard SPLIT with strength 1
                if move == GameBoard.Action.SPLIT and strength == 1:
                    done = 0
                # Discard JUMP (either direction) with strength 1
                if (move == GameBoard.Action.JUMP_LEFT and move == GameBoard.Action.JUMP_RIGHT) and strength == 1:
                    done = 0

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    def isMoveRight(self, pos):
        """Determine whether or not moving 'Right' is appropriate for a piece
        at the given location. This mainly comes down to determining whether
        this Bot started on the right or left half of the board, and using
        that information to decide what the proper interval is.
        """

        if (self.myStart < self.oppStart):
            # Started on the left.
            if (self.myStart < pos and pos < self.oppStart):
                return True
            return False

        # Started on the right.
        if (pos < self.oppStart or self.myStart < pos):
            return True
        return False

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'runnerBot' and the correct line numbers for
        the debug challenge.
        """
        if challenge == "debug":
            return ("f07c69b36fb8af7f8cdb699f38826f96"
                    "541c616e026a7ccaf410a8819aab0a8d")
        elif challenge == "database":
            return ("f06f4d7e7f1c1cbe06297e8b0ad60dae"
                    "3ea98674af6e8834dc89313cce657da8")

        return "Default Bot Signature :("
