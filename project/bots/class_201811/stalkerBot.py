"""
New Bot that splits, stays or moves towards an opponent, 
according to what the left and right nearest neighbours are. 

If both neighbours are friends, stay but split instead when strength>10 and is odd

Otherwise, target is the neighbour that is an opponent. 
If both are opponents, it's the weaker one, choosing the nearer in a tie. 
If target is weaker that you, move towards it. If it's very weak, split first. 
Otherise stay to gather strength 

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return StalkerBot()


class StalkerBot(BotBase.BotBase):
    """
    Moves towards a neighbouring enemy if strong enough 
    """


    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
                Moves towards a neighbouring enemy if strong enough 
                   """
        BOARD_SIZE = self.board.boardSize
        
        my_pieces = self.board.findMyPieces()
        opponent_pieces = self.board.findOpponentPieces()
        all_pieces = my_pieces+opponent_pieces

        moves = []
        for location in my_pieces:
            
            my_strength = self.board.tileStrength[location]
            
            arg = 1
            # find the nearest neighbours
            ldistance, lneighbour, rdistance, rneighbour = self.find_nearest_neighbours(BOARD_SIZE, location, all_pieces)
            
            # if both are friends, gather strength
            if lneighbour in my_pieces and rneighbour in my_pieces:
                moves.append([location, self.gather_strength(my_strength, GameBoard), arg])
                continue
             
            # find weakest neighbouring enemy
            op_strength = 100 ## stands in for when it's a friend
            if lneighbour in opponent_pieces:
                op_strength = self.board.tileStrength[lneighbour]
                dirn = str(GameBoard.Action.LEFT)
                gap = ldistance
                                        
            if rneighbour in opponent_pieces:
                if op_strength > self.board.tileStrength[rneighbour]:
                    op_strength = self.board.tileStrength[rneighbour]
                    dirn = str(GameBoard.Action.RIGHT)
                    gap = rdistance
                    
            # if both neighbours are enemies and equal strength want to move towards the nearer one 
            if lneighbour in opponent_pieces and rneighbour in opponent_pieces and self.board.tileStrength[lneighbour] == self.board.tileStrength[rneighbour]:
                    if ldistance <= rdistance:
                        dirn = str(GameBoard.Action.LEFT)
                        gap = ldistance
                    else:
                        dirn= str(GameBoard.Action.RIGHT)
                        gap=rdistance    
             
                    
            # move towards if he's weaker, including attempt to kill when he's next door    
            # ... but split if we're more than twice as strong
            if (2*op_strength) < my_strength:
                moves.append([location, str(GameBoard.Action.SPLIT), arg])
                continue
                  
            # try jumping if we can make a kill?       
            if gap > 1 and gap < 5 and op_strength+gap < my_strength:
                if dirn == str(GameBoard.Action.RIGHT):
                    dirn = str(GameBoard.Action.JUMP_RIGHT)
                else:    
                    dirn = str(GameBoard.Action.JUMP_LEFT)
                    
                moves.append([location, dirn, gap])
                continue
            else:
                if op_strength < my_strength:
                    moves.append([location, dirn, arg])
                    continue

                  
            # otherwise (if enemy is too strong) gather strength 
            # this avoids draws where both players just stay  
                else:   
                    moves.append([location, self.gather_strength(my_strength, GameBoard), arg])
                    continue
                
        return moves

    def gather_strength(self, my_strength, GameBoard): 
        if my_strength < 10 or 0 == (my_strength & 1):
            return str(GameBoard.Move.STAY)
        return str(GameBoard.Move.SPLIT)
    
    def find_nearest_neighbours(self, board_size, location, all_pieces):
        for i in range(1, board_size):
            if (location+i)%board_size in all_pieces:
                rneighbour = (location+i)%board_size
                rdistance = i
                break
        for i in range(1, board_size):
            if (location-i) % board_size in all_pieces:
                lneighbour = (location-i)%board_size
                ldistance = i
                break
        return ldistance, lneighbour, rdistance, rneighbour    
            
    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
