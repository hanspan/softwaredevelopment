"""
This bot has two moves, which it cycles through for each piece.
It does surprisingly well.
"""

import random
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """

    return SarahBot()


class SarahBot(BotBase.BotBase):
    """
    This Bot just cycles through two possibilities!
    """

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        Create the moves list - every piece does the same thing, depending
        only on what turn we are on.
        """
        pieces = self.board.findMyPieces()
        moves = []
        blockSize = 100
        #currBlock = currTurn // blockSize
        #print("currTurn = ",currTurn)
        #print("pieces = ", pieces)
        block = currTurn // blockSize
        #if ((block % 2) == 0):
        if not (500 < currTurn < 510):   
            #self.printPiecesAndStrength(pieces)
            moves = self.sarahBotBehaviour(pieces, currTurn)
        else:
            #print("I don't get here?")
            #moves = self.randomBotBehaviour(pieces)
            moves = self.stayOrJump(pieces, currTurn)
        return moves


    def printPiecesAndStrength(self,pieces):
        for location in pieces:
            print("(loc, strength) = {0}, {1}".format(location, self.board.getPieceStrength(location)))
        

    def jumpMove(self, pos, arg):
        """Do a random jump move"""
        move = random.choice(list(GameBoard.Action))
        arg = 1
        if (move == GameBoard.Action.JUMP_LEFT
            or move == GameBoard.Action.JUMP_RIGHT):
            arg = int(self.board.getPieceStrength(pos) / 2)

        return [pos, str(move), arg]

    def stayOrJump(self, pieces, currTurn):
        """Alternate stay or randomly move"""
        moves = []
        arg = 1
        if (currTurn % 2 == 0):
            moves = self.randomBotBehaviour(pieces)
        else:
            for location in pieces:
                moves.append([location, str(GameBoard.Action.STAY), arg])
        return moves
    
    def randomBotBehaviour(self, pieces):
        moves = []
        for location in pieces:
            move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])
        return moves
    
    def sarahBotBehaviour(self,pieces, currTurn):
        """Do the Sarah Bot"""
        moves = []
        for location in pieces:
            if(currTurn % 2 == 0):
                move = GameBoard.Action.SPLIT
            else:
                move = GameBoard.Action.STAY
            arg = 1
            moves.append([location, str(move), arg])

        return moves

        

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'sarahBot' and the correct line numbers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("df0e55b6d9dae2963eb6016fe1806537"
                    "c63c973c386748786a155538c345941a")
        elif challenge == "database":
            return ("7a32e1b3fd01db82331cb4ac2f5cf5c5"
                    "0d571247d20510ae672e77aae3821930")
        return "Default Bot Signature :("
