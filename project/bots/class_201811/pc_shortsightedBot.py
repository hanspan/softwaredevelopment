"""
This is a bot that performs simple moves based on adjacent pieces
and its own strength. Will jump fairly blindly if max strength.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PcShortSightedBot()

class PcShortSightedBot(BotBase.BotBase):
    """This Bot just performs simple moves based on adjacent pieces and its own strength"""

    def calculateMove(self, location):
        """
        Calculates move to do given board state
        """
        pieceStrength = self.board.getPieceStrength(location)
        leftStrength = self.board.getPieceStrength((location-1)%self.board.boardSize)
        rightStrength = self.board.getPieceStrength((location+1)%self.board.boardSize)
        # Calculate possible sum of strengths if moving left or right by one
        sumStrengthLeft = int(pieceStrength)+leftStrength + 1
        sumStrengthRight = int(pieceStrength)+rightStrength + 1
        maxStrength = self.board.maxTileStrength
        # split if odd strength and not wasteful, jump if maximum strength
        if sumStrengthLeft < maxStrength and sumStrengthRight < maxStrength:
            if pieceStrength%2 == 1 and pieceStrength > 1:
                move = GameBoard.Action.SPLIT
            else:
                move = GameBoard.Action.STAY
        elif pieceStrength == self.board.maxTileStrength:
            move = GameBoard.Action.JUMP_LEFT
        else:
            move = GameBoard.Action.STAY
        return move

    def dontJumpLeft(self, location, distance):
        """
        Returns condition to not use current jump left as will land on own piece and waste strength
        Returns True if should not jump
        """
        jump_location = (location - distance) % self.board.boardSize
        jump_control = self.board.tileControl[jump_location]
        jump_strength = self.board.getPieceStrength(jump_location)
        player_colour = self.board.player
        # Do not jump if land on own piece with even strength (would stay) and would waste strength
        if jump_control == player_colour and jump_strength > distance -1 and jump_strength%2 == 0:
            return True
        return False

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Locate all of our pieces on the board, and then make a move for
        each one.
        """

        myPieces = self.board.findMyPieces()

        moves = []

        for location in myPieces:
            move = self.calculateMove(location)

            # Most move commands don't need an argument, but the jump ones do.
            arg = 1
            if move == GameBoard.Action.JUMP_LEFT:
                arg = int(self.board.getPieceStrength(location) / 2)
                # Do not jump to somewhere that will lose strength
                while self.dontJumpLeft(location, arg):
                    arg -= 1

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of the bot and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("061b3ce920de79ad0d03a31ea672c1a5"
                    "b5ea057ffccb846fd9aa219bbb891b4f")
        elif challenge == "database":
            return ("2eb02b687b4dea8656e0f2825e70d475"
                    "ba8910d74e283c7542ec60cc04ac9b43")
        return "Default bot signature"
