"""
This bot is meant to use a simple minimax approach.
i.e. It will look at every possible combination of player move and opponent move.
It will score the resulting board.
It will choose the player move that maximises the minimum score.

It now broadly works but:
 - it is too slow to do it properly
 - it doesn't resolve battles properly
 - it doesn't take into account special squares
 - it never allows jumps
"""

import copy        # Performs a deep copy of an object
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


MAXPLAYERMOVES = 1000
MAXOPPONENTMOVES = 10

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return MinimaxBot()


class MinimaxBot(BotBase.BotBase):
    """This Bot should use a minimax approach"""

    @staticmethod
    def getMovesForLocation(board, location):
        """Returns some possible moves for a piece at a given location"""
        colour = board.tileControl[location]
        size = board.boardSize
        locationLeft = (location+size-1)%size
        locationRight = (location+1)%size
        locationTwoLeft = (location+size-2)%size
        locationTwoRight = (location+2)%size

        moves = []

        # If there is no piece here, then there can be no moves
        if board.getPieceStrength(location) == 0:
            return moves

        # At least, allow a piece to STAY
        moves.append([location, GameBoard.Action.STAY, 1])

        # Only allow a piece to jump 2 left if the two spaces to its left are uncontrolled
        if ((board.tileControl[locationLeft] == GameBoard.Colour.NEUTRAL) and
                (board.tileControl[locationTwoLeft] == GameBoard.Colour.NEUTRAL)):
            moves.append([location, GameBoard.Action.JUMP_LEFT, 2])

        # Only allow a piece to move left, if it doesn't control the space to its left
        elif board.tileControl[locationLeft] != colour:
            moves.append([location, GameBoard.Action.LEFT, 1])

        # Only allow a piece to jump 2 right if the two spaces to its right are uncontrolled
        if ((board.tileControl[locationRight] == GameBoard.Colour.NEUTRAL) and
                (board.tileControl[locationTwoRight] == GameBoard.Colour.NEUTRAL)):
            moves.append([location, GameBoard.Action.JUMP_RIGHT, 2])

        # Only allow a piece to move right if it doesn't control the space to its right
        elif board.tileControl[locationRight] != colour:
            moves.append([location, GameBoard.Action.RIGHT, 1])

        # Allow a piece to split if a space to either side is empty or enemy controlled
        if ((board.tileControl[locationLeft] != colour) or
                (board.tileStrength[locationLeft] == 0)):
            if ((board.tileControl[locationRight] != colour) or
                    (board.tileStrength[locationRight] == 0)):
                moves.append([location, GameBoard.Action.SPLIT, 1])

        return moves

    def getMoves(self, board, colour):
        """This should return a list of possible set of moves for a given colour"""

        allMoves = []
        allMoves.append([])
        for location in self.board.findPieces(colour):
            locationMoves = self.getMovesForLocation(board, location)
            if (colour == self.board.player and len(allMoves) >= MAXPLAYERMOVES):
                locationMoves = locationMoves[:1]
            elif (colour == self.board.opponent and len(allMoves) >= MAXOPPONENTMOVES):
                locationMoves = locationMoves[:1]

            allMoves = [a+[b] for a in allMoves for b in locationMoves]

        return allMoves

    @staticmethod
    def makeMove(board, strength, moveArray):
        """Applies the given move to the supplied strength array (inaccurately)"""
        size = board.boardSize

        location = moveArray[0]
        move = moveArray[1]
        arg = moveArray[2]

        if move == GameBoard.Action.STAY:
            strength[location] = min(
                strength[location] + board.tileStrength[location]+1,
                board.maxTileStrength)

        elif move == GameBoard.Action.RIGHT:
            strength[(location+1)%size] = min(
                strength[(location+1)%size] + board.tileStrength[location],
                board.maxTileStrength)

        elif move == GameBoard.Action.LEFT:
            strength[(location+size-1)%size] = min(
                strength[(location+size-1)%size] + board.tileStrength[location],
                board.maxTileStrength)

        elif move == GameBoard.Action.JUMP_RIGHT:
            strength[(location+arg)%size] += board.tileStrength[location] - (arg-1)

        elif move == GameBoard.Action.JUMP_LEFT:
            strength[(location+size-arg)%size] += board.tileStrength[location] - (arg-1)

        elif move == GameBoard.Action.SPLIT:
            amount = int(board.tileStrength[location] / 2)
            strength[(location+1)%size] += amount
            strength[(location+size-1)%size] += amount
            strength[location] += board.tileStrength[location] - (2 * amount)

    def processMoves(self, board, playerMove, opponentMove):
        """Apply a set of moves to a board. This is incomplete and inaccurate currently"""

        playerStrength = []
        opponentStrength = []

        for location in range(board.boardSize):
            playerStrength.append(0)
            opponentStrength.append(0)

        for moveArray in playerMove:
            self.makeMove(board, playerStrength, moveArray)

        for moveArray in opponentMove:
            self.makeMove(board, opponentStrength, moveArray)

        # Resolve battles (inaccurately)
        for location in range(board.boardSize):
            if playerStrength[location] > opponentStrength[location]:
                board.tileControl[location] = self.board.player
                board.tileStrength[location] = playerStrength[location] - opponentStrength[location]
            elif playerStrength[location] < opponentStrength[location]:
                board.tileControl[location] = self.board.opponent
                board.tileStrength[location] = opponentStrength[location] - playerStrength[location]
            else:
                board.tileStrength[location] = 0

    @staticmethod
    def printBoard(board):
        """Simple graphical output of board"""
        boardString = ""
        for location in range(board.boardSize):
            if board.tileControl[location] == GameBoard.Colour.NEUTRAL:
                boardString = boardString + "_"
            elif board.tileControl[location] == GameBoard.Colour.RED:
                if board.tileStrength[location] == 0:
                    boardString = boardString + "r"
                else:
                    boardString = boardString + "R"
            elif board.tileControl[location] == GameBoard.Colour.BLUE:
                if board.tileStrength[location] == 0:
                    boardString = boardString + "b"
                else:
                    boardString = boardString + "B"
        return boardString

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Generate a list of possible sets of moves, score them, and choose the highest scoring
        """

        allPlayerMoves = self.getMoves(self.board, self.board.player)[:MAXPLAYERMOVES]
        allOpponentMoves = self.getMoves(self.board, self.board.opponent)[:MAXOPPONENTMOVES]

        maxPlayerScore = 0
        maxPlayerMove = None
        for playerMove in allPlayerMoves:

            minOpponentMove = None
            minOpponentScore = 0
            for opponentMove in allOpponentMoves:
                boardcopy = copy.deepcopy(self.board)
                self.processMoves(boardcopy, playerMove, opponentMove)
                opponentScore = boardcopy.calculateScore(self.board.player)
                opponentScore -= boardcopy.calculateScore(self.board.opponent)
                if (minOpponentMove is None or opponentScore < minOpponentScore):
                    minOpponentScore = opponentScore
                    minOpponentMove = opponentMove

            playerScore = minOpponentScore
            if (playerScore > maxPlayerScore or maxPlayerMove is None):
                maxPlayerScore = playerScore
                maxPlayerMove = playerMove

        # debug stuff
        #print(self.printBoard(self.board))
        #print("allPlayerMoves: ",len(allPlayerMoves))
        #for i in range(len(allPlayerMoves)): print(i, allPlayerMoves[i])
        #print("allOpponentMoves: ",len(allOpponentMoves))
        #for i in range(len(allPlayerMoves)): print(i, allPlayerMoves[i])
        #print(maxPlayerMove)

        # must return the best move in a particular format
        return list([x[0], str(x[1]), x[2]] for x in maxPlayerMove)

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'minimaxBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("386f7bed2781624cfd1afdf234bd5313"
                    "21b3851088e3df3c1d5de6b78e45efab")
        elif challenge == "database":
            return ("601b556bb39f799251d8987c3135f795"
                    "83b96591aba96c6e0570688fe1f3b0d1")
        return "Default bot signature"
