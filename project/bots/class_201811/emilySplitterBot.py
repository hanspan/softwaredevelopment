"""
This is a splitter bot. It always splits if it can,
otherwise it stays.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SplitterBot()


class SplitterBot(BotBase.BotBase):
    """This Bot just splits all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one. Action will be split wherever possible, else stay.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            if strength == 1:
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.SPLIT
            arg = 1
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
