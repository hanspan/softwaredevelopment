"""
This is like Sarah's Bot (which is SPLIT on the first move, STAY on the second move, SPLIT on the third move, and so on, alternating between SPLIT and STAY)

except that on odd numbered moves
we STAY with probability 3/4
JUMP 10 to the left with probability 1/8
JUMP 10 to the right with probability 1/8

This seems to beat Sarah's Bot regularly (Vague reason: we try to obliterate Sarah's tiles by jumping to them)
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return peterb3Bot3()


class peterb3Bot3(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """See the top of the file for a description of what this bot does."""

        myPieces = self.board.findMyPieces()
        opponentPieces = self.board.findOpponentPieces()
        
        moves = []

        for location in myPieces:
            #split = 0
            #if currTurn != 0 and ((location+1 in opponentPieces) or (location-1 in opponentPieces)):
               # split = 1
            #if (currTurn % 2) == 0:
               # split = 1
            # grow or split depending on what move we are on.
            choice = random.choice([0,1,2,3,4,5,6,7])
            if currTurn % 2 == 0:
                move = GameBoard.Action.SPLIT
            else:
                if choice % 8 != 4:
                    move = GameBoard.Action.STAY
                elif choice == 0:
                    move = GameBoard.Action.JUMP_LEFT
                else:
                    move = GameBoard.Action.JUMP_RIGHT
                    
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                or move == GameBoard.Action.JUMP_RIGHT):
                arg = 10
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
