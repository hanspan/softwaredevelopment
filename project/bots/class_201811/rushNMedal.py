"""
This is Will's bot. There are many like it, but this one is mine

"""

# import sys
# import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return RushNMedal()


class RushNMedal(BotBase.BotBase):
    """
    This Bot currently rushes to populate the grow squares on the map, because they are valuable
    and greed is good
    The strategy may evolve
    """
    def __init__(self):
        '''
        Initialise some variables that persist over time.
        originalPiece is the location of where we start. We (perhaps pointlessly) treat this
            space as special
        Deathmode is a boolean to say how violent we are being. Deathmode means Brexit
        TurnsOfDeath is a decrementing counter that determines when we cease to try and kill
            everything
        Upcoming movesDict stores future moves for spaces. This allows us to relatively
            efficiently encode some simple strategy
            (e.g. we could tell a square to grow twice, then split)
            This is non-responsive to outside stimuli
        '''
        BotBase.BotBase.__init__(self)
        self.originalPiece = 0
        self.deathmode = False
        self.turnsOfDeath = 0
        self.upcomingMovesDict = ()
        self.currTurn = 0
    def generateMoves(self, currTurn, maxTurns):
        """
        This should take in a board state and return a list of moves for us to perform
        Strategy:
        At the start
          Stay twice, split into 3
          Send "runnerList" to take the grow points
          Leave the middle guy spewing out people
         At some point, we should just grow, then enter deathmode and steamroll everyone
        """
        self.currTurn = currTurn
        movesDict = dict()
        myPieces = dict()
        for piece in self.board.findMyPieces():
            myPieces[piece] = self.board.tileStrength[piece]
            movesDict[piece] = [str(GameBoard.Action.STAY), 1000]
        if currTurn <= 15:
            self.doEarlyTurns(movesDict, currTurn)
        else:
            self.doLateTurns(movesDict)
        if currTurn >= 16:
            runnerList = self.detectMiddleRunners(self.originalPiece)
#             print(runnerList, file=sys.stderr)
            for piece in runnerList:
                direction = self.findDirectionToLocation(self.originalPiece, piece)
                if piece in myPieces:
                    movesDict[piece][0] = str(direction)
        self.queryEnterDeathmode()
        if self.deathmode:
            self.deathModeBehaviour(movesDict)
        if self.originalPiece in myPieces:
            movesDict[self.originalPiece][0] = str(self.originalPieceBehaviour(currTurn))
        moves = []
        for piece in myPieces:
            moves.append([piece] + movesDict[piece])
        return moves
    def queryEnterDeathmode(self):
        '''
        Test if we want to enter Deathmode!
        The conditions for this are having a lot of dudes at full health
        Growers are lazy, fighters are brave supporters of the cause
        '''
        fullTiles = [self.board.tileStrength[piece] for piece in self.board.findMyPieces()]
        if fullTiles.count(self.board.maxTileStrength) > self.board.boardSize/3:
            #Enter Death Mode. Kill everything
            self.deathmode = True
            self.turnsOfDeath = 40
            #Choose a new original square, roughly in the center of our territory
            self.originalPiece = self.findClosestFriendly(self.findCenterOfMass(True))
    def doEarlyTurns(self, movesDict, currTurn):
        '''
        We hand craft the early moves to be especially efficient
        This means we can start putting on early pressure
        '''
        myPieces = self.board.findMyPieces()
        #Default behaviour is to stay
        for piece in myPieces:
            movesDict[piece][0] = str(GameBoard.Action.STAY)
        if currTurn == 1:
            self.originalPiece = sorted(self.board.findMyPieces())[0]
        if currTurn < 4 or 5 <= currTurn <= 6 or 8 <= currTurn <= 13:
            return
        if currTurn == 4: #Action away from the middle
            for piece in myPieces:
                movesDict[piece][0] = str(self.findDirectionToLocation(self.originalPiece, piece))
            return
        if currTurn == 7:
            for piece in myPieces:
                movesDict[piece][0] = str(GameBoard.Action.SPLIT)
            return
        self.doMediumTurns(movesDict, currTurn)
    def doMediumTurns(self, movesDict, currTurn):
        '''
        We need to join our towers into a single giant tower
        We then jump as far as we can, leaving 3 dudes
        '''
        myPieces = self.board.findMyPieces()
        if currTurn == 14:
            for piece in myPieces:
                if piece in [(self.originalPiece+1) % self.board.boardSize,
                             (self.originalPiece-3) % self.board.boardSize]:
                    movesDict[piece][0] = str(GameBoard.Action.RIGHT)
                elif piece in [(self.originalPiece-1) % self.board.boardSize,
                               (self.originalPiece+3) % self.board.boardSize]:
                    movesDict[piece][0] = str(GameBoard.Action.LEFT)
            return
        if currTurn == 15:
            for piece in myPieces:
                if (piece+2) % self.board.boardSize == self.originalPiece:
                    movesDict[piece][0] = str(GameBoard.Action.JUMP_LEFT)
                    movesDict[piece][1] = self.board.tileStrength[piece]-2
                elif (piece-2) % self.board.boardSize == self.originalPiece:
                    movesDict[piece][0] = str(GameBoard.Action.JUMP_RIGHT)
                    movesDict[piece][1] = self.board.tileStrength[piece]-2
            return
    def doLateTurns(self, movesDict):
        '''
        This is some generic strategy for regular play
        Generally we want to stay where we are, unless we can helpfully split
        '''
        # Set everything to something vanilla
        brdS = self.board.boardSize
        myPieces = self.board.findMyPieces()
        for piece in myPieces:
            movesDict[piece][0] = str(GameBoard.Action.STAY)
        strongPieces = []
        for piece in myPieces:
            if self.board.tileStrength[piece] >= 2:
                strongPieces.append(piece)
        for piece in strongPieces:
            for sign in [1, -1]:
                spaceToMove = ((piece+1*sign) % brdS) not in myPieces
                safeToMove = ((piece+2*sign) % brdS) not in self.board.findOpponentPieces()
                if spaceToMove and safeToMove:
                    if self.board.tileStrength[piece] % 2 == 1:
                        movesDict[piece][0] = str(GameBoard.Action.SPLIT)
                    if (piece-1*sign) % brdS in myPieces:
                        if self.board.tileStrength[(piece-1*sign) % brdS] >= 2:
                            movesDict[piece][0] = str(GameBoard.Action.SPLIT)
                            movesDict[(piece-1*sign)% brdS][0] = str(GameBoard.Action.SPLIT)
                            continue
    def originalPieceBehaviour(self, currTurn):
        '''
        The original piece behaves a bit weirdly. We want it to be an endless source of
        belly-splitting fun, spewing out dudes regardless of violence around it
        '''
        if self.deathmode:
            if len(self.detectMiddleRunners(self.originalPiece)) < 2:
                if self.findDist(self.originalPiece,
                                 self.findClosestFriendly(self.originalPiece, True)) < 4:
                    return GameBoard.Action.STAY
        if currTurn in [3, 10]:
            return GameBoard.Action.SPLIT
        elif currTurn < 15:
            return GameBoard.Action.STAY
        # The origin piece is magical and will spawn nice things for us hopefully
        elif self.board.tileStrength[self.originalPiece] >= 3:
            if self.board.tileStrength[self.originalPiece] % 2:
                return GameBoard.Action.SPLIT
        return GameBoard.Action.STAY
    def deathModeBehaviour(self, movesDict):
        '''
        Once death mode is initiated, all bets are off
        Go for the Jugular
        We don't want to be in death mode too long, so decrement a counter and
        potentially turn it off
        '''
        assert self.deathmode
        self.turnsOfDeath -= 1
        if self.turnsOfDeath <= 0:
            self.deathmode = False
            return
        # We are doomed if we are sending runner bots into combat. Sensibly turn it off
        if max([self.board.tileStrength[piece] for piece in self.board.findMyPieces()]) < 5:
            self.deathmode = False
            return
        for piece in self.board.findMyPieces():
            # All strong squares valiantly into battle
            if self.board.getPieceStrength(piece) > 0: #Todo: make based on maximum stacksize
                direction = self.findDirectionToLocation(piece, self.findClosestEnemy(piece))
                direction = self.findDirectionToLocation(self.originalPiece, piece)
                movesDict[piece][0] = str(direction)
            if self.findDist(self.originalPiece, piece) < 5:
                direction = self.findDirectionToLocation(self.originalPiece, piece)
                movesDict[piece][0] = str(direction)
    def detectMiddleRunners(self, middle):
        '''
        We may have sent out a bunch of runners from the middle
        if so, detect them and return a list of them
        if they have collided with another friendly guy, stop running, and return nothing
        They will be generated from a 3-spawner, which takes 3 turns to spawn, so the spacing
            should be 3
        We are looking for strings that look like 01001001001000* or 1001001001000* or 001001000*
        If we see e.g. 100101 we have hit something, so abort
        '''
        brdS = self.board.boardSize
        totalRunners = []
        for sign in [-1, 1]:
            tempRunners = []
            pos = -1
            if self.board.tileStrength[(middle+sign*1) % brdS] > 0:
                pos = (middle+1*sign) % brdS
                tempRunners.append(pos)
            elif self.board.tileStrength[(middle+sign*2) % brdS] > 0:
                pos = (middle+2*sign) % brdS
                tempRunners.append(pos)
            elif self.board.tileStrength[(middle+sign*3) % brdS] > 0:
                pos = (middle+3*sign) % brdS
                tempRunners.append(pos)
            if pos == -1:
                continue
            breakFlag = 0
            while True:
                tempRunners.append(pos)
                pos = (pos+3*sign) % brdS
                if self.board.tileStrength[(pos) % brdS] == 0:
                    breakFlag = 1
                if self.board.tileStrength[(pos-1*sign) % brdS] != 0:
                    breakFlag = 1
                if self.board.tileStrength[(pos-2*sign) % brdS] != 0:
                    breakFlag = 1
                if breakFlag:
                    break
            if self.board.tileStrength[(pos+1*sign) % brdS] == 0:
                if self.board.tileStrength[(pos+2*sign) % brdS] == 0:
                    #we have reached a large gap, and so wish to perform
                    totalRunners += tempRunners
            tempRunners = []
        return totalRunners
    def findDist(self, item1, item2):
        '''
        return the distance between item1 and item2
        '''
        #Verify both items are on the board
        assert isinstance(item1, int)
        assert isinstance(item2, int)
        assert max(item1, item2) < self.board.boardSize
        assert min(item1, item2) >= 0
        return min(abs(item1-item2), self.board.boardSize - abs(item1-item2))
    def findCenterOfMass(self, goodOrEvil=True):
        '''
        Todo: Find the point that minimises the average distance to friendly pieces.
        Also todo: Work out the correct norm.
        '''
        if goodOrEvil:
            pieces = self.board.findMyPieces()
        else:
            pieces = self.board.findOpponentPieces()
        bestScore = self.board.boardSize ** 2
        bestScoreL2Norm = None
        bestPiece = None
        for potentialPosition in range(self.board.boardSize):
            score = 0
            scoreL2Norm = 0
            for piece in pieces:
                score += self.findDist(potentialPosition, piece)
                scoreL2Norm += self.findDist(potentialPosition, piece)**2
            if score < bestScore or (score == bestScore and scoreL2Norm < bestScoreL2Norm):
                bestScore = score
                bestScoreL2Norm = scoreL2Norm
                bestPiece = potentialPosition
        assert bestPiece != None
        return bestPiece
    def findMostVulnerable(self, friendlyList, enemyList):
        '''
        Given all of my pieces, find the ones that are closest to any enemy pieces
        '''
        minDist = self.board.boardSize
        retList = []
        for friendly in friendlyList:
            for enemy in enemyList:
                if self.findDist(friendly, enemy) < minDist:
                    retList = [friendly]
                    minDist = self.findDist(friendly, enemy)
                elif self.findDist(friendly, enemy) == minDist:
                    retList.append(friendly)
        return retList
    def findClosestFriendly(self, boardSquare, notCurrentSquare=False):
        '''
        Given a square on the board, return the friendly piece that is closest to it
        Our board is taken mod boardSize, so we the min of 2 distances, rotating the mod value
        notCurrentSquare allows you to exclude the square you are measuring from.
        '''
        pieces = self.board.findMyPieces()
        closestPiece = None
        closestDist = self.board.boardSize
        for piece in pieces:
            if (piece != boardSquare or not notCurrentSquare):
                dist = self.findDist(piece, boardSquare)
                if dist < closestDist:
                    closestDist = dist
                    closestPiece = piece
        assert closestPiece != None
        return closestPiece
    def findClosestEnemy(self, boardSquare):
        '''
        Given a square on the board, return the enemy piece that is closest to it
        Our board is taken mod boardSize, so we the min of 2 distances, rotating the mod value
        '''
        pieces = self.board.findOpponentPieces()
        closest_piece = None
        closest_dist = self.board.boardSize
        for piece in pieces:
            dist = self.findDist(piece, boardSquare)
            if dist < closest_dist:
                closest_dist = dist
                closest_piece = piece
        assert closest_piece != None
        return closest_piece
    def findDirectionToLocation(self, startLocation, endLocation):
        '''
        Given our startLocation, find the direction of travel (in LEFT, RIGHT, STAY)
        which will minimise the time to the endLocation space
        examples:
            sL = 2, eL = 2, stay
            sL = 4, eL = 2, go left
            sl = 4, eL = 6 go right
            sL = 2, eL = boardSize - 2, go left
        '''
        leftSteps = (startLocation - endLocation) % self.board.boardSize
        rightSteps = (endLocation - startLocation) % self.board.boardSize
        if leftSteps > rightSteps:
            return GameBoard.Action.RIGHT
        elif leftSteps < rightSteps:
            return GameBoard.Action.LEFT
        return GameBoard.Action.STAY
    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'rushNMedal' and the hopefully correct answers for
        the debug challenge.
        """
        if challenge == "debug":
            return "60cbf6f6a3cab0c0b210b6c33d9ef76ece7d9a7375bdef8ae5b8a24ae3e6d823"
        elif challenge == "database":
            return "3a5629051686856cd1b658d30da0cea706b115d27032e6da2c71cb9d958cf200"
        return "Default bot signature"
