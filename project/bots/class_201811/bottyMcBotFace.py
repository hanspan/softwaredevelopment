"""
This is a bot that begins by splitting in two and
moving these pieces away from each other. After
twenty moves it begins to choose randomly between
staying and splitting.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return BottyMcBotFace()


class BottyMcBotFace(BotBase.BotBase):
    """This Bot chooses randomly between stay and split, except for
    its first 20 moves where it tries to get two pieces moving
    away from each other to generate better board coverage"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board.
        If currTurn is less than 20, hand the work off
        to generateEarlyMoves. Otherwise choose whether
        this time we're going to stay or split and make that
        move for every one of our pieces.
        """

        pieces = self.board.findMyPieces()
        if currTurn < 20:
            moves = self.generateEarlyMoves(currTurn, pieces)
            if moves:
                return moves
        # Decide whether we're going to stay or split this time
        move = random.choice([GameBoard.Action.SPLIT, GameBoard.Action.STAY])
        arg = 1
        moves = []

        for location in pieces:
            moves.append([location, str(move), arg])

        return moves

    def generateEarlyMoves(self, currTurn, myPieces):
        """
        Method to generate early moves, which will
        either be staying when I only have one piece
        of strength 1, splitting when I have one piece
        of strength > 1, or moving apart when I have
        two pieces. If none of the above situations
        hold then return None and let the calling
        function generate the moves instead.
        """
        if len(myPieces) == 1:
            strength = self.board.getPieceStrength(myPieces[0])
            if strength == 1:
                return [[myPieces[0], str(GameBoard.Action.STAY), 1]]
            return [[myPieces[0], str(GameBoard.Action.SPLIT), 1]]

        if len(myPieces) != 2:
            return None

        leftLocation = myPieces[1]
        rightLocation = myPieces[0]
        if ((myPieces[1] - myPieces[0]) % self.board.boardSize) < (self.board.boardSize // 2):
            leftLocation = myPieces[0]
            rightLocation = myPieces[1]
        moves = [[leftLocation, str(GameBoard.Action.LEFT), 1],
                 [rightLocation, str(GameBoard.Action.RIGHT), 1]]
        return moves


    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'bottyMcBotFace' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("30f0f14eb177e244887b3ef8fe69d094"
                    "db6904064e641266bcc24dfb08d6cae6")
        elif challenge == "database":
            return ("b3a8a3facfc2384a563a041f5fd9e01d"
                    "b883a475415c1049f49b2a64c45430f2")
        return "Default bot signature"
