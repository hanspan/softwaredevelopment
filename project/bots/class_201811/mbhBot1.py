"""
This is a basic Bot; this bot only stays put or moves right.
It should not take much to outperform this bot...
"""


import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return MbhBot1()


class MbhBot1(BotBase.BotBase):
    """This Bot just moves right or stays still!"""
    
    myStart = -1

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        This bot will split if even strength or stay if odd strength
        """        
        pieces = self.board.findMyPieces()
        moves = []
        if currTurn == 1:
            self.myStart = pieces[0]
            location = self.myStart
            move = GameBoard.Action.STAY
        elif currTurn < 4:
            move = GameBoard.Action.STAY
            location = self.myStart    
        elif currTurn == 4:
            move = GameBoard.Action.SPLIT
            location = self.myStart    #Strength now 3 and safe to split
        else:

                    for location in pieces:
                        piece_strength = self.board.getPieceStrength(location)
                    if (piece_strength % 2 == 0):
                        move = GameBoard.Action.SPLIT
                    else:
                        move = self.bunchUp(self.myStart, location, pieces)
                        move = GameBoard.Action.STAY
        arg = 1
        moves.append([location, str(move), arg])
    
        return moves
    
    def bunchUp(self,start, location, myPieces):
        """
        """
        leftNeighbour = location-1
        rightNeighbour = location+1
        if ((location > start+1) and not (leftNeighbour in myPieces)):
            print("bunching left")
            move = GameBoard.Action.LEFT
        elif ((location < start-1) and not (rightNeighbour in myPieces)):
            print("bunching right")
            move = GameBoard.Action.RIGHT
        else:
            move = GameBoard.Action.STAY
        return move
   
    def showGameState(self):
        """ shows some stats on the current state of the board"""
        print()
        print( "Board Size:        ", self.board.boardSize)
        print( "Max Tile Strength: ", self.board.maxTileStrength)
        print("Me:")
        myPieces = self.board.findMyPieces()
        for piece in myPieces:
            print( "  ", piece, "\t", self.board.getPieceStrength(piece) )
        print( "Them:")
        theirPieces = self.board.findOpponentPieces()
        for piece in theirPieces:
            strength = self.board.getPieceStrength(piece)
            print( "  ", piece, "\t", strength )
        print()

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
