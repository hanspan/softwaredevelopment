"""
harassSarahBot is a possibly improved version of sarahBot.
Question: The title of this bot has a special property. What is it?
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return harassSarahBot()


class harassSarahBot(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        harassSarahBot is a possibly improved version of sarahBot 
        (which is SPLIT on the first move, STAY on the second move, 
        SPLIT on the third move, and so on, 
        alternating between SPLIT and STAY)
        
        except that on odd numbered moves (counting 0-up)
        we STAY with probability 7/8
        JUMP 10 to the right with probability 1/8

        This seems to beat sarahBot regularly.
        Vague possible reason: we try to obliterate sarahBot's tiles 
        by jumping to them.
        There is scope for improvement,
        for example we don't use the locations of sarahBot's tiles.
        A reviewer has highlighted that JUMPing 10 with a strength of 10
        is equivalent to a STAY.
        """

        myPieces = self.board.findMyPieces()
        
        moves = []

        for location in myPieces:
            # Split on every other move, starting with the first.
            if currTurn % 2 == 0:
                move = GameBoard.Action.SPLIT
            # Otherwise, STAY with probablility 7/8, and JUMP_RIGHT by 10 with probability 1/8
            else:
                choice = random.choice([0,1,2,3,4,5,6,7])
                if choice == 0:
                    move = GameBoard.Action.JUMP_RIGHT
                else:
                    move = GameBoard.Action.STAY
            
            # If we're jumping, we need to specify an argument of 10: otherwise no argument is needed (we set it to 1 by default)
            arg = 1
            if move == GameBoard.Action.JUMP_RIGHT:
                arg = 10
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'harassSarahBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("21345a8860111d579ab7f9a96f7fe6d8"
                    "44d1ffeeedcec66d22e0920b8a25b80b")
        elif challenge == "database":
            return ("cc1ed1ac2e7143bb24a1e6cacffe94c1"
                    "bedf70eccc230b9375069275d8ece8c1")
        return "Default bot signature"
