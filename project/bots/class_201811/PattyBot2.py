"""
This is a basic Bot; this bot only stays put or moves right.
It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PattyBot()


class PattyBot(BotBase.BotBase):
    """This Bot just moves right or stays still!"""
    

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        This bot will always either stay put, or move to the right.
        """
        #if currTurn <= 10:
        #    self.showGameState()

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            
            if self.okToTheRight( pieces, location) :
                move = GameBoard.Action.RIGHT
            else:
                move = GameBoard.Action.STAY


            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    def okToTheRight( self, otherMes, me):
        """ 
        Return True if the closest piece to my right is an opponent that is weaker
        than I am
        """
        closestMe = me;
        for other in otherMes:
            if other <= me:
                next
            closestMe = other
            break
        
        otherGuys = self.board.findOpponentPieces()
        for guy in otherGuys:
            if guy < me:
                next
            if (guy > closestMe) and (closestMe != me):
                return False
            if self.board.getPieceStrength( guy ) >= self.board.getPieceStrength(me):
                return False
            break
        return True
    
    def showGameState(self):
        """ shows some stats on the current state of the board"""
        print()
        print( "Board Size:        ", self.board.boardSize)
        print( "Max Tile Strength: ", self.board.maxTileStrength)
        print("Me:")
        myPieces = self.board.findMyPieces()
        for piece in myPieces:
            print( "  ", piece, "\t", self.board.getPieceStrength(piece) )
        print( "Them:")
        theirPieces = self.board.findOpponentPieces()
        for piece in theirPieces:
            strength = self.board.getPieceStrength(piece)
            print( "  ", piece, "\t", strength )
        print()

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
