"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RunawayBot()


class RunawayBot(BotBase.BotBase):
    """Tries to get away!"""


    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

           For each piece find the nearest opponent. If it's < SAFE_DISTANCE away, stick, otherwise move 1 step away if you can 
            
        """

        BOARD_SIZE=self.board.boardSize 
        SAFE_DISTANCE=BOARD_SIZE//4

        
        my_pieces = self.board.findMyPieces()
        opponent_pieces=self.board.findOpponentPieces()
        all_pieces=my_pieces+opponent_pieces

        moves = []

        for location in my_pieces:
            
            # what if there aren't any opponent pieces?

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1

            ## if you can't move, stick
            if location+1 in all_pieces and (location-1)%BOARD_SIZE in all_pieces:
                moves.append([location, str(GameBoard.Action.STAY), arg])
                continue

            moved=False
            for i in range(1, SAFE_DISTANCE):
                if (location+i)%BOARD_SIZE in opponent_pieces and (location-i)%BOARD_SIZE not in opponent_pieces:
                    moves.append([location, str(GameBoard.Action.LEFT), arg])
                    moved=True
                    break
                
                if (location-i)%BOARD_SIZE in opponent_pieces and (location+i)%BOARD_SIZE not in opponent_pieces:
                    moves.append([location, str(GameBoard.Action.RIGHT), arg])
                    moved=True
                    break

                ## if there's a tie, stick 
                if (location-i)%BOARD_SIZE in opponent_pieces and (location+i)%BOARD_SIZE in opponent_pieces:
                    moves.append([location, str(GameBoard.Action.STAY), arg])
                    moved=True
                    break
  
            ## safe so stick 
            if not moved:
                moves.append([location, str(GameBoard.Action.STAY), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
