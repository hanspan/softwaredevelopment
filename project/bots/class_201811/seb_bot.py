"""
This is a Seb's Bot, and this is the docstring for Seb's bot.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SebBot()


class SebBot(BotBase.BotBase):
    """
    This bot randomly decides whether to split with probability 2/3, or stay with probability 1/3.
    This is more than likely terrible.
    """

    def generateMoves(self, currTurn, maxTurns):
        """
        The function that works out what moves to run

        :param currTurn: Presumably the current move number
        :param maxTurns: The maximum number of moves?
        :return: Moves to do
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # We will randomly decide whether we should split, or stay.
            # Play it defensive and just sit and wait for the opponent to make an attack on us.

            if random.random() < 1/3:
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.SPLIT

            arg = 1
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        Not sure what this is, just stole it from the randomBot.

        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
