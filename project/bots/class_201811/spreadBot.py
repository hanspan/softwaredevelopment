"""
This is a simple spreading bot that spreads out its pieces from the
centre location.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SpreadBot()


class SpreadBot(BotBase.BotBase):
    """This Bot creates a spreading pattern."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one."""
        
        pieces = self.board.findMyPieces()
        opponent_pieces = self.board.findOpponentPieces()
        moves = []

        if currTurn == 1:
            self.start_position = pieces[0]
            self.leftmost = self.start_position - 1
            self.rightmost = self.start_position + 1
        elif currTurn < self.board.boardSize/2:
            for location in pieces:
                strength = self.board.getPieceStrength(location)
                if strength < 2:
                    move, arg = GameBoard.Action.STAY, 1
                elif location == self.leftmost:
                    if currTurn%2 == 1:
                        move, arg = GameBoard.Action.JUMP_LEFT, 2
                    else:
                        move, arg = GameBoard.Action.SPLIT, 1
                    self.leftmost = (self.leftmost - arg)%self.board.boardSize
                elif location == self.rightmost:
                    if currTurn%2 == 1:
                        move, arg = GameBoard.Action.JUMP_RIGHT, 2
                    else:
                        move, arg = GameBoard.Action.SPLIT, 1
                    self.rightmost = (self.rightmost + arg)%self.board.boardSize
                else:
                    move, arg = GameBoard.Action.SPLIT, 1
                moves.append([location, str(move), arg])
            return moves

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            
            # Split if:
            #   1. Piece strength is odd and greater or equal to 
            #   2. Piece strength is equal to maximal strength
            arg = 1
            left = (location - 1)%self.board.boardSize
            right = (location + 1)%self.board.boardSize
            if location in self.board.growthTiles and strength < self.board.maxTileStrength - self.board.growthBonusValue:
                move = GameBoard.Action.STAY
            elif left in opponent_pieces and self.board.getPieceStrength(left) < strength:
                if self.board.getPieceStrength(left) < strength/2 and False:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.LEFT
                arg = 1
            elif right in opponent_pieces and self.board.getPieceStrength(right) < strength:
                if self.board.getPieceStrength(right) < strength/2 and False:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.RIGHT
                arg = 1
            elif strength >= self.board.maxTileStrength/2:
                if left in opponent_pieces or right in opponent_pieces:
                    move = GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.SPLIT
            elif location in self.board.defenceTiles:
                move = GameBoard.Action.STAY
            elif (strength >= 3 and strength%2 == 1):
                move = GameBoard.Action.SPLIT
            else:
                move = GameBoard.Action.STAY

            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
