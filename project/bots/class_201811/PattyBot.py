"""
This is a basic Bot; This bot always moves right.

It is nearly impossible not to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PattyBot()


class PattyBot(BotBase.BotBase):
    """This Bot just moves right all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        This bot always moves right.
        """

        pieces = self.board.findMyPieces()
        boardSize = self.board.boardSize
        moves = []
        #arg = 1

        for location in pieces:

            #print( "I am taking a turn.", currTurn, " of ", maxTurns )
            
            move = GameBoard.Action.RIGHT
            arg = 1
            #if okToTheRight( location, pieces ):
             #   move = "Right"

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
    
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

 

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
