"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

import sys


global print_toggle_variable
print_toggle_variable = 0


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return WillPseudorandomBot()


class WillPseudorandomBot(BotBase.BotBase):
    """This Bot currently rushes to populate the grow squares on the map, because they are valuable"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """
        myPieces = dict()
        for piece in self.board.findMyPieces():
            myPieces[piece] = self.board.tileStrength[piece]
        yourPieces = dict()
        for piece in self.board.findOpponentPieces():
            yourPieces[piece] = self.board.tileStrength[piece]
        # Note, turn counters are 1-up
        if currTurn < 10:
            pass
            if print_toggle_variable:
                print(myPieces, file=sys.stderr)
            #print(yourPieces, file=sys.stderr)
        else:
            pass
        
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            
            
            moves.append([location, str(move), arg])
        
        #Strategy:
        #At the start
        #  Stay twice, split into 3
        #  Send "runners" to take the grow points
        #  Leave the middle guy spewing out people
        #Store based on location for easier referencing
        
        
        moves_dict = dict()
        for move in moves:
            moves_dict[move[0]] = move[1:]
        
        
        
        
        for i in moves_dict:
            if self.board.tileStrength[i] < 3:
                moves_dict[i][0] = str(GameBoard.Action.STAY)
            elif (self.board.tileStrength[i] > self.board.maxTileStrength - 3) and moves_dict[i][0] == str(GameBoard.Action.STAY):
                # nearly capped out
                # check if can be sensibly split
                pass
            
            elif self.board.tileStrength[i] >= 2:
                # odd number with 2 neighbours, split
                if self.board.tileStrength[(i+1)%self.board.boardSize] == 0 and self.board.tileStrength[(i-1)%self.board.boardSize] == 0:
                    if self.board.tileStrength[(i+2)%self.board.boardSize] >= 2 and self.board.tileStrength[(i+3)%self.board.boardSize] == 0:
                        moves_dict[i][0] = str(GameBoard.Action.LEFT)
                    else:
                        moves_dict[i][0] = str(GameBoard.Action.SPLIT)
                elif self.board.tileStrength[(i+1)%self.board.boardSize] == 0 and self.board.tileStrength[(i+2)%self.board.boardSize] == 0:
                    moves_dict[i][0] = str(GameBoard.Action.RIGHT)
                elif self.board.tileStrength[(i-1)%self.board.boardSize] == 0 and self.board.tileStrength[(i-2)%self.board.boardSize] == 0:
                    moves_dict[i][0] = str(GameBoard.Action.LEFT)
            if moves_dict[i][0].startswith("JUMP"):
                moves_dict[i][0] = str(GameBoard.Action.STAY)
            if i in self.board.growthTiles and self.board.tileStrength[i] < self.board.maxTileStrength -3:
                moves_dict[i][0] = str(GameBoard.Action.STAY)
        
        if currTurn < 3:
            for i in moves_dict:
                moves_dict[i][0] = str(GameBoard.Action.STAY)
        elif currTurn == 3:
            for i in moves_dict:
                moves_dict[i][0] = str(GameBoard.Action.SPLIT)
        else:
            for growthTile in self.board.growthTiles:
                closestFriendly = self.findClosestFriendly(growthTile)
                if currTurn < 10:
                    if print_toggle_variable:
                        print([growthTile,closestFriendly], file=sys.stderr)
                if closestFriendly != growthTile:
                    growthDirection = self.findDirectionToLocation(closestFriendly, growthTile)
                    moves_dict[closestFriendly][0] = str(growthDirection)
        #Hardcoded hack because both growthtiles are in positive direction
        for growthTile in self.board.growthTiles:
            if growthTile in moves_dict and self.board.tileStrength[growthTile] == 1:
                moves_dict[growthTile][0] = str(GameBoard.Action.STAY)
            closestFriendly = self.findClosestFriendly(growthTile)
            if closestFriendly != growthTile and closestFriendly in self.board.growthTiles and self.board.tileStrength[closestFriendly] > 1:
                moves_dict[closestFriendly][0] = str(GameBoard.Action.SPLIT)
        moves = []
        for i in moves_dict:
            moves.append([i] + moves_dict[i])
        return moves
    
    def findDist(self, item1, item2):
        '''
        return the distance between item1 and item2
        '''
        #Verify both items are on the board
        assert(max(item1,item2) < self.board.boardSize)
        assert(min(item1,item2) < self.board.boardSize)
        return min(abs(item1-item2), self.board.boardSize - abs(item1-item2))
        
    def findCenterOfMass(self, goodOrEvil):
        '''
        Todo: Find the point that minimises the average distance to friendly pieces.
        Also todo: Work out the correct norm. 
        '''
    def findMostVulnerable(self, friendlyList, enemyList):
        '''
        Given all of my pieces, find the ones that are closest to any enemy pieces
        '''
        minDist = self.board.boardSize
        retList = []
        for friendly in friendlyList:
            for enemy in enemyList:
                if self.findDist(friendly, enemy) < minDist:
                    retList = [friendly]
                    minDist = self.findDist(friendly, enemy)
                elif self.findDist(friendly, enemy) == minDist:
                    retList.append(friendly)
        return retList
    def findClosestFriendly(self, boardSquare):
        '''
        Given a square on the board, return the friendly piece that is closest to it
        Our board is taken mod boardSize, so we the min of 2 distances, rotating the mod value
        '''
        pieces = self.board.findMyPieces()
        closest_piece = None
        closest_dist = self.board.boardSize
        for piece in pieces:
            dist = self.findDist(piece,boardSquare)
            if dist < closest_dist:
                closest_dist = dist
                closest_piece = piece
        assert(closest_piece != None)
        return closest_piece
    
    def findDirectionToLocation(self, startLocation, endLocation):
        '''
        Given our startLocation, find the direction of travel (in LEFT, RIGHT, STAY)
        which will minimise the time to the endLocation space
        examples:
            sL = 2, eL = 2, stay
            sL = 4, eL = 2, go left
            sl = 4, eL = 6 go right
            sL = 2, eL = boardSize - 2, go left
        '''
        leftSteps = (startLocation - endLocation) % self.board.boardSize
        rightSteps = (endLocation - startLocation) % self.board.boardSize
        if leftSteps > rightSteps:
            return GameBoard.Action.RIGHT
        elif leftSteps < rightSteps:
            return GameBoard.Action.LEFT
        else:
            return GameBoard.Action.STAY
        
    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
