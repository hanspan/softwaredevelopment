"""
I, rob bot
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return IRobBot()


class IRobBot(BotBase.BotBase):
    """
    Pick a direction early on and always move in that direction,
    unless you're on a growth tile.
    """

    direction = None

    def signedDistance(self, start, end):
        """
        return the distance from start to end; negative if
        end is to the left of start (taking wrapping into account)
        """
        dist = (end - start) % self.board.boardSize
        if dist > self.board.boardSize/2:
            dist -= self.board.boardSize
        return dist


    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []

        # pick a direction based on getting to the nearest
        # growth tile as quickly as possible.
        # This only happens once, before any tiles move

        if self.direction is None:
            location = pieces[0]

            minDistance = 999999
            for tile in self.board.growthTiles:
                distance = self.signedDistance(location, tile)
                if abs(distance) < abs(minDistance):
                    minDistance = distance

            if minDistance <= 0:
                self.direction = GameBoard.Action.LEFT
            else:
                self.direction = GameBoard.Action.RIGHT

        # all pieces move in the chosen direction all the time,
        # except those on growth tiles, which SPLIT if they can

        for location in pieces:
            if location in self.board.growthTiles:
                move = GameBoard.Action.SPLIT
                if self.board.getPieceStrength(location) < 7:
                    move = GameBoard.Action.STAY
            else:
                move = self.direction

            arg = 1
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'iRobBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("367a6c9477e6e354beeaabb416acb900"
                    "6ee7a90e5d6b10414d01ddfc5df8abd9")
        elif challenge == "database":
            return ("8bb1cfc0dafd54d2548e29bc9cb4fc78"
                    "2d874f822cb6faa05698e949f31bf064")
        return "Default bot signature"
