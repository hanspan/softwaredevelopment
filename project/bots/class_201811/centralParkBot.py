"""
This module provides a bot based using a simple generator of runners from
the starting position.  The bot builds runners up to full strength as they
move away from the centre.  New runners are released from the centre as
soon as they are strong enough.
"""

__author__ = "Andrew"
__version__ = "1.0"

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

def getBot() -> BotBase.BotBase:
    """Creates an instance of the class supported by this module.

    Returns:
        A instance of the BaseBot representing the class supported by this
        module.
    """

    return CentralParkBot()


class CentralParkBot(BotBase.BotBase):
    """CentralParkBot() -> object

    Creates a instance of the CentralParkBot class.  See module documentation
    for details.
    """
    def __init__(self):
        BotBase.BotBase.__init__(self)

        # Reserve a variable for the start location
        self._start_location = -1

    def generateMoves(self, currTurn: int, maxTurns: int) -> list:     # @UnusedVariable
        """x.generateMoves(...) -> list of moves

        Generates a set of moves to be made by this bot.

        Arguments:
            currTurn  The current turn number.  The first turn is one.
            maxTurns  The number of turns in the game.

        Returns:
            A list of moves.  Each move is represented by a list containing
            three elements: the location, a move string (given by
            GameBoard.Action) and the move's argument.  For example,
                [[25, "Stay", 1], [26, "Jump_Right", 2]]
        """

        pieces = self.board.findMyPieces()
        moves = []

        if currTurn == 1:
            # Set the starting position and stay put
            self._start_location = pieces[0]

        for location in pieces:
            arg = 1
            strength = self.board.getPieceStrength(location)
            distance = self.getManhattanDistance(location, self._start_location)

            if distance == 0 and len(pieces) > 1:
                if currTurn < 6 or strength < 5:
                    move = GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.JUMP_LEFT
                    arg = 3
            elif distance <= 2:
                if strength >= 3:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
            else:
                if currTurn%2 == 0:
                    move = GameBoard.Action.STAY
                else:
                    move = self.getDirection(location, self._start_location)

            moves.append([location, str(move), arg])

        return moves

    def getDirection(self, location: int, reference: int) -> GameBoard.Action:
        """x.getDirection(location, reference) -> direction

        Provides the direction from a reference position to a piece taking
        into account the periodicity of the board.

        Arguments:
            location   The location of the piece.
            reference  The reference position (e.g. start position).

        Returns:
            A GameBoard.Action object specifying the direction to take or
            STAY if the distance in both directions is equal (i.e. same
            tile or opposite tile).
        """
        left_distance = (reference - location)%self.board.boardSize
        right_distance = (location - reference)%self.board.boardSize

        if right_distance < left_distance:
            return GameBoard.Action.RIGHT
        elif left_distance < right_distance:
            return GameBoard.Action.LEFT
        return GameBoard.Action.STAY

    def getManhattanDistance(self, location: int, start: int) -> int:
        """x.getManhattanDistance(location, reference) -> distance

        Gets the Manhattan distance from the start position taking into
        account periodicity.

        Arguments:
            location   The location of piece.
            reference  The reference position (e.g. start position).
        Returns:
            The shortest distance from the location to the reference
            position.
        """

        left_distance = (start - location)%self.board.boardSize
        right_distance = (location - start)%self.board.boardSize
        return min(left_distance, right_distance)

    @staticmethod
    def getSignature(challenge: str = "debug"):
        """
        This is the hash of CentralParkBot and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("a26407dec38844494f022b6877bef248"
                    "5a1c74d200afd3264080e85a2b5447ef")
        elif challenge == "database":
            return ("629a784b617f655f5d63a83ef339fa08"
                    "8fbde98ec5a173ea0ba89c88dc093f75")
        return "Default bot signature"
