"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomSpreadBot()


class RandomSpreadBot(BotBase.BotBase):
    """This Bot makes random moves but with an effort to spread and stay alive"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        Randomly choose a move per piece, but with the following constraints:
        - If strength less than 4, only stay or move 1
        - If choice is a move (by 1 or a jump), choose direction away from 
          my nearest piece
        - When jumping, lose 1/3 of strength
        """

        boardSize = self.board.boardSize
        pieces = self.board.findMyPieces()
        moves = []
        for pieceNum in range(len(pieces)):
            location = pieces[pieceNum]
            arg = 1
            # Restrict to safe choices if low on strength 
            if self.board.getPieceStrength(location) < 4:
                move = random.choice([GameBoard.Action.STAY, GameBoard.Action.LEFT, GameBoard.Action.RIGHT])
            else:
                move = random.choice(list(GameBoard.Action))
            # If we have other pieces, ensure any moves are directed away from the nearest piece
            if len(pieces) > 1:
                nearestRight = pieces[(pieceNum+1) % len(pieces)]
                nearestLeft = pieces[(pieceNum-1) % len(pieces)]
                if move in (GameBoard.Action.LEFT, GameBoard.Action.RIGHT):
                    if ((nearestRight - location) % boardSize > (location - nearestLeft) % boardSize):
                        move = GameBoard.Action.RIGHT
                    else:
                        move = GameBoard.Action.LEFT
                elif move in (GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT):
                    if ((nearestRight - location) % boardSize > (location - nearestLeft) % boardSize):
                        move = GameBoard.Action.JUMP_RIGHT
                    else:
                        move = GameBoard.Action.JUMP_LEFT

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up 1/4 their strength while jumping.
            if (move == GameBoard.Action.JUMP_LEFT
                or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 4)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
