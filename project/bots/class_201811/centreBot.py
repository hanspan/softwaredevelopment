"""
This is a simple bot that generates runners from the starting position and
sends them out, building up to maximum strength as it heads along. 
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return CentreBot()


class CentreBot(BotBase.BotBase):
    """A centre section splits to create runners and these are grown at 
       each square to the required strength."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one."""

        pieces = self.board.findMyPieces()
        moves = []
        if currTurn == 1:
            # Set the starting position
            self.start_location = pieces[0]
          
        for location in pieces:
            arg = 1
            strength = self.board.getPieceStrength(location)
            
            if self.start_location - 2 <= location <= self.start_location + 2:
                if strength > 3:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
            else:
                if strength < self.getDistance(location, self.start_location)*1 and strength < self.board.maxTileStrength:
                    move = GameBoard.Action.STAY
                else:
                    move = self.getDirection(location, self.start_location)

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            moves.append([location, str(move), arg])

        return moves

    def getDirection(self, location, start):
        """Get the of location relative to the start location"""
        left_distance = (start - location)%self.board.boardSize
        right_distance = (location - start)%self.board.boardSize
        
        if right_distance < left_distance:
            return GameBoard.Action.RIGHT
        elif left_distance < right_distance:
            return GameBoard.Action.LEFT
        else:
            return GameBoard.Action.STAY
    def getDistance(self, location, start):
        
        """Get the distance from the start position"""
        left_distance = (start - location)%self.board.boardSize
        right_distance = (location - start)%self.board.boardSize
        return min(left_distance, right_distance)
        
    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
