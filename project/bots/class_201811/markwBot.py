"""
This bot is meant to use a simple minimax approach.
i.e. It will look at every possible combination of player move and opponent move.
It will score the resulting board.
It will choose the player move that maximises the minimum score.

It doesn't work yet.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import copy        # Performs a deep copy of an object

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return MarkWBot()


class MarkWBot(BotBase.BotBase):
    """This Bot should use a minimax approach"""

    def getAllMovesForLocation(self, board, location):
        """Returns all possible moves for a piece at a given location"""
        moves = []
        if self.board.getPieceStrength(location) > 0:
            for move in list(GameBoard.Action):
                if (move == GameBoard.Action.JUMP_LEFT
                        or move == GameBoard.Action.JUMP_RIGHT):
                    for arg in range(2,self.board.getPieceStrength(location)):
                        moves.append([location, move, arg]) 
                else:
                    moves.append([location, move, 1])  
        return moves
     
    def getAllMoves(self, board, colour):
        """This should return a list of every possible set of moves for a given colour
        Currently this just returns a simple subset"""
        allMoves = []
        location = self.board.findPieces(colour)[0]
        locationMoves = self.getAllMovesForLocation(board, location)
        for move in locationMoves:
            allMoves.append([move])
        return allMoves
     
    def scoreBoard(self, board, colour):
        """Returns a score for the board, hopefully mirroring what the server does""" 
        control = 0
        pieces = 0
        strength = 0
        for i in range(len(board.tileControl)):
            if board.tileControl[i] == colour:
                control += 1
                strength += board.tileStrength[i]
                if board.tileStrength[i] > 0:
                    pieces += 1
     
        total = control + pieces + strength
        #print(colour,pieces,control,strength,total)
        return total
 
    def makeMove(self, board, colour, location, move):
        """An incomplete implementation of an individual pieces move applied to a board""" 
        size = len(board.tileStrength)
 
        if move == GameBoard.Action.STAY:
            board.tileStrength[location] += 1
            if (board.tileStrength[location] > 25): board.tileStrength[location] = 25 
 
        if move == GameBoard.Action.RIGHT:
            board.tileStrength[(location+1)%size] = board.tileStrength[location]
            board.tileStrength[location] = 0
 
        if move == GameBoard.Action.LEFT:
            board.tileStrength[(location+size-1)%size] = board.tileStrength[location]
            board.tileStrength[location] = 0
 
        if move == GameBoard.Action.JUMP_RIGHT:
            arg = int(board.getPieceStrength(location) / 2)
            board.tileStrength[(location+arg)%size] = board.tileStrength[location] - (arg-1)
            board.tileStrength[location] = 0
 
        if move == GameBoard.Action.JUMP_LEFT:
            arg = int(board.getPieceStrength(location) / 2)
            board.tileStrength[(location+size-arg)%size] = board.tileStrength[location] - (arg-1)
            board.tileStrength[location] = 0
   
        if move == GameBoard.Action.SPLIT:
            amount = board.tileStrength[location] / 2;
            board.tileStrength[(location+1)%size] = amount
            board.tileStrength[(location+size-1)%size] = amount
            board.tileStrength[location] = board.tileStrength[location] - (2 * amount)

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Generate a list of all possible sets of moves, score them, and choose the highest scoring
        """

        allRedMoves = self.getAllMoves(self.board,GameBoard.Colour.RED)
        allBlueMoves = self.getAllMoves(self.board,GameBoard.Colour.BLUE)
         
        #print("allRedMoves: ",len(allRedMoves))
        #print("0: ", allRedMoves[0])
        #print("1: ", allRedMoves[1])
        #print("allBlueMoves: ",len(allBlueMoves))
         
        redScores={}
        for redMove in allRedMoves:
            #blueScores=[]
            #for blueMove in allBlueMoves:
            #    boardcopy = copy.deepcopy(self.board)
            #    #boardcopy.processMoves(redMove, blueMove)
            #    blueScores[blueMove]=self.scoreBoard(boardcopy,GameBoard.Colour.RED)-self.scoreBoard(boardcopy,GameBoard.Colour.BLUE)
            #minBlueMove=min(blueScores.keys(), key=lambda x: blueScores[x])
            #redScores[redMove]=blueScores[minBlueMove]
            redScores[tuple([tuple(x) for x in redMove])]=1
        maxRedMove=max(redScores.keys(), key=lambda x: redScores[x])

        #print("maxRedMove: ",maxRedMove)
        #print("maxRedMove: ",list([x[0],str(x[1]),x[2]] for x in maxRedMove))
        moves = list([x[0],str(x[1]),x[2]] for x in maxRedMove)
        return moves
        

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
