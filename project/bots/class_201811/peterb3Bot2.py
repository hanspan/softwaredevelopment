"""
This is a fairly basic Bot.

First it finds the strength of my strongest piece(s), and the location of one of them
and the strength of my opponents strongest piece(s), and the location of one of them

then 

it identifies the pieces it has on the game and then picks a random move for each piece.
Except that if the location is that of my strongest piece, 
and is 10 or less either way from my opponents strongest piece,
and is 2* or more,
and the strength of my piece is greater than the distance,
a JUMP move is made onto my opponents strongest piece.

* because jumps of 1 are not allowed

This bot should be better than the random but, 
but it should still not take much to outperform it...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return peterb3Bot2()


class peterb3Bot2(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """See the top of the file for a description of what this bot does."""

        myPieces = self.board.findMyPieces()
        opponentPieces = self.board.findOpponentPieces()
        
        # Find (one of) my strongest pieces, and its location
        myLargestStrength = -1
        myLargestStrengthLoc = 0
        for location in myPieces:
            strength = int(self.board.getPieceStrength(location))
            if strength > myLargestStrength:
                myLargestStrength = strength
                myLargestStrengthLoc = location
        
        # Find (one of) my opponents pieces, and its location
        opponentLargestStrength = -1
        opponentLargestStrengthLoc = 0
        for location in opponentPieces:
            strength = int(self.board.getPieceStrength(location))
            if strength > opponentLargestStrength:
                opponentLargestStrength = strength
                opponentLargestStrengthLoc = location
            
        moves = []

        for location in myPieces:
            
            # Basic (probably poor) strategy: 
            # If the location is that of my strongest piece, 
            # and is 10 or less either way from my opponents strongest piece,
            # and is 2* or more,
            # and the strength of my piece is greater than the distance,
            # a JUMP move is made onto my opponents strongest piece.
            # Otherwise, make a random move.
            #
            # * because jumps of 1 are not allowed
            if (location == myLargestStrengthLoc and opponentLargestStrengthLoc < location 
                and location - opponentLargestStrengthLoc <= 10
                and location - opponentLargestStrengthLoc >= 2
                and myLargestStrength > location - opponentLargestStrengthLoc):
                move = GameBoard.Action.JUMP_LEFT
                arg = location - opponentLargestStrengthLoc
            elif (location == myLargestStrengthLoc and location < opponentLargestStrengthLoc
                  and opponentLargestStrengthLoc - location <= 10
                  and opponentLargestStrengthLoc - location >= 2
                  and myLargestStrength > opponentLargestStrengthLoc - location):
                move = GameBoard.Action.JUMP_RIGHT
                arg = opponentLargestStrengthLoc - location
                
            else:
                move = random.choice(list(GameBoard.Action))

                # Most move commands don't need an argument, but the jump ones do.
                # Just have them use up half their strength while jumping.
                arg = 1
                if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                    arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
