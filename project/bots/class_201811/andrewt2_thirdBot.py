"""
This Bot looks to cpature and hold a given set of strategic locations
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return AndrewT2ThirdBot()


class AndrewT2ThirdBot(BotBase.BotBase):
    """This Bot attempts to take over strategic locations on the board"""
    def calculateDistanceDirection(self, origin, point):
        """
        function to calculate:
             distance between two points
             direction of shortest distance
        """
        # function calculate
        #     distance between two points
        #     direction of shortest distance
        raw_distance = point - origin
        if raw_distance > self.board.boardSize/2:
            distance = self.board.boardSize - raw_distance
            direction = "Left"
        elif raw_distance > 0:
            distance = raw_distance
            direction = "Right"
        elif raw_distance > -(self.board.boardSize/2):
            distance = abs(raw_distance)
            direction = "Left"
        else:
            distance = self.board.boardSize + raw_distance
            direction = "Right"
        return distance, direction
    
    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This bot assesses the strategic locations (0,20,40,60,80)
        Firstly If a piece is already on one of these locations, it strengthens (or splits)
        Next, for each strategic location, it determines whether it is currently "owned"
        and if not, assesses whether it can be taken. If the opponent piece is too strong,
        jump towards it for future analysis
        """

        pieces = self.board.findMyPieces()
        moves = []
        board_size = self.board.boardSize/2
        strategic_locations = []
        for location_percentage in [0,20,40,60,80]:
            strategic_locations += board_size * location_percentage / 100
        opponent_pieces = self.board.findOpponentPieces()
        arg = 1

        for location in pieces:
            # store initial location or location of single piece on board
            if len(pieces) == 1:
                self.init_location = location

            # if piece is on starting point, stay put...
            if location == self.init_location or location in strategic_locations:
                move = GameBoard.Action.STAY
                # ...unless we can split and leave a piece behind
                if self.board.getPieceStrength(location) in range(3,self.maxTileStrength,2):
                    move = GameBoard.Action.SPLIT
            else:
                for potential in strategic_locations:
                    # do we own this location
                    if potential not in pieces:
                        # we don't - but can we get closer
                        if potential in opponent_pieces:
                            # store opponent strength:
                            opposition_strength = self.board.getPieceStrength(potential)
                        else:
                            opposition_strength = 0
                        distance, direction = self.calculateDistanceDirection(location, potential)
                        # can we take it?
                        if self.board.getPieceStrength(location) >= opposition_strength + distance:
                            # take it
                            arg = distance
                            if direction == "Right":
                                move = GameBoard.Action.JUMP_RIGHT
                            else:   
                                move = GameBoard.Action.JUMP_LEFT
                        else:
                            if self.board.getPieceStrength(location) < self.maxTileStrength:
                                # stay and build
                                move = GameBoard.Action.STAY
                            else:
                                move = GameBoard.Action.SPLIT


            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
