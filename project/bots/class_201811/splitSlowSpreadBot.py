"""
This bot splits sufficiently strong pieces, then lets them
gradually move away from each other (with rests in between, to gain strength).
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

LEFT = -1
RIGHT = 1
CONFIGURABLE_PARAMS = ["separateDist",
                       "oppositeDist",
                       "changeMaxStrengthAt",
                       "initialMaxStrength",
                       "finalMaxStrength"]


def getBot(**kwargs):
    """This function needs to return an instance of this Bot's class."""
    return SplitSlowSpreadBot(**kwargs)


class SplitSlowSpreadBot(BotBase.BotBase):
    """Strong enough pieces are moved away from their nearest friendly
    piece, or split if none nearby. Weak pieces stay to gain strength"""

    def __init__(self, botConfig=None):
        """Set (to defaults) the parameters used in generating moves"""
        BotBase.BotBase.__init__(self)
        self.separateDist = 3
        self.oppositeDist = 20
        self.changeMaxStrengthAt = 30
        self.initialMaxStrength = 2
        self.finalMaxStrength = 16
        if botConfig is not None:
            if not isinstance(botConfig, dict):
                raise TypeError("botConfig must be a dictionary")
            for key in botConfig.keys():
                if key not in CONFIGURABLE_PARAMS:
                    raise ValueError("{} is not one of the parameters which "
                                     "can be set in botConfig: {}".format(
                                         key, ", ".join(CONFIGURABLE_PARAMS)))
                if not (isinstance(botConfig[key], int) and botConfig[key] >= 0):
                    raise ValueError("Value of {} in botConfig must be a "
                                     "positive integer".format(key))
                setattr(self, key, botConfig[key])

    def setup(self, config):
        """Gather information on the Game Board configuration and
        optionally config for this bot. Check our parameters are
        consistent with the game board."""

        BotBase.BotBase.setup(self, config)
        # Check our parameters are consistent with this game board
        for dist in ("separateDist", "oppositeDist"):
            if getattr(self, dist) > self.board.boardSize:
                raise ValueError("{} is {} but must be between 0 and board size "
                                 "({})".format(dist, getattr(self, dist),
                                               self.board.boardSize))

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one, dependent on the piece's strength"""

        pieces = self.board.findMyPieces()
        if len(pieces) == 1 and self.board.getPieceStrength(pieces[0]) == 2:
            moves = [[pieces[0], str(GameBoard.Action.SPLIT), 1]]
            return moves
        if currTurn < self.changeMaxStrengthAt:
            maxStrength = self.initialMaxStrength
        else:
            maxStrength = self.finalMaxStrength

        moves = self.generateMovesWithMaxStrength(pieces, maxStrength)
        return moves

    def nearestPieceDir(self, pieces, location):
        """Finds the direction of my nearest piece to this location.
        Reports left in the case of a draw and None if no nearest piece
        within self.separateDist"""
        for i in range(1, self.separateDist):
            if (location - i) % self.board.boardSize in pieces:
                return LEFT
            if (location + i) % self.board.boardSize in pieces:
                return RIGHT
        return 0

    def spaceToRight(self, pieces, location):
        """Returns True if I have no pieces within self.oppositeDist to
        the right"""
        for i in range(1, self.oppositeDist):
            if (location + i) % self.board.boardSize in pieces:
                return False
        return True

    def spaceToLeft(self, pieces, location):
        """Returns True if I have no pieces within self.oppositeDist to
        the left"""
        for i in range(1, self.oppositeDist):
            if (location - i) % self.board.boardSize in pieces:
                return False
        return True

    def generateMovesWithMaxStrength(self, pieces, maxStrength):
        """For the given value of the max strength for which we always stay,
        generated the move for each piece.
        This bot only cares about where its nearest own pieces are (then
        moves away from them or splits, if strong enough).
        It doesn't care where the opponent's pieces or special tiles are."""
        moves = []
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            if strength >= maxStrength:
                # Strong piece - move away from my nearest piece
                # (if any near, and none close on the other side)
                nearestDir = self.nearestPieceDir(pieces, location)
                if nearestDir == LEFT and self.spaceToRight(pieces, location):
                    move = GameBoard.Action.RIGHT
                elif nearestDir == RIGHT and self.spaceToLeft(pieces, location):
                    move = GameBoard.Action.LEFT
                # If no near pieces, or near pieces weren't limited to one side,
                # we want to split. But if strength is odd or at the limit,
                # stay instead
                elif strength % 2 and not strength == self.board.maxTileStrength:
                    move = GameBoard.Action.STAY
                else:
                    move = GameBoard.Action.SPLIT
            else:
                # Weak piece - gain strength
                move = GameBoard.Action.STAY
            moves.append([location, str(move), 1])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'splitSlowSpreadBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e8be3dd657a5a275ac6907ed92fd91c9"
                    "7f5d3e9ed2398c03a2d2559853c167a8")
        elif challenge == "database":
            return ("bf6a33ec12fdc802ab6f96f7f0e6ad4c"
                    "add79bb8da1fa42efdeddd2b5b821833")
        return "Default bot signature"
