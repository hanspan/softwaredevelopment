"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RobBot()


class RobBot(BotBase.BotBase):
    """ rob's bot
    Pick a direction and always move in that direction, unless you're on
    a growth tile.
    """

    direction = None

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []
        pdict = {}

        if self.direction is None:
            location = pieces[0]

            minDistance = 999999
            for tile in self.board.growthTiles:
                distance = tile-location
                if (distance > self.board.boardSize/2):
                    distance -= self.board.boardSize
                if (distance < -self.board.boardSize/2):
                    distance += self.board.boardSize
                if abs(distance) < abs(minDistance):
                    minDistance = distance
            if distance < 0:
                self.direction = GameBoard.Action.LEFT
            else:
                self.direction = GameBoard.Action.RIGHT

        for location in pieces:
            pdict[location] = self.board.getPieceStrength(location)
            # move = random.choice(list(GameBoard.Action))
            if location in self.board.growthTiles:
                move = GameBoard.Action.STAY
                if self.board.getPieceStrength(location)>2:
                  move = GameBoard.Action.SPLIT
            else:
                move = self.direction

            # Most move commands don't need an argument, but the jump ones do.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        #print(pdict)
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
