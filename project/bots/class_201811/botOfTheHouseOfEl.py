"""
This is a bot that spreads and strengthens.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return botEl()


class botEl(BotBase.BotBase):
    """This Bot just spreads out a lot.  Whenever the center piece of my pieces is 3,
    everything to its left moves left, everything right move right, and the middle piece splits."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        This bot spreads out from the middle - right now it does not calculate the middle in a
        smart fashion.
        """
        #if currTurn <= 10:
            #self.showGameState(currTurn)

        pieces = self.board.findMyPieces()
        moves = []

        leftEdge, rightEdge = self.findEdges(pieces)
        middleIndex = self.findMiddle(leftEdge, rightEdge, len(pieces) )
        # We move and split whenever the middle is piece is odd and >=3
        middleStrength = self.board.getPieceStrength(pieces[middleIndex])

        if( middleStrength >= 3) and (middleStrength % 2 == 1 ):
            if leftEdge <= rightEdge:
                #print(" no wrap, splitting ", leftEdge, " is less than ", rightEdge)
                moves.extend(self.addSomeMoves(leftEdge, middleIndex - 1,
                                               str(GameBoard.Action.LEFT), 1, pieces, ))
                moves.extend(self.addSomeMoves(middleIndex + 1, rightEdge,
                                               str(GameBoard.Action.RIGHT), 1, pieces))
            elif middleIndex < ((len(pieces))/2):
                #print( "wrap to the left, splitting" )
                moves.extend(self.addSomeMoves(leftEdge, len(pieces) - 1,
                                               str(GameBoard.Action.LEFT), 1, pieces))
                moves.extend(self.addSomeMoves(0, middleIndex - 1,
                                               str(GameBoard.Action.LEFT), 1, pieces))
                moves.extend(self.addSomeMoves(middleIndex + 1, rightEdge,
                                               str(GameBoard.Action.RIGHT), 1, pieces))
            else:
                #print( "wrap to the right, splitting")
                moves.extend(self.addSomeMoves(leftEdge, middleIndex - 1,
                                               str(GameBoard.Action.LEFT), 1, pieces))
                moves.extend(self.addSomeMoves(0, rightEdge,
                                               str(GameBoard.Action.RIGHT), 1, pieces))
                moves.extend(self.addSomeMoves(middleIndex + 1, len(pieces) - 1,
                                               str(GameBoard.Action.RIGHT), 1, pieces))
            moves.append([pieces[middleIndex], str(GameBoard.Action.SPLIT), 1])

        else:
            #print("stay")
            moves.extend(self.addSomeMoves(0, len(pieces) - 1,
                                           str(GameBoard.Action.STAY), 1, pieces))

        return moves

    def findEdges(self, pieces):
        """ Finds the indexes of the left and right edge of our span of bots, including dealing with the span
        having wrapped around the edge of the board.
        """
        lenPieces = len(pieces)
        leftEdge = 0
        rightEdge = lenPieces-1
        bigSpace = pieces[leftEdge] + lenPieces - pieces[rightEdge]
        for i in range(1, len(pieces)):
            space = pieces[i]-pieces[i-1]
            if(space) > bigSpace:
                bigSpace = space
                leftEdge = i
                rightEdge = i-1
        return (leftEdge, rightEdge)

    def findMiddle(self, leftEdge, rightEdge, length):
        """ This bot spreads from the middle, so we assume we have a
        string of tiles that are ours, from the left edge to the right edge.
        The length of the tile string is the length of 'pieces', that gets handed to this routine.
        If we have wrapped around, we have to count from left edge to middle, not from piece[0]
        """

        if( rightEdge < leftEdge ):
            rightEdge += length
        middle = int( leftEdge + ( ( length-(length % 2) ) / 2 ) )
        #print( leftEdge, rightEdge, maxValue, middle)
        if( middle >= length ):
            middle -= length
        #print( middle)
        return middle


    def addSomeMoves(self, start, end, move, arg, pieces):
        """creates an array of moves from start piece through end piece (inclusive)"""
        newMoves = []
        for i in range(start, end+1):
            index = i
            newMoves.append( [pieces[index], move, arg] )
        return newMoves



    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' I didn't change it for botEl, but I probably should
        and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        return "Default bot signature"
