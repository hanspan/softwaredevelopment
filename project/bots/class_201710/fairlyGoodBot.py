"""This bot attempts to grow to size 14,
then splits to the adjascent tiles which continue to grow until
size 14"""

import BotBase     # The basic bot function.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function returns an instance of this Bot's class."""

    return FairlyGoodBot()


class FairlyGoodBot(BotBase.BotBase):
    """This Bot will grow while it's below a certain strength,
    or move if it's strong enough"""
    def __init__(self):
        '''Init variables '''
        # init method for future use
        self.help = 15
        self.moves = []

    def generateMoves(self, currTurn, maxTurns):
        """Locate all of the pieces on the board, and then make a move for
        each one.
        """
        targetLocation = self.board.growthTiles
        pieces = self.board.findMyPieces()
        self.moves = []
        for location in pieces:
            if currTurn == 0:
                self.moves.append([location, str(GameBoard.Action.STAY), 1])
            if currTurn == maxTurns:
                self.moves.append([location, str(GameBoard.Action.SPLIT), 1])
            if currTurn < 44:
                if self.board.getPieceStrength(location) < 2:
                    self.moves.append([location, str(GameBoard.Action.STAY), 1])
                else:
                    self.moves.append([location, str(GameBoard.Action.SPLIT), 1])
            elif location in targetLocation:
                self.growTiles(location)
            else:
                if self.board.getPieceStrength(location) < 13:
                    # Stay until strength 13
                    self.moves.append([location, str(GameBoard.Action.STAY), 1])
                else:
                    # Split if strength 13
                    self.moves.append([location, str(GameBoard.Action.SPLIT), 1])
                if self.board.getPieceStrength(location) < 2:
                    self.moves.append([location, str(GameBoard.Action.LEFT), 1])
                else:
                    self.moves.append([location, str(GameBoard.Action.STAY), 1])
        return self.moves

    def growTiles(self, location):
        ''' A function that deals with what to do on grow tiles '''
        if self.board.getPieceStrength(location) < 25:
            # Decide what to do based on the strength of the piece.
            self.moves.append([location, str(GameBoard.Action.STAY), 1])
        else:
            self.moves.append([location, str(GameBoard.Action.SPLIT), 1])

    def getSignature(self):
        '''A function to return the magic signture which should grant the holder
        five more points'''
        return '5040b8c1dadcd59993f7fcf913040880eb72ae0693453dee1ebdd7f1fdd0035d'
