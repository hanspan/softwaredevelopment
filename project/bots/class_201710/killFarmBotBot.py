"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then they just sit still.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random      # Source for random numbers.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return KillFarmBotBot()


class KillFarmBotBot(BotBase.BotBase):
    """This Bot sits still gathering karma"""

    def __init__(self):
        self.zen = True
        self.left = False
        self.right = False

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'sit still' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        if currTurn == 1:
            self.target = self.board.findOpponentPieces()[0]
            print(self.target)
        pieces = self.board.findMyPieces()
        opponent = self.board.findOpponentPieces()
        moves = []

        if self.zen:
            for location in pieces:
                if (not self.right) and ((location - 1)%self.board.boardSize) in opponent:
                    move = GameBoard.Action.JUMP_LEFT
                    arg = 7
                    self.left = True
                    if location - 7 == self.target:
                        self.zen = False
                elif (not self.left) and ((location + 1)%self.board.boardSize) in opponent:
                    move = GameBoard.Action.JUMP_RIGHT
                    arg = 7
                    self.right = True
                    if location - 7 == self.target:
                        self.zen = False
                else:
                    move = GameBoard.Action.STAY
                    arg = 1
                moves.append([location, str(move), arg])
        else:
            for location in pieces:
                if (self.board.getPieceStrength(location) >= 2):
                    if ((location + 1)%100 in pieces) and ((location - 1)%100 in pieces): # if both sides are owned by me
                        moves.append([location, str(GameBoard.Action.STAY), 1])
                    else:
                        if ((location + 1)%100 in opponent) or ((location - 1)%100 in opponent): # if at least one sides is owned by opponent
                            moves.append([location, str(GameBoard.Action.STAY), 1])
                        else: moves.append([location, str(GameBoard.Action.SPLIT), 1])
                    
                else:
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                
        return moves
