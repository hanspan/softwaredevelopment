import os
import json

import BotTestCase
import spreadBot

class SpreadBotTest(BotTestCase.BotTestCase):
    """
    Some very simple tests for a very simple military strategy.
    """

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = spreadBot.SpreadBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, spreadBot.getBot())


    def testFirstMove(self):
        #Test that the first move is a 
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        #self.assertEqual(results, [[25, 'Stay', 1]])
    
    def testSignature(self):
        # Check that the expected signature is returned.

        sig = self.bot.getSignature()
        self.assertTrue(sig.endswith(""))
