"""
Needs:
No PEP8 errors
Longest function < 50 lines

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return TobyBot()


class TobyBot(BotBase.BotBase):
    """
        This was originally splitBot
    """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """If a piece is a single, stay (grow)
           If a pieces is more than a single, split.
           Need to order decisions so that obvious ones are done first.
           (Use something like while(len(moves) < len(pieces)): )

           Should look up size of game in config so 100 is not fixed.

           In future, look to see if you are splitting
           into an empty place (good) or filled (bad)

           Even more in future, avoid splitting into
           places the opponent will immediately attack (bad)

           Another thing I want to not forget in the future is to use special
           defence/growing spaces
           Also, note how games are being adjudicated after 1000 moves.
           Apparantly: territory + pieces + totalPieceStrength

           Finally, this program assumes no opponent will jump,
           nor does it jump itself.
           Should look into whether this is flawed.
        """

        # Note: tileStrength[pos] doesn't have a tileStrength[100],
        # goes from 0-99

        pieces = self.board.findMyPieces()
        moves = []

        if currTurn == 1:  # find start tile
            self.start_tile = pieces[0]

        while pieces != []:  # Allows for ordering of decision in future
            for location in pieces:
                locationStrength = self.board.getPieceStrength(location)

                # The following are ordered from high importance downwards.
                # The ordering could be changed if sensible in the future.

                # Copied from FarmBot
                if location == 25 or location == 74:
                    direction = "NA"
                elif self.start_tile == 25:
                    if 25 < location < 74:
                        direction = "right"
                    else:
                        direction = "left"
                elif self.start_tile == 74:
                    if 25 < location < 74:
                        direction = "left"
                    else:
                        direction = "right"
                distance = abs(location - self.start_tile)
                if currTurn < 25:  # for the first 24 turns
                    move = self.earlyGameMoves(distance,
                                               location, direction)
                    moves.append([location, str(move), 1])
                    pieces.remove(location)
                    # print (location)
                    continue

                # # Copied from fairlyGoodBot
                # if location in targetLocation:
                #    if self.board.getPieceStrength(location) < 25:
                #        # Decide what to do based on the strength of the piece
                #        moves.append([location, str(GameBoard.Action.STAY), 1])
                #        pieces.remove(location)
                #        continue
                #    else:
                #        moves.append([location, str(GameBoard.Action.SPLIT), 1])
                #        pieces.remove(location)
                #        continue

                # If a piece can attack to the left

                if currTurn < 46:
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                    pieces.remove(location)
                    # print (location)
                    continue

                # Now the stacks are 22 deep, let's attack for 50 turns

                if self.shouldStronglyAttackTheLeft(location):
                    moves.append([location, str(GameBoard.Action.JUMP_LEFT), 3])
                    pieces.remove(location)
                    # print (location)
                    continue

                if self.shouldStronglyAttackTheRight(location):
                    moves.append([location, str(GameBoard.Action.JUMP_RIGHT), 3])
                    pieces.remove(location)
                    # print (location)
                    continue

                # Should look for weak attacks here.

                # If attack stack is on the left
                if ((direction == "left") and (distance > (currTurn - 46))):
                    moves.append([location, str(GameBoard.Action.LEFT), 1])
                    pieces.remove(location)
                    # print (location)
                    continue

                # If attack stack is on the right
                if ((direction == "right") and (distance > (currTurn - 46))):
                    moves.append([location, str(GameBoard.Action.RIGHT), 1])
                    pieces.remove(location)
                    # print (location)
                    continue

                # If a piece is a single, stay (grow)
                if locationStrength < 2:
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                    pieces.remove(location)
                    continue

                # If strength is even, stay
                if (locationStrength % 2 == 0):
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                    pieces.remove(location)
                    continue

                # If strength is odd, split
                # This should be changed to look each side before splitting
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
                pieces.remove(location)
                continue

        # print('normal')
        # print(self.board.tileStrength)

        return moves

    def shouldStronglyAttackTheRight(self, location):
        """ This function looks right of the location
            If: it is 5 opposing pieces there
            and the location is strength at least 12
            Then: Jump_Right 3

            If we want to do the above, return True
            If not, return False
        """
        if self.board.getPieceStrength(location) < 12:
            return False
        if self.board.getPieceStrength(location + 3) < 10:
            return False
        opponentsPieces = self.board.findOpponentPieces()
        for i in range(5):
            tempLocation = (location + 5 - i) % 100
            if tempLocation not in opponentsPieces:
                return False
        return True

    # This works exactly as expected...
    def shouldStronglyAttackTheLeft(self, location):
        """ This function looks left of the location
            If: it is 5 opposing pieces there
            and the location is strength at least 12
            Then: Jump_Left 3

            If we want to do the above, return True
            If not, return False
        """
        if self.board.getPieceStrength(location) < 12:
            return False
        if self.board.getPieceStrength(location - 3) < 10:
            return False
        opponentsPieces = self.board.findOpponentPieces()
        for i in range(5):
            tempLocation = (location - 5 + i) % 100
            if tempLocation not in opponentsPieces:
                return False
        return True

    # Copied from FarmBot
    def earlyGameMoves(self, distance, location, direction):
        '''
        Determines early game moves
        '''
        strength = self.board.getPieceStrength(location)
        if distance in [0, 1] and strength < 3:
            return GameBoard.Action.STAY  # centre grows
        elif distance in [0, 3] and strength == 3:
            return GameBoard.Action.SPLIT  # then splits
        elif direction == "right":  # expand
            return GameBoard.Action.RIGHT
        elif direction == "left":  # expand
            return GameBoard.Action.LEFT

    def getSignature(self):
        """
        This is the hash of 'tobyBot' and the correct line numbers for
        the debug challenge.
        """
        return \
            "ac7d61aa7b5d492e0311c49aedc61e3d0a5038da1ae50b0eea88e9fabe1219b4"
