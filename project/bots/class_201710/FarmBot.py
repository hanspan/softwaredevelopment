'''
FarmBot is obviously the best bot
'''

import BotBase
import GameBoard


def getBot():
    '''
    This functions returns the bot
    '''
    return TubsBot()


class TubsBot(BotBase.BotBase):
    '''
    This is my bot
    '''
    def __init__(self):  # add in start tile
        '''
        creates start tile
        '''
        self.start_tile = 0

    def generateMoves(self, currturn, maxturns):
        '''
        Function to generate bot move
        '''
        pieces = self.board.findMyPieces()
        moves = []
        if currturn == 1:  # find start tile
            self.start_tile = pieces[0]
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            distance = abs(location - self.start_tile)
            if location == 25 or location == 74:
                direction = "NA"
            elif self.start_tile == 25:
                if 25 < location < 74:
                    direction = "right"
                else:
                    direction = "left"
            elif self.start_tile == 74:
                if 25 < location < 74:
                    direction = "left"
                else:
                    direction = "right"
            if currturn < 30:  # for the first 24 turns
                move = self.earlyGameMoves(distance, strength,
                                           location, direction)
            elif currturn < maxturns:  # for the remaining turns
                move = self.lateGameMoves(distance, strength,
                                          location, direction)
            moves.append([location, str(move), 1])
        return moves

    def earlyGameMoves(self, distance, strength, location, direction):
        '''
        Determines early game moves
        '''
        strength = self.board.getPieceStrength(location)
        if distance in [0, 1] and strength < 3:
            return GameBoard.Action.STAY  # centre grows
        elif distance in [0, 3] and strength == 3:
            return GameBoard.Action.SPLIT  # then splits
        elif direction == "right":  # expand
            return GameBoard.Action.RIGHT
        elif direction == "left":  # expand
            return GameBoard.Action.LEFT

    def lateGameMoves(self, distance, strength, location, direction):
        '''
        Determines late game moves
        '''
        por = (location + 1) % 100  # piece on right
        pol = (location - 1) % 100  # piece on left
        attack = self.findOpponents(por, pol, strength)
        if attack is not None:
            return attack
        elif distance % 3 == 0 and strength > 2 and strength % 2 == 1:
            return GameBoard.Action.SPLIT
        elif distance < 10 and strength < 16:
            return GameBoard.Action.STAY
        elif (direction == "right" and
              (strength == 25 or
               self.board.getPieceStrength(pol)
               == 25)):
            return GameBoard.Action.RIGHT
        elif (direction == "left" and
              (strength == 25 or
               self.board.getPieceStrength(por)
               == 25)):
            return GameBoard.Action.LEFT  # attack centre
        else:
            return GameBoard.Action.STAY  # grow strong

    def findOpponents(self, por, pol, strength):
        '''
        Determines whether to attack opponents
        '''
        opp = self.board.findOpponentPieces()
        our = self.board.findMyPieces()
        spor = self.board.getPieceStrength(por)
        spol = self.board.getPieceStrength(pol)
        if por in opp and pol in opp and strength > spor + spol:
            return GameBoard.Action.SPLIT
        elif por in opp and strength > spor and pol in our:
            return GameBoard.Action.RIGHT
        elif pol in opp and strength > spol and por in our:
            return GameBoard.Action.LEFT
        else:
            return None

    def getSignature(self):
        '''
        This is the signature bit
        '''
        return \
            "54674c631a0ffc85e73014d6428679d33f01d05d6fe40bcd7c9e73cfae6dc64d"
