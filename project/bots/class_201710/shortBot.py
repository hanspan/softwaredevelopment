"""same strategy as sarahBot but in reverse - STAY then SPLIT"""

import BotBase       # All of the basic Bot functionality.
import GameBoard     # Utilities for working with the game.


def getBot():
    """Build the Bot class"""
    sig = 'f7489c30ef16e63051bf9ae1296273ab788cadea00685d9b318d83738c224f74'
    return type('ShortBot', (BotBase.BotBase,),
                {'generateMoves': generateMoves,
                 'getSignature': lambda self: sig})()


def generateMoves(self, currTurn, maxTurns):
    """Alternate between STAY and SPLIT"""
    move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]
    return [[location, str(move), 1] for location in self.board.findMyPieces()]
