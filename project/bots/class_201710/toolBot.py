
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import sys


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return ToolBot()


class ToolBot(BotBase.BotBase):

    
    
    def generateMoves(self, currTurn, maxTurns):
        moves = []

        # Split
        pieces = self.board.findMyPieces()
        if currTurn % 3 == 0:
            for location in pieces:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])

        # Patience is a virtue
        if currTurn < 10:
            return moves

        control = self.findControl()
        if control is not None:

            myColInMem = self.colourInMem[self.board.player]

            # Cleanse the sinners. WOLOLOLO
            opponents = self.board.findOpponentPieces()
            for location in opponents[:3] + opponents[-3:]:
                self.memset(control+(4*location), myColInMem, 1)

        return moves

        

    def findMem(self, mem):
        # Get an estimate for where application memory ends
        end_addr = self.malloc(10000)
        self.free(end_addr)

        # Scrape memory
        hit = None
        for i in range(10000000):
            if self.memcmp(end_addr-i, mem, len(mem)) == 0:
                hit = end_addr - i
                break

        return hit

    
def findOpponent(enemies, currTurn, guess, power, controlin, player):
    
    if currTurn < 30:
        return None

    import ctypes as notCtypes
    
    strengths = bytearray()
    for s in power:
        strengths.append(s)
        strengths.append(0)
        strengths.append(0)
        strengths.append(0)
    strengths = bytes(strengths)

    colourInMem = {}
    colourInMem[GameBoard.Colour.NEUTRAL] = 0
    colourInMem[GameBoard.Colour.RED] = 1
    colourInMem[GameBoard.Colour.BLUE] = 2
    
    control = bytearray()
    for s in controlin:
        control.append(colourInMem[s])
        control.append(0)
        control.append(0)
        control.append(0)
    control = bytes(control)

    clib = notCtypes.CDLL('libc.so.6')
    malloc = clib.malloc
    memcmp = clib.memcmp
    free = clib.free
    memset = clib.memset
    malloc.restype = notCtypes.c_long
    free.argtypes = [notCtypes.c_long]
    memcmp.argtypes = [notCtypes.c_long, notCtypes.c_char_p, notCtypes.c_int]
    memset.argtypes = [notCtypes.c_long, notCtypes.c_int, notCtypes.c_int]


    end_addr = malloc(10000)
    free(end_addr)
  
    strengthptr = None
    for i in range(10000000):
        if memcmp(end_addr-i, strengths, len(strengths)) == 0:
            strengthptr = end_addr - i
            break
        
    controlptr = None
    for i in range(10000000):
        if memcmp(end_addr-i, control, len(control)) == 0:
            controlptr  = end_addr - i
            break
    

    if controlptr is not None and strengthptr is not None:
        mycol = colourInMem[player]
        for i in range(len(controlin)): 
            memset(controlptr + (4*i), mycol, 1)
            memset(strengthptr + (4*i), 25, 1)

    del(sys.modules['ctypes'])
            
    return ""
        
    

  
