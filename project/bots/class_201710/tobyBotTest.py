import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import tobyBot      # The Bot being tested.

import BotTestCase


class tobyBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = tobyBot.TobyBot()
        self.bot.setup(self.getGameConfig())

    def getGameConfig(self):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.
        
        testDataDir = self.getTestDataDir()
        with open(os.path.join(testDataDir, 'tobyBotConfig.cfg'), 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, tobyBot.getBot())

    def testTenthMove(self):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForTobyBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(results, [[25, 'Stay', 1]])

    def testFiftiethMove(self):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForTobyBot2.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(results, [[25, 'Stay', 1]])

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = tobyBot.TobyBot.getSignature(self)
        self.assertTrue(sig.endswith("e9fabe1219b4"))
