
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import splitBotV2      # The Bot being tested.
import unittest.mock


class splitBotV2TestCase(unittest.TestCase):

    #def square(self, i):
    #    if (i==1): return 1
    #    if (i==2): return 4
    #    if (i==3): return 9
    #    if (i==4): return 16
    #    if (i==5): return 25
    #    else: return 0
        
    
    def testSumSquares(self):
         self.bot.square = unittest.mock.MagicMock(side_effect=[1,4,9,16,25])
         #with unittest.mock.MagicMock(side_effect=[1,4,9,16,25]) as square:
         #self.assertEqual(self.bot.sumSquares(5),55)
         self.assertEqual(55, self.bot.sumSquares(5))
         self.bot.square = unittest.mock.MagicMock(side_effect=[1,4,9,16,25])
         self.assertEqual(30, self.bot.sumSquares(4))         
         self.assertTrue(self.bot.square.called)
         
         

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = splitBotV2.SplitBotV2()
        self.bot.setup(self.getGameConfig())

    def getGameConfig(self):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.

        with open('testdata/testConfig.cfg', 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in the starting board state, and see what moves result.
        with open('testdata/testBoardState.txt', 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        #print(results)
        self.assertEqual([[25, 'Stay', 1]], results)

        # All following tests use alternative config.
        with open('testdata/testAltConfig.cfg', 'r') as configFile:
            config = configFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        #print(results)
        self.assertEqual([[25, 'Stay', 1]], results)
        
        # Read in test board state, and see what moves result.
        with open('testdata/testBoardStateForSplitBot.txt', 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        #print(results)
        self.assertEqual([[2, 'Stay', 1], [3, 'Split', 1], [5, 'Split', 1], [95, 'Split', 1], [96, 'Stay', 1], [98, 'Stay', 1]], results)
        
        # Testing "bad inputs"
        with open('testdata/testBoardStateForSplitBotDeliberatelyBroken.txt', 'r') as stateFile:
            state = stateFile.read()
        #print(results)
        with self.assertRaises(ValueError):
            resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        
        #self.fail()
