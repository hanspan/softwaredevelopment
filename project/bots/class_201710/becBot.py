"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random      # Source for random numbers.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return BotBot()


class BotBot(BotBase.BotBase):
    """Unsure what BotBot does"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            move = random.choice(list(GameBoard.Action))

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT or move == GameBoard.Action.JUMP_RIGHT):
                if self.board.getPieceStrength(location)!=1:
                    arg = int(self.board.getPieceStrength(location) / 2)
                    moves.append([location, str(move), arg])
                else:
                    moves.append([location, str(GameBoard.Action.STAY), 1])
            elif (move == GameBoard.Action.SPLIT and self.board.getPieceStrength(location) == 1):
                moves.append([location, str(GameBoard.Action.STAY), 1])
            else:
                moves.append([location, str(move), arg])
            return moves
