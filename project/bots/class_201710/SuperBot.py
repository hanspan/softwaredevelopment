"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

def getBot():
    """This function needs to return an instance of this Bot's class."""
    return SuperBot()


class SuperBot(BotBase.BotBase):
    """Bots split if they are strong enough, and move towards where most enemy pieces are, as a super-unit. 
    TODO: If enemies are sufficiently far away, wait and gather strength.
    TODO: If the total strength of the super-unit is big enough, send off the outer-most pieces to fight.
    TODO: If a piece is detatched from the super-unit, head towards it using the shortest direction."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Hello."""
        pieces = self.board.findMyPieces()
        moves = []
        center_of_strength = self.getCenterOfStrength(pieces)
        enemy_center_of_strength = self.getCenterOfStrength(self.board.findOpponentPieces())
        
        if abs(enemy_center_of_strength - center_of_strength) >= 30:
            direction = 'S'
        else: 
            direction = self.chooseDirection(center_of_strength)
        
        arg = 1

        for location in pieces:
#            if detatched == True:
            if self.board.getPieceStrength(location) >= 15:
                move = GameBoard.Action.SPLIT
            elif direction == 'R':
                move = GameBoard.Action.RIGHT
            elif direction == 'L':
                move = GameBoard.Action.LEFT
            else:
                move = GameBoard.Action.STAY

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    def getCenterOfStrength(self, pieces):
        """Calculate the 'center of strength' for the superbot and return the location closest to it which is occupied by one of our pieces."""
        total_strength = sum(self.board.getPieceStrength(location) for location in pieces)
        COS = sum(self.board.getPieceStrength(location)*location for location in pieces)/total_strength
        return min(pieces, key = lambda x : abs(x - COS))

    def chooseDirection(self, location):
        """Go towards where the most enemy pieces are."""
        enemy_locations = self.board.findOpponentPieces()
            
        if location >= 50:
            enemies_to_left = [i for i in enemy_locations if i < location - 50]
            enemies_to_right = set(enemy_locations) - set(enemies_to_left)
        else:
            enemies_to_right = [i for i in enemy_locations if i >= location + 50]
            enemies_to_left = set(enemy_locations) - set(enemies_to_right) 

        if len(enemies_to_left) > len(enemies_to_right):
            return 'L'
        else:
            return 'R'
  


