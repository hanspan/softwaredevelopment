"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then they just sit still.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random      # Source for random numbers.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return ZenBot()


class ZenBot(BotBase.BotBase):
    """This Bot sits still gathering karma"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'sit still' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            move = GameBoard.Action.STAY

            arg = 1

            moves.append([location, str(move), arg])

        return moves
