import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import SuperBot      # The Bot being tested.


class SuperBotTestCase(unittest.TestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = SuperBot.SuperBot()
        self.bot.setup(self.getGameConfig())

    def getGameConfig(self):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.

        with open('testdata/testConfig.cfg', 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        with open('testdata/testSuperBotBoardState.txt', 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        print(results)

        self.assertEqual(3, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(24, results[0][0])
        self.assertEqual(25, results[1][0])
        self.assertEqual(26, results[2][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[0][1])
        self.assertEqual(1, results[0][2])
        
        
        
        
        