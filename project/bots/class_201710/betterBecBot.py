"""
betterBecBot.py - better than becBot
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return BecBot()


class BecBot(BotBase.BotBase):
    """Unsure what BecBot does"""
    # pylint: disable=unused-argument
    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        For first 11 turns, stays and then splits if the move number
        is mod 3 =0
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            if currTurn % 3 == 0:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
            else:
                moves.append([location, str(GameBoard.Action.STAY), 1])
        return moves

    @staticmethod
    def getSignature():
        """
        This is the hash of 'betterBecBot' and the correct line numbers for
        the debug challenge.
        """
        return "25a65af26f248c0dfc3951e51da1ea1ae" +\
            "aa595123f36224cedb938bc8aa63a2b"
