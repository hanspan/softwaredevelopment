"""This bot attempts to occupy the strategic tiles (defense and growth
bonus) and then grow until it is strong enough to split and occupy
more such tiles.

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return DefenseBot()


class DefenseBot(BotBase.BotBase):
    """This Bot moves to strategic tiles and then grows until it is strong
    enough to split."""

    def __init__(self):
        self.minSplitStrength = 20
        self.minMoveStrength = 5
        self.maxMoveDistance = 4
    
    def generateMoves(self, currTurn, maxTurns):   # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        On the first turn, it identifies where the strategic tiles
	are.  It then moves toward the nearest one, and sits there
	until the strength is large enough, and odd, at which point it
	splits. The children then repeat this process.

	No attempt is made to have the various units deconflict, so
	they may well head for the same tiles.

        """

        pieces = self.board.findMyPieces()
        moves = []

        # First turn? Locate starting pieces.
        if currTurn == 1:
            self.growthTiles = self.board.growthTiles
            self.defenseTiles = self.board.defenceTiles
        
        # No longer the first turn, so more decisions to consider.
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            move = GameBoard.Action.STAY
            if (location in self.defenseTiles) or (location in self.growthTiles):
                if strength > self.minSplitStrength and (strength%2==1):
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
            else:
                minDistance = self.board.boardSize+1
                for tileLocation in self.growthTiles + self.defenseTiles:
                    leftDistance = (location - tileLocation)%self.board.boardSize
                    rightDistance = (tileLocation - location)%self.board.boardSize
                    if leftDistance < minDistance:
                        move = GameBoard.Action.LEFT
                        minDistance = leftDistance
                    if rightDistance < minDistance:
                        move = GameBoard.Action.RIGHT
                        minDistance = rightDistance
                if minDistance > self.maxMoveDistance:
                    if strength > self.minSplitStrength and (strength%2==1):
                        move = GameBoard.Action.SPLIT
                    else:
                        move = GameBoard.Action.STAY
            moves.append([location, str(move), 1])

        return moves
