"""

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return NotQuiteStationaryBot()


class NotQuiteStationaryBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""
    
    def __init__(self):
        self.stopMoving = False

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        Action toward the nearest strategic tile until reaching it or
        strong enough to split, then become stationaryBot.
        """

        pieces = self.board.findMyPieces()
        moves = []

        if not self.stopMoving:
            location = pieces[0] # Can't have split at this point
            if self.board.getPieceStrength(location) >= self.board.maxTileStrength:
                self.stopMoving = True
            else:
                strategic_tiles = self.board.growthTiles + self.board.defenceTiles
                min_distance = self.board.boardSize
                nearest_strategic_tile = strategic_tiles[0]
                for tile in strategic_tiles:
                    distance = min((location - tile)%self.board.boardSize,
                                   (tile - location)%self.board.boardSize)
                    if distance < min_distance:
                        min_distance = distance
                        nearest_strategic_tile = tile
                
                if min_distance == 0:
                    self.stopMoving = True
                elif (location - nearest_strategic_tile)%self.board.boardSize < (nearest_strategic_tile - location)%self.board.boardSize:
                    moves.append([location, str(GameBoard.Action.LEFT), 1])
                else:
                    moves.append([location, str(GameBoard.Action.RIGHT), 1])

        if self.stopMoving:
            for location in pieces:
                if (self.board.getPieceStrength(location) > 13):
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
                
                # Most move commands don't need an argument, but the jump ones do.
                # Just have them use up half their strength while jumping.
                arg = 1
    
                # Store the move. Note the str() entry: we need a form that
                # can be passed to the server, and this turns the "thing of type
                # Action" into "a string", which is easier for the JSON library
                # to understand.
                moves.append([location, str(move), arg])

        return moves
