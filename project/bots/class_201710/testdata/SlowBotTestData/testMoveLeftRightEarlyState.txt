{
  "control": ["Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Red","Red","Red","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Blue","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral",
  			  "Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral","Neutral"],
  "strengths":[0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,3,3,3,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,1,0,0,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0,
  			   0,0,0,0,0,0,0,0,0,0],
  "currTurn": 3,
  "maxTurns": 1000,
  "random": 1802744220
}
