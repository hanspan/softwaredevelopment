
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import thomasg1BetterBot      # The Bot being tested.

import BotTestCase


class thomasg1BetterBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = thomasg1BetterBot.ThomasBetterBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, thomasg1BetterBot.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateThomasg1BettererBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        boardStateJson = json.loads(state)
        self.bot.board.updateState(boardStateJson)
        results = self.bot.generateMoves(1, 1000)

        self.assertEqual(3, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(24, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = thomasg1BetterBot.getBot().getSignature()
        self.assertTrue(sig.endswith("381299ccee0"))
