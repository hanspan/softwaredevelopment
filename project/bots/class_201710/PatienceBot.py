"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random      # Source for random numbers.

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RandomBot()


class RandomBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            #move = random.choice(list(GameBoard.Action))
            if (int(self.board.getPieceStrength(location)) < 4):
                move= GameBoard.Action.STAY
                    #moves.append([location, 'Stay', 1])
            elif(int(self.board.getPieceStrength(location)) >=8):
                move = GameBoard.Action.SPLIT
                #moves.append([location, 'Split',  1])
            else:
                #print("Im Here SomeHow")
                #move = random.choice(GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT, GameBoard.Action.LEFT, GameBoard.Action.RIGHT)
                move = random.choice(list(GameBoard.Action))
                #move = random.choice(list('Left', 'Right', 'Jump_Left', 'Jump_Right'))
                #print(move)
            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            arg = 1
            if (move == GameBoard.Action.JUMP_LEFT or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])
        return moves
