"""
This is a better splitBot; see generateMoves
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SplitBotV2()


class SplitBotV2(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """If a piece is a single, stay (grow)
           If a pieces is more than a single, split.

           Should look up size of game in config so 100 is not fixed.

           In future, look to see if you are splitting
           into an empty place (good) or filled (bad)

           Even more in future, avoid splitting into
           places the opponent will immediately attack (bad)

           Another thing I want to not forget in the future is to use special
           defence/growing spaces
           Also, note how games are being adjudicated after 1000 moves.
           Apparantly: territory + pieces + totalPieceStrength

           Finally, this program assumes no opponent will jump,
           nor does it jump itself.
           Should look into whether this is flawed.
        """

        # Note: tileStrength[pos] doesn't have a tileStrength[100],
        # goes from 0-99

        pieces, opponentsPieces = self.findAllPieces()

        # mostImportantMoves = []
        # middleImportantMoves = []
        # lowImportantMoves = []
        moves = []

        for location in pieces:
            localArray = self.findLocalPieces(location)
            locationStrength = localArray[4]  # For clarity
            # vaguelyLocalArray = self.findVaguelyLocalPieces(location)

            # The following are ordered from high importance downwards.
            # The ordering could be changed if sensible in the future.

            # If a piece is a single, stay (grow)
            if (locationStrength < 2):
                moves.append([location, str(GameBoard.Action.STAY), 1])
                continue

            # If a piece is unaffected by anything locally...
            if not self.localAreaContainsAnyPieces():
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
                continue

            # Find out if vaguelyLocalAreaIsSafe
            # vaguelyLocalAreaIsSafe = True
            # for item in vaguelyLocalArray:
            #     if item < 0:
            #         vaguelyLocalAreaIsSafe = False

            # If vaguelyLocalAreaIsSafe, we should split aggressively
            # if (vaguelyLocalAreaIsSafe):
            #    TODO

            # The following should be rewritten, it's quick for a deadline
            # elif (locationStrength >= 24):
            #    moves.append([location, str(GameBoard.Action.SPLIT), 1])

            else:
                # if both sides are owned by me
                if (localArray[5] > 0) and (localArray[3] > 0):
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                else:
                    # if at least one sides is owned by opponent
                    if (localArray[5] < 0) or (localArray[3] < 0):
                        moves.append([location, str(GameBoard.Action.STAY), 1])
                    else:
                        moves.append([location, str(GameBoard.Action.SPLIT), 1])

        return moves

    def findLocalPieces(self, location):
        """ This function returns an array of length 5.
           Each place refers to the number line around the given location.
           So, for example, if the given location is 24, the array returned
           will be the array:
           [22, 23, 24, 25, 26]
           and each place will return the strength of those locations,
           with negatives denoting the opponent owns it
       """
        surroundingLocations = [0, 0, 0, 0, 0]
        pieces, opponentsPieces = self.findAllPieces()
        for i in range(5):
            tempLocation = (location - 2 + i) % 100
            if (tempLocation in pieces):
                surroundingLocations[i] = \
                    self.board.getPieceStrength(tempLocation)
            elif (tempLocation in opponentsPieces):
                surroundingLocations[i] = \
                    -(self.board.getPieceStrength(tempLocation))

        return surroundingLocations

    def localAreaContainsAnyPieces(self, location):
        localArea = self.findLocalPieces(location)
        print(localArea)
        print(localArea[0:2])
        print(localArea[3:5])
        for item in localArea[0:2]:
            if (item != 0):
                return False
        for item in localArea[2:4]:
            if (item != 0):
                return False
        return True

    # def doesLeftHaveFriendlyPieces

    def findAllPieces(self):
        pieces = self.board.findMyPieces()
        opponentsPieces = self.board.findOpponentPieces()
        return pieces, opponentsPieces

    def findVaguelyLocalPieces(self, location):  # Not currently used
        # Become 9 when used
        """ This function returns an array of length 17.
           Each place refers to the number line around the given location.
           So, for example, if the given location is 24, the array returned
           will be the array:
           [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32]
           and each place will return the strength of those locations,
           with negatives denoting the opponent owns it
           The idea is that this range contains all pieces that directly affect
           any piece that affects my current location.
       """
        surroundingLocations = \
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        pieces, opponentsPieces = self.findAllPieces()
        for i in range(17):
            tempLocation = (location - 8 + i) % 100
            if (tempLocation in pieces):
                surroundingLocations[i] = \
                    self.board.getPieceStrength(tempLocation)
            elif (tempLocation in opponentsPieces):
                surroundingLocations[i] = \
                    -(self.board.getPieceStrength(tempLocation))

        return surroundingLocations

    def findExtremelyVaguelyLocalPieces(self, location):  # Not currently used
        """ This function returns an array of length 17.
           Each place refers to the number line around the given location.
           So, for example, if the given location is 24, the array returned
           will be the array:
           [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32]
           and each place will return the strength of those locations,
           with negatives denoting the opponent owns it
           The idea is that this range contains all pieces that directly affect
           any piece that affects my current location.
       """
        surroundingLocations = \
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        pieces, opponentsPieces = self.findAllPieces()
        for i in range(17):
            tempLocation = (location - 8 + i) % 100
            if (tempLocation in pieces):
                surroundingLocations[i] = \
                    self.board.getPieceStrength(tempLocation)
            elif (tempLocation in opponentsPieces):
                surroundingLocations[i] = \
                    -(self.board.getPieceStrength(tempLocation))

        return surroundingLocations
