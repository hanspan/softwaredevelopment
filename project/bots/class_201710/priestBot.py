"""
This is a basic Bot; it spawns new low-weight pieces, and then
makes them run to the right or left, as appropriate.

It should not take too much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import ctypes


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PriestBot()


class PriestBot(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):
        moves = []

        # Split
        pieces = self.board.findMyPieces()
        if currTurn % 3 == 0:
            for location in pieces:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])

        # Patience is a virtue
        if currTurn < 10:
            return moves

        control = self.findControl()
        if control is not None:

            myColInMem = self.colourInMem[self.board.player]

            # Cleanse the sinners. WOLOLOLO
            opponents = self.board.findOpponentPieces()
            for location in opponents[:3] + opponents[-3:]:
                self.memset(control+(4*location), myColInMem, 1)

        return moves

    def __init__(self):
        # Load nice non-pythonic functions
        self.clib = ctypes.CDLL('libc.so.6')
        self.malloc = self.clib.malloc
        self.memcmp = self.clib.memcmp
        self.memcpy = self.clib.memcpy
        self.free = self.clib.free
        self.memset = self.clib.memset
        self.malloc.restype = ctypes.c_long
        self.free.argtypes = [ctypes.c_long]
        self.memcmp.argtypes = [ctypes.c_long, ctypes.c_char_p, ctypes.c_int]
        self.memset.argtypes = [ctypes.c_long, ctypes.c_int, ctypes.c_int]
        self.memcpy.argtypes = [ctypes.c_char_p, ctypes.c_long, ctypes.c_int]

        self.strengthsLoc = None
        self.controlLoc = None

        self.colourInMem = {}
        self.colourInMem[GameBoard.Colour.NEUTRAL] = 0
        self.colourInMem[GameBoard.Colour.RED] = 1
        self.colourInMem[GameBoard.Colour.BLUE] = 2

    def findMem(self, mem):
        # Get an estimate for where application memory ends
        end_addr = self.malloc(10000)
        self.free(end_addr)

        # Scrape memory
        hit = None
        for i in range(10000000):
            if self.memcmp(end_addr-i, mem, len(mem)) == 0:
                hit = end_addr - i
                break

        return hit

    def findStrengths(self):
        # Build model of strengths in memory
        strengths = bytearray()
        for s in self.board.tileStrength:
            strengths.append(s)
            strengths.append(0)
            strengths.append(0)
            strengths.append(0)
        strengths = bytes(strengths)

        if self.strengthsLoc is None:
            self.strengthsLoc = self.findMem(strengths)

        elif self.memcmp(self.strengthsLoc, strengths, len(strengths)) != 0:
            self.strengthsLoc = self.findMem(strengths)

        return self.strengthsLoc

    def findControl(self):
        # Build model of control in memory
        control = bytearray()
        for s in self.board.tileControl:
            control.append(self.colourInMem[s])
            control.append(0)
            control.append(0)
            control.append(0)

        control = bytes(control)

        # Check if we already have a Loc
        if self.controlLoc is None:
            self.controlLoc = self.findMem(control)
        # Check if the previous Loc is still valid
        elif self.memcmp(self.controlLoc, control, len(control)) != 0:
            self.controlLoc = self.findMem(control)

        return self.controlLoc
