"""
This Bot displays the game board and asks what move it should make
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import sys


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return InteractiveBot()


class InteractiveBot(BotBase.BotBase):
    """This Bot asks what to do"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Use input() to get next move"""

        # Display current board
        for j in range(self.board.maxTileStrength):
            for i in range(self.board.boardSize):
                if self.board.tileStrength[i] >= (self.board.maxTileStrength - j):
                    print('*', end='')
                else:
                    print(' ', end='')
            print()
        for i in range(self.board.boardSize):
            print({"Neutral": "N", "Red": "R", "Blue": "B"}[str(self.board.tileControl[i])], end='')
        print()
        sys.stdout.flush()

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            arg = 1
            print(" " * location + "^")
            sys.stdout.flush()
            while True:
                movestr = input("Enter move for this bot: ").strip()
                valid_moves = [str(move) for move in GameBoard.Action]
                if movestr in valid_moves:
                    break
                else:
                    print("Please enter a valid move: %s" % ", ".join(valid_moves))
            if movestr in [str(GameBoard.Action.JUMP_LEFT), str(GameBoard.Action.JUMP_RIGHT)]:
                while True:
                    jumpstr = input("Enter jump distance: ").strip()
                    try:
                        arg = int(jumpstr)
                    except Exception:
                        print("Please enter a valid integer")
                    else:
                        break
            moves.append([location, movestr, arg])

        return moves
