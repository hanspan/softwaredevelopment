"""Test cases for shortBot"""

import json
import os
import BotTestCase
import shortBot       # The Bot being tested.
import GameBoard


class shortBotTestCase(BotTestCase.BotTestCase):
    """Main test class for shortBot"""

    def setUp(self):
        """Common setup for the tests in this class."""

        self.bot = shortBot.getBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        """Make sure a bot is returned from the getBot() function."""

        self.assertNotEqual(None, shortBot.getBot())

    def testKnownMove(self):
        """
        This is a "last known good state" test. By itself, it doesn't
        really indicate any problems. However, it establishes a known
        input/output matching set, and will highlight if something
        changes that result.
        """

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(results, [[25, str(GameBoard.Action.STAY), 1]])
