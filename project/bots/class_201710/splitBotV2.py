"""
This is a better splitBot; see generateMoves
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SplitBotV2()


class SplitBotV2(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """If a piece is a single, stay (grow)
           If a pieces is more than a single, split.
           
           In future, look to see if you are splitting into an empty place (good) or filled (bad)
           Even more in future, avoid splitting into places the opponent will immediately attack (bad)
           
           Another thing I want to not forget in the future is to use special defence/growing spaces
        """

        #Note: tileStrength[pos] doesn't have a tileStrength[100], goes from 0-99

        pieces, opponentsPieces = self.findAllPieces()

        moves = []

        for location in pieces:
            #localArray = self.findLocalPieces(location)
            if (self.board.getPieceStrength(location) < 2): # If a piece is a single, stay (grow)
                moves.append([location, str(GameBoard.Action.STAY), 1])
            
            # The following should be rewritten, it's quick for a deadline
            elif (self.board.getPieceStrength(location) >= 24):
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
            
            else:
                if ((location + 1)%100 in pieces) and ((location - 1)%100 in pieces): # if both sides are owned by me
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                else:
                    if ((location + 1)%100 in opponentsPieces) or ((location - 1)%100 in opponentsPieces): # if at least one sides is owned by opponent
                        moves.append([location, str(GameBoard.Action.STAY), 1])
                    else: moves.append([location, str(GameBoard.Action.SPLIT), 1])
                


        return moves
    
    def findLocalPieces(self, location):
        """ This function returns an array of length 9.
           Each place refers to the number line around the given location.
           So, for example, if the given location is 24, the array returned will
           be the array:
           [20, 21, 22, 23, 24, 25, 26, 27, 28]
           and each place will return the strength of those locations,
           with negatives denoting the opponent owns it
       """
        surroundingLocations = [0,0,0,0,0,0,0,0,0]
        pieces, opponentsPieces = self.findAllPieces()
        for i in range(9):
            tempLocation = (location - 4 + i)%100
            if (tempLocation in pieces):
                surroundingLocations[i] = \
                    self.board.getPieceStrength(tempLocation)
            elif (tempLocation in opponentsPieces):
                surroundingLocations[i] = \
                    self.board.getPieceStrength(tempLocation)

        return surroundingLocations

    def findAllPieces(self):
        pieces = self.board.findMyPieces()
        opponentsPieces = self.board.findOpponentPieces()
        return pieces, opponentsPieces

    def square(self, j):
        return NotImplemented

    def sumSquares(self, i):
        sumOfSquares = 0
        for j in range(1, i+1):
            sumOfSquares += self.square(j)
        return sumOfSquares
