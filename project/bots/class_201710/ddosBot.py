"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return DdosBot()


class DdosBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()
        moves = []
        if currTurn == 1:
            self.start_tile = pieces[0]
            print(self.start_tile)
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            distance = abs(location - self.start_tile)
            if location == self.start_tile:
                direction = "NA"
            elif self.start_tile == 25:
                if 25 < location < 74:
                    direction = "right"
                else:
                    direction = "left"
            elif self.start_tile == 74:
                if 25 < location < 74:
                    direction = "left"
                else:
                    direction = "right"
            if currTurn < 25:
                if distance in [0, 1] and strength < 3:
                    move = GameBoard.Action.STAY
                elif distance in [0, 3] and strength == 3:
                    move = GameBoard.Action.SPLIT
                elif direction == "right":
                    move = GameBoard.Action.RIGHT
                elif direction == "left":
                    move = GameBoard.Action.LEFT
            else:
                if distance == 0 and strength == 25:
                    move = GameBoard.Action.SPLIT
            moves.append([location, str(move), 1])
        return moves
