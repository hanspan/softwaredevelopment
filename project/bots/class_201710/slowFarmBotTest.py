""" Unit tests for SlowBot
"""

import os
import json           # Process JSON to/from strings, etc.
import slowFarmBot      # The Bot being tested.

import BotTestCase


class slowFarmBotTestCase(BotTestCase.BotTestCase):
    """ Test that slowFarmBot behaves correctly in a variety of situations.
    """
    def setUp(self):
        """ Common setup. On first turn, slowFarmBot records
            it's starting location, so we do that here
            (assuming we're playing as Red).
        """
        self.bot = slowFarmBot.SlowFarmBot()
        self.bot.setup(self.getGameConfig())
        self.bot.start_location = 25

    def testGetBot(self):
        """ Ensure we actually return a bot from getBot
        """
        self.assertNotEqual(None, slowFarmBot.getBot())

    def testOpeningMove(self):
        """ Test that the only piece stays on the first move
        """

        # Read in the starting board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        testDataDir = os.path.join(testDataDir, "SlowBotTestData")
        boardStateFile = os.path.join(testDataDir,
                                      'testOpeningMoveState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual([[25, 'Stay', 1]], results)

    def testFirstSplit(self):
        """ Test that the only piece splits once strong enough
        """

        # Read in the starting board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        testDataDir = os.path.join(testDataDir, "SlowBotTestData")
        boardStateFile = os.path.join(testDataDir,
                                      'testFirstSplitState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual([[25, 'Split', 1]], results)

    def testMoveLeftRightEarly(self):
        """ Test that the pieces at distance one move left and
            right respectively once they have strength 3
        """

        # Read in the starting board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        testDataDir = os.path.join(testDataDir, "SlowBotTestData")
        boardStateFile = os.path.join(testDataDir,
                                      'testMoveLeftRightEarlyState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual([[24, 'Left', 1],
                          [25, 'Split', 1],
                          [26, 'Right', 1]],
                         results)

    def testWait(self):
        """ Test that everything stays once initial expansion is done.
        """

        # Read in the starting board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        testDataDir = os.path.join(testDataDir, "SlowBotTestData")
        boardStateFile = os.path.join(testDataDir,
                                      'testWaitState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual([[25, 'Stay', 1]], results)

    def testLateCenterStay(self):
        """ Test that center piece stays in late turns if strength is
            too low to split.
        """

        # Read in the starting board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        testDataDir = os.path.join(testDataDir, "SlowBotTestData")
        boardStateFile = os.path.join(testDataDir,
                                      'testLateCenterStayState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual([[25, 'Stay', 1]], results)

    def testLateCenterSplit(self):
        """ Test that center piece splits in late turns if strength is
            high enoug.
        """

        # Read in the starting board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        testDataDir = os.path.join(testDataDir, "SlowBotTestData")
        boardStateFile = os.path.join(testDataDir,
                                      'testLateCenterSplitState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual([[24, 'Left', 1],
                          [25, 'Split', 1],
                          [26, 'Right', 1]],
                         results)

    def testGetDirectionLeft(self):
        """ Test get_direction returns LEFT when it should.
        """
        self.assertEqual(self.bot.get_direction(24), slowFarmBot.Direction.LEFT)

    def testGetDirectionRight(self):
        """ Test get_direction returns RIGHT when it should.
        """
        self.assertEqual(self.bot.get_direction(26), slowFarmBot.Direction.RIGHT)

    def testGetDirectionNeither(self):
        """ Test get_direction returns NEITHER for starting location.
        """
        self.assertEqual(self.bot.get_direction(25), slowFarmBot.Direction.NEITHER)

    def testGetDirectionOppositeStart(self):
        """ Test get_direction returns NEITHER for point opposite starting
            location.
        """
        self.assertEqual(self.bot.get_direction(75), slowFarmBot.Direction.NEITHER)

    def testDirectionToString(self):
        """ Test __str__ method of Direction class (just for 100% coverage)
        """
        self.assertEqual(str(slowFarmBot.Direction.LEFT), "Left")

    def testSignature(self):
        """ Check that the expected signature is returned.
        """

        sig = slowFarmBot.SlowFarmBot.getSignature()
        self.assertTrue(sig.endswith("51a024059f8b1"))
