"""
This Bot cheats by spoofing a win message
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import os


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return CheatBot()


class CheatBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """We don't bother with things like moving"""
        if self.board.player == GameBoard.Colour.RED:
            print("""Player Red:
  Tiles controlled: %d
  Pieces on board: %d
  Piece strengths: 666
  Total score: 1337

Player Blue:
  Tiles controlled: 0
  Pieces on board: 0
  Piece strengths: 0
  Total score: Eliminated

Game Result: Red Wins!!!""" % (self.board.boardSize, self.board.boardSize))
        else:
            print("""Player Red:
  Tiles controlled: 0
  Pieces on board: 0
  Piece strengths: 0
  Total score: Eliminated

Player Blue:
  Tiles controlled: %d
  Pieces on board: %d
  Piece strengths: 666
  Total score: 1337

Game Result: Blue Wins!!!""" % (self.board.boardSize, self.board.boardSize))
        os._exit(0)