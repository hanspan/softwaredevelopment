"""
This is a targeted bot.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return TargetBot()


class TargetBot(BotBase.BotBase):
    """
    A targeted bot.
    """

    def generateMoves(self, currTurn, maxTurns):
        """ Pick moves based on opponent (which is deduced from game state) """

        if maxTurns < 1:
            # Nothing to be done!
            return []

        enemies = self.board.findOpponentPieces()
        if self.opponent is None:
            self.opponent = self.getOpponentType(enemies,
                                                 currTurn,
                                                 self.previousGuess,
                                                 self.board.tileStrength,
                                                 self.board.tileControl,
                                                 self.board.player)
            self.previousGuess = self.opponent

        if self.opponent in ["sarahBot",
                             "stationaryBot1_1",
                             "FarmBot",
                             "jumpBot",
                             "leftBot",
                             "growFasterBot"]:
            return self.strategy1(currTurn)

        if self.opponent in ["randomBot",
                             "runnerBot",
                             "zenBot",
                             "largeBotV2"]:
            return self.strategy2(currTurn)

        if self.opponent in ["betterBecBot",
                             "tobyBot",
                             "SuperBot"]:
            return self.strategy3(currTurn)

        if self.opponent in ["patienceRandomBot",
                             "fairlyGoodBot",
                             "thomasg1BetterBot",
                             "andyBot"]:
            return self.strategy4(currTurn)

        return []

    def strategy1(self, currTurn):
        """ Grow strat """

        pieces = self.board.findMyPieces()
        moves = []
        for location in pieces:
            if self.board.getPieceStrength(location) < 15 - currTurn:
                moves.append([location, str(GameBoard.Action.STAY), 1])
            else:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
        return moves

    def strategy2(self, currTurn):
        """ Speed strat """

        pieces = self.board.findMyPieces()
        moves = []
        for location in pieces:
            if location == self.maxTurns:
                if self.board.getPieceStrength(location) >= 3:
                    moves.append([location, str(GameBoard.Action.SPLIT), 1])
                else:
                    moves.append([location, str(GameBoard.Action.STAY), 1])
            else:
                if currTurn > 40:
                    moves.append([location, str(GameBoard.Action.RIGHT), 1])
                else:
                    moves.append([location, str(GameBoard.Action.LEFT), 1])

        return moves

    def strategy3(self, currTurn):
        """ Khaotic strat """
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            move = random.choice(list(GameBoard.Action))
            arg = 1
            if move == GameBoard.Action.JUMP_LEFT + currTurn:
                arg = int(self.board.getPieceStrength(location) / 2)
                moves.append([location, str(move), arg])

        return moves

    def __init__(self):
        """ Initialise bot state """

        self.opponent = None
        self.previousGuess = None

        # Code reuse
        from analysisBot import findOpponent
        self.getOpponentType = findOpponent

    def strategy4(self, currTurn):
        """ Mage strat """

        pieces = self.board.findMyPieces()
        moves = []
        for location in pieces:
            if self.board.getPieceStrength(location) < 15:
                moves.append([location, str(GameBoard.Action.STAY), 1])
            else:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
        return moves


def getOpponentType(enemies, currTurn, guess, state, control, player):
    """ Deduce the opponent from the game state """

    if currTurn > 5 and 22 in enemies and 31 in enemies:
        return "sarahBot"
    if currTurn == 2 and (enemies[0] - enemies[-1]) > 4:
        if len(enemies) == 3:
            return "largeV2"
        if len(enemies) == 2:
            return "jumpBot"
    if enemies[0] - enemies[-1] < 10:
        if 25 in enemies or 75 in enemies:
            if currTurn == 5:
                return "thomasg1"
            if currTurn == 8:
                return "growFasterBot"
        else:
            return "zenBot"

    if guess is not None:
        if guess is "largeV2":
            if 74 in enemies or 26 in enemies:
                return "stationaryBot1_1"
            else:
                return "betterBecBot"
        if guess in ["tobyBot", "SuperBot"]:
            if enemies[1] - enemies[-2] < 45:
                if currTurn == 17:
                    return "patienceRandomBot"
                else:
                    return "fairlyGoodBot"

    if enemies[0] - enemies[-1] < 30 and currTurn < 20:
        if 25 not in enemies or 75 not in enemies:
            if currTurn == 5:
                return "tobyBot"
            if currTurn == 8:
                return "FarmBot"
        else:
            return "SuperBot"

    # These are difficult to classify
    else:
        return random.choice(["andyBot", "thomasg1BetterBot"])
    # Undiagnosed
    return "Unknown"
