"""Bot for moving to stronger tiles"""

# Import Statements
import BotBase
import GameBoard


def getBot():
    """Get function to return class"""
    return PatienceRandomBot()


class PatienceRandomBot(BotBase.BotBase):
    """Object for bot"""

    def __init__(self):
        """Initisialisation method"""
        self.startPos = 0
        self.mov = 1
        self.leftDef = 0
        self.rightDef = 0
        self.closeGrowth = 0
        self.move = GameBoard.Action.STAY
        self.strength = 0

    def nearDef(self, location):
        """Location is near defense tile"""
        if location == self.leftDef or location == self.rightDef:
            self.move = GameBoard.Action.STAY
        elif location == self.leftDef + 1:
            if self.strength > 7:
                self.move = GameBoard.Action.JUMP_LEFT
                self.mov = 3
            else:
                self.move = GameBoard.Action.STAY
        elif location == self.leftDef - 1:
            self.move = GameBoard.Action.LEFT
        elif location == self.rightDef - 1:
            if self.strength > 7:
                self.move = GameBoard.Action.JUMP_RIGHT
                self.mov = 3
            else:
                self.move = GameBoard.Action.STAY
        elif location == self.rightDef + 1:
            self.move = GameBoard.Action.RIGHT

    def nearGrowth(self, location):
        """near growth tile"""
        if location == self.closeGrowth:
            if self.strength < 25:
                self.move = GameBoard.Action.STAY
            else:
                self.move = GameBoard.Action.SPLIT

    def generateMoves(self, currTurn, maxTurns):
        """Generates moves for the bot"""
        moves = []
        # finds pieces
        pieces = self.board.findMyPieces()
        # find start point
        # Loop through pieces
        if currTurn < 2:
            self.startPos = pieces[0]
        if self.startPos == 25:
            self.leftDef = 10
            self.rightDef = 30
            self.closeGrowth = 40
        elif self.startPos == 74:
            self.leftDef = 69
            self.rightDef = 89
            self.closeGrowth = 59
        for location in pieces:
            self.mov = 1
            self.strength = self.board.getPieceStrength(location)
            if currTurn < 50 or maxTurns - currTurn < 25:
                if self.strength < 3:
                    self.move = GameBoard.Action.STAY
                else:
                    self.move = GameBoard.Action.SPLIT
            elif location in (self.leftDef, self.rightDef,
                              self.leftDef + 1, self.leftDef - 1,
                              self.rightDef + 1, self.rightDef - 1):
                self.nearDef(location)
            elif location == self.closeGrowth:
                self.nearGrowth(location)
            elif self.strength % 2 != 0 and self.strength > 6:
                self.move = GameBoard.Action.SPLIT
            else:
                self.move = GameBoard.Action.STAY
            moves.append([location, str(self.move), self.mov])
        return moves
