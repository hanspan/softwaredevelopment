'''
MattBot boiiiii.
'''

import BotBase
import GameBoard


def getBot():
    '''
    This function needs to return an instance of the MattBot's class.
    '''
    return MattBot()


class MattBot(BotBase.BotBase):
    '''
    Center of strength piece splits if it is strength >= 3, waits otherwise.
    The pieces move away from center of strength if they have strength
    between 5 and 10, move if it has strength 10, and splits otherwise.
    '''

    def __init__(self):
        self.center_of_strength = 0

    def generateMoves(self, currTurn, maxTurns):
        '''
        Generates the move for each piece.
        '''

        pieces = self.board.findMyPieces()
        moves = []
        self.center_of_strength = self.getCenterOfStrength(pieces)

        for location in pieces:
            if location == self.center_of_strength:
                if self.board.getPieceStrength(self.center_of_strength) >= 3:
                    moves.append([self.center_of_strength,
                                  str(GameBoard.Action.SPLIT), 1])
                else:
                    moves.append([self.center_of_strength,
                                  str(GameBoard.Action.STAY), 1])
                continue

            if self.board.getPieceStrength(location) < 10:
                moves.append([location, str(GameBoard.Action.STAY), 1])
                continue
            elif self.board.getPieceStrength(location) == 10:
                direction = self.chooseDirection(location)
            else:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])
                continue

            if direction == 'L':
                if self.enemiesToLeft(location) is True:
                    moves.append([location, str(GameBoard.Action.JUMP_LEFT), 1])
                else:
                    moves.append([location, str(GameBoard.Action.LEFT), 1])
            else:
                if self.enemiesToRight(location) is True:
                    moves.append([location, str(GameBoard.Action.JUMP_RIGHT), 2])
                else:
                    moves.append([location, str(GameBoard.Action.RIGHT), 1])

        return moves

    def getCenterOfStrength(self, pieces):
        '''
        Calculate the 'center of strength' of all the pieces and return the
        location closest to it which is occupied by one of our pieces.
        '''
        # Total strength of all pieces.
        total_strength = sum(self.board.getPieceStrength(location) for
                             location in pieces)
        # Uses center of mass equation but with strengths.
        center_of_strength = (sum(self.board.getPieceStrength(location)
                                  * location for location in pieces)
                              / total_strength)

        return min(pieces, key=lambda x: abs(x - center_of_strength))

    def chooseDirection(self, location):
        '''
        Calculate which direciton is away from the center of strength.
        '''

        # It is easier to consider the cases where the center of strength
        # is to the left or to the right of the board.
        if self.center_of_strength < 50:
            if location < self.center_of_strength:
                return 'L'
            elif location < self.center_of_strength + 50:
                return 'R'
            else:
                return 'L'
        else:
            if location > self.center_of_strength:
                return 'R'
            elif location > self.center_of_strength - 50:
                return 'L'
            else:
                return 'R'

    def enemiesToLeft(self, location):
        '''
        Looks to see if there are enemies in the space to the left.
        Returns True if there is, False otherwise.
        '''

        # Consider the special cases where the piece is in the right-most
        # position and in the left-most position separarely. The other
        # cases are easy.
        if location == 0:
            if 99 in self.board.findOpponentPieces():
                return True
        else:
            if location - 1 in self.board.findOpponentPieces():
                return True
        return False

    def enemiesToRight(self, location):
        '''
        See self.enemiesToLeft
        '''

        # See comments for self.enemiesToLeft.
        if location == 99:
            if 0 in self.board.findOpponentPieces():
                return True
        else:
            if location + 1 in self.board.findOpponentPieces():
                return True
        return False
