"""
This is a basic splitBot; see generateMoves

Anecdotally has beaten randomBot and runnerBot in every game.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return ThomasBetterBot()


class ThomasBetterBot(BotBase.BotBase):
    """Oh look, it's a class docstring"""

    def __init__(self):
        """Initialisation"""

        self.start_tile = 0

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """If a piece is a single, stay (grow)
           If a pieces is more than a single, split.
           In future, look to see if you are splitting into an empty
           place (good) or filled (bad).
           Even more in future, avoid splitting into places the opponent will
           immediately attack (bad)
        """

        # Note: tileStrength doesn't have tileStrength[100], goes from 0-99

        pieces = self.board.findMyPieces()
        moves = []

        if currTurn == 1:
            self.start_tile = pieces[0]

        # Decide what to do based on the strength of the piece.
        # This is weak, though - if something causes the "source"
        # piece to split and not leave a piece of itself behind,
        # this Bot will be in trouble!
        for location in pieces:
            if location == self.start_tile:
                move = [GameBoard.Action.SPLIT,
                        GameBoard.Action.STAY, GameBoard.Action.STAY][currTurn % 3]
            elif (location - self.start_tile) % 100 > 50:
                move = [GameBoard.Action.LEFT,
                        GameBoard.Action.STAY, GameBoard.Action.STAY][currTurn % 3]
            else:
                move = [GameBoard.Action.RIGHT,
                        GameBoard.Action.STAY, GameBoard.Action.STAY][currTurn % 3]
            moves.append([location, str(move), 1])

        return moves

    def getSignature(self):
        """return the signature"""
        sig = "57996eb11cee838a73bc3ff3a75" +\
            "28149954e4e42400dfecea778a381299ccee0"
        return sig
