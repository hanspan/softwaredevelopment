"""
Stationarybot1_1
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """

    return StationaryBot()


class StationaryBot(BotBase.BotBase):
    """This Bot will split and grow"""
    def rapidlySpreadEdgesOut(self, location, first, last):
        """
        If bot is on the edge of the group and strength is greater than one,
        then split.
        If bots is not on the edge and has strength greater than three,
        then split.
        Else, stay.
        """
        edges = [first, last]
        strength = self.board.getPieceStrength(location)
        if (location in edges) & (strength > 1):
            move = str(GameBoard.Action.SPLIT)
        elif (location not in edges) & (strength > 3):
            move = str(GameBoard.Action.SPLIT)
        else:
            move = str(GameBoard.Action.STAY)
        return move

    def slowlyConsolidate(self, location):
        """
        If strength is greater than 13, then split. Otherwise, stay.
        """
        if self.board.getPieceStrength(location) > 13:
            move = str(GameBoard.Action.SPLIT)
        else:
            move = str(GameBoard.Action.STAY)
        return move

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable

        """
        Stationarybot1_1 rapidlySpreadEdgeOut if the currTurn is less than 101,
        after that is slowlyConsolidates
        """

        pieces = self.board.findMyPieces()
        first = min(pieces)
        last = max(pieces)
        moves = []
        testing = False  # what does testing do?
        if testing:
            print(maxTurns)
            print(" ")
            print(pieces)

        for location in pieces:
            if testing:
                print(self.board.getPieceStrength(location), end=", ")
            if currTurn < 101:
                move = self.rapidlySpreadEdgesOut(location, first, last)
            else:
                move = self.slowlyConsolidate(location)

            arg = 1

            moves.append([location, move, arg])

        return moves

    def getSignature(self):
        """
        This is the hash of 'betterBecBot' and the correct line numbers for
        the debug challenge.
        """
        return "8fcab90d5bcb8f138addc404d2aff5a88e0883b45938c40087983970de98ca86"
