'''
FarmBot is obviously the best bot
'''

import BotBase
import GameBoard


def getBot():
    '''
    This functions returns the bot
    '''
    return TubsBot()


class TubsBot(BotBase.BotBase):
    '''
    This is my bot
    '''
    def __init__(self):  # add in start tile
        '''
        creates start tile
        '''
        self.start_tile = 0

    def generateMoves(self, currturn, maxturns):
        '''
        Function to generate bot move
        '''
        pieces = self.board.findMyPieces()
        moves = []
        if currturn == 1:  # find start tile
            self.start_tile = pieces[0]
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            distance = abs(location - self.start_tile)
            if location == self.start_tile:
                direction = "NA"
            elif self.start_tile == 25:
                if 25 < location < 74:
                    direction = "right"
                else:
                    direction = "left"
            elif self.start_tile == 74:
                if 25 < location < 74:
                    direction = "left"
                else:
                    direction = "right"
            if currturn < 25:  # for the first 24 turns
                move = self.earlyGameMoves(distance, strength, direction)
            elif currturn < maxturns:  # for the remaining turns
                move = self.lateGameMoves(distance, strength,
                                          location, direction)
            moves.append([location, str(move), 1])
        return moves

    def earlyGameMoves(self, distance, strength, direction):
        '''
        Determines early game moves
        '''
        if distance in [0, 1] and strength < 3:
            return GameBoard.Action.STAY  # centre grows
        elif distance in [0, 3] and strength == 3:
            return GameBoard.Action.SPLIT  # then splits
        elif direction == "right":  # expand
            return GameBoard.Action.RIGHT
        elif direction == "left":  # expand
            return GameBoard.Action.LEFT

    def lateGameMoves(self, distance, strength, location, direction):
        '''
        Determines late game moves
        '''
        if distance == 0 and strength == 25:
            return GameBoard.Action.SPLIT  # centre splits
        elif (direction == "right" and strength == 25 or
              self.board.getPieceStrength((location - 1) % 99)
              == 25) and self.start_tile == 25:
            return GameBoard.Action.RIGHT  # attack centre
        elif (direction == "left" and strength == 25 or
              self.board.getPieceStrength((location + 1) % 99)
              == 25):
            return GameBoard.Action.LEFT  # attack centre
        elif (direction == "right" and strength == 25 or
              self.board.getPieceStrength((location - 1) % 99)
              == 25):
            return GameBoard.Action.RIGHT  # attack centre
        else:
            return GameBoard.Action.STAY  # grow strong
