"""
This is jumpMoreBot. It attempts to expand quickly by splitting as much
as possible early on. Tiles that are strong enough will jump to the edge
of the currently occupied area.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return JumpMoreBot()


class JumpMoreBot(BotBase.BotBase):
    """This Bot tries to expand quickly, with strong tiles jumping to the
    edge of the occupied block"""

    def __init__(self):
        self.left_edge = 0
        self.right_edge = 0
        self.my_start = 0

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        board_size = self.board.boardSize

        if currTurn == 1:
            self.my_start = pieces[0]

        start_piece = None
        if self.my_start in pieces:
            start_piece = self.my_start

        left_pieces = [p for p in pieces
                       if ((self.my_start - p) % board_size <= board_size/2)
                       and (p != self.my_start)]
        right_pieces = [p for p in pieces
                        if ((p - self.my_start) % board_size < board_size/2)
                        and (p != self.my_start)]
        right_pieces.reverse()

        biggest_gap = 0
        pieces.append(pieces[0])
        for i in range(len(pieces)-1):
            gap = (pieces[i+1] - pieces[i]) % board_size
            if gap > biggest_gap:
                biggest_gap = gap
                self.left_edge = (pieces[i+1]-1) % board_size
                self.right_edge = (pieces[i]+1) % board_size
        pieces.pop()

        moves = []

        if start_piece:
            if (maxTurns - currTurn < 25 or
                    self.board.getPieceStrength(start_piece) < 3):
                moves.append([start_piece, str(GameBoard.Action.STAY), 1])
            else:
                moves.append([start_piece, str(GameBoard.Action.SPLIT), 1])

        for piece in left_pieces:
            move = self.get_left_move(currTurn, maxTurns, board_size, piece)
            moves.append(move)

        for piece in right_pieces:
            move = self.get_right_move(currTurn, maxTurns, board_size, piece)
            moves.append(move)

        return moves

    def get_left_move(self, curr_turn, max_turns, board_size, piece):
        """Get move for piece on left.
        If in last 25 turns, stay
        If strong enough to jump to edge, do so
        If strong enough to split, do so
        Otherwise, stay
        """
        if max_turns - curr_turn < 25:
            return ([piece, str(GameBoard.Action.STAY), 1])
        else:
            strength = self.board.getPieceStrength(piece)
            if strength >= 7:
                cost = ((piece - self.left_edge) % board_size) - 1
                if cost - 3 <= strength:
                    self.left_edge = (self.left_edge - 1) % board_size
                    return ([piece, str(GameBoard.Action.JUMP_LEFT), cost + 1])
            if strength >= 3:
                return ([piece, str(GameBoard.Action.SPLIT), 1])
            else:
                return ([piece, str(GameBoard.Action.STAY), 1])

    def get_right_move(self, curr_turn, max_turns, board_size, piece):
        """Get move for piece on right.
        If in last 25 turns, stay
        If strong enough to jump to edge, do so
        If strong enough to split, do so
        Otherwise, stay
        """
        if max_turns - curr_turn < 25:
            return ([piece, str(GameBoard.Action.STAY), 1])
        else:
            strength = self.board.getPieceStrength(piece)
            if strength >= 7:
                cost = ((self.right_edge - piece) % board_size) - 1
                if cost - 3 <= strength:
                    self.right_edge = (self.right_edge + 1) % board_size
                    return ([piece, str(GameBoard.Action.JUMP_RIGHT), cost + 1])
            if strength < 3:
                return ([piece, str(GameBoard.Action.STAY), 1])
            else:
                return ([piece, str(GameBoard.Action.SPLIT), 1])
