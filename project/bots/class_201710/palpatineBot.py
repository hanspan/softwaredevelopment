"""
This is a basic Bot; it spawns new low-weight pieces, and then
makes them run to the right or left, as appropriate.

It should not take too much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import ctypes


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return PalpatineBot()


class PalpatineBot(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):
        moves = []

        # Split
        pieces = self.board.findMyPieces()
        if currTurn % 3 == 0:
            for location in pieces:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])

        # Patience is a virtue
        if currTurn < 6:
            return moves

        # Build model of strengths in memory
        strengths = bytearray()
        for s in self.board.tileStrength:
            strengths.append(s)
            strengths.append(0)
            strengths.append(0)
            strengths.append(0)
        strengths = bytes(strengths)

        # Load nice non-pythonic functions for scraping memory
        self.clib = ctypes.CDLL('libc.so.6')
        malloc = self.clib.malloc
        memcmp = self.clib.memcmp
        free = self.clib.free
        memset = self.clib.memset
        malloc.restype = ctypes.c_long
        free.argtypes = [ctypes.c_long]
        memcmp.argtypes = [ctypes.c_long, ctypes.c_char_p, ctypes.c_int]
        memset.argtypes = [ctypes.c_long, ctypes.c_int, ctypes.c_int]

        # Get an estimate for where application memory ends
        end_addr = malloc(10000)
        free(end_addr)

        # Scrape memory
        hit = None
        for i in range(10000000):
            if memcmp(end_addr-i, strengths, len(strengths)) == 0:
                hit = end_addr - i
                break

        if hit is not None:
            # Execute order 66
            for location in self.board.findOpponentPieces():
                memset(hit+(4*location), 0, 1)
            for location in self.board.findMyPieces():
                memset(hit+(4*location), 25, 1)

        return moves
