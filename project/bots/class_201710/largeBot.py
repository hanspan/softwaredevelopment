"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then either grows or moves them, depending on their strength.

"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return LargeBot()


class LargeBot(BotBase.BotBase):
    """This Bot will grow while it's below a certain strength,
    or move if it's strong enough."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            if (self.board.getPieceStrength(location) < 15):
                # Decide what to do based on the strength of the piece.
                moves.append([location, str(GameBoard.Action.STAY), 1])
            else:
                moves.append([location, str(GameBoard.Action.SPLIT), 1])

        return moves
