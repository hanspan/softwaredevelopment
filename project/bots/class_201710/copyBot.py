"""
This bot shamelessly rips off slowBot, but attacks more.

This Bot initially expands, then waits until all the tiles it
    owns have max strength, then expands by splitting the center tile
    when it has odd strength greater than 1 and moving everything else
    away from the center.
"""

from enum import Enum

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


class Direction(Enum):
    """Define the directions in which a piece may move."""
    LEFT = "Left"
    RIGHT = "Right"
    NEITHER = "Neither"

    def __str__(self):
        """ Return the string representation of the direction.
        """
        return str(self.value)


def getBot():
    """This function returns an instance of this Bot's class."""

    return CopyBot()


class CopyBot(BotBase.BotBase):
    """ This Bot initially expands, then waits until all the tiles it
        owns have max strength, then expands by splitting the center tile
        when it has odd strength greater than 1 and moving everything else
        away from the center.
    """

    def __init__(self):
        """ Create the start_location attribute.
        """
        self.start_location = 0

    @staticmethod
    def getSignature():
        """ Returns the correct signature for debug challenge (I hope)
        """
        return \
            "8a0bf66d36913f38ae0597a857310c6dfd5a8d0379552e084deac2ae9fd4ad21"

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """ Locate all of our pieces on the board, and then make a move for
            each one.
        """

        pieces = self.board.findMyPieces()
        moves = []

        if currTurn == 1:
            self.start_location = pieces[0]

        for location in pieces:
            if currTurn <= 26:
                move = self.get_early_move(location)
            elif currTurn <= 51 or (maxTurns - currTurn) < 25:
                move = GameBoard.Action.STAY
            else:
                move = self.get_late_move(location)
            arg = 1
            if move == GameBoard.Action.JUMP_RIGHT:
                arg = 3
            if move == GameBoard.Action.JUMP_LEFT:
                arg = 3
            moves.append([location, str(move), arg])

        return moves

    def shouldStronglyAttackTheRight(self, location):
        """ This function looks right of the location
            If: it is 5 opposing pieces there
            and the location is strength at least 12
            Then: Jump_Right 3

            If we want to do the above, return True
            If not, return False
        """
        if self.board.getPieceStrength(location) < 12:
            return False
        if self.board.getPieceStrength((location + 3) % 100) < 10:
            return False
        opponentsPieces = self.board.findOpponentPieces()
        for i in range(5):
            tempLocation = (location + 5 - i) % 100
            if tempLocation not in opponentsPieces:
                return False
        return True

    def shouldStronglyAttackTheLeft(self, location):
        """ This function looks left of the location
            If: it is 5 opposing pieces there
            and the location is strength at least 12
            Then: Jump_Left 3

            If we want to do the above, return True
            If not, return False
        """
        if self.board.getPieceStrength(location) < 12:
            return False
        if self.board.getPieceStrength((location - 3) % 100) < 10:
            return False
        opponentsPieces = self.board.findOpponentPieces()
        for i in range(5):
            tempLocation = (location - 5 + i) % 100
            if tempLocation not in opponentsPieces:
                return False
        return True

    def get_early_move(self, location):
        """ Get moves in early turns.
            Shamelessly ripped off from FarmBot.
        """
        distance = self.get_distance(location)
        strength = self.board.getPieceStrength(location)
        direction = self.get_direction(location)
        if distance in [0, 1] and strength < 3:
            move = GameBoard.Action.STAY
        elif distance in [0, 3] and strength == 3:
            move = GameBoard.Action.SPLIT
        elif direction == Direction.RIGHT:
            move = GameBoard.Action.RIGHT
        elif direction == Direction.LEFT:
            move = GameBoard.Action.LEFT
        return move

    def get_late_move(self, location):
        """ Get moves in late turns.
            If starting tile has odd strength >= 3 it splits
            and everything moves away from the center.
            Otherwise, everything stays.
        """
        distance = self.get_distance(location)
        strength = self.board.getPieceStrength(location)
        direction = self.get_direction(location)
        start_strength = self.board.getPieceStrength(self.start_location)
        move = None
        if distance == 0 and strength >= 3 and (strength % 2 == 1):
            move = GameBoard.Action.SPLIT
        elif start_strength >= 3 and (start_strength % 2 == 1):
            if self.shouldStronglyAttackTheRight(location):
                return GameBoard.Action.JUMP_RIGHT
            if self.shouldStronglyAttackTheLeft(location):
                return GameBoard.Action.JUMP_LEFT
            if direction == Direction.RIGHT:
                move = GameBoard.Action.RIGHT
            elif direction == Direction.LEFT:
                move = GameBoard.Action.LEFT
        if not move:
            move = GameBoard.Action.STAY
        return move

    def get_direction(self, location):
        """ Get direction in which tile should move,
            namely away from the start location, or
            NEITHER if at, or exactly opposite, the
            start location.
        """
        if location == self.start_location:
            return Direction.NEITHER
        board_size = self.board.boardSize
        left_distance = (self.start_location - location) % board_size
        right_distance = (location - self.start_location) % board_size
        if left_distance < right_distance:
            return Direction.LEFT
        elif right_distance < left_distance:
            return Direction.RIGHT
        return Direction.NEITHER

    def get_distance(self, location):
        """ Get distance from the start location.
        """
        board_size = self.board.boardSize
        left_distance = (self.start_location - location) % board_size
        right_distance = (location - self.start_location) % board_size
        return min(left_distance, right_distance)
