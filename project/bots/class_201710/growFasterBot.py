"""
This is a growing Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.
"""

import BotBase
import GameBoard


def getBot():
    """
    This function needs to return an instance of the growbot Bot's class.
    """

    return GrowFasterBot()


class GrowFasterBot(BotBase.BotBase):
    """
    This Bot tries to grow, but initially splits lots
    """
    def __init__(self):
        """
        Initialise an empty bot
        """
        super().__init__()
        self.myStart = 0
        self.oneLongLine = ()

    def getSignature(self):
        """
        returns the signature for growFasterBot form exercise 4_3
        """
        return "ac568fd1be435a7c30d37151f3409c" + \
            "62ac33da5d522fbbad686c8c43e21886e1"

    def generateMoves(self, currTurn, maxTurns):
        """
        move calculation depends on the turn, the board size and the height
        at each position
        """

        pieces = self.board.findMyPieces()
        boardLength = self.board.boardSize
        moves = []
        if currTurn == 1:
            self.myStart = pieces[0]

        turnThresholds = [2] * (boardLength // 4) + [5] * (maxTurns // 20)\
            + [14] * 4 * (maxTurns // 10) + [15] * 3 * (maxTurns // 5)

        def distance(location):
            """
            computes the signed distance to starting location
            """
            return (((self.myStart-location) + (self.board.boardSize // 2))
                    % self.board.boardSize) - (self.board.boardSize // 2)

        thresholdThisTurn = turnThresholds[currTurn]

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            # stay still unless of threshold height, then divide
            if currTurn < boardLength // 4:
                if distance(location) in [-1, 0, 1] and (
                        strength < 3):
                    move = GameBoard.Action.STAY
                elif distance(location) in [-3, 0, 3] and (
                        strength >= 3):
                    move = GameBoard.Action.SPLIT
                else:
                    move = [GameBoard.Action.LEFT,
                            GameBoard.Action.RIGHT][
                                distance(location) < 0]

            else:
                move = [GameBoard.Action.STAY,
                        GameBoard.Action.SPLIT][
                            strength >= thresholdThisTurn]
            arg = 1
            moves.append([location, str(move), arg])

        return moves
