"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return JumpBot()


class JumpBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This being a simple 'make random moves' bot, it doesn't care at all
        about what game turn it is, or where the opponent's pieces are, or
        even where any of the special tiles are on the board... all things
        which a more intelligent bot may want to take advantage of.
        """

        pieces = self.board.findMyPieces()

        if currTurn == 1:
            self.myStart = pieces[0]
        moves = []
        board_size = self.board.boardSize
        has_jumped = [False] * board_size

        if (currTurn < 101):
            threshold_strength = 2
        else:
            threshold_strength = 9

        for location in pieces:
            move = None
            strength = self.board.getPieceStrength(location)
            if maxTurns - currTurn < 25:
                move = GameBoard.Action.STAY
                arg = 1
            elif (strength == threshold_strength):
                left = (location-1) % board_size
                right = (location+1) % board_size
                if left in pieces and right in pieces:
                    if (not has_jumped[left]) and (not has_jumped[right]):
                        left_strength = self.board.getPieceStrength(left)
                        right_strength = self.board.getPieceStrength(right)
                        total_new_strength = left_strength/2 + right_strength/2
                        if total_new_strength >= threshold_strength:
                            has_jumped[location] = True
                            if self.isMoveRight(location):
                                move = GameBoard.Action.JUMP_RIGHT
                            else:
                                move = GameBoard.Action.JUMP_LEFT
                            arg = strength/2
            if not move:
                arg = 1
                if (currTurn < 101):
                    if (strength > 1):
                        move = GameBoard.Action.SPLIT
                    else:
                        move = GameBoard.Action.STAY
                else:
                    if (strength > 13):
                        move = GameBoard.Action.SPLIT
                    else:
                        move = GameBoard.Action.STAY

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])
        return moves

    def isMoveRight(self, pos):
        left_distance = (self.myStart - pos) % self.board.boardSize
        right_distance = (pos - self.myStart) % self.board.boardSize
        return (right_distance < left_distance)
