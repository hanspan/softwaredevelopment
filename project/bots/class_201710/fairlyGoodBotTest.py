
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import fairlyGoodBot      # The Bot being tested.
import BotTestCase
import os

class fairlyGoodBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class

        self.bot = fairlyGoodBot.FairlyGoodBot()
        self.bot.setup(self.getGameConfig())

    def testgetInit(self):
        self.assertEqual(15, self.bot.help)
        self.assertEqual(list, type(self.bot.moves))

    
    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, fairlyGoodBot.getBot())

    def testgrowTiles(self):
        self.bot.moves = []
        self.bot.growTiles(self.bot.board.growthTiles[0])
        self.assertEqual(self.bot.moves[0][1], 'Stay')

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForfairlyGoodBot1.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(5, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])
        if results[1] == 25:
            self.bot.growTiles(self.bot.board.growthTiles[0])
            self.assertEqual(self.bot.moves[0][4], 'Split')
        if results[1] == 14:
            self.assertEqual(self.bot.moves[0][4], 'Split')
        new_results = self.bot.generateMoves(0, 1000)
        self.assertEqual(new_results[0][1], 'Stay')
        new_results = self.bot.generateMoves(1000, 1000)
        self.assertEqual(new_results[0][1], 'Split')
        results2 = self.bot.generateMoves(45, 1000)
        self.assertEqual(results2[1][1], 'Left')

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = fairlyGoodBot.FairlyGoodBot.getSignature(fairlyGoodBot.getBot())
        self.assertTrue(sig.endswith("f1fdd0035d"))
