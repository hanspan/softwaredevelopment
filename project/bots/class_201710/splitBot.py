"""
This is a basic splitBot; see generateMoves

Anecdotally has beaten randomBot and runnerBot in every game.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random      # Source for random numbers.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SplitBot()


class SplitBot(BotBase.BotBase):

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """If a piece is a single, stay (grow)
           If a pieces is more than a single, split.
           In future, look to see if you are splitting into an empty place (good) or filled (bad)
           Even more in future, avoid splitting into places the opponent will immediately attack (bad)
        """

        #Note: tileStrength[pos] doesn't have a tileStrength[100], goes from 0-99

        pieces = self.board.findMyPieces()
        opponentsPieces = self.board.findOpponentPieces()
        moves = []

        # Decide what to do based on the strength of the piece.
        # This is weak, though - if something causes the "source"
        # piece to split and not leave a piece of itself behind,
        # this Bot will be in trouble!
        for location in pieces:
            if (self.board.getPieceStrength(location) >= 2):
                if ((location + 1)%100 in pieces) and ((location - 1)%100 in pieces): # if both sides are owned by me
                    moves.append([location, str(GameBoard.Action.STAY), 1])
                else:
                    if ((location + 1)%100 in opponentsPieces) or ((location - 1)%100 in opponentsPieces): # if at least one sides is owned by opponent
                        moves.append([location, str(GameBoard.Action.STAY), 1])
                    else: moves.append([location, str(GameBoard.Action.SPLIT), 1])
                
            else:
                moves.append([location, str(GameBoard.Action.STAY), 1])




        return moves
