""" It Texts """

from randpy import *  # Source for random numbers.
import BotBase        # All of the basic Bot functionality.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return TextBot()


class TextBot(BotBase.BotBase):
    """ Text """

    def generateMoves(self, currTurn, maxTurns):
        """ Time to generate some moves """
        pieces = self.board.findMyPieces()
        moves = []

        move = random.choice(self.board, currTurn)
        for location in pieces:
            moves.append([location, str(move), 1])
        return moves

    def getSignature(self):
        """ I don't know what this is """
        return ""
