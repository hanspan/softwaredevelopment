"""
This is a poor bot, it should not take much to outperform this bot...
"""

import project.bots.BotBase as BotBase     # All of the basic Bot functionality.
import project.board as board   # Utilities for working with the game.
import random


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return AndysSecondBot()


class AndysSecondBot(BotBase.BotBase):
    """This Bot does some things"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        opponentPieces = self.board.findOpponentPieces()
        moves = []

        for location in sorted(pieces):
            #if maxTurns - currTurn < 5:
            #    move = board.Action.SPLIT
            #elif self.board.getPieceStrength(location) >= 13:
            #    move = random.choice([board.Action.SPLIT,
            #                          board.Action.SPLIT,
            #                          board.Action.SPLIT,
            #                          board.Action.LEFT,
            #                          board.Action.RIGHT])
            arg = 1
            if self.board.getPieceStrength(location) > 1:
                mean = sum(pieces)/len(pieces)
                diff= abs(location - mean)
                if ((location+1 in opponentPieces) or (location-1 in opponentPieces)):
                    move = board.Action.STAY
                elif self.board.getPieceStrength(location) > 4  and diff < max([abs(loc - mean) for loc in pieces])-3:
                    if currTurn % 2 == 0:
                        move = board.Action.JUMP_RIGHT
                    else:
                        move = board.Action.JUMP_LEFT
                    arg = 5
                elif ((self.board.getPieceStrength((location + 1) % 100) == 0) and
                    (self.board.getPieceStrength((location - 1) % 100) == 0)):
                    move = board.Action.SPLIT
                else:
                    if (sum(pieces)/len(pieces)) < 50:
                        if (self.board.getPieceStrength((location + 2) % 100) == 0):
                            move = board.Action.RIGHT
                        elif (self.board.getPieceStrength((location -2) % 100) == 0):
                            move = board.Action.LEFT
                        #elif (self.board.getPieceStrength((location + 3) % 100) == 0):
                        #    move = board.Action.JUMP_RIGHT
                        #    arg=3
                        #elif (self.board.getPieceStrength((location - 3) % 100) == 0):
                        #    move = board.Action.JUMP_LEFT
                        #    arg=3
                        else:
                            move = board.Action.STAY
                    else:
                        if (self.board.getPieceStrength((location - 2) % 100) == 0):
                            move = board.Action.LEFT
                        elif (self.board.getPieceStrength((location + 2) % 100) == 0):
                            move = board.Action.RIGHT
                        #elif (self.board.getPieceStrength((location - 3) % 100) == 0):
                        #    move = board.Action.JUMP_LEFT
                        #    arg=3
                        #elif (self.board.getPieceStrength((location + 3) % 100) == 0):
                        #    move = board.Action.JUMP_RIGHT
                        #    arg=3
                        else:
                            move = board.Action.STAY
                    
            else:
                move = board.Action.STAY

            if (move == board.Action.JUMP_LEFT
                    or move == board.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            moves.append([location, str(move), arg])

        return moves
