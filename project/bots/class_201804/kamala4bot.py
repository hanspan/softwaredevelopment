"""
This bot has two moves, which it cycles through for each piece.
It does surprisingly well.
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """

    return KamalaBotParity()


class KamalaBotParity(BotBase.BotBase):
    """
    This Bot just cycles through two possibilities!
    """

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        Create the moves list - every piece does the same thing, depending
        only on what turn we are on.
        """
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # grow or split depending on what move we are on.
            currTurn += 1  # change parity
            move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]

            arg = 1
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge="debug"):
        """
        This is the hash of 'sarahBot' and the correct line numbers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("df0e55b6d9dae2963eb6016fe1806537"
                    "c63c973c386748786a155538c345941a")
        elif challenge == "database":
            return ("7a32e1b3fd01db82331cb4ac2f5cf5c5"
                    "0d571247d20510ae672e77aae3821930")
        else:
            return "Default Bot Signature :("
