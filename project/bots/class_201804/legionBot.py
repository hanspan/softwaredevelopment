"""
This is LegionBot, which builds a configurable amount of strength, 
and then expands into the world.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
ExpansionRate = 3  # Determines whether we expand or build strength
                   # Three was callabrated to beat SarahBot =P

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return LegionBot()


class LegionBot(BotBase.BotBase):
    """This Bot just makes random moves all the time!"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        In LegionBot, we ignore the opponent, but spread like a virus given
        a certain minimum strength (which is configurable).
        The Legion will take over the world!!!!
        """

        pieces = self.board.findMyPieces()
        moves = []            

        #We spread out if we're strong enough, or we bide our time...
        for location in pieces:

            if self.board.getPieceStrength(location) < ExpansionRate:
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.SPLIT

            moves.append([location, str(move), 1])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is not the hash of 'legionBot' and the correct answers for
        the debug and database challenges. (Because the challenge hasn't happened yet).
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
