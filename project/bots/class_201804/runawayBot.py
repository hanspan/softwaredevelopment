"""
This bot is going to be a wuss, and run away from nearby bigger opponent pieces
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return runawayBot()



    


class runawayBot(BotBase.BotBase):
    
    """Runaway from stronger opponents"""
    
    def getDistance(self,location1,location2):
        """ get the smallest distance between the two pieces """
        ''' would like to also return which direction it is'''
        dist=min((location1-location2)%self.board.boardSize,(location2-location1)%self.board.boardSize)
     
        return dist
        

    def getDirection(self,location,target):
        ''' which direction to travel to get to target soonest'''
        if ((location-target)%self.board.boardSize > (target-location)%self.board.boardSize):
            return 'RIGHT'
        else:
            return 'LEFT'
        
        
    def printMoveToMake(self,move):
        """ Print this  move we are about to make"""
        #print(move)
        #print(self.board.getPieceStrength(move[0]))
        pass
                
    def canJumpToGrowthTile(self,location,growthTilesToOccupy):
        """ Is there a GrowthTile nearby that we can jump to?
        """
        for growthTile in growthTilesToOccupy:
            if (0 < self.getDistance(location, growthTile) < (self.board.getPieceStrength(location) - 1)):
                return True
        return False
            
    def jumpToGrowthTile(self,location,growthTilesToOccupy):
        """ jump to the first growth tile available and not occupied by us and finish with this location
        """
        for growthTile in growthTilesToOccupy:
            if (0 < self.getDistance(location, growthTile) < (self.board.getPieceStrength(location) - 1)):
        #print("can move towards growthTile",growthTile,location,strength)
                if (location > growthTile): 
                    move = GameBoard.Action.JUMP_LEFT
                    arg = location - growthTile               
                else:
                    move = GameBoard.Action.JUMP_RIGHT
                    arg = growthTile - location
        #print("moved to growthtile ",growthTile,"with move",move)
        return move,arg,growthTile
        
    def moveGrowthTile(self,location):
        ''' We are on a growthTile ... decide upon the move
        Split if strong enough and odd, or getting too strong. Otherwise stay'''
        strength=self.board.getPieceStrength(location)
        if (strength>10)and(strength&1):#odd
            move=GameBoard.Action.SPLIT
        elif (strength>20):
            move=GameBoard.Action.SPLIT
        else:
            move=GameBoard.Action.STAY
        print("GT",location,strength,move)
        return move
        
    def moveNormalTile(self,location,currTurn):
        ''' We are not on a growthTile ... decide upon the move'''
        strength=self.board.getPieceStrength(location)
        pieces = self.board.findMyPieces()

        # if near a growth tile jump away from it
        if (currTurn<40)and(strength&1)and(strength>2):
            move=GameBoard.Action.SPLIT
        elif (strength>4):
            #move = [GameBoard.Action.RIGHT, GameBoard.Action.STAY][currTurn % 2]
            move=GameBoard.Action.SPLIT
            
        else:
            move=GameBoard.Action.STAY
            #move = random.choice(list(GameBoard.Action))
        print("NT",location,strength,move)

        return move

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        
        """

        #print("\n new set of moves ")

        pieces = self.board.findMyPieces()
        opponent_pieces=self.board.findOpponentPieces()
        moves = []
        growthTilesToOccupy=[]

        # keep track of any growthTiles that we don't occupy, and therefore could try to
        for tile in self.board.growthTiles:
            if tile not in pieces:
                growthTilesToOccupy.append(tile)
        
        # Should check to see if opponent occupies it, and is stronger than us....later
                
                

        for location in pieces:
            arg=1  # default jump
            
            
            if(self.canJumpToGrowthTile(location,growthTilesToOccupy)):
                #print("going to call jump to growth tile",growthTilesToOccupy)
                move,arg,growthTile = self.jumpToGrowthTile(location,growthTilesToOccupy)
                #print("returned from call")
                growthTilesToOccupy.remove(growthTile)
                #print("growthtiles to occupy is now ",growthTilesToOccupy)
                moves.append([location, str(move), arg])
                nextmove=(location,str(move),arg)
                #print("jumping to a growthTile")
                self.printMoveToMake(nextmove)
                moves.append([location, str(move), arg])
                continue
                
            
            
            # don't move a piece until is has a strength of 4.
            strength=self.board.getPieceStrength(location)
            if (strength<4):
                move = GameBoard.Action.STAY
            elif (location in self.board.growthTiles):
                move = self.moveGrowthTile(location)
            else:
                move = self.moveNormalTile(location,currTurn)
            # sort out argument for normal jumps
            if (move == GameBoard.Action.JUMP_LEFT or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)

            
            # leaving above in place (not necessarily sensible) let's see if we need to run away instead!
            # run away if within max jumping distance
            # only running away from first one we find!
            '''for opp in opponent_pieces:
                if self.getDistance(opp,location)<self.board.maxTileStrength:
                    # run the opposite way - needs working on for 99-0 join!
                    if (opp>location):
                        move=GameBoard.Action.JUMP_LEFT
                    else:
                        move=GameBoard.Action.JUMP_RIGHT
                    arg=self.board.getPieceStrength(location) 
                    break
             '''   
                        
            #print(move)
            
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            nextmove=(location,str(move),arg)
            self.printMoveToMake(nextmove)
            moves.append([location, str(move), arg])
            

        return moves
    


    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
