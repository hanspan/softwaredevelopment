import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard # Utilities for working with the game.
# import bot_scorer



def getBot():
    """This function needs to return an instance of this Bot's class."""
    
    return jackBotSep()



class jackBotSep(BotBase.BotBase):
    
    def __init__(self):
        self.split_indices = set([31,39,47,55,63,71,79]) # odd to fan when everything else is staying

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        essentially this strategy copies SarahBot but fans out the left most and right most tiles at various intervals to hopefully capture more space - by luck I've managed to just do better.

        todo - work out a better set of fan and jump indicies. 
        todo - come up with my own strategy
        todo - what to do with growth tiles
        todo - take into account other team
        """

        pieces = self.board.findMyPieces()
        moves = []
        
        for i, location in enumerate(pieces):
            arg = 1
          
            if currTurn in self.split_indices:
                move = self.splitMoves(i, location, arg, len(pieces)-1)
                moves.append(move)
            
            else:
                move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]
                moves.append([location, str(move), arg])
                
        return moves
    
   
    def splitMoves(self, index, location, arg2,lenPieces):
        """what to do when fanning left most and right most piece outwards"""
        
        if index == 0:
            stren = int(self.board.getPieceStrength(location))
            if (stren < 2):
                move = GameBoard.Action.STAY
            elif (stren < 3):
                move = GameBoard.Action.LEFT
            else:
                arg2 = stren-1
                move = GameBoard.Action.JUMP_LEFT
           
        elif index == (lenPieces):
            stren = int(self.board.getPieceStrength(location))
            if (stren < 2):
                move = GameBoard.Action.STAY
            elif (stren < 3):
                move = GameBoard.Action.RIGHT
            else:
                arg2 = stren-1
                move = GameBoard.Action.JUMP_RIGHT
            
        else:
            move = GameBoard.Action.STAY
        return [location, str(move), arg2]
    
    

    
    
    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"


