"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
SUFFICIENT_SIZE = 12
EARLY_GAME = 80

def getBot():
    """This function needs to return an instance of this Bot's class."""

    return SumoBot()


class SumoBot(BotBase.BotBase):
    """This is jameso2's bot"""

    def generateMoves(self, currTurn, maxTurns): 
        """SumoBot will spread out quickly at the start, and then takes
        its time to grow in place to SUFFICIENT_SIZE, and then splits 
        when it reaches a certain amount.
        """
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            arg = 1
            if currTurn < EARLY_GAME:
                move = self.earlyGameMoves(currTurn)  # Follow early game tactics
            else:
                move = self.lateGameMoves(location)   # Follow late game tactics

            move = self.sanityCheck(move, location)   # Do a final check for terrible moves
                
            # Store the move.
            moves.append([location, str(move), arg])
            
        return moves

    def earlyGameMoves(self, currTurn):
        # Alternate stay and split in order to spread out to cover a large area
        return [GameBoard.Action.STAY, GameBoard.Action.SPLIT][currTurn % 2]

    def lateGameMoves(self, location):
        # To consume sarahBot we need more power, so STAY and grow to
        # SUFFICIENT_SIZE (can be changed at the top of this file), then split
        # 8 was not big enough, 12 was (to beat sarahBot)
        # TODO: in the future, could move rather than split? But which way?
        if self.board.getPieceStrength(location) < SUFFICIENT_SIZE:
            move = GameBoard.Action.STAY
        else:
            move = GameBoard.Action.SPLIT
        return move
        
    def sanityCheck(self, move, location):
        # SPLITTING when at strength 1 does nothing, so always better to STAY
        if move == GameBoard.Action.SPLIT and self.board.getPieceStrength(location) == 1:
            return GameBoard.Action.STAY
        else:
            return move
        

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
