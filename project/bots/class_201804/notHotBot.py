"""
This is a fairly stupid bot, which
- tries to maintain a stronghold near where it starts
- wanders towards opponent pieces that it thinks it can beat
- makes vague attempts to expand its empire

"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return notHotBot()


def directionFrom(myloc, otherloc):
    """Find the (shorter) direction from one location to another."""
    if myloc > otherloc and myloc-otherloc < 50:
        return "LEFT"
    if myloc < otherloc and otherloc-myloc > 50:
        return "LEFT"
    return "RIGHT"


def distanceBetween(myloc, otherloc):
    """Find the distance between two locations."""
    rawdist = abs(myloc-otherloc)
    if rawdist < 50:
        return rawdist
    else:
        return 100-rawdist


def movePick(mystrength, dist_start, side_start):
    """Return the chosen move when close to start or far from foes."""
    if dist_start < 3:
        if mystrength >= 9:
            return "SPLIT"
        else:
            return "STAY"
    else:
        if mystrength >= 5:
            return "SPLIT"
        else:
            if random.random() < 0.5:
                return "STAY"
            else:
                return side_start


def findNearestEnemy(myloc, op_pieces):
    """Find the nearest vulnerable enemy piece."""
    mindist = 1000
    best = -1
    direction = "UNKNOWN"
    for loc in op_pieces:
        dist = distanceBetween(myloc, loc)
        if dist < mindist:
            mindist = dist
            best = loc
            direction = directionFrom(myloc, loc)
    return (best, direction, mindist)


class notHotBot(BotBase.BotBase):
    """This Bot moves as defined below, and is not very good at its task."""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        Locate all of our pieces on the board, and then make a move for
        each one.

        This bot tries the following strategy:
        - If it is near the initial location, it will stay (increasing
           strength) up to strength = 9, then split.
        - Otherwise, if the nearest enemy piece is within 4 spaces, it
           determines if the enemy is beatable:
        --- If so, it moves towards it.
        --- If not, it stays still to build strength.
        - Otherwise, if strength is at least 5, it splits.
        - Otherwise, it moves away from initial location with probability 1/2
        """

        pieces = self.board.findMyPieces()
        if currTurn == 1:
            self.startPos = pieces[0]

        op_pieces = self.board.findOpponentPieces()
        moves = []

        for location in pieces:
            mystrength = self.board.getPieceStrength(location)

            # Enemy stats = strength, direction and distance
            #                from enemy
            enemy_stats = findNearestEnemy(location, op_pieces)
            opstrength = self.board.getPieceStrength(enemy_stats[0])

            dist_start = distanceBetween(location, self.startPos)
            side_start = directionFrom(self.startPos, location)

            # Close to initial location, or far from enemies
            if dist_start < 3 or enemy_stats[2] >= 5:
                choice = movePick(mystrength, dist_start, side_start)

            # Close to enemy square
            else:
                # Enemy is weak, so advance
                if mystrength > opstrength + enemy_stats[2]:
                    choice = enemy_stats[1]
                # Enemy is strong, so stay still and build up strength
                else:
                    choice = "STAY"

            if choice == "SPLIT":
                move = GameBoard.Action.SPLIT
            elif choice == "STAY":
                move = GameBoard.Action.STAY
            elif choice == "RIGHT":
                move = GameBoard.Action.RIGHT
            elif choice == "LEFT":
                move = GameBoard.Action.LEFT

            arg = 1

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("9e88fede01ae33d8d7678a822d340912"
                    "bdb2584a155a4e09eff0d23a298e694b")
        elif challenge == "database":
            return ("465b30cb80797103fc9404b94443d90f"
                    "3026b54e40a5f601d3985c436e99f876")
        else:
            return "Default bot signature"
