
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import mmmBot      # The Bot being tested.
import BotTestCase


class ca_simpleRandBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.
        
        self.bot = mmmBot.CaBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.
        
        self.assertNotEqual(None, mmmBot.getBot())

    def testKnownMoveStay(self):
        # Test one instance such that the strength is small (2), so the piece should stay.
        
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForCaSimpleRandBot_1.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownMoveSplit(self):
        # Testing that the expected output comes from a move that should result in a split.

        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForCaSimpleRandBot_2.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1, results[0][2])

    def testRandomMove(self):
        # Testing one instance such that for the appropriate conditions (large even strength), the move is selected from the appropriate list.
        allowedMoves = ["Stay", "Left", "Right"]
        
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForCaSimpleRandBot_3.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(1, results[0][2])
        self.assertTrue(results[0][1] in allowedMoves)
        
    def testBadInput(self):
        # Test if inputting integer state acts appropriately
        state = 0
        resultsRaw = self.bot.makeMoves(state)    


    def testSignature(self):
        # Check that the expected signature is returned.

        sig = mmmBot.CaBot.getSignature('debug')
        self.assertTrue(sig.endswith("68c799a7b3d1"))
        sig = mmmBot.CaBot.getSignature('database')
        self.assertTrue(sig.endswith("421d7539167e"))
        sig = mmmBot.CaBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
