"""
All about that bot
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return SexyBot()


def reduceListMod(aList, modulus):
    """ Reduce every value in a list (or any iterable) modulo a modulus """
    return [(i % modulus) for i in aList]


class SexyBot(BotBase.BotBase):
    """ Deterministic sexy bot. This bot cycles through a list of actions.
    Each action either specifies how all the pieces move based on where
    the pieces are in relation to the starting and ending positions.
    """

    def __init__(self):
        """ Fix parameters not dependent on board state"""
        self.homeSize = 3

    def setHomeRegion(self):
        """ Make a list of positions close to home """
        lowerBound = self.myStart - self.homeSize
        upperBound = self.myStart + self.homeSize + 1
        homeRegion = reduceListMod(range(lowerBound, upperBound),
                                   self.board.boardSize)
        return homeRegion

    def setRightRegion(self):
        """ Make a list of positions to the right of home """
        lowerBound = self.myStart + self.homeSize + 1
        upperBound = self.oppStart
        if self.oppStart < self.myStart:
            upperBound += self.board.boardSize
        rightRegion = reduceListMod(range(lowerBound, upperBound),
                                    self.board.boardSize)
        return rightRegion

    def setNeutralRegion(self):
        """ Make a list of positions at opponents home """
        neutralRegion = [self.oppStart]
        return neutralRegion

    def setLeftRegion(self):
        """ Make a list of positions to the left of home """
        lowerBound = self.oppStart + 1
        upperBound = self.myStart - self.homeSize
        if self.myStart < self.oppStart:
            upperBound += self.board.boardSize
        leftRegion = reduceListMod(range(lowerBound, upperBound),
                                   self.board.boardSize)
        return leftRegion

    def setInitialValues(self, pieces):
        """ Initialise the regions """
        self.myStart = pieces[0]
        self.oppStart = self.board.findOpponentPieces()[0]
        self.homeRegion = self.setHomeRegion()
        self.rightRegion = self.setRightRegion()
        self.neutralRegion = self.setNeutralRegion()
        self.leftRegion = self.setLeftRegion()

    def executeMove(self, location, globalAction, currTurn):
        """ Execute a given action for a particular piece """
        if globalAction == 'Twist':
            if location in self.homeRegion:
                move = GameBoard.Action.SPLIT
            elif location in self.neutralRegion:
                move = GameBoard.Action.STAY
            elif location in self.leftRegion:
                move = GameBoard.Action.LEFT
            else:
                move = GameBoard.Action.RIGHT
        else:
            move = GameBoard.Action.STAY
        return move

    def generateMoves(self, currTurn, maxTurns):
        """ Locate all of our pieces on the board
        and then make a move for each one """

        # Initialise the board regions
        pieces = self.board.findMyPieces()
        if currTurn == 1:
            self.setInitialValues(pieces)

        # Action pieces according to the chosen action
        globalAction = ['Stick', 'Twist'][currTurn % 2]
        moves = []
        for location in pieces:
            move = self.executeMove(location, globalAction, currTurn)
            arg = 1
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
