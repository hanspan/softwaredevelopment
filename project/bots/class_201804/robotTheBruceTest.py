"""
Unit tests for robotTheBruce
"""
import os
import json           # Process JSON to/from strings, etc.
from unittest.mock import MagicMock
from collections import namedtuple
import GameBoard      # Information on game board properties.
import robotTheBruce  # Bot being tested.

import BotTestCase


class RobotTheBruceTestCase(BotTestCase.BotTestCase):
    """
    All the unit tests for robotTheBruce
    """

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = robotTheBruce.RobotTheBruce()
        self.bot.setup(self.getGameConfig())
        self.pieceInfo = namedtuple('pieceInfo', ['currTurn', 'location',
                                                  'strength', 'direction',
                                                  'growthDir'])

    def testGetBot(self):
        """
        Make sure a bot is returned from the getBot() function.
        """

        self.assertNotEqual(None, robotTheBruce.getBot())

    def testKnownMoveEarlyGame(self):
        """
        This is a "last known good state" test. By itself, it doesn't
        really indicate any problems. However, it establishes a known
        input/output matching set, and will highlight if something
        changes that result.

        Read in a standard board state, and see what moves result.
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownMoveLateGame(self):
        """
        Read in a bespoke late-game board state, and see what move results
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'testLateGameBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(39, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
        self.assertEqual(1, results[0][2])

    def testGenerateMoves(self):
        """
        Tests the generateMoves function chooses early or late game
        Note: have not mocked out distToNearestOpponent as it's
        a simplr function
        """
        self.bot.board.findOpponentPieces = MagicMock(return_value=[75])
        self.bot.board.getPieceStrength = MagicMock(return_value=10)
        self.bot.earlyGameMoves = MagicMock()
        self.bot.lateGameMoves = MagicMock()
        with self.subTest("early game"):
            testPieceInfoeceInfo = self.pieceInfo(2, 5, 10, 'right', 'right')
            self.bot.board.findtestPieceInfoeces = MagicMock(return_value=[25])
            self.bot.generateMoves(2, 1000)
            self.bot.earlyGameMoves.assert_called_with(testPieceInfoeceInfo)
        with self.subTest("late game"):
            testPieceInfoeceInfo = self.pieceInfo(2, 74, 10, 'left', 'right')
            self.bot.board.findtestPieceInfoeces = MagicMock(return_value=[74])
            self.bot.generateMoves(2, 1000)
            self.bot.lateGameMoves.assert_called_with(testPieceInfoeceInfo)

    def testFindDirectionOfTravel(self):
        """
        Test to see if we're left or right of our starting location
        """
        direction = self.bot.findDirectionOfTravel(0, 49)
        self.assertEqual(direction, "right")
        direction = self.bot.findDirectionOfTravel(0, 51)
        self.assertEqual(direction, "left")
        direction = self.bot.findDirectionOfTravel(70, 19)
        self.assertEqual(direction, "right")
        direction = self.bot.findDirectionOfTravel(70, 21)
        self.assertEqual(direction, "left")

    def testFindNearestGrowthSquareDir(self):
        """
        Test to find the nearest growth square direction
        Decided not to mock findDirectionOfTravel as it's a simple function
        """
        direction = self.bot.nearestGrowthSquare(49)
        self.assertEqual(direction, "left")
        direction = self.bot.nearestGrowthSquare(50)
        self.assertEqual(direction, "right")

    def testGrowOnGrowthSquare(self):
        """
        Test for tactic of how to grow on growth square
        """
        with self.subTest("stay if less than sufficient health"):
            move = self.bot.growOnGrowthSquare(13, "right")
            self.assertEqual(move, GameBoard.Action.STAY)
        with self.subTest("split if at suffient health"):
            move = self.bot.growOnGrowthSquare(25, "right")
            self.assertEqual(move, GameBoard.Action.RIGHT)
            move = self.bot.growOnGrowthSquare(25, "left")
            self.assertEqual(move, GameBoard.Action.LEFT)

    def testGrowthSquareTactics(self):
        """
        Test correct tactics when heading for a growth square
        """
        self.bot.startTile = 25
        growthSquares = robotTheBruce.GROWTH_SQUARES

        def testPieceInfo(a, b, c):
            """
            currTurn = a
            location = b
            strength = 1
            direction = c
            growthDir = NA
            """
            return self.pieceInfo(a, b, 1, c, "NA")

        with self.subTest("not there yet"):
            move = self.bot.growthSquareTactics(
                testPieceInfo(5, growthSquares[0]-1, "right"))
            self.assertEqual(move, GameBoard.Action.RIGHT)
            move = self.bot.growthSquareTactics(
                testPieceInfo(5, growthSquares[0]+1, "left"))
            self.assertEqual(move, GameBoard.Action.LEFT)
        with self.subTest("gone past it"):
            move = self.bot.growthSquareTactics(
                testPieceInfo(6, growthSquares[0]+1, "right"))
            self.assertEqual(move, GameBoard.Action.STAY)
            move = self.bot.growthSquareTactics(
                testPieceInfo(7, growthSquares[0]-1, "left"))
            self.assertEqual(move, GameBoard.Action.SPLIT)
        with self.subTest("reached it"):
            self.bot.growOnGrowthSquare = MagicMock()
            move = self.bot.growthSquareTactics(
                testPieceInfo(5, growthSquares[0], "right"))
            self.bot.growOnGrowthSquare.assert_called_with(1, "right")

    def testEarlyGameMoves(self):
        """
        Test early game tactics
        One side (left or right) will race to the nearest growth square
        The other will alternate stay and move
        """
        with self.subTest("first turn stay"):
            move = self.bot.earlyGameMoves(self.pieceInfo(1, 0, 0, 0, 0))
            self.assertEqual(move, GameBoard.Action.STAY)
        with self.subTest("second turn split"):
            move = self.bot.earlyGameMoves(self.pieceInfo(2, 0, 0, 0, 0))
            self.assertEqual(move, GameBoard.Action.SPLIT)
        with self.subTest("racing for a growth square, not there yet"):
            testPieceInfo = self.pieceInfo(3, 1, 1, "right", "right")
            self.bot.growthSquareTactics = MagicMock()
            move = self.bot.earlyGameMoves(self.pieceInfo(3, 1, 1,
                                                          "right", "right"))
            self.bot.growthSquareTactics.assert_called_with(testPieceInfo)
        with self.subTest("if not racing for growth, alternate stay and move"):
            move = self.bot.earlyGameMoves(self.pieceInfo(3, 0, 1,
                                                          "right", "left"))
            self.assertEqual(move, GameBoard.Action.SPLIT)
            move = self.bot.earlyGameMoves(self.pieceInfo(4, 0, 1,
                                                          "left", "right"))
            self.assertEqual(move, GameBoard.Action.STAY)

    def testMustMove(self):
        """
        Test that if the number of moves we have left is less than or equal
        to the space we have to cover, go left or right appropriately
        """
        currTurn = 10
        targetTurn = 20
        targetLocation = 32

        def testPieceInfo(a):
            """
            currTurn = currTurn
            location = a
            strength = NA
            direction = NA
            growthDir = NA
            """
            return self.pieceInfo(currTurn, a, "NA", "NA", "NA")

        with self.subTest("must move left"):
            # If we are n or more spaces to the right of the target square,
            # with n moves to get there
            move = self.bot.mustMove(testPieceInfo(43),
                                     targetTurn, targetLocation)
            self.assertEqual(move, GameBoard.Action.LEFT)
            move = self.bot.mustMove(testPieceInfo(42),
                                     targetTurn, targetLocation)
            self.assertEqual(move, GameBoard.Action.LEFT)
            move = self.bot.mustMove(testPieceInfo(41),
                                     targetTurn, targetLocation)
            self.assertEqual(move, None)
        with self.subTest("must move right"):
            # If we are n or more spaces to the left of the target square,
            # with n moves to get there
            move = self.bot.mustMove(testPieceInfo(21),
                                     targetTurn, targetLocation)
            self.assertEqual(move, GameBoard.Action.RIGHT)
            move = self.bot.mustMove(testPieceInfo(22),
                                     targetTurn, targetLocation)
            self.assertEqual(move, GameBoard.Action.RIGHT)
            move = self.bot.mustMove(testPieceInfo(23),
                                     targetTurn, targetLocation)
            self.assertEqual(move, None)

    def testFocusOnOneSquare(self):
        """
        Tests that we move if we must, else split if strength > 1
        """
        mustMoveReturns = {(0, 0, 0, 0): GameBoard.Action.LEFT,
                           (1, 0, 0, 1): GameBoard.Action.RIGHT,
                           (2, 0, 0, 2): None,
                           (3, 0, 0, 2): None,
                           (3, 0, 0, 3): None,
                           (4, 0, 0, 1): GameBoard.Action.LEFT,
                           (4, 0, 0, 2): None,
                           (4, 0, 0, 3): None,
                           (4, 0, 0, 4): None}

        def testPieceInfo(a, b, c):
            """
            currTurn = a
            location = b
            strength = c
            direction = NA
            growthDir = NA
            """
            return self.pieceInfo(a, b, c, "NA", "NA")

        def side_effect(arg0, arg1, arg2):
            """
            Mocking out mustMove(currTurn, targetTurn, targetLoc, location)
            """
            return mustMoveReturns[(arg0.currTurn, arg1, arg2, arg0.location)]
        self.bot.mustMove = MagicMock(side_effect=side_effect)

        with self.subTest("must move left"):
            move = self.bot.focusOnOneSquare(testPieceInfo(0, 0, 0), 0, 0)
            self.assertEqual(move, GameBoard.Action.LEFT)
        with self.subTest("must move right"):
            move = self.bot.focusOnOneSquare(testPieceInfo(1, 1, 0), 0, 0)
            self.assertEqual(move, GameBoard.Action.RIGHT)
        with self.subTest("stay when strength = 1"):
            move = self.bot.focusOnOneSquare(testPieceInfo(2, 2, 1), 0, 0)
            self.assertEqual(move, GameBoard.Action.STAY)
        with self.subTest("split if it doesn't take us too far away"):
            move = self.bot.focusOnOneSquare(testPieceInfo(3, 3, 2), 0, 0)
            self.assertEqual(move, GameBoard.Action.SPLIT)
        with self.subTest("stay if it would take us too far away"):
            move = self.bot.focusOnOneSquare(testPieceInfo(3, 2, 2), 0, 0)
            self.assertEqual(move, GameBoard.Action.STAY)

    def testLateGameMoves(self):
        """
        Test that the correct tactic is followed in the late game
        """
        def testPieceInfo(a, b):
            """
            currTurn = NA
            location = a
            strength = b
            direction = NA
            growthDir = NA
            """
            return self.pieceInfo("NA", a, b, "NA", "NA")
        sufficientSize = robotTheBruce.SUFFICIENT_SIZE

        with self.subTest("if nearby attack decided, then attack"):
            self.bot.jumpAttack = MagicMock(return_value=(None, 1))
            self.bot.nearbyAttack = MagicMock(
                return_value=GameBoard.Action.RIGHT)
            move = self.bot.lateGameMoves(testPieceInfo(0, 1))
            self.assertEqual(move, GameBoard.Action.RIGHT)
        with self.subTest("else if jump attack decided, then jump attack"):
            self.bot.jumpAttack = MagicMock(
                return_value=(GameBoard.Action.JUMP_RIGHT, 5))
            self.bot.nearbyAttack = MagicMock(return_value=None)
            move = self.bot.lateGameMoves(testPieceInfo(0, 1))
            self.assertEqual(move, GameBoard.Action.JUMP_RIGHT)
        with self.subTest("else if sufficiently big, split"):
            self.bot.jumpAttack = MagicMock(return_value=(None, 1))
            self.bot.nearbyAttack = MagicMock(return_value=None)
            move = self.bot.lateGameMoves(testPieceInfo(0, sufficientSize))
            self.assertEqual(move, GameBoard.Action.SPLIT)
        with self.subTest("else if sufficiently big, stay"):
            self.bot.jumpAttack = MagicMock(return_value=(None, 1))
            self.bot.nearbyAttack = MagicMock(return_value=None)
            move = self.bot.lateGameMoves(testPieceInfo(0, sufficientSize-1))
            self.assertEqual(move, GameBoard.Action.STAY)

    def testJumpAttack(self):
        """
        Test that the jump attack is performed correctly
        self.bot.jumpAttack(strength, location)
        """
        def testPieceInfo(a, b):
            """
            currTurn = NA
            location = a
            strength = b
            direction = NA
            growthDir = NA
            """
            return self.pieceInfo("NA", a, b, "NA", "NA")
        self.bot.board.findOpponentPieces = MagicMock(return_value=[75])

        with self.subTest("it's just a jump to the left"):
            move, arg = self.bot.jumpAttack(testPieceInfo(78, 8))
            self.assertEqual(move, GameBoard.Action.JUMP_LEFT)
            self.assertEqual(arg, 3)
        with self.subTest("and then a step to the right"):
            move, arg = self.bot.jumpAttack(testPieceInfo(74, 5))
            self.assertEqual(move, GameBoard.Action.JUMP_RIGHT)
            self.assertEqual(arg, 1)
        with self.subTest("not enough strength"):
            move, arg = self.bot.jumpAttack(testPieceInfo(78, 5))
            self.assertIsNone(move)
            self.assertEqual(arg, 3)

    def testAttackOrRetreat(self):
        """
        Tests the decision to attack or retreat
        """
        self.bot.board.findOpponentPieces = MagicMock(return_value=[10, 12])
        pieceStrength = {8: 0, 10: 2, 12: 2, 14: 0, 16: 0}

        def side_effect(loc):
            """
            Mocking out getting piece strength of various locations
            """
            return pieceStrength[loc]
        self.bot.board.getPieceStrength = MagicMock(side_effect=side_effect)
        with self.subTest("opponents either side - attack"):
            move = self.bot.nearbyAttack(10, 12, 5)
            self.assertEqual(move, GameBoard.Action.SPLIT)
        with self.subTest("opponents either side - don't attack"):
            move = self.bot.nearbyAttack(10, 12, 3)
            self.assertEqual(move, None)
        with self.subTest("opponent on left - attack"):
            move = self.bot.nearbyAttack(12, 14, 3)
            self.assertEqual(move, GameBoard.Action.LEFT)
        with self.subTest("opponent on left - retreat"):
            move = self.bot.nearbyAttack(12, 14, 1)
            self.assertEqual(move, GameBoard.Action.RIGHT)
        with self.subTest("opponent on right - attack"):
            move = self.bot.nearbyAttack(8, 10, 3)
            self.assertEqual(move, GameBoard.Action.RIGHT)
        with self.subTest("opponent on right - retreat"):
            move = self.bot.nearbyAttack(8, 10, 1)
            self.assertEqual(move, GameBoard.Action.LEFT)
        with self.subTest("no opponent nearby"):
            move = self.bot.nearbyAttack(14, 16, 1)
            self.assertIsNone(move)

    def testSanityCheck(self):
        """
        Test that sanityCheck doesn't allow through a split when weight is 1
        """
        with self.subTest("sanityCheck fails"):
            move = self.bot.sanityCheck(GameBoard.Action.SPLIT,
                                        self.pieceInfo("NA", "NA", 1,
                                                       "NA", "NA"))
            self.assertEqual(move, GameBoard.Action.STAY)
        with self.subTest("sanityCheck passes"):
            move = self.bot.sanityCheck(GameBoard.Action.SPLIT,
                                        self.pieceInfo("NA", "NA", 2,
                                                       "NA", "NA"))
            self.assertEqual(move, GameBoard.Action.SPLIT)

    def testSignature(self):
        """
        Check that the expected signature is returned.
        """
        sig = robotTheBruce.RobotTheBruce.getSignature('debug')
        self.assertTrue(sig.endswith("413a7dc292e86"))
        sig = robotTheBruce.RobotTheBruce.getSignature('database')
        self.assertTrue(sig.endswith("155b74f3b4a6"))
        sig = robotTheBruce.RobotTheBruce.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
