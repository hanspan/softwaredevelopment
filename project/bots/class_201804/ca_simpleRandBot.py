"""
Strategy: for every piece:
If strength small, stay.  Otherwise:
If odd strength, split.
If even stength, then select stay, move left or right at random.
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

minStrength = 5

def getBot():
    """This function needs to return an instance of this Bot's class."""
    return caBot()


class caBot(BotBase.BotBase):
    """
    Uses generateMoves to pick move. 
    """

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """
        If odd strength, split.
        if even then stay (with larger prob), move left or right (with small prob).
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            strength = self.board.getPieceStrength(location)
            arg = 1
            if strength < minStrength:
                move = GameBoard.Action.STAY
            elif strength % 2 == 1 :
                move = GameBoard.Action.SPLIT
            else:
                move = random.choice([GameBoard.Action.STAY, GameBoard.Action.STAY, GameBoard.Action.STAY, GameBoard.Action.STAY, GameBoard.Action.STAY, GameBoard.Action.JUMP_LEFT, GameBoard.Action.JUMP_RIGHT])
            moves.append([location, str(move), arg])
        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
