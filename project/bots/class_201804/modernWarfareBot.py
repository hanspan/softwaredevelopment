"""
This is ModernWarfare Bot, a fustion of several over bots.
"""
# NOTE: you need to add . to your PYTHONPATH!

# import random     Source for random numbers. Not used in musketeerBot yet.
import logging     # For debugging
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random
import bots.musketeerBot as MBot
import bots.sexyBot as SBot
import bots.IllBeBot as IBot
import json

def getBot():
    """
    This function needs to return an instance of this Bot's class.
    In this case, it returns ModernWarfareBot, and sets up the others.
    """
    S = SBot.SexyBot()
    M = MBot.MusketeerBot()
    I = IBot.IllBeBot()
    return ModernWarfareBot(M, S, I)

class ModernWarfareBot(BotBase.BotBase):
    M = None
    S = None
    I = None

    def setup(self, config):
        """Gather information on the Game Board configuration."""

        # Until Python 3.6, json.loads requires a string.
        # Exceptions are not caught here, since setup is so critical.
        configString = config.decode("utf-8")
        serverConfig = json.loads(configString)
        self.board = GameBoard.GameBoard(serverConfig)
        self.M.board = GameBoard.GameBoard(serverConfig)
        self.S.board = GameBoard.GameBoard(serverConfig)
        self.I.board = GameBoard.GameBoard(serverConfig)
        # Return the config back to the server, just to support debugging.
        return json.dumps(serverConfig)


    def __init__(self, M, S, I):
        self.M = M
        self.S = S
        self.I = I
    
    def generateMoves(self, currTurn, maxTurns):
        choice = random.randint(0, 2)
        self.M.board = self.board
        self.I.board = self.board
        self.S.board = self.board
        if currTurn == 1:
            pieces = self.board.findMyPieces()
            self.S.setInitialValues(pieces)
        if choice == 0:
            return self.M.generateMoves(currTurn, maxTurns)
        if choice == 1:
            return self.S.generateMoves(currTurn, maxTurns)
        if choice == 2:
            return self.I.generateMoves(currTurn, maxTurns)
        
    @staticmethod
    def getSignature(challenge='debug'):
        """
        Returns the default signature for the answers to a challenge.
        This is not correct!
        """
        return "Default BotBase sig"

    
