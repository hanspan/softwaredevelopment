"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return TopToBotTom()


class TopToBotTom(BotBase.BotBase):
    """ Tom's random-ish bot"""

    # Function which reduces every value in a list modulo a modulus    
    def reduceListMod(self, aList, modulus):
        return [(i % modulus) for i in aList]
    
    # Divide the board into regions   
    def setHomeRegion(self):
        """ Make a list of positions close to home """
        lowerBound = self.myStart - self.homeSize
        upperBound = self.myStart + self.homeSize + 1
        homeRegion = self.reduceListMod( list( range(lowerBound, upperBound) ), self.board.boardSize )
        return homeRegion
    
    def setRightRegion(self):
        """ Make a list of positions to the right of home """
        lowerBound = self.myStart + self.homeSize + 1
        upperBound = self.oppStart
        if self.oppStart < self.myStart:
            upperBound += self.board.boardSize
        rightRegion = self.reduceListMod( list(range(lowerBound, upperBound)) , self.board.boardSize)
        return rightRegion
    
    def setNeutralRegion(self):
        """ Make a list of positions at opponents home """
        neutralRegion   = [self.oppStart]
        return neutralRegion
    
    def setLeftRegion(self):
        """ Make a list of positions to the left of home """
        lowerBound = self.oppStart + 1
        upperBound = self.myStart - self.homeSize        
        if self.myStart < self.oppStart:
            upperBound += self.board.boardSize
        leftRegion  = self.reduceListMod( list(range(lowerBound, upperBound)) , self.board.boardSize )
        return leftRegion
    
    def setInitialValues(self, pieces):
        """ Initialise the regions"""
        self.myStart = pieces[0]
        self.oppStart = self.board.findOpponentPieces()[0]
        self.homeSize      = 4
        self.homeRegion    = self.setHomeRegion()
        self.rightRegion   = self.setRightRegion()
        self.neutralRegion = self.setNeutralRegion()
        self.leftRegion    = self.setLeftRegion()
    

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.
        """

        pieces = self.board.findMyPieces()
        moves = []
        
        # Initialise the starting positions, and everything to the left of myStart
        if (currTurn == 1):
            self.setInitialValues(pieces)
            
            
        # Usually, all the pieces will move the same
        # Determine what this action is before looping through the pieces           
        if ( currTurn in list(range(20)) ):
            globalAction = random.choice(['AllSoldiersSplit'] * 1 + ['AllSoldiersStay'] * 2)           
        else:
            globalAction = random.choice(['ArmyWidens'] * 1 + ['AllSoldiersSplit'] * 1 + ['AllSoldiersStay'] * 2)

        # Default arg
        arg = 1
        
        # Loop over all pieces and move in unison
        for location in pieces:
            if globalAction == 'ArmyWidens':
                if location in self.homeRegion:
                    move = GameBoard.Action.SPLIT
                elif location in self.neutralRegion:
                    move = GameBoard.Action.STAY
                elif location in self.leftRegion:
                    move = GameBoard.Action.LEFT
                else:
                    move = GameBoard.Action.RIGHT
                    
            elif globalAction == 'AllSoldiersSplit':
                move = GameBoard.Action.SPLIT
            elif globalAction == 'AllSoldiersStay':
                move = GameBoard.Action.STAY
            
            moves.append([location, str(move), arg])

        return moves


    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
