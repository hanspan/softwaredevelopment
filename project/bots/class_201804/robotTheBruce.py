"""
This bot will:

Early game: will race to a Growth square if there's one nearby, and grow there
            else will alternate stay and move

Late game: if near an apponent, will decide whether to attack or retreat
           else will stay in one place until big enough, then split
"""

from collections import namedtuple
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
GROWTH_SQUARES = [40, 59]
DEFENSE_SQUARES = [10, 30, 69, 89]
SUFFICIENT_SIZE = 25
HOW_FAR_TO_JUMP = 3
CLOSE_OPPONENT = 6


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return RobotTheBruce()


class RobotTheBruce(BotBase.BotBase):
    """
    This is jameso2's bot, RobotTheBruce.
    Like Robert the Bruce in the cave with that spider, RobotTheBruce will
    take inspiration from legendary arachnids and try, try again
    """

    def __init__(self):  # add in start tile
        '''
        creates start tile
        '''
        self.startTile = 0
        self.arg = 1

    def generateMoves(self, currTurn, maxTurns):
        """
        RobotTheBruce will spread out quickly at the start, and then takes
        its time to grow in place to SUFFICIENT_SIZE, and then splits
        when it reaches a certain amount.
        """
        pieces = self.board.findMyPieces()
        moves = []

        # Get starting tile for later
        if currTurn == 1:
            self.startTile = pieces[0]

        for location in pieces:
            # Get strength, direction and growthDir
            pieceInfo = self.makePieceInformation(currTurn, location)

            # Decide what to do based on how close to an enemy we are
            nearestOpponent = self.distToNearestOpponent(location)
            if nearestOpponent < CLOSE_OPPONENT:
                move = self.lateGameMoves(pieceInfo)
            else:
                move = self.earlyGameMoves(pieceInfo)

            # Do a final check for terrible moves
            move = self.sanityCheck(move, pieceInfo)

            # Store the move.
            moves.append([location, str(move), self.arg])

        return moves

    def makePieceInformation(self, currTurn, location):
        """
        Make a namedTuple with information to pass around functions
        """
        pieceInformation = namedtuple('pieceInformation',
                                      ['currTurn', 'location', 'strength',
                                       'direction', 'growthDir'])
        # Calculate lots of useful information
        strength = self.board.getPieceStrength(location)
        direction = self.findDirectionOfTravel(self.startTile, location)
        growthDir = self.nearestGrowthSquare(self.startTile)
        pieceInfo = pieceInformation(currTurn, location, strength,
                                     direction, growthDir)
        return pieceInfo

    def distToNearestOpponent(self, location):
        """
        Finds distance to nearest opponent
        Have to consider the wrap-around the board
        """
        opps = self.board.findOpponentPieces()
        oppDists = [abs(location - x) for x in opps]
        oppDists += [100 - abs(location - x) for x in opps]
        return min(oppDists)

    @staticmethod
    def findDirectionOfTravel(startTile, location):
        """
        Sees if this person's location is left or right of the starting point,
        and returns that, as it will keep travelling that way.
        """
        direction = "NA"
        if startTile < 50:
            if startTile < location < startTile + 50:
                direction = "right"
            else:
                direction = "left"
        else:
            if startTile - 50 < location < startTile:
                direction = "left"
            else:
                direction = "right"
        return direction

    def nearestGrowthSquare(self, startTile):
        """
        Given starting location, find direction of the nearest growth square
        """
        distances = [abs(x - startTile) for x in GROWTH_SQUARES]
        closest = GROWTH_SQUARES[distances.index(min(distances))]
        direction = self.findDirectionOfTravel(startTile, closest)
        return direction

    @staticmethod
    def growOnGrowthSquare(strength, direction):
        """
        Stay on the growth square until sufficient strength, then move off
        """
        if strength < SUFFICIENT_SIZE:
            move = GameBoard.Action.STAY
        else:
            if direction == "right":
                move = GameBoard.Action.RIGHT
            else:
                move = GameBoard.Action.LEFT
        return move

    def earlyGameMoves(self, pieceInfo):
        """
        Early game tactics
        One side (left or right) will race to the nearest growth square
        The other will cycle through move, stay and split
        """
        # First split into two
        if pieceInfo.currTurn == 1:
            move = GameBoard.Action.STAY
        elif pieceInfo.currTurn == 2:
            move = GameBoard.Action.SPLIT

        # If we're racing to the nearest growth square, follow those tactics
        elif pieceInfo.direction == pieceInfo.growthDir:
            move = self.growthSquareTactics(pieceInfo)

        # If not racing to a growth square, alternate STAY and MOVE LEFT/RIGHT
        else:
            move = [GameBoard.Action.STAY,
                    GameBoard.Action.SPLIT][pieceInfo.currTurn % 2]
        return move

    def growthSquareTactics(self, pieceInfo):
        """
        If we're not there yet, move to it
        If we're on it, follow those tactics
        If we're past it, cycle through move, stay and split
        """
        currTurn = pieceInfo.currTurn
        location = pieceInfo.location
        strength = pieceInfo.strength
        direction = pieceInfo.direction
        if location not in GROWTH_SQUARES:
            # Are we past it yet?
            distances = [abs(x - self.startTile) for x in GROWTH_SQUARES]
            closestGS = GROWTH_SQUARES[distances.index(min(distances))]
            if direction == "left":
                if closestGS - location < 0:
                    move = GameBoard.Action.LEFT  # Not there yet
                else:
                    # Gone past it
                    move = [GameBoard.Action.STAY,
                            GameBoard.Action.SPLIT][currTurn % 2]
            else:
                if closestGS - location > 0:  # Not there yet
                    move = GameBoard.Action.RIGHT
                else:
                    # Gone past it
                    move = [GameBoard.Action.STAY,
                            GameBoard.Action.SPLIT][currTurn % 2]
        else:
            move = self.growOnGrowthSquare(strength, direction)
        return move

    @staticmethod
    def mustMove(pieceInfo, targetTurn, targetLoc):
        """
        If the number of moves we have left is less than or equal to
        the space we have to cover, go left or right appropriately
        """
        currTurn = pieceInfo.currTurn
        location = pieceInfo.location
        if (targetTurn - currTurn) <= (location - targetLoc):
            return GameBoard.Action.LEFT
        elif (targetTurn - currTurn) <= (targetLoc - location):
            return GameBoard.Action.RIGHT
        else:
            return None

    def focusOnOneSquare(self, pieceInfo, targetTurn, targetLoc):
        """
        A tactic to focus as much strength on one square
        within a certain number of moves
        currTurn = current turn
        targetTurn = turn by which we must have finished
        strength = current strength
        loc = current location
        targetLoc = location upon which we want a concentration of energy
        """
        currTurn = pieceInfo.currTurn
        strength = pieceInfo.strength
        location = pieceInfo.location
        if self.mustMove(pieceInfo, targetTurn, targetLoc):
            move = self.mustMove(pieceInfo, targetTurn, targetLoc)
        elif strength > 1:
            # If a split would take one half too far away, don't do it
            tooFarRight = pieceInfo._replace(currTurn=currTurn+1,
                                             location=location+1)
            tooFarLeft = pieceInfo._replace(currTurn=currTurn+1,
                                            location=location-1)
            if (self.mustMove(tooFarRight, targetTurn, targetLoc) or
                    self.mustMove(tooFarLeft, targetTurn, targetLoc)):
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.SPLIT
        else:
            move = GameBoard.Action.STAY
        return move

    def lateGameMoves(self, pieceInfo):
        """
        Late game tactics
        If there's a piece next to us we could take, attack it
        Else stay where you are until you've grown to a SUFFICIENT_SIZE
        Then split
        """
        strength = pieceInfo.strength
        location = pieceInfo.location
        por = (location + 1) % 100  # piece on right
        pol = (location - 1) % 100  # piece on left
        attack = self.nearbyAttack(por, pol, strength)
        jumpAttack, arg = self.jumpAttack(pieceInfo)
        if attack:
            move = attack
        elif jumpAttack:
            move = jumpAttack
            self.arg = arg
        else:
            if strength < SUFFICIENT_SIZE:
                move = GameBoard.Action.STAY
            else:
                move = GameBoard.Action.SPLIT
        return move

    def jumpAttack(self, pieceInfo):
        """
        Determines if we can do a winning jump attack
        """
        strength = pieceInfo.strength
        location = pieceInfo.location
        opps = self.board.findOpponentPieces()
        nearestOpp = self.distToNearestOpponent(location)
        if location - nearestOpp in opps:
            direction = "left"
        else:
            direction = "right"
        if strength > nearestOpp + HOW_FAR_TO_JUMP:
            # We have enough strength for an attack
            if direction == "left":
                move = GameBoard.Action.JUMP_LEFT
            else:
                move = GameBoard.Action.JUMP_RIGHT
        else:
            move = None
        return move, nearestOpp

    def nearbyAttack(self, pol, por, strength):
        """
        Determines whether to attack opponents
        If we can take an opponent's piece either side by splittting, do that
        Else if we're bigger than an opponent on the right, go right
        Same for on the left
        If we're smaller than an opponent on the right, retreat left
        Same the other side
        Otherwise don't attack

        pol = location of piece on left
        por = location of piece on right
        """
        opp = self.board.findOpponentPieces()
        spor = self.board.getPieceStrength(por)
        spol = self.board.getPieceStrength(pol)
        if por in opp and pol in opp:
            if strength > spor + spol:
                move = GameBoard.Action.SPLIT
            else:
                move = None
        elif por in opp:
            if strength > spor:
                move = GameBoard.Action.RIGHT
            else:
                move = GameBoard.Action.LEFT
        elif pol in opp:
            if strength > spol:
                move = GameBoard.Action.LEFT
            else:
                move = GameBoard.Action.RIGHT
        else:
            move = None
        return move

    @staticmethod
    def sanityCheck(move, pieceInfo):
        """
        Final check that we're not doing a really stupid move
        SPLITTING when at strength 1 does nothing, so always better to STAY
        """
        if move == GameBoard.Action.SPLIT and pieceInfo.strength < 2:
            return GameBoard.Action.STAY
        else:
            return move

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("4dd5e92eb6d9bd5b61d22f58e4e0fabd"
                    "334d58a509b46d51903413a7dc292e86")
        elif challenge == "database":
            return ("d6bd4d889a1a6b0c10429b633fdd431c"
                    "f90e1e1a11ad6d6ba6ee155b74f3b4a6")
        else:
            return "Default bot signature"
