"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then chooses a move for each piece.

"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return semiRandomBot()


class semiRandomBot(BotBase.BotBase):
    """This Bot makes random moves all the time according to a weighted distribution"""
    
    def getRandom(self):
        """A function to return a random value. Set up so that I can mock
        it for testing purposes
        """
        return random.random()

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        This bot selects moves according to the following distribution. 

        STAY       0.25
        LEFT       0.40
        RIGHT      0.55
        JUMP_LEFT  0.65
        JUMP_RIGHT 0.75
        SPLIT      0.90
        RANDOM     1.00
        """

        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            rand = self.getRandom()
            arg = 1
            if rand<0.25:
                move = GameBoard.Action.STAY
            elif rand<0.40:
                move = GameBoard.Action.LEFT
            elif rand<0.55:
                move = GameBoard.Action.RIGHT
            elif rand<0.65:
                move = GameBoard.Action.JUMP_LEFT
                arg = int(self.board.getPieceStrength(location) / 2)
            elif rand<0.75:
                move = GameBoard.Action.JUMP_RIGHT
                arg = int(self.board.getPieceStrength(location) / 2)
            elif rand<0.90:
                move = GameBoard.Action.SPLIT
            else:
                move = random.choice(list(GameBoard.Action))

                # Most move commands don't need an argument, but the jump ones do.
                # Just have them use up half their strength while jumping.
                arg = 1
                if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                    arg = int(self.board.getPieceStrength(location) / 2)

                    # Store the move. Note the str() entry: we need a form that
                    # can be passed to the server, and this turns the "thing of type
                    # Action" into "a string", which is easier for the JSON library
                    # to understand.
            moves.append([location, str(move), arg])

        return moves

