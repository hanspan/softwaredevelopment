
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import unittest.mock
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import IllBeBot       # The Bot being tested.
import GameBoard
import random

from BotTestCase import BotTestCase # A setup utility to generate config

_INITIAL_BOARD = "testBoardState.txt"
_NO_ENEMY_BOARD = "testBoardStateIllBeBotRedOnly.txt"
_TURN2_BOARD = "testBoardStateIllBeBotRedStrength2.txt"
_MOVE_LEFT_BOARD = "testBoardStateIllBeBotMoveLeft.txt"
_OTHER_MOVES_BOARD = "testBoardStateIllBeBotOtherMoves.txt"

class IllBeBotTestCase(BotTestCase):

    def setUp(self):
        self.bot = IllBeBot.IllBeBot()
        self.bot.setup(self.getGameConfig()) # Inherited from BotTestCase

    def _loadSampleBoard(self, basename=_INITIAL_BOARD):
        # Load the sample board from a file. Hopefully unittest ignores this.
        # Returns the state encoded as UTF-8
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, basename)
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        return state.encode("utf-8")

    def testGetBot(self):
        # Make sure we return an instance of IllBeBot
        self.assertIsInstance(IllBeBot.getBot(), IllBeBot.IllBeBot)

    def testKnownMove(self):

        # Load the initial state of the board
        board_state = self._loadSampleBoard()
        
        # Get the moves from the bot
        resultsRaw = self.bot.makeMoves(board_state)
        results = json.loads(resultsRaw)

        # Check
        self.assertIsInstance(results, list)
        self.assertEqual(1, len(results))
        self.assertEqual(25, results[0][0]) # results[0][0] is position
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1]) # First move is always to STAY
        self.assertEqual(1, results[0][2]) # results[0][2] is the arg. Probably could reasonably be anything.

    def testInvalidInput(self):

        # Create some rubbish input. It's not valid JSON.
        config = b'The future is not set. There is not fate but what we make '\
                 b'for ourselves.'

        # Down with context managers
        self.assertRaises(json.decoder.JSONDecodeError, self.bot.setup, config)


    def testFindNearestEnemy(self):
        # Test we find the nearest enemy correctly
        board_state = self._loadSampleBoard()
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        enemies = self.bot.board.findOpponentPieces()
        self.assertEqual(len(pieces), 1)

        result = IllBeBot.findNearestEnemy(self.bot.board, pieces[0])
        self.assertEqual(result, enemies[0])

    def testFindNearestEnemyError(self):
        # Test we error when there arne't any enemies to find
        board_state = self._loadSampleBoard(_NO_ENEMY_BOARD)
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        self.assertRaises(ValueError, IllBeBot.findNearestEnemy, self.bot.board,
                          pieces[0])

    def testDistToNearestEnemy(self):
        # Test we compute the correct distance at startup
        board_state = self._loadSampleBoard()
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        enemies = self.bot.board.findOpponentPieces()
        self.assertEqual(len(pieces), 1)

        result = IllBeBot.distToNearestEnemy(self.bot.board, pieces[0])
        self.assertEqual(result, 49)

    def testDistToNearestEnemyError(self):
        # Test we error when there aren't any enemies
        board_state = self._loadSampleBoard(_NO_ENEMY_BOARD)
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        self.assertRaises(ValueError, IllBeBot.distToNearestEnemy,
                          self.bot.board, pieces[0])


    def testTowardsNearestEnemyRight(self):
        # test we move right when we should
        board_state = self._loadSampleBoard()
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        enemies = self.bot.board.findOpponentPieces()
        self.assertEqual(len(pieces), 1)

        move = IllBeBot.towardsNearestEnemy(self.bot.board, pieces[0])
        self.assertEqual(move, GameBoard.Action.RIGHT)

    def testTowardsNearestEnemyLeft(self):
        # test we move left when we should
        board_state = self._loadSampleBoard(_MOVE_LEFT_BOARD)
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        enemies = self.bot.board.findOpponentPieces()
        self.assertEqual(len(pieces), 1)

        move = IllBeBot.towardsNearestEnemy(self.bot.board, pieces[0])
        self.assertEqual(move, GameBoard.Action.LEFT)

    def testChooseMoveAggressiveSplitStage(self):
        # Test we split during the "aggressive split" stage (at strength 2)
        board_state = self._loadSampleBoard(_TURN2_BOARD)
        self.bot.makeMoves(board_state)

        pieces = self.bot.board.findMyPieces()
        self.assertEqual(len(pieces), 1)

        move = self.bot._chooseMove(pieces[0])
        self.assertEqual(move, str(GameBoard.Action.SPLIT))

    @unittest.mock.patch('random.random', unittest.mock.Mock(return_value=0.5))
    def testStrongMoveLeft(self):
        # Test a strong piece moves left when its nearest enemy is to the left
        board_state = self._loadSampleBoard(_OTHER_MOVES_BOARD)

        # Get the moves from the bot
        resultsRaw = self.bot.makeMoves(board_state)
        results = json.loads(resultsRaw)

        self.assertEqual(results[0][1], str(GameBoard.Action.LEFT))

    @unittest.mock.patch('random.random', unittest.mock.Mock(return_value=0.0))
    def testStrongMoveSplit(self):
        # Test a strong piece splits when instructed by random
        board_state = self._loadSampleBoard(_OTHER_MOVES_BOARD)

        # Get the moves from the bot
        resultsRaw = self.bot.makeMoves(board_state)
        results = json.loads(resultsRaw)

        self.assertEqual(results[0][1], str(GameBoard.Action.SPLIT))

    @unittest.mock.patch('random.random', unittest.mock.Mock(return_value=0.0))
    def testStrongMoveStay(self):
        # Test a string piece stays still if spliting wouldn't be efficient
        board_state = self._loadSampleBoard(_OTHER_MOVES_BOARD)
              
        # Get the moves from the bot
        resultsRaw = self.bot.makeMoves(board_state)
        results = json.loads(resultsRaw)

        self.assertEqual(results[1][1], str(GameBoard.Action.STAY))

    @unittest.mock.patch('random.random', unittest.mock.Mock(return_value=0.0))
    def testMaximalStrengthSplit(self):
        # Test a piece that is surrounded by friendly pieces always splits if
        # it's at maximum strength
        board_state = self._loadSampleBoard(_OTHER_MOVES_BOARD)
              
        # Get the moves from the bot
        resultsRaw = self.bot.makeMoves(board_state)
        results = json.loads(resultsRaw)

        self.assertEqual(results[2][1], str(GameBoard.Action.SPLIT))

    def testDefendStrongTiles(self):
        # Test we stay on the special tiles
        board_state = self._loadSampleBoard(_OTHER_MOVES_BOARD)

        # Get the moves from the bot
        resultsRaw = self.bot.makeMoves(board_state)
        results = json.loads(resultsRaw)

        self.assertEqual(results[9][1], str(GameBoard.Action.STAY))

    def testHashSigDebug(self):
        # Test the hash signature for debug challenge
        correct = "eeacf01ca2f711b55a52064d0b885f469f6e233990726dd"\
                  "8fe73f7ebbab67cb2"
        self.assertEqual(self.bot.getSignature(), correct)
