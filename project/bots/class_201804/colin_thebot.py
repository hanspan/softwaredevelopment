"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return ColinTheBot()


class ColinTheBot(BotBase.BotBase):
    """This Bot makes variously random moves"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        Deterministic Bot jumps at where it thinks the opponent is, if
        it's advantageous to try
        """

        pieces = self.board.findMyPieces()
        foe_pieces = self.board.findOpponentPieces()
        moves = []

        for location in pieces:
            # Default move is to wait, and grow stronger
            move = GameBoard.Action.STAY
            arg = 1

            #If we're strong enough, split
            if self.board.getPieceStrength(location) > 4:
                move = GameBoard.Action.SPLIT

            # If I'm near to a foe, jump on it
            for foe_location in foe_pieces:
                foe_distance = foe_location - location

                if foe_distance in range(1,3):
                    move = GameBoard.Action.JUMP_RIGHT
                    arg = foe_distance
                if -foe_distance in range(1,3):
                    move = GameBoard.Action.JUMP_LEFT
                    arg = -foe_distance

            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
