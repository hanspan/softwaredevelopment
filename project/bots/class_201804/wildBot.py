"""
This is a wild bot. It does wild things (occasionally)
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.
import random
import datetime

# the IDE I created this bot in
IDE = 'eclipse'

# Here are some important constants. Later versions of this 
# bot will calculate them more accurately, but for now I'll
# hardcode them

COSMIC_RAYS_FLIP_BIT = random.random()< 1.4e-15
M_x_butterfly = (IDE=='emacs' and random.random()<0.00005)
BOT_BECOMES_SENTIENT = (datetime.datetime.now().year>3001 or random.random()< 1e-20)
PULL_REQUEST_IS_ACCEPTED_FIRST_TIME = False

def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """

    return wildBot()


class wildBot(BotBase.BotBase):
    """
    This bot is mostly the same as cautiousBot, but occasionally it does something really WILD!
    """

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        Most of the time wildBot alternates STAY and SPLIT. When certain rare events happen it'll get wild!
        """
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # check to see if there's a piece nearby
            oppo = self.board.findOpponentPieces()
            arg=1
            if (location+1) % self.board.boardSize in oppo or (location-1) % self.board.boardSize in oppo:
                # if there's a piece nearby then don't do anything.
                move = GameBoard.Action.STAY
                
            # if we're at strength <2 then we'd waste a turn SPLITting, so STAY instead
            elif self.board.getPieceStrength(location)<2:
                move = GameBoard.Action.STAY
            
            # Cosmic rays have flipped a bit    
            elif COSMIC_RAYS_FLIP_BIT:
                move = GameBoard.Action.JUMP_LEFT
                arg = len(random(2,self.board.getPieceStrength(location)))
            
            # emacs...    
            elif M_x_butterfly:
                move = GameBoard.Action.JUMP_RIGHT
                arg = len(random(2,self.board.getPieceStrength(location)))
            
            # uh oh...    
            elif BOT_BECOMES_SENTIENT:
                move = GameBoard.Action.JUMP_LEFT
                arg = len(random(2,self.board.getPieceStrength(location)))
            
            # Not a chance...    
            elif PULL_REQUEST_IS_ACCEPTED_FIRST_TIME:
                move = GameBoard.Action.JUMP_RIGHT
                arg = len(random(2,self.board.getPieceStrength(location))) 
                
            # otherwise we can STAY or SPLIT depending on the current turn
            else:
                arg = 1
                move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]

            
            moves.append([location, str(move), arg])

        return moves

