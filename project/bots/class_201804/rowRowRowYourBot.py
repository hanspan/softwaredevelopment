"""
... gently down the stream ...
"""

import random
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

# Bot doesn't move until it is at least this strong
MINIMUM_STRENGTH = 11

# Bot will loiter on a growth square if it is below this strength
LOITER_STRENGTH = 20

# Probability that the bot will SPLIT (in the general case)
P_SPLIT = 0.5


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """
    return RowRowRowYourBot()


class RowRowRowYourBot(BotBase.BotBase):
    """
    This bot will try and move towards enemy bots if it is strong enough,
    loiter on growth squares, or else randomly SPLIT or move towards the enemy
    """

    def flipDistance(self, distance):
        """
        Calculates the wraparound distance on the board
        """
        if distance < 0:
            return distance + self.board.boardSize
        else:
            return distance - self.board.boardSize

    def findNearestEnemy(self, location):
        """
        Calculates the nearest enemy to the bot, and the distance
        to that enemy

        @param location : the location of the bot
        @return: nearestEnemy, the location of the nearest enemy
        @return: distanceToEnemy, the distance to nearest enemy
        """
        oppo = self.board.findOpponentPieces()

        # Set up initial values for nearest enemy
        nearestEnemy = None
        distanceToEnemy = 100

        # Loop through all enemies and record if they're the nearest so far.
        for enemy in oppo:

            # Get distance to the enemy
            distance = enemy - location

            # If they're more than half a board away,
            # there will be a shorter route
            if abs(distance) > self.board.boardSize/2:
                distance = self.flipDistance(distance)

            # Check if they're nearer than the current nearest enemy
            # Store the details if they are
            if abs(distance) < abs(distanceToEnemy):
                nearestEnemy = enemy
                distanceToEnemy = distance

        return (nearestEnemy, distanceToEnemy)

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        Consider the current state of the board, and decide what move to make

        @param currTurn: the current game turn (not currently used)
        @param maxTurns: the maximum number of turns in the game
                         (not currently used)

        @return: moves: a list of moves to make
                 [[location, move, argument], ...]
        """
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:

            strength = self.board.getPieceStrength(location)

            # This piece is too weak to move, so STAY
            # This bot goes up to 11!
            if strength < MINIMUM_STRENGTH:
                move = GameBoard.Action.STAY

            # This piece is on a growth square, so STAY if its
            # not high-strength
            elif (location in self.board.growthTiles and
                  strength < LOITER_STRENGTH):
                move = GameBoard.Action.STAY

            # otherwise SPLIT or move towards the enemy!
            elif random.random() < P_SPLIT:
                move = GameBoard.Action.SPLIT
            else:
                _, enemyDistance = self.findNearestEnemy(location)

                # Determine the direction to move based on the sign of
                # the distance returned.
                if enemyDistance < 0:
                    move = GameBoard.Action.LEFT
                else:
                    move = GameBoard.Action.RIGHT

            arg = 1
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'rowRowRowYourBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("4ee92ed7905a132d97d6f5d22c8a26b9"
                    "54872c358344c02e19b7a37aa721e275")
        elif challenge == "database":
            return ("fdf5d095a182940bb5b97e55e08377c0"
                    "c0149a6a5932525d6022860c99d7ba07")

        else:
            return "Default bot signature"
