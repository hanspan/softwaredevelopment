"""
This is a basic Bot; it identifies the pieces it has on the game
board, and then picks a random move for each piece.

It should not take much to outperform this bot...
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""

    return helenBot()


class helenBot(BotBase.BotBase):
    """This Bot is now, and will always be pretty useless"""

    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one.

        
        """

        #print("\n new set of moves ")

        pieces = self.board.findMyPieces()
        #print(pieces)
        moves = []

        for location in pieces:
            arg=1  # default jump
            # don't move a piece until is has a strength of 10.
            # stay put it on a growthTile, until max strength
            strength=self.board.getPieceStrength(location)
            if (strength<10):
                move=GameBoard.Action.STAY
            elif (location in self.board.growthTiles and strength<self.board.maxTileStrength):
                move=GameBoard.Action.STAY
            else:
                move = random.choice(list(GameBoard.Action))

                if (move == GameBoard.Action.JUMP_LEFT or move == GameBoard.Action.JUMP_RIGHT):
                    arg = int(self.board.getPieceStrength(location) / 2)

            # let's move towards a growth Tile if close by.. should check not occupied
            for growthTile in self.board.growthTiles:
                if (0<abs(location-growthTile)<(strength-1)):
                    #print("can move towards growthTile",growthTile,location,strength)
                    if (location > growthTile):
                        move=GameBoard.Action.JUMP_LEFT
                        arg=location-growthTile
                        #print("moving left by ",arg,"from ",location," to ",growthTile)
                    else:
                        move=GameBoard.Action.JUMP_RIGHT
                        arg=growthTile-location
                        #print("moving right by ",arg,"from ",location," to ",growthTile)
            

            # Most move commands don't need an argument, but the jump ones do.
            # Just have them use up half their strength while jumping.
            '''arg = 1
            if (move == GameBoard.Action.JUMP_LEFT
                    or move == GameBoard.Action.JUMP_RIGHT):
                arg = int(self.board.getPieceStrength(location) / 2)
'''
            # Store the move. Note the str() entry: we need a form that
            # can be passed to the server, and this turns the "thing of type
            # Action" into "a string", which is easier for the JSON library
            # to understand.
            moves.append([location, str(move), arg])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
