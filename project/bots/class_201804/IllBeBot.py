"""
Random bot should split or jump (in either case halving its strength) half the
time, so the probabliity of reaching high strength shouldn't be very high. So I
wonder what happens if I spawn fewer, stronger bots and go around clobbering
the randomBot's spawn?
"""

import random
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.

# Minimum number of pieces before we stop aggressively splitting
_MIN_NUM_PIECES = 30
# Minimum strength of a piece before it splits/moves after we stop aggressively
# splitting
_MIN_STRENGTH = 9
# Once we've stopped aggressively splitting, this is P(split)
_P_FACTORY = 0.01
# Terminators are the pieces which are attacking other pieces
_P_TERMINATOR = 1 - _P_FACTORY


def getBot():
    """
    Return an instance of an implacable killing machine
    """
    return IllBeBot()


def distToNearestEnemy(board, piece):
    """
    Returns the (signed) distance to the nearest enemy
    """
    min_dist = None
    for enemy in board.findOpponentPieces():
        dist = abs(piece - enemy)
        if (min_dist is None) or (dist < min_dist):
            min_dist = dist
    if min_dist is None:
        raise ValueError("Can't set distance to enemy")
    return min_dist


def findNearestEnemy(board, position):
    """
    Return the position of the nearest enemy to a given position.
    """
    enemies = board.findOpponentPieces()
    nearest_enemy = None
    min_dist = None
    for enemy in enemies:
        dist = abs(position - enemy)
        if min_dist is None or dist < min_dist:
            nearest_enemy = enemy
            min_dist = dist
    if nearest_enemy is None:
        raise ValueError("Can't find the nearest enemy. Are you sure there are"
                         "any enemies?")
    return nearest_enemy


def towardsNearestEnemy(board, position):
    """
    Return a move (right or left) to take us towards the nearest enemy. Note
    that we should never be on top of an enemy when deciding our move.
    """
    nearest_enemy = findNearestEnemy(board, position)
    if nearest_enemy > position:
        return GameBoard.Action.RIGHT
    else:
        return GameBoard.Action.LEFT


def smartSplit(board, position):
    """
    Decides whether to SPLIT or STAY. If both the adjacent tiles are friendly
    and have non-zero strength, it's better to stay than split.
    Returns a GameBoard.Action object.
    """
    pieces = board.findMyPieces()
    if board.getPieceStrength(position) == board.maxTileStrength:
        return GameBoard.Action.SPLIT
    if (position-1) % board.boardSize in pieces and \
       (position+1) % board.boardSize in pieces:
        return GameBoard.Action.STAY
    return GameBoard.Action.SPLIT


class IllBeBot(BotBase.BotBase):
    """
    Random bot should split or jump (in either case halving its strength) half
    the time, so the probablity of reaching high strength shouldn't be very
    high. So I wonder what happens if I spawn fewer, stronger bots and go
    around clobbering the randomBot's spawn?
    """

    def _chooseMove(self, piece, min_pieces=_MIN_NUM_PIECES,
                    min_strength=_MIN_STRENGTH):
        """
        Choose a move for a piece and return a string representation of the
        move.
        If we don't have many pieces yet (fewer than min_pieces), we heavily
        favour splitting.
        If we have enough pieces, we follow a strategy of growing strong pieces
        (at least min_strength) which then either move towards the nearest
        enemy (i.e. become "Terminators") or split (i.e. become "Factories").
        The proportion of both is controlled by global parameters in this
        module.
        """
        strength = self.board.getPieceStrength(piece)
        num_pieces = len(self.board.findMyPieces())
        if strength > 1 and (piece in self.board.defenceTiles or
                             piece in self.board.growthTiles):
            return str(smartSplit(self.board, piece))
        if num_pieces < min_pieces:
            # Aggressively split
            if strength > 1:
                move = smartSplit(self.board, piece)
            else:
                move = GameBoard.Action.STAY
        elif strength < min_strength:
            move = GameBoard.Action.STAY
        else:
            # Choose whether to move or split
            if random.random() < _P_FACTORY:
                move = smartSplit(self.board, piece)
            else:
                move = towardsNearestEnemy(self.board, piece)

        return str(move)

    def generateMoves(self, currTurn, maxTurns):
        """
        Return a list of [position, move, argument] lists
        """

        pieces = self.board.findMyPieces()
        moves = []

        for piece in pieces:
            move = self._chooseMove(piece)
            moves.append([piece, move, 1])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        Return hash of answers for bonus points

        POINTS MEAN PRIZES
        """
        if challenge == 'debug':
            return "eeacf01ca2f711b55a52064d0b885f469f6e233990726dd8fe73f7ebb"\
                "ab67cb2"
