"""
Bot to clamp opponent in my jaws
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return YoureGonnaNeedABiggerBot()


def convertMoveID(moveID):
    """Turn a MoveID into an actual move string """
    if moveID == 0:
        return GameBoard.Action.STAY
    elif moveID == 1:
        return GameBoard.Action.LEFT
    elif moveID == 2:
        return GameBoard.Action.RIGHT
    else:
        return GameBoard.Action.SPLIT


class YoureGonnaNeedABiggerBot(BotBase.BotBase):
    """ This bot cycles through a list of actions.
    Each action either specifies how all the pieces move based on where
    the pieces are in relation to the starting and ending positions.
    """

    def __init__(self):
        """ Set default parameters """
        self.myStart = 0

    def fMagic(self, location, currTurn):
        """ Define move_id based solely on location and currTurn
        using a formula I worked out on paper.  These values
        should work well for board-sizes around 25 or larger.
        The value 7 (and 6) were chosen based on performance
        against other bots."""
        turnParity = currTurn % 2
        distHome = (self.myStart - location) % self.board.boardSize
        if currTurn < 30:
            temp = 9
        else:
            temp = 3
        regionSize = (self.board.boardSize // 2) - temp + 1
        scaleToThreeRegions = ((distHome - temp) // regionSize) % 3
        moveID = turnParity * (1 + scaleToThreeRegions)
        return convertMoveID(moveID)

    def usePiecesAndTurn(self, pieces, currTurn):
        """ Action based on location on board """
        if currTurn == 1:
            self.myStart = pieces[0]
        moves = []
        for location in pieces:
            move = self.fMagic(location, currTurn)
            moves.append([location, str(move), 1])

        return moves

    def generateMoves(self, currTurn, maxTurns):
        """ Action based on location on board """
        return self.usePiecesAndTurn(self.board.findMyPieces(), currTurn)

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return "unknownHash"
        elif challenge == "database":
            return ("0e3bbba7074680305077c67ac0dcecd4"
                    "0772f4942aad07630f15c340bdf9f5d1")
        else:
            return "Default bot signature"
