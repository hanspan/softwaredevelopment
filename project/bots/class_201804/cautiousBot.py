"""
A bot to alternate STAY and SPLIT but will check for the presence of the enemy nearby before SPLITting
"""

import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    """

    return cautiousBot()


class cautiousBot(BotBase.BotBase):
    """
    This bot will alternate STAY and SPLIT, but will not split if it is next to an enemy bot
    """

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """
        moves
        """
        pieces = self.board.findMyPieces()
        moves = []

        for location in pieces:
            # check to see if there's a piece nearby
            oppo = self.board.findOpponentPieces()
            if (location+1) % 100 in oppo or (location-1) % 100 in oppo:
                # if there's a piece nearby then don't do anything.
                move = GameBoard.Action.STAY
                
            # if we're at strength <2 then we'd waste a turn SPLITting, so STAY instead
            elif self.board.getPieceStrength(location)<2:
                move = GameBoard.Action.STAY
                
            # otherwise we can STAY or SPLIT depending on the current turn
            else:
                move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]

            arg = 1
            moves.append([location, str(move), arg])

        return moves

