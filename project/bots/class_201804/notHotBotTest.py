import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import notHotBot      # The Bot being tested.

import BotTestCase


class randomBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = notHotBot.notHotBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, notHotBot.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownSplitMove(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotSplit.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(3, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1, results[0][2])
        self.assertEqual(95, results[1][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])
        self.assertEqual(1, results[1][2])
        self.assertEqual(99, results[2][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[2][1])
        self.assertEqual(1, results[2][2])

    def testKnownStay(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotMoveL.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testBigGap(self):
        # Testing that the code copes with a "big" gap between pieces.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotBigGap.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(99, results[1][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[1][1])
        self.assertEqual(1, results[1][2])
        
    def testSignature(self):
        # Check that the expected signature is returned.

        sig = notHotBot.notHotBot.getSignature('debug')
        self.assertTrue(sig.endswith("d23a298e694b"))
        sig = notHotBot.notHotBot.getSignature('database')
        self.assertTrue(sig.endswith("5c436e99f876"))
        sig = notHotBot.notHotBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))


    def testKnownRMove(self):
        # Testing that the expected output comes from a move that
        # should include a step to the right.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotMoveR.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(3, len(results))
        self.assertEqual(3, len(results[1]))
        self.assertEqual(23, results[1][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[1][1])
        self.assertEqual(1, results[1][2])
        self.assertEqual(97, results[2][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[2][1])
        self.assertEqual(1, results[2][2])
        
    def testKnownLMove(self):
        # Testing that the expected output comes from a move that
        # should include a step to the right.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotMoveL.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[1]))
        self.assertEqual(99, results[1][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])
        self.assertEqual(1, results[1][2])

    def testKnownDist(self):
        # Testing that the expected output comes from a move that
        # should include a step to the right.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotDist.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[1]))
        self.assertEqual(1, results[1][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[1][1])
        self.assertEqual(1, results[1][2])

    def testKnownRandom(self):
        # Testing that the expected output comes from a move that
        # should include a step to the right.

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testNotHotBotRand.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[1]))
        self.assertEqual(5, results[1][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[1][1])
        self.assertEqual(1, results[1][2])
