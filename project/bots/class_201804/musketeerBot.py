"""
This is MusketeerBot, which looks for the high ground
(the defensible or growthy tiles) but more aggressively than HuscarlBot
"""

# import random     Source for random numbers. Not used in musketeerBot yet.
import logging     # For debugging
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """
    This function needs to return an instance of this Bot's class.
    In this case, it returns the MusketeerBot.
    """

    return MusketeerBot()


def findWantedTiles(defensiveSpots, growthSpots,
                    controlledPieces, opponentPieces):
    """
    This function finds the tiles we want to attack
    it adds them in priority order.
    Input: The defensiveSpots, growthSpots, our controlled pieces
           and our opponent's pieces. (All lists of ints)
    Return: A list tiles we would like to grab.
    """
    wantedTiles = []
    wantedTiles += list(set(growthSpots) - set(controlledPieces))
    wantedTiles += list(set(defensiveSpots) - set(controlledPieces))
    wantedTiles += list(set(opponentPieces) - set(wantedTiles))
    return wantedTiles


class MusketeerBot(BotBase.BotBase):

    """
    This Bot should rush for the useful tiles...!
    After it has expanded as much as it can, it then attacks opposing tiles
    looking to recover the special tiles first.
    """

    def __init__(self, level=logging.ERROR):
        """
        __init__
        sets up the logging
        """
        super().__init__()
        logging.basicConfig(level=level)
        self.logger = logging.getLogger()

    def neighbours(self, queryTile, tileList):
        """
        Find if the neighbours of a queryTile exist in a list of tiles.
        Inputs: the queryTile where we want to know about their neighbours,
                and a list of tiles.
        Returns: a tuple corresponding to
        the left adjacent and right adjacent tiles
        so if both neighbours are in the tilelist,
        it should return (True, True).
        """
        return ((queryTile - 1) % self.board.boardSize in tileList,
                (queryTile + 1) % self.board.boardSize in tileList)

    def dist(self, sourceTile, destinationTile):
        """
        Inputs: two tiles.
        Return: the distance between two points in the space.
        """
        return min((sourceTile - destinationTile) % self.board.boardSize,
                   (destinationTile - sourceTile) % self.board.boardSize)

    def goodMove(self, controlledTile):
        """
        Decide if a split is good to do.
        Right now the basis for this is, if we are very strong, and odd.
        Input: the controlledTile we want to consider splitting.
        Return: true or false, based on our criteria.
        """
        strength = self.board.getPieceStrength(controlledTile)
        return strength > self.board.maxTileStrength - 1

    def decideIfAttackIsViable(self, controlledTile,
                               destinationTile, distance):
        """
        This function decides if we can aggressively jump to a given tile.
        Inputs: Out tile, the destination tile, and the distance.
        Returns: True or false, depending on viability.
        """
        endPower = self.board.getPieceStrength(controlledTile) - distance

        return (endPower >
                max(self.board.getPieceStrength(destinationTile),
                    self.board.getPieceStrength((destinationTile + 1)
                                                % self.board.boardSize),
                    self.board.getPieceStrength((destinationTile - 1)
                                                % self.board.boardSize)))

    def lookForAgressiveMove(self, controlledTile, destinationTile, attacked):
        """
        This function looks to see if
        an aggressive move is viable.
        Inputs: the controlled tile,
        the tile we wish to attack,
        and the tiles we have already attacked.
        Output: None if not viable,
        otherwise the move and the jumpPower
        """
        if destinationTile in attacked:
            return None
        distance = self.dist(destinationTile, controlledTile)
        # If we can jump to a good tile, and survive, then do it
        if self.decideIfAttackIsViable(controlledTile, destinationTile,
                                       distance):
            jumpPower = distance
            if (distance ==
                    (destinationTile - controlledTile) % self.board.boardSize):
                aggressiveMove = GameBoard.Action.JUMP_RIGHT
            else:
                aggressiveMove = GameBoard.Action.JUMP_LEFT
            return (aggressiveMove, jumpPower)
        return None

    def analyseState(self, pieces, opponentPieces):
        """
        analyse the current state of the game
        inputs: a list of pieces
        output: prints the current state
        """
        power = sum([self.board.getPieceStrength(piece)
                     for piece in pieces])
        opppower = sum([self.board.getPieceStrength(piece)
                        for piece in opponentPieces])
        # C-style string formatting is the best.
        output = "%02i %02i %03i %03i" % (len(pieces),
                                          len(opponentPieces),
                                          power,
                                          opppower)
        return output

    def testExtendedNeighbourhood(self, opponentPieces, controlledTile):
        """
        Tests a wider neighbourhood for enemies
        Takes in opponentPieces, and the tile in question.
        Returns true or false.
        """
        return (self.neighbours(controlledTile - 2, opponentPieces)
                == (False, False)
                and self.neighbours(controlledTile + 2, opponentPieces)
                == (False, False)
                and self.neighbours(controlledTile + 1, opponentPieces)
                == (False, False)
                and self.neighbours(controlledTile - 1, opponentPieces)
                == (False, False))

    def generateMoves(self, currTurn, maxTurns):
        """
        Locate all of our pieces on the board, and then make a move for
        each one.

        In MusketeerBot, we rush for the useful tiles by jumping
        then we fire rounds across the bow, to disrupt the enemy.
        """
        # Set up some information on the tiles
        defensiveSpots = self.board.defenceTiles
        growthSpots = self.board.growthTiles
        pieces = self.board.findMyPieces()
        opponentPieces = self.board.findOpponentPieces()

        # For analysing defeats only
        self.logger.debug(self.analyseState
                          (pieces, opponentPieces))

        # Find the tiles we want to attack
        wantedTiles = findWantedTiles(defensiveSpots, growthSpots,
                                      pieces, opponentPieces)

        # We look to jump to the important pieces, after building up strength
        moves = []
        attacked = []
        for controlledTile in pieces:
            move = GameBoard.Action.STAY
            # Only split if there are no neightbours,
            # and not surrounded by friends
            jumpPower = 1
            # We stay if our bot is weak
            if self.board.getPieceStrength(controlledTile) < 3:
                move = GameBoard.Action.STAY
                moves.append([controlledTile, str(move), jumpPower])
                continue

            # If we are heading for a stalemate, but I have more pieces.
            if (maxTurns - currTurn < self.board.maxTileStrength
                    and len(pieces) > self.board.boardSize / 2):
                move = GameBoard.Action.STAY
                moves.append([controlledTile, str(move), jumpPower])
                continue

            if (self.neighbours(controlledTile, opponentPieces)
                    == (False, False)):
                if (self.neighbours(controlledTile, pieces)
                        != (True, True)):
                    if self.testExtendedNeighbourhood(opponentPieces,
                                                      controlledTile):
                        move = GameBoard.Action.SPLIT
                        moves.append([controlledTile, str(move), jumpPower])
                        continue
                    else:
                        move = GameBoard.Action.STAY
                        moves.append([controlledTile, str(move), jumpPower])
                        continue

            # We split according to the goodMove function
            if self.goodMove(controlledTile):
                move = GameBoard.Action.SPLIT
                moves.append([controlledTile, str(move), jumpPower])
                continue

            # We look for aggressive jumps
            for destinationTile in wantedTiles:
                aggressiveMove = self.lookForAgressiveMove(controlledTile,
                                                           destinationTile,
                                                           attacked)
                if aggressiveMove is not None:
                    move = aggressiveMove[0]
                    jumpPower = aggressiveMove[1]
                    attacked.append(destinationTile)
                    break

            moves.append([controlledTile, str(move), jumpPower])

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is not the hash of 'musketeerbot' and the correct answers for
        the debug and database challenges.
        (Because the challenge hasn't happened yet).
        """
        if challenge == "debug":
            return ("9ccf702339a5a696b8dc1c2f0d592218"
                    "877cde6000dc7bbd3731ab0a9d55dc52")
        elif challenge == "database":
            return ("91acb0e27fd914df9a9fcc539f57efd3"
                    "4a8a3ae03197d5b683aed36b321e7a3e")
        else:
            return "Default bot signature"
