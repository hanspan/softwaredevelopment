"""
First Bot from Hell

So far only implemented jumping to growthTiles, with some helper functions to do so
getDistance
getDirection
canJumpToGrowthTile
jumpToGrowthTile

This bot is shit!
"""

import random      # Source for random numbers.
import BotBase     # All of the basic Bot functionality.
import GameBoard   # Utilities for working with the game.


def getBot():
    """This function needs to return an instance of this Bot's class."""
    return grimreaperBot()


class grimreaperBot(BotBase.BotBase):

    def getDistance(self,location1,location2):
        """ get the smallest distance between the two pieces"""
        dist=min((location1-location2)%self.board.boardSize,(location2-location1)%self.board.boardSize)        
        return dist
        

    def getDirection(self,location,target):
        ''' which direction to travel to get to target soonest
        returns 'RIGHT' or 'LEFT' '''
        if ((location-target)%self.board.boardSize > (target-location)%self.board.boardSize):
            return 'RIGHT'
        else:
            return 'LEFT'

    def getNearestTileInSet(self,location,tileSet):
        """ Find the distance from location to the nearest tile in tileSet
        return the distance, location and direction of the nearest tile
        If two tiles are equidistant will return the first one
        
        """
        distance=self.board.boardSize/2
        nearestTile=None
        direction=None
        for tile in tileSet:
            if distance > self.getDistance(location,tile):
                distance=self.getDistance(location,tile)
                nearestTile=tile
        if nearestTile:
            direction=self.getDirection(location, nearestTile)       #if (0 < self.getDistance(location, tile) < self.board.getPieceStrength(location)):
        return distance,nearestTile,direction
            
    def jumpToTile(self,location,destination):
        """ jump to destination tile
        if not within range, stay instead
        if already occupied and we can't win, stay instead
        """
        distance=self.getDistance(location, destination)
        if distance<self.board.getPieceStrength(location):
            direction = self.getDirection(location, destination)
            if direction == 'RIGHT':
                move = GameBoard.Action.JUMP_RIGHT
            else:
                move = GameBoard.Action.JUMP_LEFT
        else:
            move=GameBoard.Action.STAY
        return move,distance
        
    
    def getNormalMove(self,location,currTurn):
        ''' return move,arg for a normal file
        if it has a strenght >2/3maxstrength then jump in random directon (strength-4)
        otherwise SPLIT or STAY if currTurn even or odd respectively'''
        if self.board.getPieceStrength(location)>2*self.board.maxTileStrength/3:
            # big jump ... random direction for now
            if random.randint(0,1):
                move = GameBoard.Action.JUMP_RIGHT
            else:
                move = GameBoard.Action.JUMP_LEFT

            arg = self.board.getPieceStrength(location)-4
        else:
            move = [GameBoard.Action.SPLIT, GameBoard.Action.STAY][currTurn % 2]
            arg = 1
        return move,arg


    def generateMoves(self, currTurn, maxTurns):     # @UnusedVariable
        """Locate all of our pieces on the board, and then make a move for
        each one. 
        """
        pieces = self.board.findMyPieces()
        moves = []
        growthTilesToOccupy=[]

        # keep track of any growthTiles that we don't occupy
        for tile in self.board.growthTiles:
            if tile not in pieces:
                growthTilesToOccupy.append(tile)
                
        for location in pieces:
            # default arg for the move
            arg=1 
            piece_strength=self.board.getPieceStrength(location)
            if piece_strength==self.board.maxTileStrength:
                print("max strength at ",location)
            # get these values ready ... this is what I was trying to avoid in refactoring :(
            distance,tile,direction = self.getNearestTileInSet(location, growthTilesToOccupy)
            if location in self.board.growthTiles:
                # default will be to split if on a growth tile if a reasonable size
                if piece_strength>1:
                    move = GameBoard.Action.SPLIT
                else:
                    move = GameBoard.Action.STAY
            # there is a growthTile available can we get to it
            elif distance in range(1,piece_strength):
                if direction == 'RIGHT':
                    move = GameBoard.Action.JUMP_RIGHT
                else:
                    move = GameBoard.Action.JUMP_LEFT
                arg=distance
                #print("jump",direction,"to growthTile",tile,"from",location)
                #remove the growth tile from list as we have jumped to it
                growthTilesToOccupy.remove(tile)
                
            else:
                move,arg = self.getNormalMove(location,currTurn)

            # Store the move. Note the str() entry
            moves.append([location, str(move), arg])
            #print(location,move,arg,piece_strength)   

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        This is the hash of 'randomBot' and the correct answers for
        the debug and database challenges.
        """
        if challenge == "debug":
            return ("e81e676a7b0b8a15746a755cc7e5e17b"
                    "fca2387ff7004c7362de68c799a7b3d1")
        elif challenge == "database":
            return ("dfcab45744a93f4057a88d57c04c4a0b"
                    "db9ca041908711c61f22421d7539167e")
        else:
            return "Default bot signature"
