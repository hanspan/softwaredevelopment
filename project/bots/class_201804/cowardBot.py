''' TODO '''

import BotBase
ENEMY_TOO_CLOSE = 3  # Should be at least 1.
BIG_ENOUGH_TO_SPLIT = 3  # Should be at least 2.


def getBot():
    ''' Returns instance of bot '''
    return CowardBot()


class CowardBot(BotBase.BotBase):
    """ CowardBot runs away if the enemy is too close
        else it splits or stays, depending on piece strength
    """
    def __init__(self):
        ''' boardSize will be updated when board is initialised. '''
        self.boardSize = None

    def generateMoves(self, currTurn, maxTurns):  # @UnusedVariable
        """ Generate moves for each piece. Calls either
            safe or flee, depending on enemy distance
        """

        if currTurn == 1:
            self.boardSize = len(self.board.tileControl)

        pieces = self.board.findMyPieces()
        enemyPieces = self.board.findOpponentPieces()

        moves = []
        for location in pieces:
            strength = self.board.getPieceStrength(location)
            closestEnemy = self.getClosestEnemy(enemyPieces, location)
            dist = self.distance(location, closestEnemy)
            if dist > ENEMY_TOO_CLOSE:
                moves.append(self.safe(location, strength))
            else:
                moves.append(self.flee(location, closestEnemy, strength))

        return moves

    def flee(self, location, closestEnemy, strength):  # @UnusedVariable
        ''' Check with direction closest enemy is and run away. '''
        dist = self.distance(location, closestEnemy)
        leftDist = location - closestEnemy % self.boardSize
        enemyOnLeft = (dist == leftDist)
        if enemyOnLeft:
            move = [location, 'Right', 1]
        else:
            move = [location, 'Left', 1]
        return move

    @staticmethod
    def safe(location, strength):
        ''' Split if big enough, else stay. '''
        if strength < BIG_ENOUGH_TO_SPLIT:
            move = [location, 'Stay', 1]
        else:
            move = [location, 'Split', 1]
        return move

    def getClosestEnemy(self, enemyPieces, location):
        ''' find the enemy closest to location '''
        def distWrap(enemyLoc):
            ''' 1 argument wrapper function '''
            return self.distance(enemyLoc, location)
        closest = min(enemyPieces, key=distWrap)
        return closest

    def distance(self, loc1, loc2):
        ''' Find distance between 2 points with modulus '''
        diff = loc1 - loc2
        modDiff = diff % self.boardSize
        return min(self.boardSize-modDiff, modDiff)

    @staticmethod
    def getSignature(challenge='debug'):
        ''' This is the hash of 'cowardBot' and the correct line numbers for
            the debug challenge.
        '''

        if challenge == "debug":
            return ("5c401d8645018ecb9759178184c99e93"
                    "a73d576af50b8c92c2f1a24d76014349")
        if challenge == "database":
            return ("70e9afc363f6d333efabb8babda33998"
                    "1d9f01f80a09b3e940e7e796865d4e00")
        return "Default bot signature"
