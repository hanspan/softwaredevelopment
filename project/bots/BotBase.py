"""
Define the properties and functions that any standard Bot should
support.
"""

import json           # Data transfer to/from the server is via JSON.
import random         # Source for random numbers.
import traceback      # Allow full stack trace to be printed when errors are caught.

class BotBase:
    """Capture all of the basic functionality for a Bot, so that
    anything that subclasses from here just has to worry about making
    moves.
    """

    def __init__(self):
        """
        Very basic constructor - most of the setup has to occur later
        due to the order of processing in the server code.
        """
        pass

    def check(self, ignored):   # @UnusedVariable
        """Sample function to demonstrate how the game board persists
        across calls; also, provides a structure useful for debugging
        errors given that this code is typically called via a C++
        server instead of interactively.
        """

        try:
            test = {'player': self.board.player,
                    'defence': self.board.defenceTiles}

        except Exception:
            traceback.print_exc()
            return

        return json.dumps(test)

    def seedBot(self, seed):
        """ Seed the random for this bot for replayablility """

        random.seed(seed)

    def makeMoves(self, board):
        """The real core of the Bot - take the current state of the board
        (which has information on which player controls which tiles, and
        what pieces are present on the board - most other information was
        passed during setup), and determine what moves are to be made.

        Valid moves are defined in GameBoard.Moves.
        """
        self.board = board

        # If any random numbers are needed, they should be based on the
        # value passed in - this way games can be "replayed."
        self.seedBot(self.board.random)

        # Other values that may be useful.
        currTurn = self.board.currTurn
        maxTurns = self.board.maxTurns

        # Pass over to a sub-function for the actual calculations.
        try:
            moves = self.generateMoves(currTurn, maxTurns)

            
        except Exception:
            print('Something went wrong during generating moves for bot {}:'.format(self.board.player))
            traceback.print_exc()
            moves = []

        return moves

    @staticmethod
    def getSignature(challenge='debug'):
        """
        Returns the default signature for the answers to a challenge.
        This is not correct!
        """
        return "Default BotBase sig"
