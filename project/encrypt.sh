#! /bin/bash

# This script encrypts an exercise solution with the
# public key for the course, which is expected to be
# in the same directory.
#
# The course instructors have a paired decryption
# program which can extract the provided files.

# Usage: encrypt.sh [file]
# Output: [file].tgz

PUBKEY="courseKey.pub"

# Check for the filename argument.
if [ $# -lt 1 ]
then
    echo "This program takes one argument, a filename to encrypt."
    exit
fi

# Preserve the filename.
FILE=$1

# Check that the file exists.
if [ ! -f $FILE ]
then
    echo "Unable to find file '$FILE'."
    exit
fi

# Check that the public key file exists.
if [ ! -f $PUBKEY ]
then
    echo "Unable to find file '$PUBKEY'."
    exit
fi


echo "Passed all checks; proceeding to encrypt."

#
# The main work routine: generate a local key, encrypt the file,
# and bundle up the results.
#

openssl rand -base64 128 -out ${FILE}.key
openssl rsautl -encrypt -inkey $PUBKEY -pubin -in ${FILE}.key -out ${FILE}.key.enc
openssl enc -aes-256-cbc -salt -in $FILE -out ${FILE}.enc -pass file:./${FILE}.key
tar czvf ${FILE}.tgz ${FILE}.enc ${FILE}.key.enc

# Cleanup temporary files.
echo "Cleaning up."
rm ${FILE}.key
rm ${FILE}.enc

echo "Complete."



