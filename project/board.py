"""
Does everything
"""

import numpy as np

class ControlColour:
    NEUTRAL = 0
    RED = 1
    BLUE = 2



class Move:
    def __init__(self, position, action, argument=1):
        self.pos = position
        self.action = action
        self.argument = argument


class Action:
    STAY = "Stay"
    LEFT = "Left"
    RIGHT = "Right"
    JUMP_LEFT = "Jump_Left"
    JUMP_RIGHT = "Jump_Right"
    SPLIT = "Split"


MOVES = [Action.STAY, Action.LEFT, Action.RIGHT, Action.JUMP_LEFT, Action.JUMP_RIGHT, Action.SPLIT]
MOVES_REDUCED = [Action.STAY, Action.LEFT, Action.RIGHT]


class Board:

    # Constants for all boards
    DEFAULT_MAX_TILE_STRENGTH = 25
    DEFAULT_DEFENSE_BONUS_PERCENTAGE = 50
    DEFAULT_GROWTH_BONUS_VALUE = 3
    NEUTRAL = 0
    RED = 1
    BLUE = 2

    def __init__(self, size):

        # We don't want the board to be smaller than 10
        self.board_size = max(10, size)

        self.currTurn = 1
        self.random = 0

        # These values can be different if the board is created using the fromConfig method
        self.MAX_TILE_STRENGTH = Board.DEFAULT_MAX_TILE_STRENGTH
        self.DEFENSE_BONUS_PERCENTAGE = Board.DEFAULT_DEFENSE_BONUS_PERCENTAGE
        self.GROWTH_BONUS_VALUE = Board.DEFAULT_GROWTH_BONUS_VALUE

        self.tile_control = np.zeros(shape=(self.board_size,), dtype=int)
        self.tile_strengths = np.zeros(shape=(self.board_size,), dtype=int)
        self.red_strengths = np.zeros(shape=(self.board_size,), dtype=int)
        self.blue_strengths = np.zeros(shape=(self.board_size,), dtype=int)

        self.defense_tiles = []
        self.growth_tiles = []

    def getBoardSize(self):
        return self.board_size

    def countPlayerControlledTiles(self, colour):
        return np.sum(self.tile_control == colour)

    def countPlayerPieces(self, colour):
        return np.sum((self.tile_strengths > 0).astype(int) * self.tile_control == colour)

    def countPlayerPieceStrengths(self, colour):
        return np.sum((self.tile_control == colour).astype(int) * self.tile_strengths)

    def calculateScore(self, colour):
        return self.countPlayerControlledTiles(colour) + \
                self.countPlayerPieces(colour) + \
                self.countPlayerPieceStrengths(colour)

    def getTileControl(self):
        return self.tile_control

    def getTileStrength(self):
        return [int(v) for v in self.tile_strengths]

    def setTileStrength(self, location, strength):
        self.tile_strengths[location] = strength

    def processMoves(self, red_moves, blue_moves):

        # Create move objects from the passed moves
        red_moves = [Move(m[0], m[1], m[2]) for m in red_moves]
        blue_moves = [Move(m[0], m[1], m[2]) for m in blue_moves]

        red_moves = self.validateMoves(Board.RED, red_moves)
        blue_moves = self.validateMoves(Board.BLUE, blue_moves)

        # Start with all 0 strengths to build up state after these moves have taken place
        red_strengths = np.zeros(shape=(self.board_size,), dtype=int)
        blue_strengths = np.zeros(shape=(self.board_size,), dtype=int)

        red_strengths = self.applyMoves(red_moves, red_strengths)
        blue_strengths = self.applyMoves(blue_moves, blue_strengths)

        self.resolveCombat(red_strengths, blue_strengths)
        red_strengths, blue_strengths = self.growPieces(Board.RED, red_moves, red_strengths, blue_strengths)
        red_strengths, blue_strengths = self.growPieces(Board.BLUE, blue_moves, red_strengths, blue_strengths)
        red_strengths = self.applyStrengthCap(red_strengths)
        blue_strengths = self.applyStrengthCap(blue_strengths)
        self.finalizeTurn(red_strengths, blue_strengths)

    def validateMoves(self, colour, moves):
        final_moves = []
        pieces = self.findPieces(colour)
        for piece in pieces:
            chosen_move = self.selectMove(piece, moves)
            final_moves.append(chosen_move)

        return final_moves

    def getPieceStrength(self, location):
        return self.tile_strengths[location]

    def findPieces(self, colour):
        """
        Given a colour, return a numpy array of the tiles where that colour has non-zero strength.
        """
        return np.where((self.tile_control == colour).astype(int) * (self.tile_strengths > 0))[0]

    def findMyPieces(self):
        return self.findPieces(self.player)

    def findOpponentPieces(self):
        if self.player == ControlColour.RED:
            return self.findPieces(ControlColour.BLUE)
        return self.findPieces(ControlColour.RED)

    # TODO: I think there are better ways of handling a Moves object
    # I think we should have a separate class to describe the Moves made by a player
    # that must be valid
    def selectMove(self, piece, moves):
        # If there is no valid moves given, just make the piece stay
        chosen_move = Move(piece, Action.STAY)
        for move in moves:
            if move.pos != piece:
                continue
            # TODO: Writing this, think (JUMP, STAY, etc) are ActionS while a Action consists of piece, action, argument
            if move.action in [Action.JUMP_RIGHT, Action.JUMP_LEFT]:
                if move.argument > self.tile_strengths[piece]:
                    return chosen_move
                if move.argument == 0:
                    return chosen_move

            chosen_move = move
        return chosen_move

    def applyMoves(self, moves, strengths):

        for move in moves:
            piece = move.pos
            if move.action == Action.STAY:
                strengths[piece] += self.tile_strengths[piece]
            elif move.action == Action.RIGHT:
                strengths[(piece + 1) % self.board_size] += self.tile_strengths[piece]
            elif move.action == Action.LEFT:
                strengths[(piece - 1) % self.board_size] += self.tile_strengths[piece]
            elif move.action == Action.JUMP_RIGHT:
                strengths[(piece + move.argument) % self.board_size] += self.tile_strengths[piece] - (move.argument - 1)
            elif move.action == Action.JUMP_LEFT:
                strengths[(piece - move.argument) % self.board_size] += self.tile_strengths[piece] - (move.argument - 1)
            else:  # (move.action must be SPLIT)
                half_strength = self.tile_strengths[piece] // 2
                strengths[(piece + 1) % self.board_size] += half_strength
                strengths[(piece - 1) % self.board_size] += half_strength
                strengths[piece] += (self.tile_strengths[piece] - 2*half_strength)

        return strengths

    # TODO: This method looks fairly vectorisable, wait for unit tests though
    def resolveCombat(self, red_strengths, blue_strengths):

        # Create the damage array to accumulate all damage from every battle before applying it anywhere
        damage = np.zeros(shape=(self.board_size,))
        for pos in range(self.board_size):
            if (red_strengths[pos] == 0) or (blue_strengths[pos] == 0):
                continue
            bonus = self.checkDefensiveTile(pos)
            red_strength = red_strengths[pos]
            if bonus and self.tile_control[pos] == Board.RED:
                red_strength *= (100 + self.DEFENSE_BONUS_PERCENTAGE) // 100

            blue_strength = blue_strengths[pos]
            if bonus and self.tile_control[pos] == Board.BLUE:
                blue_strength *= (100 + self.DEFENSE_BONUS_PERCENTAGE) // 100

            min_strength = min(red_strength, blue_strength)

            damage[pos] += min_strength

            # Splash damage to neighbours
            damage[(pos + 1) % self.board_size] += min_strength // 2
            damage[(pos - 1) % self.board_size] += min_strength // 2
            damage[(pos + 2) % self.board_size] += min_strength // 5
            damage[(pos - 2) % self.board_size] += min_strength // 5

        red_strengths, blue_strengths = self.applyDamage(damage, red_strengths, blue_strengths)
        return red_strengths, blue_strengths

    def applyDamage(self, damage, red_strengths, blue_strengths):
        for pos in range(self.board_size):
            if damage[pos] == 0:
                continue
            bonus = self.checkDefensiveTile(pos)
            red_strengths = self.applyDamageColour(Board.RED,  red_strengths,  pos, damage[pos], bonus)
            blue_strengths = self.applyDamageColour(Board.BLUE, blue_strengths, pos, damage[pos], bonus)
        return red_strengths, blue_strengths

    def applyDamageColour(self, colour, strengths, pos, damage, bonus):
        strength = strengths[pos]
        defense_bonus = self.DEFENSE_BONUS_PERCENTAGE

        if bonus and self.tile_control[pos] == colour:
            strength = strength * (100 + defense_bonus) // 100
            strength -= damage
            strength = strength * 100 // (100 + defense_bonus)
        else:
            strength -= damage
        if strength < 0:
            strength = 0

        strengths[pos] = strength
        return strengths

    def growPieces(self, colour, moves, red_strengths, blue_strengths):
        for move in moves:
            if move.action != Action.STAY:
                continue
            pos = move.pos
            growthAmount = 1
            if self.checkGrowthTile(pos):
                growthAmount += self.GROWTH_BONUS_VALUE
            if colour == Board.RED and red_strengths[pos] > 0:
                red_strengths[pos] += growthAmount
            if colour == Board.BLUE and blue_strengths[pos] > 0:
                blue_strengths[pos] += growthAmount
        return red_strengths, blue_strengths

    def applyStrengthCap(self, strengths):
        return np.clip(strengths, 0, self.MAX_TILE_STRENGTH)

    def finalizeTurn(self, red_strengths, blue_strengths):
        red_presence = red_strengths > 0
        self.tile_strengths[red_presence] = red_strengths[red_presence]
        self.tile_control[red_presence] = Board.RED

        blue_presence = blue_strengths > 0
        self.tile_strengths[blue_presence] = blue_strengths[blue_presence]
        self.tile_control[blue_presence] = Board.BLUE

        no_presence = ~(red_presence | blue_presence)

        self.tile_strengths[no_presence] = 0

    def checkDefensiveTile(self, pos):
        return pos in self.defense_tiles

    def checkGrowthTile(self, pos):
        return pos in self.growth_tiles

    # These methods are only used when constructing a board from config

    def addDefensiveTile(self, position):
        self.defense_tiles.append(position % self.board_size)
        return

    def addGrowthTile(self, position):
        self.growth_tiles.append(position % self.board_size)

    def placePiece(self, position, colour, strength):
        if (position < 0) or (position >= self.board_size):
            raise Exception("Piece placement position error: Out of bounds position")
        if (strength < 1) or (strength > self.MAX_TILE_STRENGTH):
            raise Exception("Board placement exception: Strength out of bounds")
        self.tile_control[position] = colour
        self.tile_strengths[position] = strength

    @staticmethod
    def from_config(config):
        new_board = Board(config["boardSize"])

        new_board.MAX_TILE_STRENGTH = config["maxTileStrength"]
        new_board.GROWTH_BONUS_VALUE = config["growthBonusValue"]
        new_board.DEFENSE_BONUS_PERCENTAGE = config["defenceBonusPercentage"]

        new_board.maxTurns = config["maxTurns"]

        # Place initial player pieces
        pos_red_init = new_board.getBoardSize() // 4
        new_board.placePiece(pos_red_init, Board.RED, 1)
        pos_blue_init = new_board.getBoardSize() - 1 - pos_red_init
        new_board.placePiece(pos_blue_init, Board.BLUE, 1)

        # Place the interesting terrain parts
        if "defenceTiles" in config:
            for tile_location in config["defenceTiles"]:
                new_board.addDefensiveTile(tile_location)

        if "growthTiles" in config:
            for tile_location in config["growthTiles"]:
                new_board.addGrowthTile(tile_location)

        return new_board

