"""
Standalone functionality for scoring a single bot, as performed
by the leaderboard (apart from fighting other bots).
"""
import argparse
import sys
import json
import os
import re

import bot_leaderboard as bl


project_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, project_path[:-8])

def scoreBot(bot):
    """
    This utility function will score a bot for the various properties we want.
    - pep8 violations
    - code quality issues such as missing docstrings
    - unchanged comments from randomBot template
    - pylint score
    - signature checking (for debug challenge, ex 4.3, database challenge, ex 4.5)
    - fighting some challenge bots
    """
    print("Scoring bot: {}".format(bot))

    config = loadConfig()

    checkForPrint(bot)
    
    if config['names']:
        checkName(bot)

    if config['test_coverage']:
        checkTestCoverage(bot)

    if config['documentation']:
        checkComments(bot)

    if config['function_length']:
        checkMaxFunctionLength(bot)

    if config['pep8']:
        checkPep8Violations(bot)

    if config['pylint']:
        checkPylintScore(bot)

    for challenge in config['signatures']:
        checkSignature(bot, challenge)

    for challengeBotName in config['challenge_bots']:
        fightChallengeBot(bot, challengeBotName)


def loadConfig(configFile = './config/leaderboard_config.cfg'):
    with open(configFile, 'r') as f:
        config = json.load(f)
    return config


def checkForPrint(bot):
    print("\n** Checking for print statements.**")
    grepFileForPrint("./bots/{}.py".format(bot.name))
    try:
        grepFileForPrint("./bots/{}Test.py".format(bot.name))
    except FileNotFoundError:
        pass


def grepFileForPrint(filename):
    printRegex = re.compile('^\s*print\(.*\)\n$')
    with open(filename, 'r') as f:
        for idx, line in enumerate(f.readlines()):
            if printRegex.match(line):
                print("Print statement detected in {}:\nline {}: {}".format(filename, idx, line.strip()))


def checkName(bot):
    print("\n** Checking bot name for entertainment value.**")
    with open('./config/good_bot_names.cfg', 'r') as f:
        goodNames = [x.strip() for x in f.readlines()]
        
    if bot.name in goodNames:
        print("Ha. Ha. {} gets a point for being a good name.".format(bot.name))
    else:
        print("{} is not considered a funny name (let someone know if you changed it recently).".format(bot.name))


def checkTestCoverage(bot):
    print("\n** Checking bot test coverage score.**")
    try:
        bot.getTestCoverageScore()
        print("Test Coverage Score:", bot.testCoverageScore)
        if bot.testCoverageScore < 1:
            print("This bot does not have full test coverage or has a failing test.")
            print('Run: python3 -m pytest --cov=project.bots.{} "{}Test.py" for more information.'.format(bot,
                                                                                            os.path.join(bot.botTestDir, bot.name)))
            print("After running this, you can run 'python3 -m coverage report -m' to see which lines are not covered.")
    except FileNotFoundError:
        print("Install pytest module to get test coverage scores - talk to Jim")


def checkComments(bot):
    print("\n** Checking bot for old comments from randomBot or missing docstrings.**")
    defaultCommentCount = bot.countDefaultComments()
    commentLineCount = bot.countComments()
    docStringCounts, missingDocStringCounts = bot.countDocStrings()
    if missingDocStringCounts > 0:
        print("This bot is missing {} docstrings.".format(missingDocStringCounts))
    commentLineCount += docStringCounts
    if commentLineCount - defaultCommentCount < 5:
        print("{} has only {} (new) lines of comment.".format(bot, commentLineCount - defaultCommentCount))


def checkMaxFunctionLength(bot):
    print("\n** Checking the maximum function length in the bot.**")
    bot.checkMaxFunctionLength()


def checkPep8Violations(bot):
    print("\n** Checking bot code for pep8 violations.**")
    try:
        bot.getPep8Violations()
        if bot.pep8Violations > 0:
            print('This bot has {} pep8 violations.\n'
                  'Run: pep8 "{}/{}.py" for more information.'.format(
                  bot.pep8Violations, bot.botDir, bot.name)
                 )
    except FileNotFoundError:
        print("Run 'sudo pip3 install pep8' to get pep8 violations (password: qa)")
        # TODO: print("Run 'sudo pip3 install pycodestyle' to get pep8 violations (password: qa)")
        # pep8 renamed 2017-10-22


def checkPylintScore(bot):
    print("\n** Checking bot Pylint score.**")
    try:
        score = bot.getPylintScore()
        if score < 10:
            print('This bot has a pylint score of less than 10.')
            print('Run: python3 -m pylint --rcfile pylintrc "{}/{}.py" for more information'.format(bot.botDir, bot.name))
    except FileNotFoundError:
        print("Run 'sudo pip3 install  pylint' to get pylint score (password: qa)")


def fightChallengeBot(bot, challengeBotName):
    print("\n** Fighting House Bot: {}.**".format(challengeBotName))

    matchScores = bot.challengeFight(challengeBotName)
    print("Result: {} - {}, {} - {}".format(bot.name, matchScores[0],challengeBotName,  matchScores[1]))
    

def checkSignature(bot, challenge):
    print("\n** Checking signatures for {} challenge.**".format(challenge))
    try:
        bot.checkSignature(challenge)
    except FileNotFoundError:
        print("No answers.txt to check your signature against.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Score a single bot')
    parser.add_argument('botName', action='store',
                        help='file containing bot')
    args = parser.parse_args()
    
    botDir = bl.getBotDirectory()
    botTestDir = bl.getBotTestDirectory()
    sys.path.append(botDir)    
    bot = bl.Bot(args.botName, botDir, botTestDir)

    # bot leaderboard will return randomBot if it can't find a bot, in which
    # case we give up
    if bot.name == "randomBot" and args.botName != "randomBot":
        sys.exit()
    
    scoreBot(bot)
