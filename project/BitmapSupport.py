#
# Create Bitmaps that show the state of the game as it progresses.
# Original version of this software was contributed by a student
# in Class 1.
#

import board
import pickle

BATTLE_DATA_FILE = "bot_battle.pkl"

#
# Start off with some local helper functions.
#

def shortToString(i):
    hi = (i & 0xff00) >> 8
    lo = i & 0x00ff
    return chr(lo) + chr(hi)


def longToString(i):
    hi = (int(i) & 0x7fff0000) >> 16
    lo = int(i) & 0x0000ffff
    return shortToString(lo) + shortToString(hi)


def long24ToString(i):
    return chr(i & 0xff) + chr(i >> 8 & 0xff) + chr(i >> 16 & 0xff)


def getPixelColour(colour, strength):
    if colour == board.ControlColour.NEUTRAL:
        return 0xffffff
    elif colour == board.ControlColour.BLUE:
        if strength > 0:
            return 0x00000a * strength
        else:
            return 0x004040
    elif colour == board.ControlColour.RED:
        if strength > 0:
            return 0x0a0000 * strength
        else:
            return 0x404000
    else:
        return 0xff00ff


#
# Define the primary class for this file.
#
class BitMapper:
    """
    Generate Bitmap representations of the Game Board, so that users
    can visualize the entire game. To see the results, run:
       eog gameState.bmp
    """

    def __init__(self, boardSize, maxTurns, blueBotName, redBotName):
        """
        Setup some basic setup information; also, create the bitmap
        header.
        """
        self.headerLen = 54
        self.width = boardSize
        self.height = maxTurns + 1
        self.padding = (4 - (boardSize % 4)) % 4
        self.filename = "gameState.bmp"
        self._writeHeader()

        with open(BATTLE_DATA_FILE, "wb") as data_file:
            pickle.dump(boardSize, data_file)
            pickle.dump(blueBotName, data_file)
            pickle.dump(redBotName, data_file)


    def _writeHeader(self):
        """
        Write out the data to the bitmap file that it needs to set up
        the image. Implementation contributed by a student in Class 1.
        """

        #
        # write bitmap header
        #
        bitmap = "BM"

        # DWORD size in bytes of the file
        size = self.height * (self.width * 3 + self.padding)
        bitmap += longToString(self.headerLen + size)
        bitmap += longToString(0)            # DWORD 0
        bitmap += longToString(54)
        bitmap += longToString(40)           # DWORD header size = 40
        bitmap += longToString(self.width)   # DWORD image width
        bitmap += longToString(self.height)  # DWORD image height
        bitmap += shortToString(1)           # WORD planes = 1
        bitmap += shortToString(24)          # WORD bits per pixel = 8
        bitmap += longToString(0)            # DWORD compression = 0

        # DWORD sizeimage = size in bytes of the bitmap = width * height
        bitmap += longToString(size)
        bitmap += longToString(0)    # DWORD horiz pixels per meter (?)
        bitmap += longToString(0)    # DWORD ver pixels per meter (?)
        bitmap += longToString(0)    # DWORD number of colors used = 256
        bitmap += longToString(0)    # DWORD number of "import colors"

        #
        # Actually output the data.
        #
        with open(self.filename, "wb") as bitmap_file:
            bitmap_file.write(bytes(map(ord, bitmap)))

    def writeState(self, control, strengths, turn, scores):
        """This is the code for 2d plots one step at a time"""
                      
        # Generate a new row of the image file, based upon the current state
        # of the game board.
        # Turn the board state into a new row of pixel colours.
        state = [0] * self.width
        for i, (colour, strength) in enumerate(zip(control, strengths)):
            state[i] = getPixelColour(colour, strength)
        
        # Generate the output data for the row.
        bitmap = ""
        for pixel in state:
            bitmap += long24ToString(pixel)
        for _ in range(self.padding):
            bitmap += chr(0)
            
        # Output the data.
        old_data = open(self.filename, "rb").read()[self.headerLen:]
        self._writeHeader()

        with open(self.filename, "ab") as bitmap_file:
            bitmap_file.write(bytes(map(ord, bitmap)))
            bitmap_file.write(old_data)

        with open(BATTLE_DATA_FILE, "ab") as data_file:
            pickle.dump(scores, data_file)
            pickle.dump(zip(control, strengths), data_file)

