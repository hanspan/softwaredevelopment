# Battle Rules

This is a reference to complement the presentation from the first morning [Class_Project.pptx](../presentations/0b_Class_Project.pptx)

### Setup 
Many of these options are configurable (see [config/gameConfig.cfg](config/gameConfig.cfg)).

* The game board is a ring of tiles that wrap around.
* Players start with one piece each of strength 1.
* Each piece may make 1 move per turn.
* There is maximum strength value for pieces.
 
### Moves
* `LEFT`/`RIGHT` - Take a step to the left or right.
* `STAY` - Stay where you are, gain 1 strength point. This is the **default** move.
* `JUMP_LEFT`/`JUMP_RIGHT` - Jump a number of spaces to the left or right. Costs `num_spaces - 1` strength points to execute. If a piece has insufficient strength, it will `STAY` instead.
* `SPLIT` - Split into two pieces of equal strength, appearing on the spaces on either side. If the piece's strength is odd, a 'remainder' piece of strength 1 will be left in place.

### Collisions
* If two pieces are the same colour, they combine strength.
* If two opposing pieces try to move to the same square, they will each take damage. The lower strength piece will be eliminated and the stronger piece will take damage equivalent to the lower piece's health.
* Large battles can cause 'splash damage' to nearby tiles.

### Scoring
The game ends when either

* One player is eliminated.
* A set number of turns have been reached.

Each player's final score combines:

* Number of pieces left on the board.
* Total strength of all the pieces.
* The number of controlled tiles (when a piece lands on a tiles, it takes control of that tile for the player until another piece lands there).
