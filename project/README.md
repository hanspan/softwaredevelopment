# Bot vs Bot
A system for playing games where the competitors are
automated bots. This file gives a brief summary of the directories
and files available.

In essence, there is a server and there are bots (all written in Python). 
Most of the course exercises will focus on the bots themselves, although 
you are welcome to examine the server as well.

### Bot Utilities
These will help you visualise the battles and see how your bot is doing
during the week. Be prepared for the scoring to change!

#### Visualisation Tools
gameState.bmp - This picture updates after every battle to show how the
  bots did. Time goes from the top of the picture down, with each line
  showing a single state of the game. The strength of the pieces is shown
  by gradations in the colour.

[battlegraphics.py](./battlegraphics.py) - This utility lets you 
  visualise the last battle that was played as a video. You can change 
  the delay between each step.

#### Rules
[BattleRules.md](./BattleRules.md) - This describes the rules of the bot battles. 
  
[BotScoring.md](./BotScoring.md) - This describes how bot scoring will 
  change as the course progresses. If you're interested in the leaderboard, 
  you'll need to adapt your bot over the course.
  
#### Utility scripts
[bot_scorer.py](./bot_scorer.py) - A utility for estimating how well your
  bot is doing in the various categories it is measured against.

[bot_signature.py](./bot_signature.py) - Used later in the course as part of 
  a few of the exercises.

[bot_leaderboard.py](./bot_leaderboard.py) - A script that is run during 
  the course, to determine which bots are best! As the course progresses, 
  more and more capabilities are expected from your bots, so scores can
  change quite drastically.

### Folder structure

[bots](./bots) - The directory where the bots live. The default game
  configuration file will only recognize bots that are in this
  directory.

[bots/class*](./bots) - Bots from prior classes.

[tests/bots/testdata](tests/bots/testdata) - Information needed to run unit
  tests for the bots (this will be discussed in more detail during the course).

[config](./config) - Where the main game configuration files are stored. You
  could always experiment with some custom configs, to see how it
  changes the gameplay.

### Other files

[pylintrc](./pylintrc) - Customizes the "code quality" portion of the bot scores.

