"""
Viewing tool for seeing the most recently played battle before your very eyes.
"""

import sys

try:
    import pygame
except ImportError:
    print("Please install pygame to run this utility.\nsudo pip3 install pygame")
    sys.exit(1)
    
from pygame import Rect, Color, font
from pygame.locals import *
from pygame.draw import rect
pygame.init()

import board
import pickle
import time
import argparse

PIECE_COLOURS = {board.ControlColour.BLUE : Color('Blue'),
                 board.ControlColour.RED : Color('Red')}

MAX_PIECE_STRENGTH = 25
SCREEN_HEIGHT = MAX_PIECE_STRENGTH + 10
BOT_WIDTH = 10

NAME_X_PADDING = 10 * BOT_WIDTH
NAME_Y_PADDING = 10


BATTLE_DATA_FILE = 'bot_battle.pkl'

class BattleViewer:
    """
    Pygame class for all your bot battle viewing needs.
    """

    def __init__(self, time_step):
        """
        Set the time step between frames and do other necessary
        initialisation of the surface.
        """
        self.time_step = time_step
        self._running = True
        self._display_surf = None
        self.data_file = open(BATTLE_DATA_FILE, 'rb')
        self.board_size = pickle.load(self.data_file)
        self.blue_bot_name = pickle.load(self.data_file)
        self.red_bot_name = pickle.load(self.data_file)
        self.size = self.width, self.height = self.board_size * BOT_WIDTH, SCREEN_HEIGHT * BOT_WIDTH
        self.font = pygame.font.SysFont('Arial', 24)
        self.turn_id = 1


    def on_init(self):
        """
        Set up the surface and set the class as 'running'.
        """
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True
 
    def on_event(self, event):
        """
        The only event we care about is allowing users to quit.
        """
        if event.type == pygame.QUIT:
            self._running = False

    def on_loop(self):
        """
        On each loop, we want to draw the next frame.
        Once we have drawn all the turns, we do nothing.
        """

        data = self.get_next_state()
        if data is not None:
            scores, board_state = data
            self.draw_state(scores, board_state)
            self.turn_id += 1

    def get_next_state(self):
        try:
            scores = pickle.load(self.data_file)
            board_state = pickle.load(self.data_file)
            return scores, board_state
        except EOFError:
            return 

    def draw_state(self, scores, board_state):
        """
        Draw the state of the board
        """
        self._display_surf.fill(Color('White'))
        self.display_bot_names()
        self.display_game_state(scores)
        for idx, (control, strength) in enumerate(board_state):
            if strength != 0:
                col = PIECE_COLOURS[control]
                bot_rect = Rect(idx * BOT_WIDTH, (SCREEN_HEIGHT - strength) * BOT_WIDTH, BOT_WIDTH, strength * BOT_WIDTH)
                self._display_surf.fill(col, bot_rect)

    def display_bot_names(self):
        """
        Display the bot names in the appropriate colours.
        """
        red_text = self.font.render(self.red_bot_name, True, Color("Red"))
        blue_text = self.font.render(self.blue_bot_name, True, Color("Blue"))
        self._display_surf.blit(red_text, (NAME_X_PADDING, NAME_Y_PADDING*3))
        self._display_surf.blit(blue_text, (self.width-(NAME_X_PADDING+blue_text.get_width()), NAME_Y_PADDING*3))

    def display_game_state(self, scores):
        """
        Display the turn we're currently on and the live scores.
        """
        red_score_text = self.font.render(str(scores["Red"]), True, Color("Red"))
        self._display_surf.blit(red_score_text, (NAME_X_PADDING, NAME_Y_PADDING*5))

        blue_score_text = self.font.render(str(scores["Blue"]), True, Color("Blue"))
        self._display_surf.blit(blue_score_text, (self.width-(NAME_X_PADDING+blue_score_text.get_width()), NAME_Y_PADDING*5))

        turn_text = self.font.render("Turn: {:0>3}".format(self.turn_id), True, Color("Black"))
        self._display_surf.blit(turn_text, ((self.width-turn_text.get_width())//2, NAME_Y_PADDING))
        

    def on_render(self):
        """
        Update the display and pause for the defined time_step.
        """
        pygame.display.update()
        time.sleep(self.time_step)

    def on_cleanup(self):
        """
        Tidy up.
        """
        pygame.quit()
 
    def on_execute(self):
        """
        Handler to run the main loop of the display
        and cleanup after quit.
        """
        if self.on_init() == False:
            self._running = False
 
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="View a bot battle as a movie.")
    parser.add_argument('-t', dest='time_step', type=float, default=0.2) 
    args = parser.parse_args()
    time_step = args.time_step

    viewer = BattleViewer(time_step)
    viewer.on_execute()

    
