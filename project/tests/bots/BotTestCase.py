import unittest
import os
import json
from project import board


class BotTestCase(unittest.TestCase):

    def getTestDataDir(self):
        """
        Constructs absolute path to testdata dir and returns it
        """
        return os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            'testdata')

    def get_board_from_config(self, config_file_name="testConfig.cfg"):
        """
        Grabs the server config and returns it in a form usable by a bot
        :return: game board object configured as provided file
        """

        configFilePath = os.path.join(self.getTestDataDir(), config_file_name)
        with open(configFilePath, 'r') as configFile:
            board_config = json.loads(configFile.read())

        # Build the board
        game_board = board.Board.from_config(board_config)
        game_board.player = board.ControlColour.RED

        return game_board
