import json           # Process JSON to/from strings, etc.
import os

import project.board as board     # Information on game board properties.
import project.bots.runnerBot as runnerBot      # The Bot being tested.

import project.tests.bots.BotTestCase as BotTestCase


class runnerBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        """Common setup for the tests in this class."""

        self.bot = runnerBot.RunnerBot()

    def testGetBot(self):
        """Make sure a bot is returned from the getBot() function."""

        self.assertNotEqual(None, runnerBot.getBot())

    def testFirstMove(self):
        """Test that runnerBot's first move is to Stay."""

        # Read in a standard board state, and see what moves result.
        game_board = self.get_board_from_config()
        results = self.bot.makeMoves(game_board)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(board.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testSignature(self):
        """Check that the expected signature is returned."""

        sig = runnerBot.RunnerBot.getSignature()
        self.assertTrue(sig.endswith("410a8819aab0a8d"))
