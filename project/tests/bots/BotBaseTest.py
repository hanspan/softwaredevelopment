import json           # Process JSON to/from strings, etc.
import os
import unittest.mock  # Bring in Python's built-in unit testing functionality.

import project.board as board      # Provides some game-specific enumerations.
import project.bots.BotBase as BotBase        # The class to test.
import project.tests.bots.BotTestCase as BotTestCase


class BotBaseTestCase(BotTestCase.BotTestCase):

    def testMoveProcessing(self):
        """
        Test to ensure that the generateMoves() function is called.
        """

        # Set up the Bot, first.
        bot = BotBase.BotBase()
        game_board = self.get_board_from_config()

        # Mock the function we're testing for and trigger it.
        # By setting a return value, we ensure that our 'fake' function
        # returns something that can be serialised to JSON
        bot.generateMoves = unittest.mock.MagicMock(
            name='generateMoves',
            return_value=[[5, str(board.Action.LEFT), 1]]
        )

        bot.makeMoves(game_board)
        self.assertTrue(bot.generateMoves.called)
