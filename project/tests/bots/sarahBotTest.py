import os
import json

import project.tests.bots.BotTestCase as BotTestCase
import project.bots.sarahBot as sarahBot


class SarahBotTest(BotTestCase.BotTestCase):
    """
    Some very simple tests for a very simple bot.
    """

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = sarahBot.SarahBot()

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, sarahBot.getBot())

    def testFirstMove(self):
        # Test that the first move is a Stay
        game_board = self.get_board_from_config()
        results = self.bot.makeMoves(game_board)

        self.assertEqual(results, [[25, 'Stay', 1]])

    def testSecondMove(self):
        # Test that the first move is a Split
        game_board = self.get_board_from_config()
        game_board.currTurn = 2
        results = self.bot.makeMoves(game_board)

        self.assertEqual(results, [[25, 'Split', 1]])

    def testDebugSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature('debug')
        self.assertTrue(sig.startswith("df0e55b6d9da"))

    def testDatabaseSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature('database')
        self.assertTrue(sig.startswith("7a32e1b3fd01"))

    def testWrongSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
