
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import project.board as board      # Information on game board properties.
import project.bots.jonnyRandomBot as jonnyRandomBot      # The Bot being tested.

import project.tests.bots.BotTestCase as BotTestCase


class randomBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = jonnyRandomBot.JonnyRandomBot()

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, jonnyRandomBot.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        game_board = self.get_board_from_config()
        game_board.random = 7
        results = self.bot.makeMoves(game_board)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(board.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownJumpMove(self):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and see what moves result.
        game_board = self.get_board_from_config()
        game_board.random = 0
        game_board.setTileStrength(25, 4)
        results = self.bot.makeMoves(game_board)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(board.Action.JUMP_LEFT), results[0][1])

        # The jump distance should equal half the piece strength.
        self.assertEqual(2, results[0][2])

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = jonnyRandomBot.JonnyRandomBot.getSignature('debug')
        self.assertTrue(sig.endswith("ca66"))
        sig = jonnyRandomBot.JonnyRandomBot.getSignature('database')
        self.assertTrue(sig.endswith("bb50"))
        sig = jonnyRandomBot.JonnyRandomBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
