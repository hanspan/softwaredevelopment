
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import minimaxBot      # The Bot being tested.

import BotTestCase


class minimaxBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = minimaxBot.MinimaxBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, minimaxBot.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testGetMovesForLocation(self):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        self.bot.board.updateState(json.loads(state))
        
        moves = self.bot.getMovesForLocation(self.bot.board, 0)
        self.assertEqual(0, len(moves))
        
        moves = self.bot.getMovesForLocation(self.bot.board, 25)
        self.assertEqual(4, len(moves))
        
    def testPrintBoard(self):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        self.bot.board.updateState(json.loads(state))
        
        s = self.bot.printBoard(self.bot.board)
        self.assertEqual("_________________________R________________________________________________B_________________________",s)

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = minimaxBot.MinimaxBot.getSignature('debug')
        self.assertTrue(sig.endswith("e6b78e45efab"))
        sig = minimaxBot.MinimaxBot.getSignature('database')
        self.assertTrue(sig.endswith("688fe1f3b0d1"))
        sig = minimaxBot.MinimaxBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
