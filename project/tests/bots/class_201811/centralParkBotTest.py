"""Unit test for the centralParkBot class
"""

import centralParkBot
import BotTestCase
import GameBoard
import json
import os

class centralParkBotTest(BotTestCase.BotTestCase):
    def setUp(self):
        """Creates an instance of the centralParkBot"""

        self.bot = centralParkBot.getBot()
        self.bot.setup(self.getGameConfig())

    def load_game_state(self, state_filename):
        """Loads the state of the game from a JSON formatted file."""

        if state_filename is not None:
            test_dir = self.getTestDataDir()
            with open(os.path.join(test_dir, state_filename), "rt") as fd:
                board_state = json.load(fd)
            self.bot.board.updateState(board_state)
        return self.bot


    def testFirstMove(self):
        """Test that the first move sets the _start_location attribute and
        that the piece stays in the same place."""

        self.load_game_state("testBoardState.txt")

        moves = self.bot.generateMoves(1, 1000)

        piece_location = self.bot.board.findMyPieces()[0]

        # Check that the _start_location attribute has been correctly set.
        self.assertEqual(piece_location, self.bot._start_location)

        # Check that the only move is to stay in the same space
        self.assertListEqual([[piece_location, str(GameBoard.Action.STAY), 1]], moves)

    def testGetDirection(self):
        """Test that the directions across the board are as epxected."""

        width = self.bot.board.boardSize

        for start in range(width):
            # Check that location as the start has direction STAY
            self.assertEqual(self.bot.getDirection(start, start), GameBoard.Action.STAY)

            for i in range(1, (width + 1)//2):
                # Check locations to the left
                location = (start + width - i)%width
                self.assertEqual(self.bot.getDirection(location, start), GameBoard.Action.LEFT)

                # Check locations to the right
                location = (start + i)%width
                self.assertEqual(self.bot.getDirection(location, start), GameBoard.Action.RIGHT)

            # Check location at other side of the board (only exists for even width)
            if width%2 == 0:
                location = (start + width//2)%width
                self.assertEqual(self.bot.getDirection(location, start), GameBoard.Action.STAY)

    def testGetManhattanDistances(self):
        """Test that the distances across the board are as expected."""

        width = self.bot.board.boardSize

        for start in range(width):
            # Check that location as the start has direction STAY
            self.assertEqual(self.bot.getManhattanDistance(start, start), 0)

            for i in range(1, (width + 1)//2):
                # Check locations to the left
                location = (start + width - i)%width
                self.assertEqual(self.bot.getManhattanDistance(location, start), i)

                # Check locations to the right
                location = (start + i)%width
                self.assertEqual(self.bot.getManhattanDistance(location, start), i)

            # Check location at other side of the board (only exists for even width)
            if width%2 == 0:
                location = (start + width//2)%width
                self.assertEqual(self.bot.getManhattanDistance(location, start), width//2)

    def testSubsequentEvenMove(self):
        """Test a subsequent move of the bot."""

        REQUIRED_RESULTS = [
            [21, str(GameBoard.Action.STAY) , 1],
            [22, str(GameBoard.Action.STAY) , 1],

            [23, str(GameBoard.Action.STAY), 1],
            [24, str(GameBoard.Action.STAY) , 1],
            [25, str(GameBoard.Action.STAY), 1],
            [26, str(GameBoard.Action.STAY) , 1],
            [27, str(GameBoard.Action.STAY) , 1],

            [28, str(GameBoard.Action.STAY) , 1],
            [29, str(GameBoard.Action.STAY), 1],
        ]

        # Requires a first move to have been done.
        self.load_game_state("testBoardState.txt")
        self.bot.generateMoves(1, 1000)

        # Load in the state we want to test
        self.load_game_state("testCentralParkEvenState.txt")

        # Run the test
        moves = self.bot.generateMoves(10, 1000)

        # Check the output has the correct length
        self.assertEqual(len(moves), len(REQUIRED_RESULTS))

        # Check that the moves are as expected
        for required in REQUIRED_RESULTS:
            self.assertIn(required, moves)


    def testSubsequentOddMove(self):
        """Test a subsequent move of the bot."""

        REQUIRED_RESULTS = [
            [20, str(GameBoard.Action.LEFT) , 1],
            [21, str(GameBoard.Action.LEFT) , 1],
            [22, str(GameBoard.Action.LEFT) , 1],

            [23, str(GameBoard.Action.SPLIT) , 1],
            [24, str(GameBoard.Action.SPLIT) , 1],
            [25, str(GameBoard.Action.JUMP_LEFT), 3],
            [26, str(GameBoard.Action.SPLIT) , 1],
            [27, str(GameBoard.Action.SPLIT) , 1],

            [28, str(GameBoard.Action.RIGHT) , 1],
            [29, str(GameBoard.Action.RIGHT) , 1],
            [30, str(GameBoard.Action.RIGHT) , 1],
        ]

        # Requires a first move to have been done.
        self.load_game_state("testBoardState.txt")
        self.bot.generateMoves(1, 1000)

        # Load in the state we want to test
        self.load_game_state("testCentralParkOddState.txt")

        # Run the test
        moves = self.bot.generateMoves(11, 1000)

        # Check the output has the correct length
        self.assertEqual(len(moves), len(REQUIRED_RESULTS))

        # Check that the moves are as expected
        for required in REQUIRED_RESULTS:
            self.assertIn(required, moves)

    def testSignature(self):
        """"Check that the expected signature is returned."""

        sig = centralParkBot.CentralParkBot.getSignature('debug')
        self.assertTrue(sig.endswith("e85a2b5447ef"))
        sig = centralParkBot.CentralParkBot.getSignature('database')
        self.assertTrue(sig.endswith("9c88dc093f75"))
        sig = centralParkBot.CentralParkBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
