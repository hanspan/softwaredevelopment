import os
from unittest.mock import MagicMock
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import splitSlowSpreadBot      # The Bot being tested.

import BotTestCase


class TestBotSetup(BotTestCase.BotTestCase):

    def testParametersAreSetFromConfig(self):
        botConfig = {"separateDist": 1,
                     "oppositeDist": 2,
                     "changeMaxStrengthAt": 3,
                     "initialMaxStrength": 4,
                     "finalMaxStrength": 5}
        self.bot = splitSlowSpreadBot.getBot(botConfig=botConfig)
        self.bot.setup(self.getGameConfig())
        self.assertEqual(self.bot.separateDist, 1)
        self.assertEqual(self.bot.oppositeDist, 2)
        self.assertEqual(self.bot.changeMaxStrengthAt, 3)
        self.assertEqual(self.bot.initialMaxStrength, 4)
        self.assertEqual(self.bot.finalMaxStrength, 5)

    def testNonDictConfigIsRejected(self):
        botConfig = "initialMaxStrength: 10"
        with self.assertRaisesRegex(TypeError, "botConfig must "
                                    "be a dictionary"):
            self.bot = splitSlowSpreadBot.getBot(botConfig=botConfig)

    def testUnrecognisedParameterIsRejected(self):
        botConfig = {"otherParameter": "10"}
        errorMsg = ("otherParameter is not one of the "
                    "parameters which can be set in botConfig: "
                    "{}".format(", ".join(
                        splitSlowSpreadBot.CONFIGURABLE_PARAMS)))
        with self.assertRaisesRegex(ValueError, errorMsg):
            self.bot = splitSlowSpreadBot.getBot(botConfig=botConfig)

    def testNonIntegerParameterIsRejected(self):
        botConfig = {"initialMaxStrength": "10"}
        with self.assertRaisesRegex(ValueError, "Value of " 
                                    "initialMaxStrength in "
                                    "botConfig must be a positive "
                                    "integer"):
            self.bot = splitSlowSpreadBot.getBot(botConfig=botConfig)
        
    def testNegativeParameterIsRejected(self):
        botConfig = {"oppositeDist": -1}
        with self.assertRaisesRegex(ValueError, "Value of " 
                                    "oppositeDist in botConfig "
                                    "must be a positive integer"):
            self.bot = splitSlowSpreadBot.getBot(botConfig=botConfig)
        
    def testBadSeparateDistanceIsRejected(self):
        botConfig = {"separateDist": 200}
        self.bot = splitSlowSpreadBot.getBot(botConfig=botConfig)
        with self.assertRaisesRegex(ValueError, "separateDist is 200 "
                                    "but must be between 0 and board "
                                    "size \(100\)"):
            self.bot.setup(self.getGameConfig())

class splitSlowSpreadBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = splitSlowSpreadBot.SplitSlowSpreadBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, splitSlowSpreadBot.getBot())

    def testKnownInitialSplitMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSplitSlowSpreadBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        strengths = json.loads(state)["strengths"]
        #print("1st strength: %s" % strengths.index(1))
        #print("2nd strength: %s" % strengths[strengths.index(1)+1:].index(1))
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1, results[0][2])

    def testSetsMaxStrengthForEarlyMove(self):
        # Testing that the moves will be generated with the max strength that
        # corresponds to the current turn

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSplitSlowSpreadBotEarlyMove.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        self.bot.generateMovesWithMaxStrength = MagicMock(name='generateMovesWithMaxStrength')
        try:
            resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        except:
            pass
        self.bot.generateMovesWithMaxStrength.assert_called_with([25],self.bot.initialMaxStrength)

    def testSetsMaxStrengthForLateMove(self):
        # Testing that the moves will be generated with the max strength that
        # corresponds to the current turn

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForSplitSlowSpreadBotLateMove.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        self.bot.generateMovesWithMaxStrength = MagicMock(name='generateMovesWithMaxStrength')
        try:
            resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        except:
            pass
        self.bot.generateMovesWithMaxStrength.assert_called_with([25],self.bot.finalMaxStrength)

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = splitSlowSpreadBot.SplitSlowSpreadBot.getSignature('debug')
        self.assertTrue(sig.endswith("559853c167a8"))
        sig = splitSlowSpreadBot.SplitSlowSpreadBot.getSignature('database')
        self.assertTrue(sig.endswith("dd2b5b821833"))
        sig = splitSlowSpreadBot.SplitSlowSpreadBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))

class TestGenerateMovesWithMaxStrength(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = splitSlowSpreadBot.SplitSlowSpreadBot()
        self.bot.setup(self.getGameConfig())
        self.bot.board.tileStrength = [0 for i in range(self.bot.board.boardSize)]
        self.maxStrength = 10

    def mockGetPieceStrength(self, location):
        return self.strengths[location]

    def checkStandardResults(self, results):
        """Check parts of results that should be the same for each test here"""
        self.assertEqual(len(self.locations), len(results))
        for i in range(len(self.locations)):
            location = self.locations[i]
            # Check the returned move is for this location with arg 1
            self.assertEqual(3, len(results[i]))
            self.assertEqual(location, results[i][0])
            self.assertEqual(1, results[i][2])
            # Weak pieces always stay, so check that here
            if self.strengths[location] < self.maxStrength:
                self.assertEqual(str(GameBoard.Action.STAY), results[i][1])

    def testGenerateMovesIsolatedStrongPieceEvenStrength(self):
        """Test when no near pieces"""
        self.strengths = {30:1, 50:20, 70:1}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should split because it has even strength
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1])

    def testGenerateMovesSurroundedStrongPieceEvenStrengthEqualNearest(self):
        """Test when nearest pieces are equally near"""
        self.strengths = {49:1, 50:20, 51:1}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should split because it has even strength
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1])
        
    def testGenerateMovesSurroundedStrongPieceEvenStrengthNearRight(self):
        """Test when nearest piece is to the right"""
        self.strengths = {48:1, 50:20, 51:1}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should split because it has even strength
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1])

    def testGenerateMovesSurroundedStrongPieceOddStrengthNearLeft(self):
        """Test when nearest piece is to the left"""
        self.strengths = {49:1, 50:21, 52:1}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should stay because it has odd strength
        self.assertEqual(str(GameBoard.Action.STAY), results[1][1])

    def testGenerateMovesStrongPieceWithPieceNearbyRight(self):
        self.strengths = {21:1, 50:20, 52:1}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should move left (away from the nearest piece)
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])
        
    def testGenerateMovesStrongPieceWithPieceNearbyRightWrapped(self):
        """ Tests boundary case where nearest piece is wrapped round"""
        self.strengths = {1:1, 99:20}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should move left (away from the nearest piece)
        self.assertEqual(str(GameBoard.Action.LEFT), results[1][1])

    def testGenerateMovesStrongPieceWithPieceNearbyLeftWrapped(self):
        """ Tests boundary case where nearest piece is wrapped round"""
        self.strengths = {1:20, 22:1, 99:1}
        self.locations = sorted(self.strengths.keys())
        self.bot.board.getPieceStrength = MagicMock(
            side_effect=self.mockGetPieceStrength)
        results = self.bot.generateMovesWithMaxStrength(self.locations,
                                                        self.maxStrength)
        self.checkStandardResults(results)
        # Strong piece should move right (away from the nearest piece)
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
