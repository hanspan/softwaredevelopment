
import os
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import pc_shortsightedBot      # The Bot being tested.
from unittest import mock

import BotTestCase



class shortsightedBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = pc_shortsightedBot.PcShortSightedBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, pc_shortsightedBot.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])
        
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForPCBot.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(4, len(results))

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = pc_shortsightedBot.PcShortSightedBot.getSignature('debug')
        self.assertTrue(sig.endswith("219bbb891b4f"))
        sig = pc_shortsightedBot.PcShortSightedBot.getSignature('database')
        self.assertTrue(sig.endswith("60cc04ac9b43"))
        sig = pc_shortsightedBot.PcShortSightedBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
        
    def testMoveProcessing(self):
        # Ensure the generateMoves() function is called.

        # Set up the Bot, first.
        bot = pc_shortsightedBot.PcShortSightedBot()
        bot.setup(self.getGameConfig())
        testDataDir = self.getTestDataDir()
        with open(os.path.join(testDataDir, 'testBoardState.txt'), 'r') as stateFile:
            state = stateFile.read()
        
        # Mock the function we're testing for and trigger it.
        # Since the function isn't "real", there are followon errors to catch.
        bot.generateMoves = mock.MagicMock(name='generateMoves') 
        try:
            bot.makeMoves(state.encode("utf-8"))
        except Exception as _:
            pass
        #self.assertTrue(bot.generateMoves.assert_called_with(1, 1000))
        bot.generateMoves.assert_called_with(1, 1000)
