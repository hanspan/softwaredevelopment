"""
Tests for the SebBot
"""

import os
import json  # Process JSON to/from strings, etc.

import unittest  # Bring in Python's built-in unit testing functionality.
from unittest import mock

import GameBoard  # Information on game board properties.
import seb_bot  # The Bot being tested.

import BotTestCase




class SebBotTests(BotTestCase.BotTestCase):
    """
    Tests for the SebBot
    """

    def setUp(self):
        """
        Common setup for the tests in this class.
        """

        self.bot = seb_bot.SebBot()
        self.bot.setup(self.getGameConfig())

        # Load a test board state, this basically doesn't matter
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            self.state = stateFile.read()

    def testGetBot(self):
        """
        Make sure a bot is returned from the getBot() function.
        """
        self.assertNotEqual(None, seb_bot.getBot())

    def testOnlyStayOrSplit(self):
        """
        Tests that SebBot only performs stay or split moves
        """
        resultsRaw = self.bot.makeMoves(self.state.encode("utf-8"))
        results = json.loads(resultsRaw)

        for move in results:
            self.assertTrue(move[1] in [str(GameBoard.Action.STAY), str(GameBoard.Action.SPLIT)])

    @mock.patch('random.random', return_value=0.001)
    def testStayWithSmallRandomValue(self, mock_random):
        """
        Tests that SebBot performs STAY with a low random numbers
        """
        resultsRaw = self.bot.makeMoves(self.state.encode("utf-8"))
        results = json.loads(resultsRaw)

        for move in results:
            self.assertEqual(str(GameBoard.Action.STAY), move[1])

    @mock.patch('random.random', return_value=0.999)
    def testSplitWithLargeRandomValue(self, mock_random):
        """
        Tests that SebBot performs SPLIT for large random numbers
        """
        resultsRaw = self.bot.makeMoves(self.state.encode("utf-8"))
        results = json.loads(resultsRaw)

        for move in results:
            self.assertEqual(str(GameBoard.Action.SPLIT), move[1])

    def testSignature(self):
        """
        Check that the expected signature is returned.
        """

        sig = seb_bot.SebBot.getSignature('debug')
        self.assertTrue(sig.endswith("68c799a7b3d1"))
        sig = seb_bot.SebBot.getSignature('database')
        self.assertTrue(sig.endswith("421d7539167e"))
        sig = seb_bot.SebBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))


if __name__ == "__main__":
    unittest.main()
