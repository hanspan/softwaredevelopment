
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import iRobBot      # The Bot being tested.

import BotTestCase


class IRobBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = iRobBot.IRobBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, iRobBot.getBot())

    def testSignedDistance(self):
        self.assertEqual(self.bot.signedDistance(1,99), -2)
        self.assertEqual(self.bot.signedDistance(99,1), 2)

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[0][1])
        self.assertEqual(1, results[0][2])

        boardStateFile = os.path.join(testDataDir, 'testBoardStateForRob.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        self.bot.direction = None
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(1, len(results))
        self.assertEqual(40, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = iRobBot.IRobBot.getSignature('debug')
        self.assertTrue(sig.endswith("abd9"))
        sig = iRobBot.IRobBot.getSignature('database')
        self.assertTrue(sig.endswith("f064"))
        sig = iRobBot.IRobBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
