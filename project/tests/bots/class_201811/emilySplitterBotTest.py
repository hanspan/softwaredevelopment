
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import emilySplitterBot      # The Bot being tested.

import BotTestCase


class emilySplitterBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = emilySplitterBot.SplitterBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, emilySplitterBot.getBot())

    def testKnownStayMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])
        
    def testKnownSplitMove(self):
        # Testing the expected output from a move that should result in a
        # split
        
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForRandomBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1, results[0][2])

    def testInvalidSetup(self):
        # Check that an exception is thrown if  config is given
        # in the wrong format
        bot = emilySplitterBot.SplitterBot()
        config = "{version: 1.0}"
        with self.assertRaises(AttributeError):
            bot.setup(config)


    def testSignature(self):
        # Check that the expected signature is returned.

        sig = emilySplitterBot.SplitterBot.getSignature('debug')
        self.assertTrue(sig.endswith("68c799a7b3d1"))
        sig = emilySplitterBot.SplitterBot.getSignature('database')
        self.assertTrue(sig.endswith("421d7539167e"))
        sig = emilySplitterBot.SplitterBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
