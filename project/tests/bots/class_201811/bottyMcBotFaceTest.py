
import os
import unittest.mock       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import bottyMcBotFace      # The Bot being tested.

import BotTestCase


class bottyMcBotFaceTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = bottyMcBotFace.BottyMcBotFace()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, bottyMcBotFace.getBot())
        
    def getResultOfMoveStartingFromFile(self, testStateFile):
        """
        Given the name of a test file containing a state to start at,
        load the file and make a move from that state,
        returning the result as a json object.
        """
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, testStateFile)
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        return results

    def testKnownRandomMove(self):
        """
        This tests one of our bot's random moves.
        It reads in a board state and makes a move,
        checking the result was as expected. The 
        random seed set in the board state should mean
        that the bot chooses to split.
        """
        results = self.getResultOfMoveStartingFromFile('testStateRandomMoveEmily.txt')
        self.assertEqual(3, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(27, results[1][0])
        self.assertEqual(29, results[2][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        self.assertEqual(1, results[0][2])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[2][1])
        
    def testFirstMove(self):
        """
        Testing the expected output from the first move,
        which should be to stay
        """
        results = self.getResultOfMoveStartingFromFile('testBoardState.txt')
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])
        
    def testEarlyMove(self):
        """
        Test an early move that should result
        in one left and one right move
        """
        results = self.getResultOfMoveStartingFromFile('testStateEarlyMoveEmily.txt')
        self.assertEqual(2, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.LEFT), results[0][1])
        self.assertEqual(27, results[1][0])
        self.assertEqual(str(GameBoard.Action.RIGHT), results[1][1])
        
    def testEarlyMoveFailure(self):
        """
        If we have more than two pieces and we 
        call generateEarlyMoves, None should be
        returned
        """
        results = self.bot.generateEarlyMoves(3, [2,3,4])
        self.assertEqual(None, results)
        
    def testEarlySplit(self):
        """
        If we're on an early move and we have a 
        single piece of strength greater than
        one, we should split
        """
        self.bot.board.getPieceStrength = unittest.mock.Mock(return_value=2)
        results = self.bot.generateEarlyMoves(3, [4])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[0][1])
        
        
    def testInvalidSetup(self):
        """
        Check that an exception is thrown if  config is given
        in the wrong format
        """
        bot = bottyMcBotFace.BottyMcBotFace()
        config = "{version: 1.0}"
        with self.assertRaises(AttributeError):
            bot.setup(config)


    def testSignature(self):
        # Check that the expected signature is returned.

        sig = bottyMcBotFace.BottyMcBotFace.getSignature('debug')
        self.assertTrue(sig.endswith("b08d6cae6"))
        sig = bottyMcBotFace.BottyMcBotFace.getSignature('database')
        self.assertTrue(sig.endswith("64c45430f2"))
        sig = bottyMcBotFace.BottyMcBotFace.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))

    
    
    
