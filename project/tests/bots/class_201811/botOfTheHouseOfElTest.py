
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
from class_201811 import botOfTheHouseOfEl      # The Bot being tested.

import BotTestCase


class botElTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = botOfTheHouseOfEl.botEl()
        self.bot.setup(self.getGameConfig(), "Red")

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, botOfTheHouseOfEl.getBot())

    def loadTestState(self, name):
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, name)
        with open(boardStateFile, 'r') as stateFile:
            state = json.load(stateFile)
        resultsRaw = self.bot.makeMoves(state)
        return json.loads(resultsRaw)
        
    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        results = self.loadTestState('testBoardState.txt')

        self.assertEqual(1, len(results), "only one move -stay")
        self.assertEqual(3, len(results[0]), "moves have 3 parts")
        self.assertEqual(25, results[0][0], "I start at spot 25")
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1], "My start move is Stay")
        self.assertEqual(1, results[0][2], "The useless variable is 1")

    def KnownSplitMove(self, name):
        # Testing that the expected output comes from a move that
        # should include a Jump.

        # Read in a specific board state, and sbots/botOfTheHouseOfEl.py
        results = self.loadTestState(name)

        self.assertEqual(3, len(results), "should be 3 moves")
        self.assertEqual(3, len(results[0]), "each move is 3 long")
        self.assertEqual(str(GameBoard.Action.LEFT), results[0][1], "the first move is a LEFT")
        self.assertEqual(str(GameBoard.Action.RIGHT), results[1][1], "the second move is a RIGHT")
        self.assertEqual(str(GameBoard.Action.SPLIT), results[2][1], "the last move is a SPLIT")

    def testNoWrapMoves(self):
        self.KnownSplitMove('testBoardStateForBotEl.txt')
        
    def testLefWrapMoves(self):
        self.KnownSplitMove('testBoardState2ForBotEl.txt')
       
    def testRightWrapMoves(self):
        self.KnownSplitMove('testBoardState3ForBotEl.txt')
        
    def testAddSomeMoves(self):
        moves = []
        pieces = [0,1,2,98,99]
        moves.extend( self.bot.addSomeMoves(1, 3, "Action", 1, pieces) )
        self.assertEqual(len(moves), 3, "should have addded 3")
        self.assertEqual(3, len(moves[0]), "3 long entries")
        self.assertEqual("Action", moves[1][1], "move is just move")

    def testFindEdges(self):
        original = [0,1,2,98,99]
        left, right = self.bot.findEdges(original)
        self.assertEqual(left, 3, "left edge is at index 3")
        self.assertEqual(right, 2, "right edge is index 2")
        
    def testFindMiddle(self):
        middle = self.bot.findMiddle(3, 2, 5)
        self.assertEqual(middle, 0, "middle should  be index 0")
    
    def testSignature(self):
        # Check that the expected signature is returned.

        sig = botOfTheHouseOfEl.botEl.getSignature('debug')
        self.assertTrue(sig.endswith("68c799a7b3d1"))
        sig = botOfTheHouseOfEl.botEl.getSignature('database')
        self.assertTrue(sig.endswith("421d7539167e"))
        sig = botOfTheHouseOfEl.botEl.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
