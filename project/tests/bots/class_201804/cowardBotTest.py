
# import os
# import json
# import GameBoard
import cowardBot
import unittest
from unittest.mock import MagicMock

import BotTestCase
from cowardBot import BIG_ENOUGH_TO_SPLIT

START_LOC = 25
ENEMY_LOC = 74
BOARD_SIZE = 100
MAX_TURNS = 1000


class cowardBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = cowardBot.CowardBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, cowardBot.getBot())

    def testDistance(self):
        # Test all cases of abs
        cases = [((10, 20), 10), ((5, 95), 10)]

        self.bot.boardSize = BOARD_SIZE
        msg = "Fail"
        for (a, b), c in cases:
            self.assertEqual(self.bot.distance(a, b), c, msg)
            self.assertEqual(self.bot.distance(b, a), c, msg)

    def testGenerateMoves(self):
        pieceMock = MagicMock(return_value=[START_LOC])
        enemyMock = MagicMock(return_value=[ENEMY_LOC])
        boardMock = MagicMock(return_value=BOARD_SIZE)
        strenMock = MagicMock(return_value=BIG_ENOUGH_TO_SPLIT)

        self.bot.board.findMyPieces = pieceMock
        self.bot.board.findOpponentPieces = enemyMock
        self.bot.boardSize = boardMock
        self.bot.board.getPieceStrength = strenMock

        with self.subTest("call safe if enemy is far (strength high)"):
            moves = self.bot.generateMoves(1, MAX_TURNS)
            self.assertEqual(moves, [[25, 'Split', 1]])

        with self.subTest("call safe if enemy is far (strength low)"):
            strenMock = MagicMock(return_value=BIG_ENOUGH_TO_SPLIT-1)
            self.bot.board.getPieceStrength = strenMock

            moves = self.bot.generateMoves(1, MAX_TURNS)
            self.assertEqual(moves, [[25, 'Stay', 1]])

        with self.subTest("flee right if enemy is close on left"):
            enemyMock = MagicMock(return_value=[START_LOC-1])
            self.bot.board.findOpponentPieces = enemyMock

            moves = self.bot.generateMoves(1, MAX_TURNS)
            self.assertEqual(moves, [[START_LOC, 'Right', 1]])

        with self.subTest("flee left if enemy is close on right"):
            enemyMock = MagicMock(return_value=[START_LOC+1])
            self.bot.board.findOpponentPieces = enemyMock

            moves = self.bot.generateMoves(1, MAX_TURNS)
            self.assertEqual(moves, [[START_LOC, 'Left', 1]])

    def testGetSignature(self):
        ''' Test the 3 possiblities for getSignature. '''
        sigLength = len(self.bot.getSignature(challenge='debug'))
        self.assertEqual(sigLength, 64, "Signature wrong length.")

        sigLength = len(self.bot.getSignature(challenge='database'))
        self.assertEqual(sigLength, 64, "Signature wrong length.")

        otherOutput = self.bot.getSignature(challenge=None)
        failMsg = "Didnt''t return correct string."
        self.assertEqual("Default bot signature", otherOutput, failMsg)


if __name__ == '__main__':
    unittest.main()
