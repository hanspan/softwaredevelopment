
import os
import logging
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import musketeerBot   # The Bot being tested.

import BotTestCase

class musketeerBotTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = musketeerBot.MusketeerBot(level = logging.DEBUG)
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, musketeerBot.getBot())
        
    def testNeighbours(self):
        # Everybody needs good tests, for the function neighbours.
        tileList = [3, 4, 6, 8, 9]
        tile = 5   
        self.assertEqual((True, True), self.bot.neighbours(tile, tileList))
        tile = 4
        self.assertEqual((True, False), self.bot.neighbours(tile, tileList))
        tile = 2
        self.assertEqual((False, True), self.bot.neighbours(tile, tileList))
        tile = 0
        self.assertEqual((False, False), self.bot.neighbours(tile, tileList))
        
    def testDist(self):
        # Test the dist function
                
        self.assertGreater(self.bot.board.boardSize, 1, "Board size must be at least 2.")
        
        # Because we want to allow for small board sizes, 
        # we only test the edges
        # which we know must be there
        self.assertEqual(self.bot.dist(0, 1), 1)
        
        # Test the wrap around calculation
        self.assertEqual(self.bot.dist(0, self.bot.board.boardSize - 1), 1)
        
    def testGoodSplit(self):
        # Test the goodMove function.
        # NOTE: the logic behind this may change, 
        # so we are only testing that true or false is returned. 
        
        self.assertTrue(type(self.bot.goodMove(25)) is bool)
        
    def testFindWantedTiles(self):
        # This function is basically a wrapper for
        # the addTiles function, so we just test
        # the overall functionality.
        # The function is agnostic about boardSize
        # so we don't worry about the config
        
        defensiveSpots = [1, 52]
        growthSpots = [32, 64]
        pieces = [1, 33, 34, 35, 36]
        opponentPieces = [23, 24, 25]
        
        output = musketeerBot.findWantedTiles(defensiveSpots, growthSpots, pieces, opponentPieces)
        self.assertEqual(output, [32, 64, 52, 24, 25, 23])
        
    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        self.assertEqual(1, results[0][2])

    def testKnownJumpMoves(self):
        # Testing that the expected output comes from a move that
        # should include a Jump Left and a Jump Right

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardJumpsForMusketeerBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), results[1][1])
        self.assertEqual(str(GameBoard.Action.JUMP_RIGHT), results[4][1])
        
    def testGoodSplitMove(self):
        # Testing that the expected output comes from a move that
        # should have three splits

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardGoodSplitForMusketeerBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(14, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(24, results[1][0])
        self.assertEqual(25, results[2][0])
        self.assertEqual(26, results[3][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[2][1])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[3][1])
        
    def testLateWaitMove(self):
        # Testing that the expected output comes from a move that
        # should stay, since we are near the end of the game

        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardLateWaitForMusketeerBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(99, len(results))

        for i in range(99):
            self.assertEqual(str(GameBoard.Action.STAY), results[i][1])

        
    def testInvalidInput(self):
        config = b'Not a config...'
        
        self.assertRaises(json.decoder.JSONDecodeError, self.bot.setup, config)

    def testSignature(self):
        # Check that the expected signature is returned.

        sig = musketeerBot.MusketeerBot.getSignature('debug')
        self.assertTrue(sig.endswith("9d55dc52"))
        sig = musketeerBot.MusketeerBot.getSignature('database')
        self.assertTrue(sig.endswith("321e7a3e"))
        sig = musketeerBot.MusketeerBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
