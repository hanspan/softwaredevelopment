import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
# import jack_seperation
import jack_SDLish
# The Bot being tested.

from unittest.mock import Mock

import BotTestCase


class Jack_SDLishTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = jack_SDLish.jackBotSep()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, jack_SDLish.getBot())

   
    ## mine ##
    
    def testExpectedMoveJumpLeft(self):  # given known state - the bot generates the moves you expect.... testing a bit of that
        """blah """
        
     

        self.bot.board.getPieceStrength = Mock(return_value=7)
        
        result = self.bot.splitMoves(0,1,3,1)
        self.assertEqual(3, len(result))
        self.assertEqual(str(GameBoard.Action.JUMP_LEFT), result[1])
        self.assertEqual(6, result[2])
        

    def testExpectedMoveRight(self): # test generateMove fn
        self.bot.board.getPieceStrength = Mock(return_value=2)
        
        result = self.bot.splitMoves(1,1,3,1)
        self.assertEqual(3, len(result))
        self.assertEqual(str(GameBoard.Action.RIGHT), result[1])
        self.assertEqual(3, result[2])


    

        
        
