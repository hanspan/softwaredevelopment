
#import unittest       # Bring in Python's built-in unit testing functionality.
import sexyBot
import BotTestCase
import GameBoard

class SexyBotTestCase(BotTestCase.BotTestCase):
    """Validate that the BotTom functions work as intended"""
    
    # Common setup for the tests in this class.
    def setUp(self):
        self.bot = sexyBot.SexyBot()
        self.bot.setup(self.getGameConfig())
    
    def testGetBot(self):
        self.assertIsInstance(sexyBot.getBot(), sexyBot.SexyBot)
           
    def testSetRegionsLeftToRight(self):
        self.bot.homeSize = 1 
        self.bot.myStart = 9
        self.bot.oppStart = 5
        self.bot.board.boardSize = 12
        self.assertEqual(self.bot.setHomeRegion(), [8, 9, 10] )
        self.assertEqual(self.bot.setLeftRegion(), [6, 7] )
        self.assertEqual(self.bot.setRightRegion(), [11, 0, 1, 2, 3, 4] )
        self.assertEqual(self.bot.setNeutralRegion(), [5] )
        
    def testSetRegionsRightToLeft(self):
        self.bot.homeSize = 1 
        self.bot.myStart = 5
        self.bot.oppStart = 9
        self.bot.board.boardSize = 12
        
        self.assertEqual(self.bot.setHomeRegion(), [4, 5, 6] )
        self.assertEqual(self.bot.setLeftRegion(), [10, 11, 0, 1, 2, 3] )
        self.assertEqual(self.bot.setRightRegion(), [7, 8] )
        self.assertEqual(self.bot.setNeutralRegion(), [9] )
        
    def testExecuteMove(self):
        self.assertEqual(GameBoard.Action.STAY, self.bot.executeMove(0, 'AllSoldiersStay', 0))
