
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import unittest.mock  # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
from class_201804 import guybrush      # The Bot being tested.

import BotTestCase


class guybrushTestHarness(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = guybrush.getBot()
        self.bot.setup(self.getGameConfig(), "Red")

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, guybrush.getBot())

    def testKnown_EarlyState(self):
        # This is an early game position: test forthcoming behaviour
        #Includes village behaviour and an attacker

        # Read in a standard board state, and see what moves result.
        #Set initial variables
        self.bot.home=25
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'guybrush/boardRed.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = json.load(stateFile)
        resultsRaw = self.bot.makeMoves(state)
        results = json.loads(resultsRaw)
        correctAnswers = {
            22: [22,str(GameBoard.Move.LEFT),1],
            23: [23,str(GameBoard.Move.LEFT),1],
            24: [24,str(GameBoard.Move.STAY),1],
            25: [25,str(GameBoard.Move.STAY),1],
            26: [26,str(GameBoard.Move.STAY),1],
            27: [27,str(GameBoard.Move.RIGHT),1],
            28: [28,str(GameBoard.Move.RIGHT),1],
            20: [20,str(GameBoard.Move.LEFT),1],
            21: [21,str(GameBoard.Move.LEFT),1],
            29: [29,str(GameBoard.Move.RIGHT),1],
            11: [11,str(GameBoard.Move.LEFT),1],
            12: [12,str(GameBoard.Move.LEFT),1],
            13: [13,str(GameBoard.Move.LEFT),1],
            15: [15,str(GameBoard.Move.LEFT),1],
            30: [30,str(GameBoard.Move.RIGHT),1],
            40: [40,str(GameBoard.Move.RIGHT),1],
            59: [59,str(GameBoard.Move.RIGHT),1],
            69: [69,str(GameBoard.Move.RIGHT),1],
        }
        # Resolve ordering issue
        self.assertEqual(18, len(results))
        for i in range(len(results)):
            row = results[i]
            self.assertEqual(row, correctAnswers[row[0]])
        
            
    def testKnown_InitialMoves(self):
        # Testing initial behaviour
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = json.load(stateFile)
        resultsRaw = self.bot.makeMoves(state)
        results = json.loads(resultsRaw)

        correctAnswers = [
            [25,str(GameBoard.Move.STAY),1]
        ]
        self.assertEqual(results,correctAnswers)

    def testKnown_GrowthMove(self):
        # Testing initial behaviour
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'guybrush/turn2.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = json.load(stateFile)
        resultsRaw = self.bot.makeMoves(state)
        results = json.loads(resultsRaw)

        correctAnswers = [
            [0,str(GameBoard.Move.STAY),1],
            [1,str(GameBoard.Move.STAY),1],
            [2,str(GameBoard.Move.STAY),1],
            [3,str(GameBoard.Move.STAY),1],
            [4,str(GameBoard.Move.STAY),1],
            [5,str(GameBoard.Move.STAY),1],
            [6,str(GameBoard.Move.STAY),1],
            [7,str(GameBoard.Move.STAY),1],
            [8,str(GameBoard.Move.STAY),1],
            [9,str(GameBoard.Move.STAY),1],
            [10,str(GameBoard.Move.STAY),1],
            [11,str(GameBoard.Move.STAY),1],
            [12,str(GameBoard.Move.STAY),1],
            [13,str(GameBoard.Move.STAY),1],
            [14,str(GameBoard.Move.STAY),1],
            [15,str(GameBoard.Move.STAY),1],
            [16,str(GameBoard.Move.STAY),1],
            [17,str(GameBoard.Move.STAY),1],
            [18,str(GameBoard.Move.STAY),1],
            [19,str(GameBoard.Move.STAY),1],
            [20,str(GameBoard.Move.STAY),1],
            [21,str(GameBoard.Move.STAY),1],
            [22,str(GameBoard.Move.LEFT),1],
            [23,str(GameBoard.Move.LEFT),1],
            [24,str(GameBoard.Move.STAY),1],
            [25,str(GameBoard.Move.STAY),1],
        ]
        self.assertEqual(26, len(results))
        for i in range(len(results)):
            row = results[i]
            self.assertEqual(row, correctAnswers[row[0]])
        
    def testKnown_ThirdMove(self):
        # Testing initial behaviour
        # Read in a specific board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'guybrush/turn3.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = json.load(stateFile)
        resultsRaw = self.bot.makeMoves(state)
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Move.SPLIT), results[0][1])

    def testSignature(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature()
        self.assertTrue(sig.endswith("guyBrush Bot 180209"))
        

    def testDebugExercise(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature("debug")
        self.assertTrue(sig.endswith("54ef7e2ae9dcf4ebedca0fd5562cf6d9"))
        

    def testDatabaseExercise(self):
        # Check that the expected signature is returned.
        sig = self.bot.getSignature("database")
        self.assertTrue(sig.endswith("2ca261f09f9f8f1a76ab97ce5d010b4a"))
        

