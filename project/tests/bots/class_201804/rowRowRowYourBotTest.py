'''
Unit tests for RowRowRowYourBot
'''
import os
import unittest.mock  # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.

import GameBoard      # Information on game board properties.
import rowRowRowYourBot      # The Bot being tested.

import BotTestCase


class RowRowRowYourBotTestCase(BotTestCase.BotTestCase):
    '''
    TestCase for RowRowRowYourBot
    '''

    def setUp(self):
        '''
        Common setup for the tests in this class.
        '''
        self.bot = rowRowRowYourBot.RowRowRowYourBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        '''
        Make sure a bot is returned from the getBot() function.
        '''
        self.assertNotEqual(None, rowRowRowYourBot.getBot())

    def testKnownStayMove(self):
        '''
        Testing that the expected output comes from a move that
        should include a Stay.

        Read in a specific board state, and see what moves result.
        '''
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'testBoardStateForRandomBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()

        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

    def testNearestEnemyNormal(self):
        '''
        Testing that the findNearestEnemy function works and correctly
        locates the nearest enemy.

        This tests the "normal" case i.e. no wraparound
        '''
        self.bot.board.findOpponentPieces = unittest.mock.MagicMock()
        self.bot.board.findOpponentPieces.return_value = [40, 45, 50, 55]

        nearestEnemy = self.bot.findNearestEnemy(90)
        self.assertEqual(2, len(nearestEnemy))
        self.assertEqual(55, nearestEnemy[0])
        self.assertEqual(-35, nearestEnemy[1])

    def testNearestEnemyWrap(self):
        '''
        Testing that the findNearestEnemy function works and correctly
        locates the nearest enemy.

        This tests the case where we wraparound the board
        '''
        self.bot.board.findOpponentPieces = unittest.mock.MagicMock()
        self.bot.board.findOpponentPieces.return_value = [40, 45, 50, 1]

        with (self.subTest("Testing wrapping right")):
            nearestEnemy = self.bot.findNearestEnemy(90)
            self.assertEqual(2, len(nearestEnemy))
            self.assertEqual(1, nearestEnemy[0])
            self.assertEqual(11, nearestEnemy[1])

        with (self.subTest("Testing wrapping left")):
            self.bot.board.findOpponentPieces.return_value = [40, 45, 50, 90]
            nearestEnemy = self.bot.findNearestEnemy(10)
            self.assertEqual(2, len(nearestEnemy))
            self.assertEqual(90, nearestEnemy[0])
            self.assertEqual(-20, nearestEnemy[1])

    def testStayOnGrowthSquare(self):
        '''
        Testing that we stay on a growth square

        Read in a specific board state, and see what moves result.
        '''
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'testBoardStateForRowRowRowYourBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()

        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)
        self.assertEqual(3, len(results[0]))
        self.assertEqual(40, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])

    @unittest.mock.patch('random.random', unittest.mock.Mock(return_value=0.1))
    def testSplitMove(self):
        '''
        Testing that we get a SPLIT when we expect

        Read in a specific board state, and see what moves result.
        '''
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'testBoardStateForRowRowRowYourBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()

        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(3, len(results[0]))
        self.assertEqual(41, results[1][0])
        self.assertEqual(str(GameBoard.Action.SPLIT), results[1][1])

    def testLeftRightMove(self):
        '''
        Testing that we get a LEFT/RIGHT when we expect

        Read in a specific board state, and see what moves result
        '''
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir,
                                      'testBoardStateForRowRowRowYourBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()

        resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        results = json.loads(resultsRaw)

        self.assertEqual(3, len(results[0]))
        self.assertEqual(41, results[1][0])
        with (self.subTest('Testing move RIGHT')):
            self.assertEqual(str(GameBoard.Action.RIGHT), results[1][1])
        with (self.subTest('Testing move LEFT')):
            self.assertEqual(str(GameBoard.Action.LEFT), results[2][1])

    def testSignature(self):
        '''
        Check that the expected signature is returned.
        '''
        sig = rowRowRowYourBot.RowRowRowYourBot.getSignature('debug')
        self.assertTrue(sig.endswith("a37aa721e275"))
        sig = rowRowRowYourBot.RowRowRowYourBot.getSignature('database')
        self.assertTrue(sig.endswith("860c99d7ba07"))
        sig = rowRowRowYourBot.RowRowRowYourBot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
