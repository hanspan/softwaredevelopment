
#import unittest       # Bring in Python's built-in unit testing functionality.
from class_201804 import youreGonnaNeedABiggerBot
import BotTestCase
import GameBoard

class YoureGonnaNeedABiggerBotTestCase(BotTestCase.BotTestCase):
    """Validate that the functions work as intended"""
    
    # Common setup for the tests in this class.
    def setUp(self):
        self.bot = youreGonnaNeedABiggerBot.YoureGonnaNeedABiggerBot()
        self.bot.setup(self.getGameConfig(), "Red")
    
    def testGetBot(self):
        self.assertIsInstance(youreGonnaNeedABiggerBot.getBot(), youreGonnaNeedABiggerBot.YoureGonnaNeedABiggerBot)
           
    def testFMagic(self):
        self.bot.myStart = 25
        self.bot.board.boardSize = 100
        self.assertEqual(self.bot.fMagic(25, 1), GameBoard.Move.SPLIT)
        self.assertEqual(self.bot.fMagic(25, 100), GameBoard.Move.STAY)

    def testGenerateMoves(self):
        self.bot.myStart = 74
        self.assertEqual(self.bot.generateMoves(2, 1000), [])

    def testUsePiecesAndTurn(self):
        self.bot.myStart = 74
        self.assertEqual(self.bot.usePiecesAndTurn([74], 1), [[74, str(GameBoard.Move.SPLIT), 1]])
        self.assertEqual(self.bot.usePiecesAndTurn([74], 2), [[74, str(GameBoard.Move.STAY), 1]])
              
    def testConvertMoveID(self):
        self.assertEqual(youreGonnaNeedABiggerBot.convertMoveID(0), GameBoard.Move.STAY)
        self.assertEqual(youreGonnaNeedABiggerBot.convertMoveID(1), GameBoard.Move.LEFT)
        self.assertEqual(youreGonnaNeedABiggerBot.convertMoveID(2), GameBoard.Move.RIGHT)
        self.assertEqual(youreGonnaNeedABiggerBot.convertMoveID(3), GameBoard.Move.SPLIT)
    
    def testDebugSignature(self):
        sig = self.bot.getSignature('debug')
        self.assertTrue(sig.startswith("unknown"))

    def testDatabaseSignature(self):
        sig = self.bot.getSignature('database')
        self.assertTrue(sig.startswith("0e3bbba70746"))
    
    def testWrongSignature(self):
        sig = self.bot.getSignature('not a challenge')
        self.assertTrue(sig.startswith("Default"))
