
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
import grimreaperBot      # The Bot being tested.
from unittest.mock import MagicMock

import BotTestCase


class grimreaperBotTestCase(BotTestCase.BotTestCase):
    def getGameConfig(self):
        # There are a couple of hurdles that need to be addressed in
        # taking a basic config file and converting it into something
        # that can be used to set up a Bot.
        #This is a local verself.bot.board.getPieceStrength = MagicMock(return_value=10)ion to use my own config file
        configFilePath = os.path.join(self.getTestDataDir(), 'testgrimreaperConfig.cfg')
        with open(configFilePath, 'r') as configFile:
            config = configFile.read()

        # Convert to JSON so that a starting player can be added.
        serverConfig = json.loads(config)
        serverConfig["player"] = "Red"

        # Return the appropriate format for use by BotBase.setup()
        return json.dumps(serverConfig).encode("utf-8")
    
    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = grimreaperBot.grimreaperBot()
        self.bot.setup(self.getGameConfig())

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.
        self.assertNotEqual(None, grimreaperBot.getBot())
        
    def testgetDistance(self):
        
        self.assertEqual(20, self.bot.getDistance(40, 60))
        self.assertEqual(4, self.bot.getDistance(98, 2))
        
    def testgetDirection(self):
        self.assertEqual('RIGHT', self.bot.getDirection(40, 60))
        self.assertEqual('LEFT', self.bot.getDirection(60, 40))
        self.assertEqual('RIGHT',self.bot.getDirection(98, 4))
         
    def testgetNearestTileInSet(self):
        testSet=[1,50,90]
        self.assertEqual((3,1,'RIGHT'),self.bot.getNearestTileInSet(98, testSet))
        self.assertEqual((15, 90, 'RIGHT'),self.bot.getNearestTileInSet(75, testSet))
        self.assertEqual((20, 50, 'LEFT'),self.bot.getNearestTileInSet(70, testSet))
       
    def testjumpToTile(self):  
        # Read in a specific board state, and test move.
        #mock the return value for getPieceStrength
        self.bot.board.getPieceStrength = MagicMock(return_value=12)
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForGrimreaperBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read()
        self.bot.jumpToTile(5, 15)
        self.assertEqual((GameBoard.Action.JUMP_RIGHT, 10), self.bot.jumpToTile(5, 15))
        
    def testgetNormalMove(self):
        self.bot.board.getPieceStrength = MagicMock(return_value=12)
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardStateForGrimreaperBot.txt')
        with open(boardStateFile, 'r') \
                as stateFile:
            state = stateFile.read() 
        self.assertEqual(GameBoard.Action.SPLIT, self.bot.getNormalMove(5, 12)[0])
       
    
