
import os
import unittest       # Bring in Python's built-in unit testing functionality.
import json           # Process JSON to/from strings, etc.
import GameBoard      # Information on game board properties.
from class_201811 import rushNMedal     # The Bot being tested.
import random
import sys

import BotTestCase


class rushNMedalTestCase(BotTestCase.BotTestCase):

    def setUp(self):
        # Common setup for the tests in this class.

        self.bot = rushNMedal.RushNMedal()
        self.bot.setup(self.getGameConfig(), "Red")
#         print(dir(self.bot), file=sys.stderr)
#         print(self.bot.currTurn, file=sys.stderr)

    def testGetBot(self):
        # Make sure a bot is returned from the getBot() function.

        self.assertNotEqual(None, rushNMedal.getBot())

    def testKnownMove(self):
        # This is a "last known good state" test. By itself, it doesn't
        # really indicate any problems. However, it establishes a known
        # input/output matching set, and will highlight if something
        # changes that result.

        # Read in a standard board state, and see what moves result.
        testDataDir = self.getTestDataDir()
        boardStateFile = os.path.join(testDataDir, 'testBoardState.txt')
        with open(boardStateFile, 'r') as stateFile:
            state = json.load(stateFile)
#         print(state, file=sys.stderr)
        resultsRaw = self.bot.makeMoves(state)
        results = json.loads(resultsRaw)
#         print(self.bot.currTurn, file=sys.stderr)
#         print(results, file=sys.stderr)
        self.assertEqual(1, len(results))
        self.assertEqual(3, len(results[0]))
        self.assertEqual(25, results[0][0])
        self.assertEqual(str(GameBoard.Action.STAY), results[0][1])
        
    def generateState(self, bluePositions, redPositions, strengths, currTurn, maxTurns):
        '''
        This is a wrapper to generate a board state
        You must give it a list of blue positions, a list of red positions, a dictionary of strengths and the turn information
        E.g.:
        self.generateState([1,2],[17,21], {1:2, 2:4, 17:10, 21:20}, 1, 1000)
        It returns a dict that probably wants to be stringed
        '''
        assert(set(bluePositions).intersection(redPositions) == set())
        assert(max(set(bluePositions).union(redPositions)) < self.bot.board.boardSize)
        assert(set(bluePositions).union(redPositions) == strengths.keys())
        assert(max(strengths.values()) <= self.bot.board.maxTileStrength)
        retDict = dict()
        retDict["control"] = ['Neutral'] * self.bot.board.boardSize
        for piece in bluePositions:
            retDict["control"][piece] = 'Blue'
        for piece in redPositions:
            retDict["control"][piece] = 'Red'
        retDict["strengths"] = [0] *  self.bot.board.boardSize
        for piece in strengths:
            retDict["strengths"][piece] = strengths[piece]
        retDict["currTurn"] = currTurn
        retDict["maxTurns"] = maxTurns
        retDict["random"] = 1
#         print(retDict, file=sys.stderr)
        return retDict
    def testFindDist(self):
        '''
        This tests the findDist function
        It should return the distance between 2 points
        '''
#         testDataDir = self.getTestDataDir()
#         boardStateFile = os.path.join(testDataDir, 'testBoardStateForRandomBot.txt')
#         with open(boardStateFile, 'r') as stateFile:
#              state = stateFile.read()
#          resultsRaw = self.bot.makeMoves(state.encode("utf-8"))
        assert(self.bot.findDist(2,4) == 2)
        assert(self.bot.findDist(2,self.bot.board.boardSize-1) == 3)
        with self.assertRaises(AssertionError):
            self.bot.findDist(-1,0)
        with self.assertRaises(AssertionError):
            self.bot.findDist(self.bot.board.boardSize,0)
        with self.assertRaises(AssertionError):
            self.bot.findDist(0,1.5)
        with self.assertRaises(AssertionError):
            self.bot.findDist(0,None)
        for _ in range(30):
            item1 = random.randint(0,self.bot.board.boardSize-1)
            diff = random.randint(0,self.bot.board.boardSize//2)
            item2 = (item1 + diff) % self.bot.board.boardSize
            assert(self.bot.findDist(item1,item2) == self.bot.findDist(item2,item1) == diff)



    def testFindClosestFriend(self):
        '''
        Find the closest enemy to a board square
        If the second input is true, ignore pieces on the specified square
        '''
        state = self.generateState([1,2],[17,21], {1:2, 2:4, 17:10, 21:20}, 1, 1000)
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        #resultsRaw = json.dumps(state)
        results = json.loads(resultsRaw)
        assert (self.bot.findClosestFriendly(16) == 17)
        assert (self.bot.findClosestFriendly(17) == 17)
        assert (self.bot.findClosestFriendly(17, True) == 21)
    def testFindClosestEnemy(self):
        '''
        Find the closest enemy to a board square
        '''
        state = self.generateState([1,2],[17,21], {1:2, 2:4, 17:10, 21:20}, 1, 1000)
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        #resultsRaw = json.dumps(state)
        results = json.loads(resultsRaw)
        assert (self.bot.findClosestEnemy(5) == 2)
        assert (self.bot.findClosestEnemy(self.bot.board.boardSize-1) == 1)
    def testFindMostVulnerable(self):
        '''
        return a list of friendly characters who are the shortest distance form an enemy
        '''
        goodGuys = [1,5,11]
        badGuys = [8,14]
        assert(set(self.bot.findMostVulnerable(goodGuys, badGuys)) == set([5,11]))
        badGuys = [8,13]
        assert(set(self.bot.findMostVulnerable(goodGuys, badGuys)) == set([11]))
        badGuys = list(range(10))[::2]
        goodGuys = list(range(10))[1::2] + [30]
        assert(set(self.bot.findMostVulnerable(goodGuys, badGuys)) == set(list(range(10))[1::2]))
    def testDeathModeBehaviourBoredom(self):
        '''
        Verify that the bot exits death mode after a while
        If turnsOfDeath <= 0, stop
        '''
        state = self.generateState([1,2],[17,21], {1:2, 2:4, 17:10, 21:20}, 1, 1000)
        self.bot.deathmode = True
        self.bot.turnsOfDeath = 1
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert (self.bot.deathmode == False)
    def testDeathModeBehaviourInferiority(self):
        '''
        Verify that if we have no big dudes we stop death mode
        '''
        state = self.generateState([1,2],[17,21], {1:2, 2:4, 17:2, 21:2}, 1, 1000)
        self.bot.deathmode = True
        self.bot.turnsOfDeath = 10
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert (self.bot.deathmode == False)
    def testDeathModeBehaviourKeepFighting(self):
        '''
        Verify that if we have a big dude and time left we continue in death mode
        '''
        state = self.generateState([1,2],[17,21], {1:2, 2:4, 17:self.bot.board.maxTileStrength, 21:2}, 1, 1000)
        self.bot.deathmode = True
        self.bot.turnsOfDeath = 10
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert (self.bot.deathmode == True)
    def testDeathModeBehaviourEnterDeathmode(self):
        '''
        Verify that if half the guys are maximal we enter death mode
        '''
        goodGuys = [4]
        strengthDict = {1:10, 2:20, 4:2}
        for piece in range(self.bot.board.boardSize//2, self.bot.board.boardSize):
            goodGuys.append(piece)
            strengthDict[piece] = self.bot.board.maxTileStrength
        self.bot.deathmode = False
        state = self.generateState([1,2],goodGuys, strengthDict, 100, 1000)
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert (self.bot.deathmode == True)
    def testDetectMiddleRunners(self):
        '''
        we want to check that we find runs that are spaced by 3 away from the original space and terminated by 3+ 0s
        Good things look like [00]1001001000
        Bad things look like [00]10010011 or [00]100100101
        We fire this in both directions 
        '''
        self.bot.originalPiece = 20
        inputs = [[10, 13,16,19,21, 24], [12,15,18,22,25], [11,14,17,22,24], [12,13,17,23,26], [12,13,17,23,26, 27], [16,24]]
        outputs = [[13,16,19,21, 24], [12,15,18,22,25], [11,14,17],      [23,26],          [],                    []]
        badGuys = [1,2,3]
        for i in range(len(inputs)):
            input = inputs[i]
            strengthDict = dict()
            for piece in (input + badGuys):
                strengthDict[piece] = 1 
            state = self.generateState(badGuys,input, strengthDict, 1, 1000)
            my_json = state
            resultsRaw = self.bot.makeMoves(my_json)
            results = json.loads(resultsRaw)
#             if (set(self.bot.detectMiddleRunners(self.bot.originalPiece)) != set(outputs[i])):
#                 print(input, self.bot.detectMiddleRunners(self.bot.originalPiece), outputs[i], file=sys.stderr)
        
    def testFindDirectionToLocation(self):
        '''
        Test the function which decides which direction to move in to reach its direction
        Notable edge cases:
            Already at home: STAY
            Equidistant in both directions: STAY
        '''
        assert(self.bot.findDirectionToLocation(0,0) == GameBoard.Action.STAY)
        assert(self.bot.findDirectionToLocation(0,1) == GameBoard.Action.RIGHT)
        assert(self.bot.findDirectionToLocation(1,0) == GameBoard.Action.LEFT)
        assert(self.bot.findDirectionToLocation(0,self.bot.board.boardSize-1) == GameBoard.Action.LEFT)
        assert(self.bot.findDirectionToLocation(self.bot.board.boardSize-1,0) == GameBoard.Action.RIGHT)
        if self.bot.board.boardSize % 2 == 0:
            assert(self.bot.findDirectionToLocation(0,self.bot.board.boardSize//2) == GameBoard.Action.STAY)
        for _ in range(5):
            item1 = random.randint(0,self.bot.board.boardSize-1)
            diff = random.randint(1,(self.bot.board.boardSize-1)//2-1)
            item2 = (item1 + diff) % self.bot.board.boardSize
            assert(self.bot.findDirectionToLocation(item1,item2) == GameBoard.Action.RIGHT)
            assert(self.bot.findDirectionToLocation(item2,item1) == GameBoard.Action.LEFT)
    
    def testFindCenterOfMass(self):
        state = self.generateState([1,2],[17,21], {1:2, 2:4, 17:3, 21:2}, 1, 1000)
        self.bot.deathmode = True
        self.bot.turnsOfDeath = 10
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert(self.bot.findCenterOfMass(True) == 19)
        assert(self.bot.findCenterOfMass(False) in [1,2])
        state = self.generateState([1,2],[10,21,23,24], {1:2, 2:4, 10:1, 21:2, 23:1, 24:1}, 1, 1000)
        self.bot.deathmode = True
        self.bot.turnsOfDeath = 10
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert(self.bot.findCenterOfMass(True) == 21)
        
    def testSignature(self):
        # Check that the expected signature is returned.
        sig = rushNMedal.RushNMedal.getSignature("incorrect string")
        self.assertTrue(sig.endswith("signature"))
        sig = rushNMedal.RushNMedal.getSignature("debug")
        self.assertTrue(sig.endswith("e3e6d823"))
        sig = rushNMedal.RushNMedal.getSignature("database")
        self.assertTrue(sig.endswith("cb9d958cf200"))
    def testDoEarlyTurnsFour(self):
        self.bot.originalPiece = 18
        state = self.generateState([1,2],[17,18,19], {1:2, 2:4, 17:3,18:10,19:2}, 4, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        desiredResults = {17:'Left',18:'Stay', 19:'Right'}
        for move in results:
            if move[0] in desiredResults:
                assert(move[1] == desiredResults[move[0]])    
    def testDoEarlyTurnsOne(self):
        self.bot.originalPiece = 22
        goodGuys = [17,18,19]
        state = self.generateState([1,2], goodGuys, {1:2, 2:4, 17:3,18:10,19:2}, 1, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        assert self.bot.originalPiece == min(goodGuys)
        desiredResults = {17:'Stay',18:'Stay', 19:'Stay'}
        for move in results:
            if move[0] in desiredResults:
                assert(move[1] == desiredResults[move[0]])
    def testDoEarlyTurnsSeven(self):
        self.bot.originalPiece = 18
        goodGuys = [16,18,20]
        state = self.generateState([1,2], goodGuys, {1:2, 2:4, 16:3,18:4,20:3}, 7, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        desiredResults = {16:'Split',18:'Stay', 20:'Split'}
        for move in results:
            if move[0] in desiredResults:
                assert(move[1] == desiredResults[move[0]])
    def testDoEarlyTurnsFourteen(self):
        self.bot.originalPiece = 18
        goodGuys = [15,16,17,18,19,20,21]
        state = self.generateState([1,2], goodGuys, {1:2, 2:4, 15:7,16:7,17:10,18:5,19:10,20:7,21:7}, 14, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        desiredResults = {15:"Right", 16:'Stay', 17:'Left', 18:'Stay', 19:"Right", 20:'Stay', 21:'Left'}
        for move in results:
            if move[0] in desiredResults:
                assert(move[1] == desiredResults[move[0]])
#         print(results)
    def testDoEarlyTurnsFifteen(self):
        self.bot.originalPiece = 18
        goodGuys = [16,18,20]
        state = self.generateState([68,69], goodGuys, {68:2, 69:4, 16:25,18:5,20:25}, 15, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        desiredResults = {16:["Jump_Left", 23], 20:['Jump_Right',23]}
        for move in results:
            if move[0] in desiredResults:
                assert(move[1] == desiredResults[move[0]][0])
                assert(move[2] == desiredResults[move[0]][1])                    
    def testOriginalPieceTurnThree(self):
        self.bot.originalPiece = 18
        goodGuys = [18]
        state = self.generateState([68,69], goodGuys, {68:2, 69:4, 18:3}, 3, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        desiredResults = {18:"Split"}
        for move in results:
            if move[0] in desiredResults:
                assert move[1] == desiredResults[move[0]] 
    def testDetectMiddleRunnersVanilla(self):
        goodGuys = [10,13,16,18,20,23]
        state = self.generateState([68,69], goodGuys, {68:2, 69:4, 10:1,13:1,16:1,18:5,20:1, 23:1}, 16, 1000)
        self.bot.deathmode = False
        my_json = state
        resultsRaw = self.bot.makeMoves(my_json)
        results = json.loads(resultsRaw)
        print(self.bot.detectMiddleRunners(20))
#     def testDoEarlyTurnsSixteenRunner(self):
#         self.bot.originalPiece = 18
#         goodGuys = [10,13,16,18,20,23]
#         state = self.generateState([68,69], goodGuys, {68:2, 69:4, 10:1,13:1,16:1,18:5,20:1, 23:1}, 16, 1000)
#         self.bot.deathmode = False
#         my_json = state
#         resultsRaw = self.bot.makeMoves(my_json)
#         results = json.loads(resultsRaw)
#         desiredResults = {16:"Left", 20:'Right'}
#         print(results)
#         for move in results:
#             if move[0] in desiredResults:
#                 assert(move[1] == desiredResults[move[0]])
#         print(results)
