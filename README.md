
# Software Development for Mathematicians

Welcome to the course!

The general schedule is 5 days, with each day broken up into two
sessions. Each session starts off with a discussion/lecture on a
particular topic, and then the majority of the time is spent on
exercises relating that topic.

The presentations/ subdirectory contains copies of the slides
used during the lectures.

The exercises/ subdirectory contains the exercises themselves,
broken up by session, as well as the code for the Bot vs Bot
project that is used to tie the various topics together.

Keep in mind: the entire class is stored in git, so if there are
errors or tips, *you* have the power to submit a pull request to
make corrections and improvements! 


![Random vs Random](RandomVRandom.png "A battle between two randomBots")
