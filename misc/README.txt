
---------------------------------------------------------------
Course Setup
---------------------------------------------------------------

The following instructions will allow you to connect to the
git repository, and download the course material.

The training facility resets their machines at the end of each
week; thus, if this is a version of the course being taught
over multiple weeks, you will need to repeat these instructions
in order to re-connect to the repository. In this case, you are
also advised to save any work you wish to keep into a branch of
the repository, so that it can be retrieved later.


1. Log into Gmail using the account you were given for the
   class. When you enter your username, you may be redirected
   to a JumpCloud window - this is the "corporate wrapper"
   around a suite of tools, including Gmail. In this case,
   simply enter your username and password again.

2. Change that password in accordance with the Account Creation
   forms that you were given at the start of class.

3. Create an ssh certificate for your account; it will
   automatically be stored in your ~/.ssh directory.

   > ssh-keygen -t rsa -b 4096

   [You can press enter three times to accept the default
   options.]

4. Go to bitbucket.org; click "Log In", then the "G+" icon
   to log in with your Gmail account.

5. It will ask you to link your Atlassian account with a
   password, which you likely won't have. Request a reset,
   create a password, and then use it to log in.

   From now on, as log as you are logged into Gmail, you
   will be able to skip the password step for BitBucket.

6. Register your new certificate with Bitbucket:

   a. > more ~/.ssh/id_rsa.pub

   b. In Bitbucket:
      - Click the "portrait" icon in the lower-left screen.
      - Select "View Profile".
      - Settings > Security > SSH keys
      - Add Key
      - Copy and paste theinformation from id_rsa.pub.
      - "Add Key" to confirm.

7. Accept your emailed invitation to join the "softformaths"
   team.

8. Clone the repository:

   *** Important: There is one hard-coded path in the project
       files; if you do not place this directory where expected,
       there will be complications later. ***

   > cd "/home/qa/Software Development for MDP"
   > mkdir git
   > cd git
   > git clone git@bitbucket.org:softformaths/softwaredevelopment.git

   On rare occasions, the clone command gives a permission
   error; logging out and back into the VM should fix this
   problem.


