This directory is intended for use by the Project Planning
exercise set. Please add files here, which describe the
proposal.

Students *with extra time* are invited to contribute code
towards the completion of the tasks mentioned here.

