﻿Headline of Epic
We want to build a self-contained App where people can get bots to battle, and review fights. Ultimately, this should be the one-stop-shop for bot based antics.
Total Story Points: 19

User Story
Benefit to Bot Developer: Easier to analyse performance of a bot
Benefit to User/Spectator: Visual display of battle is more interesting
Benefit to Server Developer: App-like functionality will enable future monetisation

New Features
* Bots and configs can be chosen from drop-down menus
* Visualisation of battle can be seen in ”real-time”
* Battle can be reviewed by stepping forwards and backwards through the fight
* Individual moves can be analysed in detail
* Past results can be seen
* GUI will allow for future developments such as avatars

Task List
1. GUI framework allowing the user to select bots for battle, choose config files, and run the battle script. Drop-down menus for bots and config. Simple display window for battle output. [5]
2. Database created to hold historical battle data, with simple querying and results summaries [3]
3. Code that will take the output from a battle (moves and board state), store the result in a database and present clean data back to the GUI. Uses existing debug flag to server. [1]
4. Graphical results window, showing battle animation. Initially replicating an existing script. [2]
5. Interactive control of the battle animation - step forwards/backwards or autorun. Uses a scroll bar. [3]
6. Improved interactive information - hovertext over positions to show their next move and/or where they came from [5]
Next Sprint Cycle
        Bots get avatars
        Leaderboards
        Bot vs bot historical results graphs
        In-App currency (monetisation) and wagers
        Allow users to feed in an arbitrary board state (e.g. from a previous battle)
        ...


Team Make-Up

* GUI Developer
* Database programmer
* Developer


Task Assignments

GUI Developer - 1, 5, 6 [13 points]
Database programmer - 2,3 [4 points]
Developer - 4 (+5,6?) [3-11 points]
