PRAGMA foreign_keys = ON;
CREATE TABLE owners(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);
CREATE TABLE colours(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);
CREATE TABLE ownership(property INTEGER, owner INTEGER, FOREIGN KEY(property) REFERENCES properties(id), FOREIGN KEY(owner) REFERENCES owners(id), UNIQUE(property));
CREATE TABLE properties(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, colour INTEGER, FOREIGN KEY(colour) REFERENCES colours(id));

insert into 
    colours (name) 
values 
    ("brown"),
    ("light blue"),
    ("pink"),
    ("orange"),
    ("red"),
    ("yellow"),
    ("green"),
    ("dark blue");

insert into properties 
    (name,  colour)
values 
    ("Old Kent Road", 1),
    ("Whitechapel Road", 1),
    ("The Angel Islington", 2),
    ("Euston Road", 2),
    ("Pentonville Road", 2),
    ("Pall Mall", 3),
    ("Whitehall", 3),
    ("Northumberland Avenue", 3),
    ("Bow Street", 4),
    ("Marlborough Street", 4),
    ("Vine Street", 4),
    ("The Strand", 5),
    ("Fleet Street", 5),
    ("Trafalgar Square", 5),
    ("Leicester Square", 6),
    ("Coventry Street", 6),
    ("Piccadilly", 6),
    ("Regent Street", 7),
    ("Oxford Street", 7),
    ("Bond Street", 7),
    ("Park Lane", 8),
    ("Mayfair", 8);

insert into 
    owners (name) 
values 
    ("Mr Monopoly"),
    ("Uncle Moneybags"),
    ("Top Hat"),
    ("Boat"),
    ("Boot"),
    ("Dog"),
    ("Iron");

insert into 
    ownership (property, owner) 
values 
    (12, 1),
    (22, 3),
    (1, 5),
    (2, 5),
    (6, 5),
    (3, 5),
    (4, 5),
    (5, 5),
    (18, 3),
    (20, 3),
    (15, 2),
    (16, 2),
    (17, 2);


