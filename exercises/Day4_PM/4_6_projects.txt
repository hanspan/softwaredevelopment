
At this point; there are quite a few projects available
to work on. You can look to finish up any exercises left
over from earlier sessions, or continue development on
the project you designed yesterday!

If you would like to work on something different, here
are some additional ideas:

Example 1: (Pure Python)
  Develop a bot that is designed specifically to defeat
  the RandomBot. Look up information on how rand()
  works, and use that to your advantage.

Example 2: User Interface
  The output of the game server is pretty basic and
  boring. Investigate how to generate pictures and
  display them.

Example 3a: Leaderboard
  Experiment with the MySQL database (already installed
  on these machines). Log in with root/mysql. Determine
  how to connect the game server to it, and store
  information on wins and losses.

  Obviously, this is a horrible setup for a "real"
  database, as it could be trivially hacked. There are
  a wide range of potential problems with databases,
  however, which could warrant a class in and of
  themselves. Since this is just a course with no
  sensitive information involved, we can get away with
  being slack in this particular aspect.

Option 3b: Leaderboard Display
  With the game statistics available in the database,
  determine how to pull them out and display them in
  a user-friendly manner.

