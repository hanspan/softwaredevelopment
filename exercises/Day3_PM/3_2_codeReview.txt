
==============================================================
Day 3: Afternoon Session
Exercise Set 2: Participate in a Code Review
==============================================================

Review the given code in preparation for a code review,
conducted later this afternoon.

--------------------------------------------------------------

1) [Rating: 3] The code is located in the tar file in this
   directory:

> tar xvf sequences.tar
> less sequences.py

   Consider comments, flow, readability, etc. Are there any
   bugs? (Obvious hint: Yes, there are.)

