/*!
 * exercise2.c
 * -----------
 * This exercise is based upon a poorly implemented linked list.
 * Some background information on linked lists is included in the
 * exercise notes.
 */

#include <stdio.h>       // Basic input/output.
#include <stdint.h>      // uint32_t, etc.
#include <string.h>      // strcpy().
#include <stdlib.h>      // EXIT_SUCCESS (not).


// The data that goes into each element of the list.
typedef struct LIST_ITEM
{
  char rawData[100];  // Some generic storage space.

  struct LIST_ITEM *next;   // The link part of the list.
} LIST_ITEM;

// Gather all of the operations up into a single wrapper function.
void run(int counter);
// Initialize a new list, with the given content.
void initList(LIST_ITEM **pList, LIST_ITEM **pListEnd, char *content);
// Append a new entry onto the end of the list; update the end pointer.
void appendList(LIST_ITEM **pListEnd, char *content);
// Free the memory consumed by the list.
void clearList(LIST_ITEM **pList);


/*!
 * This main function handles all of its own processing, so there's
 * no need to process any command-line arguments.
 */
int main()
{
  // Make a couple of runs through the process.
  run(0);
  run(1);

  return EXIT_SUCCESS;
}


/*!
 * Do a batch of operations with a linked list. This is done via a
 * wrapper function, so that the process can be executed multiple
 * times.
 *
 * @param counter This is the Nth run of the loop.
 */
void run(int counter)
{
  // First, allocate the list.
  // Then, make it convenient to append things to the end of the list.
  LIST_ITEM *list;
  LIST_ITEM *listEnd;

  // Create the first entry in the list.
  char buf[100];
  sprintf(buf, "[%d] This is a perfectly cromulent list.", counter);
  initList(&list, &listEnd, buf);

  // Create a batch of elements for the list.
  for (int count = 0; count < 100000; count++)
  {
    sprintf(buf, "This is element %d of the buggy linked list.", count % 100);
    appendList(&listEnd, buf);
  }

  // Print a subset of information from the list.
  LIST_ITEM *curr = list;
  for (int steps = 0; steps < 10000; steps++)
  {
    curr = curr->next;

    if (0 == (steps & 0xf))
    {
      int val = steps & (steps - 1);
      printf("Value: %d\n", val);
      continue;
    }

    if (counter == (steps & 0x10))
    {
      printf("[%d] %s\n", counter, curr->rawData);
      continue;
    }
  }

  // Clean up afterwards, and return.
  clearList(&list);
  printf("Returning from a successful run #%d!\n", counter);
}


/*!
 * Given a pointer to a new list, fill in the first element of that
 * list with the given content.
 *
 * Update list and listEnd to point to this newly allocated memory.
 *
 * @param pList    Starting point for the new list. (Output)
 * @param pListEnd Ending point for the new list. (Output)
 * @param content  Information to put into the first element.
 */
void initList(LIST_ITEM **pList, LIST_ITEM **pListEnd, char *content)
{
  // Disclaimer: This code was promised to be bad.
  LIST_ITEM *node = (LIST_ITEM *) malloc(sizeof(LIST_ITEM));
  strcpy(node->rawData, content);

  *pList    = node;
  *pListEnd = node;
}


/*!
 * Append a new node to the end of the list, with the given content.
 * Take advantage of the end-of-list information to make it trivial
 * to know where to do the insertion.
 *
 * @param pListEnd Ending point for the updated list. (Input/Output)
 * @param content  Information to put into the new element.
 */
void appendList(LIST_ITEM **pListEnd, char *content)
{
  LIST_ITEM *newNode = (LIST_ITEM *) malloc(sizeof(LIST_ITEM));
  strcpy(newNode->rawData, content);

  // Have the (current) end of the list point to the new node.
  // Then update the end of the list.
  (*pListEnd)->next = newNode;
  *pListEnd = newNode;
}


/*!
 * Clear all of the memory used by the list.
 *
 * Technically speaking, there's no need to pass in a pointer to the
 * first node of the list; however, that would make this function
 * prototype look different from all of the other really similar
 * entries, which would, in turn, be quite confusing.
 *
 * @param pList Pointer to the first node in the list.
 */
void clearList(LIST_ITEM **pList)
{
  // Start at the beginning, and keep clearing until the end.
  LIST_ITEM *curr = *pList;
  LIST_ITEM *next = curr->next;

  while (NULL != curr)
  {
    free(curr);
    curr = next;
    next = curr->next;
  }
}

