/*!
 * exercise3.c
 *
 * This program attempts to implement a sort heap. (Unfortunately,
 * heap is also typically used to refer to the "large memory"
 * space available on a computer, so there is room for confusion
 * when using this term).
 *
 * A heap sort algorithm uses a binary tree to store data, with
 * the property that a node is always smaller than either of its
 * children (although those children could be in either order
 * relative to each other).
 *
 * This property means that the root of the tree is always the
 * smallest element in the tree. Being careful when inserting
 * and deleting elements from the tree then preserves the nature
 * of the heap.
 *
 * The final trick with a heap is to observe that a binary tree
 * with N levels can readily be stored in an array with 2^N
 * entries.
 */

#include <stdio.h>      // Standard I/O functionality.
#include <stdint.h>     // For uint32_t.
#include <stdlib.h>     // rand().
#include <time.h>       // time().


// How large the heap is allowed to get.
const int MAX_DEPTH  = 5;
const int ARRAY_SIZE = (1 << MAX_DEPTH);
const int HEAP_SIZE  = ARRAY_SIZE - 2;   // Need room for inserts.

// Pretending like we aren't actually using C++....
const int TRUE  = 1;
const int FALSE = 0;

// Fill a heap with random values.
int fillHeap(uint32_t *heap);
// Output the heap in order, destroying it in the process.
void destructivePrint(uint32_t *heap, uint32_t currSize);

// Insert an element into the heap.
void insertHeap(uint32_t *heap, uint32_t currSize, uint32_t value);
// Remove the smallest element from the heap, and rebalance.
void popHeap(uint32_t *heap, uint32_t currSize);
// Determine which child node has the smaller value.
uint32_t pickSmallerChild(uint32_t *heap, uint32_t currSize, uint32_t pos);
// Utility to print out all raw elements of the heap.
void dumpHeap(uint32_t *heap, uint32_t currSize);
// Utility to check to see if a heap meets its claimed structure.
int  isValidHeap(uint32_t *heap, uint32_t currSize);

// Utilities to traverse the tree.
uint32_t getParent(uint32_t pos);
uint32_t getFirstChild(uint32_t pos);
uint32_t getSecondChild(uint32_t pos);


/*!
 * Create a heap, fill it with random values, then print out
 * those values in sorted order!
 */
int main()
{
  uint32_t heap[ARRAY_SIZE] = { 0 };
  uint32_t currSize = 0;

  // Fill in the heap.
  currSize = fillHeap(heap);

  // Output the heap in sorted order.
  destructivePrint(heap, currSize);
}


/*!
 * Fill in the heap with numbers generated at random.
 * We implicitly assume that the heap is empty to begin
 * with.
 *
 * @param heap The heap to fill.
 * @return the number of elements added to the heap.
 */
int fillHeap(uint32_t *heap)
{
  srand(time(NULL));

  for (uint32_t i = 0; i < HEAP_SIZE; i++)
  {
    uint32_t val = (uint32_t) rand();
    insertHeap(heap, i, val);
  }

  return HEAP_SIZE;
}


/*!
 * Continually print the root element from the heap, and then
 * pop that root from the heap. This will print out the entire
 * contents in sorted order.
 *
 * No effort is made to preserve the heap itself.
 *
 * @param heap     The heap to output.
 * @param currSize Number of elements in the heap.
 */
void destructivePrint(uint32_t *heap, uint32_t currSize)
{
  while (currSize)
  {
    printf("%d\n", heap[0]);
    popHeap(heap, currSize--);
  }
}


/*!
 * Insert an element into a heap, preserving the heap property.
 *
 * @param heap     The heap to insert into.
 * @param currSize Current size of the heap.
 * @param value    The value to insert.
 */
void insertHeap(uint32_t *heap, uint32_t currSize, uint32_t value)
{
  // First, place the element at the end of the heap.
  heap[currSize] = value;

  // Then, re-order elements until the heap is "valid" once more.
  uint32_t pos = currSize;
  while (0 != pos)
  {
    uint32_t parent    = getParent(pos);
    uint32_t parentVal = heap[parent];

    // If this condition is satisfied, then the heap is set.
    if (parentVal < value)
      break;

    // Otherwise, swap the element with its parent, and repeat.
    heap[pos]    = parentVal;
    heap[parent] = value;
    pos = parent;
  }
}


/*!
 * Remove the smallest element from the heap, then rebalance it
 * in order to ensure that the structure is still a heap.
 *
 * @param heap     The heap to update.
 * @param currSize Current size of the heap.
 */
void popHeap(uint32_t *heap, uint32_t currSize)
{
  if (0 == currSize)
    return;

  // Take the last element of the heap, and move it to the
  // root. Then re-sort any affected entries.
  heap[0] = heap[currSize];

  uint32_t pos = 0;
  while (true)
  {
    // Find the child node with the smaller value; if there
    // aren't any child nodes, then we are already done.
    uint32_t child = pickSmallerChild(heap, currSize, pos);
    if (0 == child)
      break;

    // Check to see if a swap should occur.
    uint32_t currVal  = heap[pos];
    uint32_t childVal = heap[child];

    if (currVal < childVal)
      break;

    // Yes, there should be a swap!
    heap[pos] == childVal;
    heap[child] = currVal;
    pos = child;
  }
}


/*!
 * Look at the children of the given node, and determine which
 * one has a smaller value associated to it, and return that
 * location. Return 0 if there are no children for the node.
 *
 * @param heap     The heap to process.
 * @param currSize Current size of the heap.
 * @param pos      The position to consider the children of.
 * @return the smaller child, or 0 if no child qualifies.
 */
uint32_t pickSmallerChild(uint32_t *heap, uint32_t currSize,
    uint32_t pos)
{
  uint32_t firstChild  = getFirstChild(pos);
  uint32_t secondChild = getSecondChild(pos);

  // Take care of the trivial cases.
  if (firstChild >= currSize)
    return 0;
  else if (secondChild >= currSize)
    return firstChild;

  if (heap[firstChild] < heap[secondChild])
    return firstChild;
  return secondChild;
}


/*!
 * Dump the raw contents of the heap.
 *
 * @param heap The heap to output.
 * @param currSize Number of elements in the heap.
 */
void dumpHeap(uint32_t *heap, uint32_t currSize)
{
  printf("--------------------------\n");
  for (uint32_t i = 0; i < currSize; i++)
    printf("Element %2d: %d\n", i, heap[i]);
}


/*!
 * Check to see if a heap meets its stated design criteria.
 *
 * @param heap     The heap to check.
 * @param currSize Number of elements in the heap.
 * @return TRUE if it passes, FALSE if it doesn't.
 */
int isValidHeap(uint32_t *heap, uint32_t currSize)
{
  int pass = TRUE;
  for (uint32_t pos = 0; pos < currSize; pos++)
  {
    uint32_t firstChild = getFirstChild(pos);
    if (firstChild < currSize)
      if (heap[firstChild] < heap[pos])
        pass = FALSE;

    uint32_t secondChild = getSecondChild(pos);
    if (secondChild < currSize)
      if (heap[secondChild] < heap[pos])
        pass = FALSE;
  }

  return pass;
}


/*!
 * Find the parent for a node in the tree.
 *
 * @param pos The node to start from.
 * @return the node's parent location.
 */
uint32_t getParent(uint32_t pos)
{
  return ((pos - 1) / 2);
}


/*!
 * Find the first child of a node. Note that this function
 * does not validate that the child exists, only that this
 * is the location where it would be!
 *
 * @param pos The node to start from.
 * @return the location of the node's first child.
 */
uint32_t getFirstChild(uint32_t pos)
{
  return (2 * pos + 1);
}


/*!
 * Find the second child of a node. As above, existence of
 * the child node is not guaranteed.
 *
 * @param pos The node to start from.
 * @return the location of the node's second child.
 */
uint32_t getSecondChild(uint32_t pos)
{
  return (2 * pos + 2);
}



