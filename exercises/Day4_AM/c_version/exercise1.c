/*!
 * exercise1.c
 * -----------
 * Track down a simple buffer overflow issue.
 */

#include <stdio.h>       // Basic input/output.
#include <stdint.h>      // uint32_t, etc.
#include <string.h>      // strcpy().
#include <stdlib.h>      // EXIT_SUCCESS (not).


// The following structure will be the source of a few problems...
typedef struct
{
  char firstName[10];
  char lastName[10];
} NAME;

// Fill in information for a name.
void setupName(NAME *name, const char *first, const char *last);
// Print information from a name.
void printName(NAME *name);


/*!
 * The main routine. It will provide its own problematic inputs,
 * so there's no need to pass in arguments on the command line.
 */
int main()
{
  NAME name;

  // A simple case to make sure everything works.
  setupName(&name, "First", "Last");
  printName(&name);

  setupName(&name, "Christopher", "Robin");
  printName(&name);

  setupName(&name, "Christopher", "Robinson-Jackson");
  printName(&name);

  return EXIT_SUCCESS;
}


/*!
 * Fill in values for a NAME structure.
 *
 * @param name  Structure to fill in.
 * @param first Person's first name.
 * @param last  Person's surname.
 */
void setupName(NAME *name, const char *first, const char *last)
{
  strcpy(name->lastName,  last);
  strcpy(name->firstName, first);
}


/*!
 * Print out a person's name; this just saves on repeating
 * the same printf command over and over again.
 *
 * @param name The name to print out.
 */
void printName(NAME *name)
{
  printf("Name: %s %s\n", name->firstName, name->lastName);
}
