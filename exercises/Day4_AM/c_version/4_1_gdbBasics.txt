
==============================================================
Day 4: Morning Session
Exercise Set 1: Basic GDB Commands
==============================================================

GDB (the Gnu DeBugger) is the most commonly used debugger for
Linux systems; there are other options available, though,
especially for Windows systems.

The purpose of these exercises is not to make you an expert at
finding and fixing bugs, but rather to exercise some of the
most commonly used features of debuggers. Thus, many of the
bugs encountered could easily be identified just via a code
review. An additional task, if you should wish to consider it,
would be to identify the good programming practices that would
prevent these bugs from even occurring.

--------------------------------------------------------------

1) You will need to compile the programs in order to work 
  with them, though.

> make

  See how the program runs...

> ./exercise1
] Name: First Last
] Name: Christopher r
] Name: Christopher r
] *** stack smashing detected ***: ./exercise1 terminated

------------------------------
Interlude: In initial runs of the course, the "stack smashing"
output was generated automatically. Updates to the g++ compiler
have changed this behaviour; the "-fstack-protector" flag has
now been added to the Makefile to restore the original
appearance.

This does reveal how significant changes can arise due to what
seem like innocuous updates to compilers...
------------------------------

  Start up the debugger!

> gdb ./exercise1

  And get it running...

> break main
> run

  The run command starts the program up; the breakpoint at
  main keeps the program from running away from us.

--------------------------------------------------------------

2) [Rating: 2] Look through the source code. Pay particular
   attention to the NAME structure.

Note: The fact that there is source code available to the
      debuggers is due to the way it was compiled! If you look
      at the Makefile, you can see where the '-g' flag was
      included; this tells the compiler to include (lots of)
      debugging assistance information, including a copy of
      the source code.

   list

   This command prints the "next" 10 lines of source code.
   Typing it repeatedly will print additional lines of code.

   This is a good time to point out one of gdb's behaviours:
   If you press "Enter", gdb will automatically repeat the
   last command you gave it (which will be incredibly
   helpful!).

--------------------------------------------------------------

3) [Rating: 1] Explore the variables briefly.

   print name

--------------------------------------------------------------

4) [Rating: 1] The content of the name variable is pretty
   worthless right now; proceed until there's something a bit
   more useful.

   next
   next

  View the name variable again; it should have something more
  reasonable, now.

--------------------------------------------------------------

5) [Rating: 1] Call C-commands from within gdb. These can be
   functions within the program itself, or within any of the
   libraries that the program has access to.

   print name
] $8 = {firstName = "First\000\000\000\020F",
        lastName = "Last\000\000 \337\377\377"}

   call strlen(name.firstName)
] $9 = 5

   call strlen(name.lastName)
] $10 = 4

   call printf("%s %s\n", name.firstName, name.lastName)
] First Last
] $11 = 11

   call printName(&name)
] Name: First Last

--------------------------------------------------------------

6) [Rating: 1] Call Python from within gdb. There are two
   options for this:
     - "one off" commands via 'py', or
     - a full interactive session via 'pi'.

   py print(2+3)
5

   py print(2**10 - 1)
1023

   pi
>>> import time
>>> time.asctime()
'Sun Oct 15 18:12:25 2017'
>>>    (Press Ctrl-D to return to gdb)
(gdb)

--------------------------------------------------------------

7) [Rating: 2] Stepping Over code is very convenient when
   there are functions being called, and you don't care to go
   deeper into what they're doing internally; but, it meant
   that the earlier exercise skipped through the call to
   setupName(), where some... interesting... things are
   happening.

   Part 0: Reset
   -------------
     Get back to the point just before the second setupName()
     call; the reset commands, in particular, are helpful if
     the debugger has gotten itself into a state you don't
     want to be in.

   run  (Select 'y' to restart.)
   next           (Repeat 3x)

   Note that gdb automatically stopped upon reaching main();
   this is because the earlier breakpoint that we set is
   still there.

   Part 1: Step Into setupName()
   -----------------------------
   step
   list

   Part 2: Examine the Variables
   -----------------------------
   call sizeof(name.firstName)
] $1 = 10

   call strlen(first)
] $2 = 11

   call sizeof(name.lastName)
] $3 = 10

   call strlen(last)
] $4 = 5

   p name.firstName
] $5 = "First\000\000\000\300\005"


   Part 3: Observe the Changes
   ---------------------------
   Step over the strcpy() calls (you almost certainly do
   not want to step into them). Observe the changes to
   the name variable as you proceed.

--------------------------------------------------------------

8) [Rating: 2] GDB has another option to make it easier to
   keep up with the value of variables during a run. The
   'display [var]' command specifies that the value of the
   given variable is to be printed after *every* command.

Restart the program again.

   display *name    (this prints the contents of name, rather
                     than just name itself)

Step/Next your way through the program.

--------------------------------------------------------------

Conclusions: The particular bug in this program is one of the
  "classics" - a buffer overflow. As should be clear from the
  various outputs: at times, the value being written into the
  first name spilled over into the space set aside for the
  last name. This is a well-known problem with the strcpy()
  function - it copies until it has reached the end of the
  source string, no matter how much room is available at the
  destination.

  There are multiple guards against this; some are:
    - Check lengths before copying,
    - Use strncpy(), which stops after N bytes are copied, or
    - Limit the size of the input strings.

  In this particular case:

    - The first setupName() went as expected, since the
      input strings are not too long.

    - The second setupName() has the "r" from "Christopher"
      copied into the first byte of the last name slot, and
      the last name is then terminated. Thus, the first name
      looks like "Christopher", while the last name looks
      like "r".

    - The third setupName() is the worst one; *both* the
      first and last names overflow, and the memory after the
      last name variable is "unknown territory". It's this
      problem that triggers the "stack smashing" error.

