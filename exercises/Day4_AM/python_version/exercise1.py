"""
exercise1.py
------------
Track down a default argument issue.
"""
import argparse

def main():
    """
    Print out some names.
    """
    name = setupName("First", "Last")
    printName(name)
    
    name = setupName("Christopher", "Robin")
    printName(name)
    

def setupName(first="", last="", name=[]):
    """
    Create the name array.
    """
    name.append(first)
    name.append(last)
    return name
    
def printName(name):
    """
    Print an array containing a name.
    """
    if len(name) < 2:
        raise Exception("Name too short")
    print("Name: {} {}".format(name[0], name[1]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Print out some names.")
    main()