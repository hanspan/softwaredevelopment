"""
exercise2.py
------------
This exercise is based on a weekly calendar class implemented with 
a poor understanding of Python lists. 
"""


DAYS_OF_THE_WEEK = ["Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                    "Sunday"]

NORMAL_WEEKEND_DAYS = ["Saturday",
                       "Sunday"]


class WeeklyCalendar(object):
    
    def __init__(self, weekendDays=NORMAL_WEEKEND_DAYS):
        self.daysOff = weekendDays
        self.getWorkingDays()
        

    def getWorkingDays(self):
        """
        Print a list of days in the working week by
        taking the whole week and removing the weekend
        days.
        """
        self.workingDays = DAYS_OF_THE_WEEK
        
        for day in self.daysOff:
            self.workingDays.remove(day)
    
        print("I will be working: {}".format(", ".join(self.workingDays)))
    
    
    def getNonWeekendDaysOff(self):
        """
        Print a list of all my days off that don't fall on a normal weekend day.
        """
                
        nonWeekendDaysOff = self.daysOff[:]
        
        for day in nonWeekendDaysOff:
            if day in NORMAL_WEEKEND_DAYS:
                nonWeekendDaysOff.remove(day)
        
        print("My non-weekend days off this week will be: {}".format(", ".join(nonWeekendDaysOff)))



def main():
    """
    Print out a couple of different week configurations, letting me know what days I'll be working,
    what days I have off and if any of those days off are not on a normal weekend day.
    """
    print("A normal working week:")
    wc = WeeklyCalendar()
    wc.getNonWeekendDaysOff()
    
    print("\nWhen I have to work on Sunday, so I get Friday off in lieu.")
    wc = WeeklyCalendar(["Friday", "Saturday"])
    wc.getNonWeekendDaysOff()
    
if __name__ == "__main__":
    main()