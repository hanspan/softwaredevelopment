"""
exercise3.py
------------
This program attempts to implement a sort heap. (Unfortunately,
heap is also typically used to refer to the "large memory"
space available on a computer, so there is room for confusion
when using this term).

A heap sort algorithm uses a binary tree to store data, with
the property that a node is always smaller than either of its
children (although those children could be in either order
relative to each other).

This property means that the root of the tree is always the
smallest element in the tree. Being careful when inserting
and deleting elements from the tree then preserves the nature
of the heap.

The final trick with a heap is to observe that a binary tree
with N levels can readily be stored in an array with 2^N
entries.
"""

from random import randint

MAX_DEPTH = 5
ARRAY_SIZE = (1 << MAX_DEPTH)
HEAP_SIZE = ARRAY_SIZE - 2

    
def main():
    heap = Heap()
    heap.fill()
    heap.destructivePrint()
    

class Heap(object):
    
    def __init__(self):
        self.contents = [0 for _ in range(ARRAY_SIZE)]
        self.size = 0


    def fill(self):
        """
        Create a heap and fill it with random values. We implicitly assume the heap
        is empty to begin with.
        """
        for _ in range(HEAP_SIZE):
            rand = randint(0, 2**32 -1)
            self.insert(rand)

    def destructivePrint(self):
        """
        Continually print the root element from the heap and then
        pop that root from the heap. This will print out the entire
        contents in sorted order.
        
        No attempt is made to preserve the heap itself.
        """
        for _ in range(HEAP_SIZE):
            print("{}".format(self.contents[0]))
            self.pop()
            

    def insert(self, value):
        """
        Insert an element into a heap, preserving the heap property.
        """
        
        # First, place the element at the end of the heap.
        self.contents[self.size] = value
        
        # Then, re-order elements until the heap is valid once more.
        pos = self.size
        while (pos != 0):
            parent = self.getParent(pos)
            parentVal = self.contents[parent]
            
            # If this condition is satisfied, then the heap is set.
            if parentVal < value:
                break
            
            # Otherwise, swap the element with its parent and repeat.
            self.contents[pos] = parentVal
            self.contents[parent] = value
            pos = parent
        
        self.size += 1  
        
    
    def pop(self):
        """
        Remove the smallest element from the heap, then rebalance it
        in order to ensure that the structure is still a heap.
        """
        if self.size == 0:
            return
        
        # Take the last element of the heap and move it to the root.
        # Re-sort any affected entries. 
        
        self.contents[0] = self.contents[self.size]
        
        pos = 0
        
        while True:
            # Find the child node with the smaller value; if there aren't
            # any child nodes, then we are already done.
            child = self.pickSmallerChild(pos)
            if child == 0:
                break
            
            # Check to see if a swap should occur.
            currVal = self.contents[pos]
            childVal = self.contents[child]
            
            if (currVal < childVal):
                break
            
            # Yes, there should be a swap
            self.contents[pos] = currVal   
            self.contents[child] = currVal
            pos = child
        
    
    def pickSmallerChild(self, pos):
        """
        Look at the children of a node and determine which one has a smaller
        value associated to it. Return that location or 0 if there are no children
        for the node.
        """
        firstChild = self.getFirstChild(pos)
        secondChild = self.getSecondChild(pos)
        
        if firstChild >= self.size:
            return 0
        elif secondChild >= self.size:
            return firstChild
        
        if self.contents[firstChild] < self.contents[secondChild]:
            return firstChild
        
        return secondChild
        

    def dump(self):
        """
        Dump the raw contents of the heap.
        """
        for idx in range(self.size):
            print("Element {}: {}".format(idx, self.contents[idx]))
            

    def isValid(self):
        """
        Check to see if a heap meets its stated design criteria.
        """
        valid = True
        
        for pos in range(self.size):
            firstChild = self.getFirstChild(pos)
            if firstChild < self.size:
                if self.contents[firstChild] < self.contents[pos]:
                    valid = False
                    break
            
            secondChild = self.getSecondChild(pos)
            if secondChild < self.size:
                if (self.contents[secondChild] < self.contents[pos]):
                    valid = False
                    break
        
        return valid
                

    def getParent(self, pos):
        """
        Find the parent for a node in the tree.
        """
        return (pos - 1)//2

    def getFirstChild(self, pos):
        """
        Find the first child for a node in the tree. 
        NB: Existence is not guaranteed!
        """
        return 2 * pos + 1
    
    def getSecondChild(self, pos):
        """
        Find the second child for a node in the tree. 
        NB: As above, existence is not guaranteed!
        """
        return 2 * pos + 2

if __name__ == "__main__":
    main()