Git Guidance

Git cheatsheet
https://www.atlassian.com/dam/jcr:8132028b-024f-4b6b-953e-e68fcce0c5fa/atlassian-git-cheatsheet.pdf

A useful blog on git branching:
https://nvie.com/posts/a-successful-git-branching-model/


Pull Requests

Before making a pull request, it's good practice to merge the latest version of Development into your branch so you can resolve any merge conflicts and test for any other problems before asking people to find these problems for you. In order to do this, follow the following steps:

- git checkout Development
- git pull
- git checkout my_branch
- git merge Development
- git push branch (if you haven't pushed your branch before, run the command that apears in the error message)
- On Bitbucket: 'Pull Requests' -> 'Create Pull Request'


In "graphical" form:

                                   4
      remote        Development  <----  my_branch
                         |                  ^
                       1 |                  | 3
                         v         2        |
      local         Development  ---->  my_branch

In this scenario, remote/Development is the "common" point that everyone in class can readily work with, and our goal is to get our changes in local/my_branch there. (Steps 3 & 4)

Our secondary goal is to detect any conflicts or errors as early as possible, which is the motivation behind Steps 1 & 2.

Step 1:
  Grab any changes that have happened on remote/Development since your last update.
  > git checkout Development
  > git pull

Step 2:
  Merge those changes into local/my_branch.
  > git checkout my_branch
  > git merge Development
  *** Look for any errors, and resolve them!

Step 3:
  Get the changes from my_branch up onto the remote system, so we can prepare for the Pull Request.
  > git push

Step 4:
  Open a Pull Request to complete the merge into remote/Development.

